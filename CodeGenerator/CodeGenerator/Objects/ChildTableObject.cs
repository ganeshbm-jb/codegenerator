﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CodeGenerator.Objects
{
    [DefaultValueAttribute(true)]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ChildTableObject
    {
        public const string NAME_CAT = "1. Table";
        public const string COLUMN_CAT = "2. Related Column";
        // public const string CONTROLLER_CAT = "5. Controller";

        // Table Name
        private string _TableName = "";
        [CategoryAttribute(NAME_CAT), DescriptionAttribute("Table Name"), DisplayName("Table Name")]
        public string tableName { get { return _TableName; } set { _TableName = value; } }

        // Generate SQL
        private string _Column = "";
        [CategoryAttribute(COLUMN_CAT), DescriptionAttribute("Column"), DisplayName("Column")]       
        public string column
        {
            get { return _Column; }
            set
            {
                _Column = value;
            }
        }


        public string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(this.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize);
                return textWriter.ToString();
            }
        }
    }
}
