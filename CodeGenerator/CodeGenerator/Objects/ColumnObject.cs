﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CodeGenerator.Objects
{
    [DefaultValueAttribute(true)]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ColumnObject
    {
        public const string NAME_CAT = "1. Name";
        public const string TYPE_CAT = "2. Data Type";
        public const string NULL_CAT = "3. Nullable / Unique";
        public const string DEFAULTVALUE_CAT = "4. Default Value";
        public const string PARENT_CAT = "5. Parent Table";
        // public const string CONTROLLER_CAT = "5. Controller";

        // Table Name
        private string _Name = "";
        [CategoryAttribute(NAME_CAT), DescriptionAttribute("Column Name"), DisplayName("Name")]
        public string name { get { return _Name; } set { _Name = value; } }

        // Generate SQL
        private string _DataType = "";
        [CategoryAttribute(TYPE_CAT), DescriptionAttribute("Data Type"), DisplayName("Data Type")]
        [TypeConverter(typeof(DataTypeListConverter))]
        public string dataType
        {
            get { return _DataType; }
            set
            {
                _DataType = value;
            }
        }

        // Generate Model
        private string _DefaultValue = "";
        [CategoryAttribute(DEFAULTVALUE_CAT), DescriptionAttribute("Default Value"), DisplayName("Default Value")]
        public string defaultValue { get { return _DefaultValue; } set { _DefaultValue = value; } }

        // Generate Service
        private string _nullunique = "";
        [CategoryAttribute(NULL_CAT), DescriptionAttribute("Null / Unique"), DisplayName("Null / Unique")]
        [TypeConverter(typeof(NullUniqueListConverter))]        
        public string nullableUnqiue { get { return _nullunique; } set { _nullunique = value; } }

        // Generate Service
        private string _parentTable = "";
        [CategoryAttribute(PARENT_CAT), DescriptionAttribute("Parent Table"), DisplayName("Parent Table")]       
        public string parentTable { get { return _parentTable; } set { _parentTable = value; } }

        private string _relatedColumn = "";
        [CategoryAttribute(PARENT_CAT), DescriptionAttribute("Related Column"), DisplayName("Related Column")]
        public string relatedColumn { get { return _relatedColumn; } set { _relatedColumn = value; } }

        private string _parentTableAlias = "";
        [CategoryAttribute(PARENT_CAT), DescriptionAttribute("Alias"), DisplayName("Alias")]
        public string parentTableAlias { get { return _parentTableAlias; } set { _parentTableAlias = value; } }


        public string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(this.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize);
                return textWriter.ToString();
            }
        }
    }

    public class DataTypeListConverter : TypeConverter
    {
        public override bool
GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true; // display drop
        }
        public override bool
        GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            return true; // drop-down vs combo
        }
        public override StandardValuesCollection
        GetStandardValues(ITypeDescriptorContext context)
        {
            string[] dataTypeList = new string[11];
            dataTypeList[0] = "varchar(255)";
            dataTypeList[1] = "decimal(18,2)";
            dataTypeList[2] = "int";
            dataTypeList[3] = "smallint";
            dataTypeList[4] = "tinyint";
            dataTypeList[5] = "date";
            dataTypeList[6] = "timestamp";
            dataTypeList[7] = "TEXT";
            dataTypeList[8] = "TINYTEXT";
            dataTypeList[9] = "MEDIUMTEXT";
            dataTypeList[10] = "LONGTEXT";
            // note you can also look at context etc to build list

            StandardValuesCollection svc = new StandardValuesCollection(dataTypeList);
            return svc;
        }
    }

    public class NullUniqueListConverter : TypeConverter
    {
        public override bool
GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true; // display drop
        }
        public override bool
        GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            return true; // drop-down vs combo
        }
        public override StandardValuesCollection
        GetStandardValues(ITypeDescriptorContext context)
        {
            string[] nullUniqueList = new string[5];
            nullUniqueList[0] = "";
            nullUniqueList[1] = "unique";
            nullUniqueList[2] = "unique not null";
            nullUniqueList[3] = "not null";
            nullUniqueList[4] = "null";            
            // note you can also look at context etc to build list

            StandardValuesCollection svc = new StandardValuesCollection(nullUniqueList);
            return svc;
        }
    }
}
