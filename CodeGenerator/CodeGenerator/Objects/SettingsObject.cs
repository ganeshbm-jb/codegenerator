﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace CodeGenerator.Objects
{
    [DefaultValueAttribute(true)]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class SettingsObject
    {
        public const string SETTINGS_CAT = "General Settings";

        // Table Name
        private string _TablePrefix = "";
        [CategoryAttribute(SETTINGS_CAT), DescriptionAttribute("Table Prefix"), DisplayName("Table Prefix")]
        public string tableprefix { get { return _TablePrefix; } set { _TablePrefix = value; } }

        public string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(this.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize);
                return textWriter.ToString();
            }
        }
    }

    public class SettingsSchema
    {
        private SettingsObject _Settings = new SettingsObject();
        public SettingsObject Settings
        { get { return _Settings; } set { _Settings = value; } }

        public T Deserialize<T>(string input) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }
    }
}
