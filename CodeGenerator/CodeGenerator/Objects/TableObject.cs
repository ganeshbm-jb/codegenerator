﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CodeGenerator.Objects
{
    [DefaultValueAttribute(true)]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class TableObject
    {
        public const string NAME_CAT = "1. Name";
        public const string SQL_CAT = "2. SQL";
        public const string MODEL_CAT = "3. Model";
        public const string SERVICE_CAT = "4. Service";
        public const string CONTROLLER_CAT = "5. Controller";
        public const string COLUMNS_CAT = "6. Columns";
        public const string CHILDTABLES_CAT = "7. Child Tables";

        // Table Name
        private string _TableName = "";
        [CategoryAttribute(NAME_CAT), DescriptionAttribute("Table Name"), DisplayName("Table Name")]
        public string name { get { return _TableName; } set { _TableName = value; } }

        // Generate SQL
        private bool _SQL = true;
        [CategoryAttribute(SQL_CAT), DescriptionAttribute("Generate SQL"), DisplayName("Generate SQL")]
        public bool generateSQL { get { return _SQL; } set { _SQL = value; } }

        // Generate Model
        private bool _Model = true;
        [CategoryAttribute(MODEL_CAT), DescriptionAttribute("Generate Model"), DisplayName("Generate Model")]
        public bool generateModel { get { return _Model; } set { _Model = value; } }

        // Generate Service
        private bool _Service = true;
        [CategoryAttribute(SERVICE_CAT), DescriptionAttribute("Generate Service"), DisplayName("Generate Service")]
        public bool generateService { get { return _Service; } set { _Service = value; } }

        // Generate Controller
        private bool _Controller = true;
        [CategoryAttribute(CONTROLLER_CAT), DescriptionAttribute("Generate Controller"), DisplayName("Generate Controller")]
        public bool generateController { get { return _Controller; } set { _Controller = value; } }


        private List<ColumnObject> _Columns = new List<ColumnObject>();
        [CategoryAttribute(COLUMNS_CAT), DescriptionAttribute("Columns"), DisplayName("Columns")]
        [Editor(typeof(ColumnCollectionEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public List<ColumnObject> Columns
        { get { return _Columns; } }

        private List<ChildTableObject> _ChildTables = new List<ChildTableObject>();
        [CategoryAttribute(CHILDTABLES_CAT), DescriptionAttribute("Child Tables"), DisplayName("Child Tables")]
        [Editor(typeof(ChildTableCollectionEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public List<ChildTableObject> ChildTables
        { get { return _ChildTables; } }


        private bool _HasRelatedTables = false;
        public bool hasRelatedTables
        {
            get { return _HasRelatedTables; }
            set { _HasRelatedTables = value; }
        }

        private string _SelectItems = "";
        public string selectItems
        {
            get { return _SelectItems; }
            set { _SelectItems = value; }
        }
        private string _JoinTables = "";
        public string joinTables
        {
            get { return _JoinTables; }
            set { _JoinTables = value; }
        }

        public string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(this.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize);
                return textWriter.ToString();
            }
        }
    }

    public class TableSchema
    {
        private List<TableObject> _Tables = new List<TableObject>();
        public List<TableObject> Tables
        { get { return _Tables; } }

        public T Deserialize<T>(string input) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }
    }

    public class ChildTableCollectionEditor : CollectionEditor
    {
        public ChildTableCollectionEditor(Type type)
            : base(type)
        {
        }

        protected override string GetDisplayText(object value)
        {
            ChildTableObject item = new ChildTableObject();
            item = (ChildTableObject)value;

            return base.GetDisplayText(string.Format("{0}", item.tableName));
        }
    }
    public class ColumnCollectionEditor : CollectionEditor
    {
        public ColumnCollectionEditor(Type type)
            : base(type)
        {
        }

        protected override string GetDisplayText(object value)
        {
            ColumnObject item = new ColumnObject();
            item = (ColumnObject)value;

            return base.GetDisplayText(string.Format("{0}", item.name));
        }
    }
}
