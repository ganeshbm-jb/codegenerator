﻿using CodeGenerator.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace CodeGenerator
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            tvSchema.Height = tvSchema.Height - 100;
        }

        private void InitializeComponentEx()
        {
            StreamReader sr;
            string path = projectFolder + @"\schema";
            DirectoryInfo di = new DirectoryInfo(path);
            ts.Tables.Clear();
            if (di.Exists)
            {
                foreach (FileInfo fi in di.GetFiles().OrderBy(f => f.FullName))
                {
                    if (fi.Extension.ToLower().Equals(".xml"))
                    {
                        using (sr = new StreamReader(fi.FullName))
                        {
                            ts.Tables.Add(ts.Deserialize<TableObject>(sr.ReadToEnd()));
                            sr.Close();
                        }
                    }
                }
            }
            else
            {
                // cerate directory
                di.Create();
            }

            // create settings directory
            path = projectFolder + @"\settings";
            di = new DirectoryInfo(path);
            if (!di.Exists)
                di.Create();
            else
            {
                foreach (FileInfo fi in di.GetFiles())
                {
                    if (fi.Extension.ToLower().Equals(".xml") && fi.Name.ToLower().Equals("settings.xml"))
                    {
                        using (sr = new StreamReader(fi.FullName))
                        {
                            ss.Settings = ss.Deserialize<SettingsObject>(sr.ReadToEnd());

                            sr.Close();
                        }

                    }
                }
            }
            pgSettings.SelectedObject = ss.Settings;
            pgSettings.Refresh();

            refreshTree();

        }

        private TableObject toSelected = new TableObject();
        private void tsmiTable_Click(object sender, EventArgs e)
        {
            TableObject to = new TableObject();
            toSelected = to;
            pgProperties.SelectedObject = to;
            pgProperties.Refresh();
        }

        private void tsmiTableSave_Click(object sender, EventArgs e)
        {
            TableObject to = pgProperties.SelectedObject as TableObject;
            string serializedContent = to.Serialize<TableObject>(to);

            serializedContent = to.Serialize<TableObject>(to);
            write2File(to.name, serializedContent);
            MessageBox.Show("Table " + to.name + " Saved successfully");
            InitializeComponentEx();


        }

        TableSchema ts = new TableSchema();
        SettingsSchema ss = new SettingsSchema();

        SortedList<string, TableObject> slTables = new SortedList<string, TableObject>();
        private void refreshTree()
        {
            tvSchema.Nodes.Clear();

            TreeNode rootNode = new TreeNode("ROOT");
            TreeNode childNode;

            slTables = new SortedList<string, TableObject>();

            foreach (TableObject to in ts.Tables)
            {
                slTables.Add(to.name, to);
            }
            foreach (var item in slTables)
            {
                childNode = new TreeNode(item.Key);
                rootNode.Nodes.Add(childNode);
            }
            tvSchema.Nodes.Add(rootNode);
            tvSchema.ExpandAll();
            if (selectedNode != "")
                setSelectedNode();
        }

        string selectedNode = "";

        private void write2File(string name, string content)
        {
            string path = projectFolder + @"\schema\" + name + @".xml";
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine(content);
                sw.Close();
            }

        }

        private void tvSchema_DoubleClick(object sender, EventArgs e)
        {

        }

        private void tvSchema_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (tvSchema.Parent == null)
            {
                return;
            }
            selectedNode = tvSchema.SelectedNode.Text;
            setSelectedNode();
            tcSchema_SelectedIndexChanged(null, null);
        }

        private void setSelectedNode()
        {
            foreach (var item in slTables)
            {
                if (item.Key.Equals(selectedNode))
                {
                    pgProperties.SelectedObject = item.Value;
                    toSelected = item.Value;
                    pgProperties.Refresh();
                }
            }

            foreach (TreeNode tn in tvSchema.Nodes[0].Nodes)
            {

                if (tn.Text == selectedNode)
                { tvSchema.SelectedNode = tn; break; }
            }
        }

        private void tvSchema_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            // tvSchema_NodeMouseDoubleClick(sender, e);
        }

        string outputpath = "";
        string sqlpath = "";
        string modelpath = "";
        string servicepath = "";
        string serviceexpath = "";
        string controllerpath = "";
        string dopath = "";
        string apipath = "";
        string uicomponentpath = "";

        private void initPath()
        {
            outputpath = projectFolder + @"\output\";
            sqlpath = projectFolder + @"\output\sql\";
            modelpath = projectFolder + @"\output\model\";
            servicepath = projectFolder + @"\output\service\";
            serviceexpath = projectFolder + @"\output\serviceex\";
            controllerpath = projectFolder + @"\output\controller\";
            dopath = projectFolder + @"\output\objectdo\";
            apipath = projectFolder + @"\output\api\";
            uicomponentpath = projectFolder + @"\output\ui-component\";
            DirectoryInfo di = new DirectoryInfo(outputpath);
            if (!di.Exists) di.Create();
            di = new DirectoryInfo(sqlpath);
            if (!di.Exists) di.Create();
            di = new DirectoryInfo(modelpath);
            if (!di.Exists) di.Create();
            di = new DirectoryInfo(servicepath);
            if (!di.Exists) di.Create();
            di = new DirectoryInfo(serviceexpath);
            if (!di.Exists) di.Create();
            di = new DirectoryInfo(controllerpath);
            if (!di.Exists) di.Create();
            di = new DirectoryInfo(dopath);
            if (!di.Exists) di.Create();
            di = new DirectoryInfo(apipath);
            if (!di.Exists) di.Create();
            di = new DirectoryInfo(uicomponentpath);
            if (!di.Exists) di.Create();
        }

        private void generateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!fGenerate) return;
            // Generate SQL files
            string sqlTemplate = "";
            using (StreamReader sr = new StreamReader(Directory.GetCurrentDirectory() + @"\codetemplate\sql.txt"))
            {
                sqlTemplate = sr.ReadToEnd();
                sr.Close();
            }
            string sqlContent = "";
            foreach (TableObject to in ts.Tables)
            {
                if (!to.generateSQL) continue;
                sqlContent = sqlTemplate;
                foreach (ColumnObject co in to.Columns)
                {
                    sqlContent += string.Format("alter table `@tablename` add column `{0}` {1} {2} {3};{4}", co.name.ToLower(), co.dataType, (co.nullableUnqiue != "") ? co.nullableUnqiue : "", (co.defaultValue != "") ? "default " + co.defaultValue : "", Environment.NewLine);
                }
                sqlContent = sqlContent.Replace("@tablename", ss.Settings.tableprefix + to.name);

                using (StreamWriter sw = new StreamWriter(sqlpath + to.name.ToLower() + ".sql"))
                {
                    sw.WriteLine(sqlContent);
                    sw.Close();
                }
            }

            // Generate Model Files
            string modelTemplate = "";
            using (StreamReader sr = new StreamReader(Directory.GetCurrentDirectory() + @"\codetemplate\model.txt"))
            {
                modelTemplate = sr.ReadToEnd();
                sr.Close();
            }
            string modelContent = "";
            string modelSaveContent = "";
            string childTablesContent = "";
            string parentTablesContent = "";
            // string relatedTableContent = "";
            foreach (TableObject to in ts.Tables)
            {
                if (!to.generateModel) continue;
                modelContent = modelTemplate;
                modelSaveContent = "";
                childTablesContent = "";
                parentTablesContent = "";
                //relatedTableContent = "";
                foreach (ColumnObject co in to.Columns)
                {
                    modelSaveContent += string.Format("if(isset($input['{0}'])){1}\t\t\t$record->{0} = $input['{0}'];{1}\t\t", co.name.ToLower(), Environment.NewLine);
                }

                modelContent = modelContent.Replace("@tableprefix", ss.Settings.tableprefix);
                modelContent = modelContent.Replace("@tablename", to.name);
                modelContent = modelContent.Replace("@saveitems", modelSaveContent);
                modelContent = modelContent.Replace("@hasjoin", (to.hasRelatedTables) ? to.name + "." : "");
                // related table content
                modelContent = modelContent.Replace("@relatedTableResult", to.hasRelatedTables ? "1" : "0");
                modelContent = modelContent.Replace("@relatedTableContent", to.hasRelatedTables ? string.Format("{0}::select('{3}{1}'){2}", to.name.ToLower(), to.selectItems.ToLower(), to.joinTables, ss.Settings.tableprefix) : string.Format("{0}::select('{1}{0}.*')", to.name.ToLower(), ss.Settings.tableprefix));
                // child tables
                foreach (ChildTableObject cto in to.ChildTables)
                {
                    childTablesContent += string.Format("{0}        if ($pushrecord == 1 && isset($request['load{1}List']) && !empty($request['load{1}List']))", Environment.NewLine, cto.tableName.ToLower());
                    childTablesContent += string.Format("{0}        {{ {0}    $record['{1}List'] = {1}::get{1}('{2}',$record['id'], $request['{1}object']); ", Environment.NewLine, cto.tableName.ToLower(), cto.column.ToLower());
                    childTablesContent += string.Format("{0}        if (isset($request['check{1}Exists']) && !empty($request['check{1}Exists'])) {0} {{", Environment.NewLine, cto.tableName.ToLower());
                    childTablesContent += string.Format("{0}        if(count($record['{1}List']) == 0) $pushrecord = 0; {0}  }} {0} }}", Environment.NewLine, cto.tableName.ToLower());

                }
                int parenttablescount = 0;
                foreach (ColumnObject co in to.Columns)
                {
                    if (co.parentTable == "")
                        continue;
                    string[] ptables = co.parentTable.Trim().Split('~');
                    parenttablescount++;
                    string[] pcolumns = new string[ptables.Length];
                    string[] palias = new string[ptables.Length];
                    for (int i = 0; i < ptables.Length; i++)
                    {
                        try
                        {
                            pcolumns[i] = co.relatedColumn.Trim().Split('~')[i];

                        }
                        catch
                        {
                            pcolumns[i] = "";
                        }

                        try
                        {
                            palias[i] = co.parentTableAlias;
                        }
                        catch
                        {
                            palias[i] = "";
                        }
                    }
                    bool hasParentAlias = false;
                    for (int tcount = 0; tcount < ptables.Length; tcount++)
                    {
                        hasParentAlias = (!palias[tcount].Equals(""));

                        parentTablesContent += string.Format("{0}    ${1}Parent = [];{0} if(!isset($request['{1}parentobject'])){{ $request['{1}parentobject'] = [];}} {0}    if ($pushrecord == 1 && isset($request['filter{1}Parent']) && !empty($request['filter{1}Parent']))", Environment.NewLine, ((hasParentAlias) ? palias[tcount].ToLower():ptables[tcount].ToLower()));
                        parentTablesContent += string.Format("{0}       {{ {0}     ${1}Parent = {4}::get{4}('{3}',$record['{2}'], $request['{1}parentobject']); ", Environment.NewLine, ((hasParentAlias) ? palias[tcount].ToLower() : ptables[tcount].ToLower()), co.name.ToLower(), pcolumns[tcount].ToLower() == "" ? "id" : pcolumns[tcount].ToLower(), ptables[tcount].ToLower());
                        //parentTablesContent += string.Format("{0}        {{ {0}    $record['{1}List'] = {1}::get{1}('{2}',$record['id'], $request['{1}object']); ", Environment.NewLine, co.parentTable.ToLower());
                        parentTablesContent += string.Format("{0}        if (isset($request['check{1}ParentExists']) && !empty($request['check{1}ParentExists'])) {0} {{", Environment.NewLine, ((hasParentAlias) ? palias[tcount].ToLower() : ptables[tcount].ToLower()));
                        parentTablesContent += string.Format("{0}        if(count(${1}Parent) == 0) $pushrecord = 0; {0}  }} {0} }}", Environment.NewLine, ((hasParentAlias) ? palias[tcount].ToLower() : ptables[tcount].ToLower()));
                        parentTablesContent += string.Format("{0} else {0} {{ {0} ${1}Parent = {4}::get{4}('{3}',$record['{2}'], $request['{1}parentobject']); }} ", Environment.NewLine, ((hasParentAlias) ? palias[tcount].ToLower() : ptables[tcount].ToLower()), co.name.ToLower(), pcolumns[tcount].ToLower() == "" ? "id" : pcolumns[tcount].ToLower(), ptables[tcount].ToLower());
                        parentTablesContent += string.Format("{0} if (($pushrecord == 1) && (count(${1}Parent) > 0)) $record['{1}Parent'] = ${1}Parent[0]; {0}", Environment.NewLine, ((hasParentAlias) ? palias[tcount].ToLower() : ptables[tcount].ToLower()));
                    }

                    /*
                    parentTablesContent += string.Format("{0}    ${1}Parent = []; {0}    if ($pushrecord == 1 && isset($request['filter{1}Parent']) && !empty($request['filter{1}Parent']))", Environment.NewLine, co.parentTable.ToLower());
                    parentTablesContent += string.Format("{0}       {{ {0}     ${1}Parent = {1}::get{1}('{3}',$record['{2}'], $request['{1}parentobject']); ", Environment.NewLine, co.parentTable.ToLower(), co.name.ToLower(), co.relatedColumn == "" ? "id" : co.relatedColumn.ToLower());
                    //parentTablesContent += string.Format("{0}        {{ {0}    $record['{1}List'] = {1}::get{1}('{2}',$record['id'], $request['{1}object']); ", Environment.NewLine, co.parentTable.ToLower());
                    parentTablesContent += string.Format("{0}        if (isset($request['check{1}ParentExists']) && !empty($request['check{1}ParentExists'])) {0} {{", Environment.NewLine, co.parentTable.ToLower());
                    parentTablesContent += string.Format("{0}        if(count(${1}Parent) == 0) $pushrecord = 0; {0}  }} {0} }}", Environment.NewLine, co.parentTable.ToLower());
                    parentTablesContent += string.Format("{0} else {0} {{ {0} ${1}Parent = {1}::get{1}('{3}',$record['{2}']); }} ", Environment.NewLine, co.parentTable.ToLower(), co.name.ToLower(), co.relatedColumn == "" ? "id" : co.relatedColumn.ToLower());
                    parentTablesContent += string.Format("{0} if (($pushrecord == 1) && (count(${1}Parent) > 0)) $record['{1}Parent'] = ${1}Parent[0]; {0}", Environment.NewLine, co.parentTable.ToLower());
                    */

                }
                modelContent = modelContent.Replace("@childtablesresult", childTablesContent);
                modelContent = modelContent.Replace("@parenttablesresult", parentTablesContent);
                modelContent = modelContent.Replace("@childtablescount", to.ChildTables.Count.ToString());
                modelContent = modelContent.Replace("@parenttablescount", parenttablescount.ToString());
                using (StreamWriter sw = new StreamWriter(modelpath + to.name.ToLower() + ".php"))
                {
                    sw.WriteLine(modelContent);
                    sw.Close();
                }
            }

            // Generate service files
            string serviceTemplate = "";
            using (StreamReader sr = new StreamReader(Directory.GetCurrentDirectory() + @"\codetemplate\service.txt"))
            {
                serviceTemplate = sr.ReadToEnd();
                sr.Close();
            }
            string serviceexTemplate = "";
            using (StreamReader sr = new StreamReader(Directory.GetCurrentDirectory() + @"\codetemplate\serviceex.txt"))
            {
                serviceexTemplate = sr.ReadToEnd();
                sr.Close();
            }

            string serviceContent = "";
            string serviceexContent = "";
            foreach (TableObject to in ts.Tables)
            {
                if (!to.generateService) continue;
                serviceContent = serviceTemplate;
                serviceexContent = serviceexTemplate;

                serviceContent = serviceContent.Replace("@tablename", to.name);
                serviceexContent = serviceexContent.Replace("@tablename", to.name);

                using (StreamWriter sw = new StreamWriter(servicepath + to.name.ToLower() + "service.php"))
                {
                    sw.WriteLine(serviceContent);
                    sw.Close();
                }
                using (StreamWriter sw = new StreamWriter(serviceexpath + to.name.ToLower() + "serviceex.php"))
                {
                    sw.WriteLine(serviceexContent);
                    sw.Close();
                }
            }

            // Generate controller files
            string controllerTemplate = "";
            using (StreamReader sr = new StreamReader(Directory.GetCurrentDirectory() + @"\codetemplate\controller.txt"))
            {
                controllerTemplate = sr.ReadToEnd();
                sr.Close();
            }
            string controllerContent = "";
            foreach (TableObject to in ts.Tables)
            {
                if (!to.generateController) continue;
                controllerContent = controllerTemplate;

                controllerContent = controllerContent.Replace("@tablename", to.name);


                using (StreamWriter sw = new StreamWriter(controllerpath + to.name.ToLower() + "controller.php"))
                {
                    sw.WriteLine(controllerContent);
                    sw.Close();
                }

            }

            // Generate object do files
            string doTemplate = "";
            using (StreamReader sr = new StreamReader(Directory.GetCurrentDirectory() + @"\codetemplate\objectdo.txt"))
            {
                doTemplate = sr.ReadToEnd();
                sr.Close();
            }
            string doContent = "";
            string colContent = "";
            string formcolContent = "";
            string formitemsContent = "";
            foreach (TableObject to in ts.Tables)
            {
                doContent = doTemplate;

                doContent = doContent.Replace("@tablename", to.name);
                colContent = "";
                formcolContent = "";
                formitemsContent = "";
                foreach (ColumnObject co in to.Columns)
                {
                    colContent += string.Format("{1}{0} : string ='';", co.name.ToLower(), Environment.NewLine);
                    formcolContent += string.Format("{1}{0} : [''],", co.name.ToLower(), Environment.NewLine);
                    formitemsContent += string.Format("'{0}',", co.name.ToLower());

                    // date or timestamp
                    if (co.dataType.Equals("date") || co.dataType.Equals("timestamp"))
                    {
                        colContent += string.Format("{1}{0}2 : string ='';", co.name.ToLower(), Environment.NewLine);
                        formcolContent += string.Format("{1}{0}2 : [''],", co.name.ToLower(), Environment.NewLine);
                        formitemsContent += string.Format("'{0}2',", co.name.ToLower());
                    }
                }
                foreach (ChildTableObject cto in to.ChildTables)
                {
                    colContent += string.Format("{1}load{0}List : boolean = false;", cto.tableName.ToLower(), Environment.NewLine);
                    colContent += string.Format("{1}check{0}Exists : boolean = false;", cto.tableName.ToLower(), Environment.NewLine);
                    colContent += string.Format("{1}{0}object : any = '';", cto.tableName.ToLower(), Environment.NewLine);
                }
                
                foreach (ColumnObject co in to.Columns)
                {
                    if (co.parentTable == "") continue;
                    string[] ptables = co.parentTable.Trim().Split('~');                   
                    string[] pcolumns = new string[ptables.Length];
                    string[] palias = new string[ptables.Length];
                    for (int i = 0; i < ptables.Length; i++)
                    {
                        try
                        {
                            pcolumns[i] = co.relatedColumn.Trim().Split('~')[i];

                        }
                        catch
                        {
                            pcolumns[i] = "";
                        }

                        try
                        {
                            palias[i] = co.parentTableAlias;
                        }
                        catch
                        {
                            palias[i] = "";
                        }
                    }
                    bool hasParentAlias = false;
                    for (int tcount = 0; tcount < ptables.Length; tcount++)
                    {
                        hasParentAlias = (!palias[tcount].Equals(""));
                        colContent += string.Format("{1}filter{0}Parent : boolean = false;", ((hasParentAlias) ? palias[tcount].ToLower() : ptables[tcount].ToLower()), Environment.NewLine);
                        colContent += string.Format("{1}check{0}ParentExists : boolean = false;", ((hasParentAlias) ? palias[tcount].ToLower() : ptables[tcount].ToLower()), Environment.NewLine);
                        colContent += string.Format("{1}{0}parentobject : any = '';", ((hasParentAlias) ? palias[tcount].ToLower() : ptables[tcount].ToLower()), Environment.NewLine);
                    }
                }
                doContent = doContent.Replace("@columns", colContent);
                doContent = doContent.Replace("@formcolumns", formcolContent);
                doContent = doContent.Replace("@formitems", formitemsContent);
                using (StreamWriter sw = new StreamWriter(dopath + to.name.ToLower() + ".do.ts"))
                {
                    sw.WriteLine(doContent);
                    sw.Close();
                }

            }

            // Generate ui component files
            string uiTemplate = "";
            using (StreamReader sr = new StreamReader(Directory.GetCurrentDirectory() + @"\codetemplate\ui-component.txt"))
            {
                uiTemplate = sr.ReadToEnd();
                sr.Close();
            }
            string uiContent = "";

            foreach (TableObject to in ts.Tables)
            {
                uiContent = uiTemplate;

                uiContent = uiContent.Replace("@tablename", to.name);
                uiContent = uiContent.Replace("@utablename", convert2UC(to.name));

                using (StreamWriter sw = new StreamWriter(uicomponentpath + to.name.ToLower() + "-ui-component.txt"))
                {
                    sw.WriteLine(uiContent);
                    sw.Close();
                }

            }

            // Generate API Files
            string apiContent = "";

            foreach (TableObject to in ts.Tables)
            {
                apiContent += string.Format(@"Route::group(['namespace' => 'meta', 'prefix' => '{0}'], function () {{ {1}", to.name.ToLower(), Environment.NewLine);
                apiContent += string.Format("Route::any('save', '{0}controller@save{0}');{1}", to.name.ToLower(), Environment.NewLine);
                apiContent += string.Format("Route::any('get', '{0}controller@get{0}');{1}", to.name.ToLower(), Environment.NewLine);
                apiContent += string.Format("Route::any('list', '{0}controller@list{0}');{1}", to.name.ToLower(), Environment.NewLine);
                apiContent += string.Format("Route::any('listcount', '{0}controller@list{0}count');{1}", to.name.ToLower(), Environment.NewLine);
                apiContent += string.Format("Route::any('getcount', '{0}controller@get{0}count');{1}", to.name.ToLower(), Environment.NewLine);
                apiContent += @"});" + Environment.NewLine + Environment.NewLine;
            }
            // Route::any('save', 'userscontroller@saveusers');
            //     //Route::any('cdf', 'UsersController@createDefault');
            //    Route::any('list', 'userscontroller@listusers');

            using (StreamWriter sw = new StreamWriter(apipath + "api.php"))
            {
                sw.WriteLine(apiContent);
                sw.Close();
            }

            MessageBox.Show("Files Generated");

        }

        private void tcSchema_SelectedIndexChanged(object sender, EventArgs e)
        {

            TabPage tp = tcSchema.SelectedTab as TabPage;
            tbSQL.Text = "";
            tbModel.Text = "";
            tbService.Text = "";
            tbController.Text = "";
            TableObject to = pgProperties.SelectedObject as TableObject;
            if (to == null) return;
            if (to.name.Equals("")) return;
            if (tp.Text.ToLower() == "sql")
            {
                if (!File.Exists(sqlpath + to.name.ToLower() + ".sql")) return;
                using (StreamReader sr = new StreamReader(sqlpath + to.name.ToLower() + ".sql"))
                {
                    try
                    {
                        tbSQL.Text = sr.ReadToEnd();
                    }
                    catch (Exception ex) { tbSQL.Text = ex.Message; }
                    finally
                    {
                        sr.Close();
                    }
                }
            }
            if (tp.Text.ToLower() == "model")
            {
                if (!File.Exists(modelpath + to.name.ToLower() + ".php")) return;
                using (StreamReader sr = new StreamReader(modelpath + to.name.ToLower() + ".php"))
                {
                    try
                    {
                        tbModel.Text = sr.ReadToEnd();
                    }
                    catch (Exception ex) { tbModel.Text = ex.Message; }
                    finally
                    {
                        sr.Close();
                    }
                }
            }
            if (tp.Text.ToLower() == "service")
            {
                if (!File.Exists(servicepath + to.name.ToLower() + "service.php")) return;
                using (StreamReader sr = new StreamReader(servicepath + to.name.ToLower() + "service.php"))
                {
                    try
                    {
                        tbService.Text = sr.ReadToEnd();
                    }
                    catch (Exception ex) { tbService.Text = ex.Message; }
                    finally
                    {
                        sr.Close();
                    }
                }
            }
            if (tp.Text.ToLower() == "controller")
            {
                if (!File.Exists(controllerpath + to.name.ToLower() + "controller.php")) return;
                using (StreamReader sr = new StreamReader(controllerpath + to.name.ToLower() + "controller.php"))
                {
                    try
                    {
                        tbController.Text = sr.ReadToEnd();
                    }
                    catch (Exception ex) { tbController.Text = ex.Message; }
                    finally
                    {
                        sr.Close();
                    }
                }
            }
        }

        private string projectFolder = "";
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openProject();

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openProject();
        }

        bool fGenerate = false;

        private void openProject()
        {
            DialogResult dr = fbDialog.ShowDialog();
            if (dr.ToString() == DialogResult.OK.ToString())
            {
                projectFolder = fbDialog.SelectedPath;
                this.Text = projectFolder;
                // Load others
                InitializeComponentEx();
                initPath();
                fGenerate = true;
            }
        }

        private string convert2UC(string input)
        {
            var arr = input.ToCharArray();
            arr[0] = Char.ToUpperInvariant(arr[0]);
            return new String(arr);
        }

        private void msMain_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void tsmiSettings_Click(object sender, EventArgs e)
        {
            SettingsObject to = pgSettings.SelectedObject as SettingsObject;
            string serializedContent = to.Serialize<SettingsObject>(to);

            serializedContent = to.Serialize<SettingsObject>(to);
            writeSettings2File("settings", serializedContent);
            MessageBox.Show("Settings Saved successfully");
            InitializeComponentEx();

        }

        private void writeSettings2File(string name, string content)
        {
            string path = projectFolder + @"\settings\settings.xml";
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine(content);
                sw.Close();
            }

        }
    }
}
