﻿import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class @tablenameDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "@tablename";
    }
	static localStorage : string = "ls_@tablename";
    // columns
    @columns
}

export class @tablenameFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id',@formitems];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],			
			@formcolumns
		});
	}
}
