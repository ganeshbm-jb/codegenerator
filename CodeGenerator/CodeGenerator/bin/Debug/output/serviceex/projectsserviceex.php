<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\projects;
use DB;
use Webpatser\Uuid\Uuid;

class projectsserviceex extends BaseService
{
	// Method to save data
	public static function saveprojects_before($id, $input)
	{
		return $input;
	}
	public static function saveprojects_after($id, $input)
	{
	}

	// Method to get all records
	public static function listprojects_before($request)
	{		
		return $request;
	}
	public static function listprojects_after($request)
	{		
	}


	// Method to get a record
	public static function getprojects_before($request)
	{	
		return $request;	
	}
	public static function getprojects_after($request)
	{		
	}
}
