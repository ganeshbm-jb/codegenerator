<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\userlogins;
use DB;
use Webpatser\Uuid\Uuid;

class userloginsserviceex extends BaseService
{
	// Method to save data
	public static function saveuserlogins_before($id, $input)
	{
		return $input;
	}
	public static function saveuserlogins_after($id, $input)
	{
	}

	// Method to get all records
	public static function listuserlogins_before($request)
	{		
		return $request;
	}
	public static function listuserlogins_after($request)
	{		
	}


	// Method to get a record
	public static function getuserlogins_before($request)
	{	
		return $request;	
	}
	public static function getuserlogins_after($request)
	{		
	}
}
