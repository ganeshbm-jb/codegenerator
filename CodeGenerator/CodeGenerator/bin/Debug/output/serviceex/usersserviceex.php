<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\users;
use DB;
use Webpatser\Uuid\Uuid;

class usersserviceex extends BaseService
{
	// Method to save data
	public static function saveusers_before($id, $input)
	{
		return $input;
	}
	public static function saveusers_after($id, $input)
	{
	}

	// Method to get all records
	public static function listusers_before($request)
	{		
		return $request;
	}
	public static function listusers_after($request)
	{		
	}


	// Method to get a record
	public static function getusers_before($request)
	{	
		return $request;	
	}
	public static function getusers_after($request)
	{		
	}
}
