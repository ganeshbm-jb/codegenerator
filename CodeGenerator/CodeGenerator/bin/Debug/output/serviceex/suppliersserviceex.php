<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\suppliers;
use DB;
use Webpatser\Uuid\Uuid;

class suppliersserviceex extends BaseService
{
	// Method to save data
	public static function savesuppliers_before($id, $input)
	{
		return $input;
	}
	public static function savesuppliers_after($id, $input)
	{
	}

	// Method to get all records
	public static function listsuppliers_before($request)
	{		
		return $request;
	}
	public static function listsuppliers_after($request)
	{		
	}


	// Method to get a record
	public static function getsuppliers_before($request)
	{	
		return $request;	
	}
	public static function getsuppliers_after($request)
	{		
	}
}
