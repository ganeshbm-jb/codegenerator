<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\dashboard;
use DB;
use Webpatser\Uuid\Uuid;

class dashboardserviceex extends BaseService
{
	// Method to save data
	public static function savedashboard_before($id, $input)
	{
		return $input;
	}
	public static function savedashboard_after($id, $input)
	{
	}

	// Method to get all records
	public static function listdashboard_before($request)
	{		
		return $request;
	}
	public static function listdashboard_after($request)
	{		
	}


	// Method to get a record
	public static function getdashboard_before($request)
	{	
		return $request;	
	}
	public static function getdashboard_after($request)
	{		
	}
}
