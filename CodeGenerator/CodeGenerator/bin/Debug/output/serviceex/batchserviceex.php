<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\batch;
use DB;
use Webpatser\Uuid\Uuid;

class batchserviceex extends BaseService
{
	// Method to save data
	public static function savebatch_before($id, $input)
	{
		return $input;
	}
	public static function savebatch_after($id, $input)
	{
	}

	// Method to get all records
	public static function listbatch_before($request)
	{		
		return $request;
	}
	public static function listbatch_after($request)
	{		
	}


	// Method to get a record
	public static function getbatch_before($request)
	{	
		return $request;	
	}
	public static function getbatch_after($request)
	{		
	}
}
