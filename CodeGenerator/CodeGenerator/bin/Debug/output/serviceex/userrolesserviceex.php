<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\userroles;
use DB;
use Webpatser\Uuid\Uuid;

class userrolesserviceex extends BaseService
{
	// Method to save data
	public static function saveuserroles_before($id, $input)
	{
		return $input;
	}
	public static function saveuserroles_after($id, $input)
	{
	}

	// Method to get all records
	public static function listuserroles_before($request)
	{		
		return $request;
	}
	public static function listuserroles_after($request)
	{		
	}


	// Method to get a record
	public static function getuserroles_before($request)
	{	
		return $request;	
	}
	public static function getuserroles_after($request)
	{		
	}
}
