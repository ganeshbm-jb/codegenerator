<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\roles;
use DB;
use Webpatser\Uuid\Uuid;

class rolesserviceex extends BaseService
{
	// Method to save data
	public static function saveroles_before($id, $input)
	{
		return $input;
	}
	public static function saveroles_after($id, $input)
	{
	}

	// Method to get all records
	public static function listroles_before($request)
	{		
		return $request;
	}
	public static function listroles_after($request)
	{		
	}


	// Method to get a record
	public static function getroles_before($request)
	{	
		return $request;	
	}
	public static function getroles_after($request)
	{		
	}
}
