<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\expensetype;
use DB;
use Webpatser\Uuid\Uuid;

class expensetypeserviceex extends BaseService
{
	// Method to save data
	public static function saveexpensetype_before($id, $input)
	{
		return $input;
	}
	public static function saveexpensetype_after($id, $input)
	{
	}

	// Method to get all records
	public static function listexpensetype_before($request)
	{		
		return $request;
	}
	public static function listexpensetype_after($request)
	{		
	}


	// Method to get a record
	public static function getexpensetype_before($request)
	{	
		return $request;	
	}
	public static function getexpensetype_after($request)
	{		
	}
}
