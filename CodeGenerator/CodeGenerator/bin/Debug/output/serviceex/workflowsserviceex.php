<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\workflows;
use DB;
use Webpatser\Uuid\Uuid;

class workflowsserviceex extends BaseService
{
	// Method to save data
	public static function saveworkflows_before($id, $input)
	{
		return $input;
	}
	public static function saveworkflows_after($id, $input)
	{
	}

	// Method to get all records
	public static function listworkflows_before($request)
	{		
		return $request;
	}
	public static function listworkflows_after($request)
	{		
	}


	// Method to get a record
	public static function getworkflows_before($request)
	{	
		return $request;	
	}
	public static function getworkflows_after($request)
	{		
	}
}
