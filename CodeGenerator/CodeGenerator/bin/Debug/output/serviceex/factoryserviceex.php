<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\factory;
use DB;
use Webpatser\Uuid\Uuid;

class factoryserviceex extends BaseService
{
	// Method to save data
	public static function savefactory_before($id, $input)
	{
		return $input;
	}
	public static function savefactory_after($id, $input)
	{
	}

	// Method to get all records
	public static function listfactory_before($request)
	{		
		return $request;
	}
	public static function listfactory_after($request)
	{		
	}


	// Method to get a record
	public static function getfactory_before($request)
	{	
		return $request;	
	}
	public static function getfactory_after($request)
	{		
	}
}
