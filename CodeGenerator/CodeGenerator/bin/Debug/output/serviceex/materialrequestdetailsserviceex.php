<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\materialrequestdetails;
use DB;
use Webpatser\Uuid\Uuid;

class materialrequestdetailsserviceex extends BaseService
{
	// Method to save data
	public static function savematerialrequestdetails_before($id, $input)
	{
		return $input;
	}
	public static function savematerialrequestdetails_after($id, $input)
	{
	}

	// Method to get all records
	public static function listmaterialrequestdetails_before($request)
	{		
		return $request;
	}
	public static function listmaterialrequestdetails_after($request)
	{		
	}


	// Method to get a record
	public static function getmaterialrequestdetails_before($request)
	{	
		return $request;	
	}
	public static function getmaterialrequestdetails_after($request)
	{		
	}
}
