<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\description;
use DB;
use Webpatser\Uuid\Uuid;

class descriptionserviceex extends BaseService
{
	// Method to save data
	public static function savedescription_before($id, $input)
	{
		return $input;
	}
	public static function savedescription_after($id, $input)
	{
	}

	// Method to get all records
	public static function listdescription_before($request)
	{		
		return $request;
	}
	public static function listdescription_after($request)
	{		
	}


	// Method to get a record
	public static function getdescription_before($request)
	{	
		return $request;	
	}
	public static function getdescription_after($request)
	{		
	}
}
