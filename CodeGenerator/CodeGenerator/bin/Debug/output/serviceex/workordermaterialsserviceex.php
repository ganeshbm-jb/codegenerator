<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\workordermaterials;
use DB;
use Webpatser\Uuid\Uuid;

class workordermaterialsserviceex extends BaseService
{
	// Method to save data
	public static function saveworkordermaterials_before($id, $input)
	{
		return $input;
	}
	public static function saveworkordermaterials_after($id, $input)
	{
	}

	// Method to get all records
	public static function listworkordermaterials_before($request)
	{		
		return $request;
	}
	public static function listworkordermaterials_after($request)
	{		
	}


	// Method to get a record
	public static function getworkordermaterials_before($request)
	{	
		return $request;	
	}
	public static function getworkordermaterials_after($request)
	{		
	}
}
