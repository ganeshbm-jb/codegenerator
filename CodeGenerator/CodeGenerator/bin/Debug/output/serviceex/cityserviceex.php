<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\city;
use DB;
use Webpatser\Uuid\Uuid;

class cityserviceex extends BaseService
{
	// Method to save data
	public static function savecity_before($id, $input)
	{
		return $input;
	}
	public static function savecity_after($id, $input)
	{
	}

	// Method to get all records
	public static function listcity_before($request)
	{		
		return $request;
	}
	public static function listcity_after($request)
	{		
	}


	// Method to get a record
	public static function getcity_before($request)
	{	
		return $request;	
	}
	public static function getcity_after($request)
	{		
	}
}
