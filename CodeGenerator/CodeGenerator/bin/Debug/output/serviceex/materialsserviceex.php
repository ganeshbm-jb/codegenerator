<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\materials;
use DB;
use Webpatser\Uuid\Uuid;

class materialsserviceex extends BaseService
{
	// Method to save data
	public static function savematerials_before($id, $input)
	{
		return $input;
	}
	public static function savematerials_after($id, $input)
	{
	}

	// Method to get all records
	public static function listmaterials_before($request)
	{		
		return $request;
	}
	public static function listmaterials_after($request)
	{		
	}


	// Method to get a record
	public static function getmaterials_before($request)
	{	
		return $request;	
	}
	public static function getmaterials_after($request)
	{		
	}
}
