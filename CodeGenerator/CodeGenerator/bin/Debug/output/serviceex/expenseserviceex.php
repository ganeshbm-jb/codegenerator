<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\expense;
use DB;
use Webpatser\Uuid\Uuid;

class expenseserviceex extends BaseService
{
	// Method to save data
	public static function saveexpense_before($id, $input)
	{
		return $input;
	}
	public static function saveexpense_after($id, $input)
	{
	}

	// Method to get all records
	public static function listexpense_before($request)
	{		
		return $request;
	}
	public static function listexpense_after($request)
	{		
	}


	// Method to get a record
	public static function getexpense_before($request)
	{	
		return $request;	
	}
	public static function getexpense_after($request)
	{		
	}
}
