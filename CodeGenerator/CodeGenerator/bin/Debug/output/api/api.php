Route::group(['namespace' => 'meta', 'prefix' => 'advance'], function () { 
Route::any('save', 'advancecontroller@saveadvance');
Route::any('get', 'advancecontroller@getadvance');
Route::any('list', 'advancecontroller@listadvance');
});

Route::group(['namespace' => 'meta', 'prefix' => 'advancetype'], function () { 
Route::any('save', 'advancetypecontroller@saveadvancetype');
Route::any('get', 'advancetypecontroller@getadvancetype');
Route::any('list', 'advancetypecontroller@listadvancetype');
});

Route::group(['namespace' => 'meta', 'prefix' => 'batch'], function () { 
Route::any('save', 'batchcontroller@savebatch');
Route::any('get', 'batchcontroller@getbatch');
Route::any('list', 'batchcontroller@listbatch');
});

Route::group(['namespace' => 'meta', 'prefix' => 'city'], function () { 
Route::any('save', 'citycontroller@savecity');
Route::any('get', 'citycontroller@getcity');
Route::any('list', 'citycontroller@listcity');
});

Route::group(['namespace' => 'meta', 'prefix' => 'codemaster'], function () { 
Route::any('save', 'codemastercontroller@savecodemaster');
Route::any('get', 'codemastercontroller@getcodemaster');
Route::any('list', 'codemastercontroller@listcodemaster');
});

Route::group(['namespace' => 'meta', 'prefix' => 'dashboard'], function () { 
Route::any('save', 'dashboardcontroller@savedashboard');
Route::any('get', 'dashboardcontroller@getdashboard');
Route::any('list', 'dashboardcontroller@listdashboard');
});

Route::group(['namespace' => 'meta', 'prefix' => 'description'], function () { 
Route::any('save', 'descriptioncontroller@savedescription');
Route::any('get', 'descriptioncontroller@getdescription');
Route::any('list', 'descriptioncontroller@listdescription');
});

Route::group(['namespace' => 'meta', 'prefix' => 'expense'], function () { 
Route::any('save', 'expensecontroller@saveexpense');
Route::any('get', 'expensecontroller@getexpense');
Route::any('list', 'expensecontroller@listexpense');
});

Route::group(['namespace' => 'meta', 'prefix' => 'expensetype'], function () { 
Route::any('save', 'expensetypecontroller@saveexpensetype');
Route::any('get', 'expensetypecontroller@getexpensetype');
Route::any('list', 'expensetypecontroller@listexpensetype');
});

Route::group(['namespace' => 'meta', 'prefix' => 'factory'], function () { 
Route::any('save', 'factorycontroller@savefactory');
Route::any('get', 'factorycontroller@getfactory');
Route::any('list', 'factorycontroller@listfactory');
});

Route::group(['namespace' => 'meta', 'prefix' => 'invoice'], function () { 
Route::any('save', 'invoicecontroller@saveinvoice');
Route::any('get', 'invoicecontroller@getinvoice');
Route::any('list', 'invoicecontroller@listinvoice');
});

Route::group(['namespace' => 'meta', 'prefix' => 'invoiceitem'], function () { 
Route::any('save', 'invoiceitemcontroller@saveinvoiceitem');
Route::any('get', 'invoiceitemcontroller@getinvoiceitem');
Route::any('list', 'invoiceitemcontroller@listinvoiceitem');
});

Route::group(['namespace' => 'meta', 'prefix' => 'invoiceothers'], function () { 
Route::any('save', 'invoiceotherscontroller@saveinvoiceothers');
Route::any('get', 'invoiceotherscontroller@getinvoiceothers');
Route::any('list', 'invoiceotherscontroller@listinvoiceothers');
});

Route::group(['namespace' => 'meta', 'prefix' => 'item'], function () { 
Route::any('save', 'itemcontroller@saveitem');
Route::any('get', 'itemcontroller@getitem');
Route::any('list', 'itemcontroller@listitem');
});

Route::group(['namespace' => 'meta', 'prefix' => 'itemmaterials'], function () { 
Route::any('save', 'itemmaterialscontroller@saveitemmaterials');
Route::any('get', 'itemmaterialscontroller@getitemmaterials');
Route::any('list', 'itemmaterialscontroller@listitemmaterials');
});

Route::group(['namespace' => 'meta', 'prefix' => 'ledger'], function () { 
Route::any('save', 'ledgercontroller@saveledger');
Route::any('get', 'ledgercontroller@getledger');
Route::any('list', 'ledgercontroller@listledger');
});

Route::group(['namespace' => 'meta', 'prefix' => 'materialrequest'], function () { 
Route::any('save', 'materialrequestcontroller@savematerialrequest');
Route::any('get', 'materialrequestcontroller@getmaterialrequest');
Route::any('list', 'materialrequestcontroller@listmaterialrequest');
});

Route::group(['namespace' => 'meta', 'prefix' => 'materialrequestdetails'], function () { 
Route::any('save', 'materialrequestdetailscontroller@savematerialrequestdetails');
Route::any('get', 'materialrequestdetailscontroller@getmaterialrequestdetails');
Route::any('list', 'materialrequestdetailscontroller@listmaterialrequestdetails');
});

Route::group(['namespace' => 'meta', 'prefix' => 'materials'], function () { 
Route::any('save', 'materialscontroller@savematerials');
Route::any('get', 'materialscontroller@getmaterials');
Route::any('list', 'materialscontroller@listmaterials');
});

Route::group(['namespace' => 'meta', 'prefix' => 'menus'], function () { 
Route::any('save', 'menuscontroller@savemenus');
Route::any('get', 'menuscontroller@getmenus');
Route::any('list', 'menuscontroller@listmenus');
});

Route::group(['namespace' => 'meta', 'prefix' => 'projects'], function () { 
Route::any('save', 'projectscontroller@saveprojects');
Route::any('get', 'projectscontroller@getprojects');
Route::any('list', 'projectscontroller@listprojects');
});

Route::group(['namespace' => 'meta', 'prefix' => 'purchasematerial'], function () { 
Route::any('save', 'purchasematerialcontroller@savepurchasematerial');
Route::any('get', 'purchasematerialcontroller@getpurchasematerial');
Route::any('list', 'purchasematerialcontroller@listpurchasematerial');
});

Route::group(['namespace' => 'meta', 'prefix' => 'purchaseorder'], function () { 
Route::any('save', 'purchaseordercontroller@savepurchaseorder');
Route::any('get', 'purchaseordercontroller@getpurchaseorder');
Route::any('list', 'purchaseordercontroller@listpurchaseorder');
});

Route::group(['namespace' => 'meta', 'prefix' => 'purchaseordermaterial'], function () { 
Route::any('save', 'purchaseordermaterialcontroller@savepurchaseordermaterial');
Route::any('get', 'purchaseordermaterialcontroller@getpurchaseordermaterial');
Route::any('list', 'purchaseordermaterialcontroller@listpurchaseordermaterial');
});

Route::group(['namespace' => 'meta', 'prefix' => 'purchases'], function () { 
Route::any('save', 'purchasescontroller@savepurchases');
Route::any('get', 'purchasescontroller@getpurchases');
Route::any('list', 'purchasescontroller@listpurchases');
});

Route::group(['namespace' => 'meta', 'prefix' => 'requesttype'], function () { 
Route::any('save', 'requesttypecontroller@saverequesttype');
Route::any('get', 'requesttypecontroller@getrequesttype');
Route::any('list', 'requesttypecontroller@listrequesttype');
});

Route::group(['namespace' => 'meta', 'prefix' => 'roles'], function () { 
Route::any('save', 'rolescontroller@saveroles');
Route::any('get', 'rolescontroller@getroles');
Route::any('list', 'rolescontroller@listroles');
});

Route::group(['namespace' => 'meta', 'prefix' => 'stock'], function () { 
Route::any('save', 'stockcontroller@savestock');
Route::any('get', 'stockcontroller@getstock');
Route::any('list', 'stockcontroller@liststock');
});

Route::group(['namespace' => 'meta', 'prefix' => 'stocklocationqty'], function () { 
Route::any('save', 'stocklocationqtycontroller@savestocklocationqty');
Route::any('get', 'stocklocationqtycontroller@getstocklocationqty');
Route::any('list', 'stocklocationqtycontroller@liststocklocationqty');
});

Route::group(['namespace' => 'meta', 'prefix' => 'stockqty'], function () { 
Route::any('save', 'stockqtycontroller@savestockqty');
Route::any('get', 'stockqtycontroller@getstockqty');
Route::any('list', 'stockqtycontroller@liststockqty');
});

Route::group(['namespace' => 'meta', 'prefix' => 'stockvalue'], function () { 
Route::any('save', 'stockvaluecontroller@savestockvalue');
Route::any('get', 'stockvaluecontroller@getstockvalue');
Route::any('list', 'stockvaluecontroller@liststockvalue');
});

Route::group(['namespace' => 'meta', 'prefix' => 'suppliers'], function () { 
Route::any('save', 'supplierscontroller@savesuppliers');
Route::any('get', 'supplierscontroller@getsuppliers');
Route::any('list', 'supplierscontroller@listsuppliers');
});

Route::group(['namespace' => 'meta', 'prefix' => 'userlogins'], function () { 
Route::any('save', 'userloginscontroller@saveuserlogins');
Route::any('get', 'userloginscontroller@getuserlogins');
Route::any('list', 'userloginscontroller@listuserlogins');
});

Route::group(['namespace' => 'meta', 'prefix' => 'userroles'], function () { 
Route::any('save', 'userrolescontroller@saveuserroles');
Route::any('get', 'userrolescontroller@getuserroles');
Route::any('list', 'userrolescontroller@listuserroles');
});

Route::group(['namespace' => 'meta', 'prefix' => 'users'], function () { 
Route::any('save', 'userscontroller@saveusers');
Route::any('get', 'userscontroller@getusers');
Route::any('list', 'userscontroller@listusers');
});

Route::group(['namespace' => 'meta', 'prefix' => 'viewaccess'], function () { 
Route::any('save', 'viewaccesscontroller@saveviewaccess');
Route::any('get', 'viewaccesscontroller@getviewaccess');
Route::any('list', 'viewaccesscontroller@listviewaccess');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workers'], function () { 
Route::any('save', 'workerscontroller@saveworkers');
Route::any('get', 'workerscontroller@getworkers');
Route::any('list', 'workerscontroller@listworkers');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workflows'], function () { 
Route::any('save', 'workflowscontroller@saveworkflows');
Route::any('get', 'workflowscontroller@getworkflows');
Route::any('list', 'workflowscontroller@listworkflows');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workordercost'], function () { 
Route::any('save', 'workordercostcontroller@saveworkordercost');
Route::any('get', 'workordercostcontroller@getworkordercost');
Route::any('list', 'workordercostcontroller@listworkordercost');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workorderitems'], function () { 
Route::any('save', 'workorderitemscontroller@saveworkorderitems');
Route::any('get', 'workorderitemscontroller@getworkorderitems');
Route::any('list', 'workorderitemscontroller@listworkorderitems');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workordermaterials'], function () { 
Route::any('save', 'workordermaterialscontroller@saveworkordermaterials');
Route::any('get', 'workordermaterialscontroller@getworkordermaterials');
Route::any('list', 'workordermaterialscontroller@listworkordermaterials');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workorders'], function () { 
Route::any('save', 'workorderscontroller@saveworkorders');
Route::any('get', 'workorderscontroller@getworkorders');
Route::any('list', 'workorderscontroller@listworkorders');
});


