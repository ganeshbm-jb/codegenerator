<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\projectsservice;


class projectscontroller  extends ParentController
{
    //
    public function saveprojects(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,projectsservice::getprojects($filter));           
        }        
        $request = Self::digestInput($request);         
        return projectsservice::saveprojects($id, $request);
    }

	// Method to get all records
	public static function listprojects(Request $request)
	{
		$request = Self::digestInput($request);
        return projectsservice::listprojects($request);
	}

    public function getprojects(Request $request)
    {
        $request = Self::digestInput($request);
        return projectsservice::getprojects($request);
    }
}

