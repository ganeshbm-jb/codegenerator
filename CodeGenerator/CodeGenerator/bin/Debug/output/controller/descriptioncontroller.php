<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\descriptionservice;


class descriptioncontroller  extends ParentController
{
    //
    public function savedescription(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,descriptionservice::getdescription($filter));           
        }        
        $request = Self::digestInput($request);         
        return descriptionservice::savedescription($id, $request);
    }

	// Method to get all records
	public static function listdescription(Request $request)
	{
		$request = Self::digestInput($request);
        return descriptionservice::listdescription($request);
	}

    public function getdescription(Request $request)
    {
        $request = Self::digestInput($request);
        return descriptionservice::getdescription($request);
    }
}

