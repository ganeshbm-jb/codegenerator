<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\expensetypeservice;


class expensetypecontroller  extends ParentController
{
    //
    public function saveexpensetype(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,expensetypeservice::getexpensetype($filter));           
        }        
        $request = Self::digestInput($request);         
        return expensetypeservice::saveexpensetype($id, $request);
    }

	// Method to get all records
	public static function listexpensetype(Request $request)
	{
		$request = Self::digestInput($request);
        return expensetypeservice::listexpensetype($request);
	}

    public function getexpensetype(Request $request)
    {
        $request = Self::digestInput($request);
        return expensetypeservice::getexpensetype($request);
    }
}

