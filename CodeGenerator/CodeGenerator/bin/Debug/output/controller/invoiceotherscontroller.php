<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\invoiceothersservice;


class invoiceotherscontroller  extends ParentController
{
    //
    public function saveinvoiceothers(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,invoiceothersservice::getinvoiceothers($filter));           
        }        
        $request = Self::digestInput($request);         
        return invoiceothersservice::saveinvoiceothers($id, $request);
    }

	// Method to get all records
	public static function listinvoiceothers(Request $request)
	{
		$request = Self::digestInput($request);
        return invoiceothersservice::listinvoiceothers($request);
	}

    public function getinvoiceothers(Request $request)
    {
        $request = Self::digestInput($request);
        return invoiceothersservice::getinvoiceothers($request);
    }
}

