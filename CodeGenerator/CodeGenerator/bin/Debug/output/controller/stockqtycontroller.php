<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\stockqtyservice;


class stockqtycontroller  extends ParentController
{
    //
    public function savestockqty(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,stockqtyservice::getstockqty($filter));           
        }        
        $request = Self::digestInput($request);         
        return stockqtyservice::savestockqty($id, $request);
    }

	// Method to get all records
	public static function liststockqty(Request $request)
	{
		$request = Self::digestInput($request);
        return stockqtyservice::liststockqty($request);
	}

    public function getstockqty(Request $request)
    {
        $request = Self::digestInput($request);
        return stockqtyservice::getstockqty($request);
    }
}

