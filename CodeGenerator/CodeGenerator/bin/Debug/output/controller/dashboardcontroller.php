<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\dashboardservice;


class dashboardcontroller  extends ParentController
{
    //
    public function savedashboard(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,dashboardservice::getdashboard($filter));           
        }        
        $request = Self::digestInput($request);         
        return dashboardservice::savedashboard($id, $request);
    }

	// Method to get all records
	public static function listdashboard(Request $request)
	{
		$request = Self::digestInput($request);
        return dashboardservice::listdashboard($request);
	}

    public function getdashboard(Request $request)
    {
        $request = Self::digestInput($request);
        return dashboardservice::getdashboard($request);
    }
}

