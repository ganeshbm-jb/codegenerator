<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\expenseservice;


class expensecontroller  extends ParentController
{
    //
    public function saveexpense(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,expenseservice::getexpense($filter));           
        }        
        $request = Self::digestInput($request);         
        return expenseservice::saveexpense($id, $request);
    }

	// Method to get all records
	public static function listexpense(Request $request)
	{
		$request = Self::digestInput($request);
        return expenseservice::listexpense($request);
	}

    public function getexpense(Request $request)
    {
        $request = Self::digestInput($request);
        return expenseservice::getexpense($request);
    }
}

