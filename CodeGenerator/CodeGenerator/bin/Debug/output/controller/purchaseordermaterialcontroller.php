<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\purchaseordermaterialservice;


class purchaseordermaterialcontroller  extends ParentController
{
    //
    public function savepurchaseordermaterial(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,purchaseordermaterialservice::getpurchaseordermaterial($filter));           
        }        
        $request = Self::digestInput($request);         
        return purchaseordermaterialservice::savepurchaseordermaterial($id, $request);
    }

	// Method to get all records
	public static function listpurchaseordermaterial(Request $request)
	{
		$request = Self::digestInput($request);
        return purchaseordermaterialservice::listpurchaseordermaterial($request);
	}

    public function getpurchaseordermaterial(Request $request)
    {
        $request = Self::digestInput($request);
        return purchaseordermaterialservice::getpurchaseordermaterial($request);
    }
}

