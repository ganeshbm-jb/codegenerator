<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\userloginsservice;


class userloginscontroller  extends ParentController
{
    //
    public function saveuserlogins(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,userloginsservice::getuserlogins($filter));           
        }        
        $request = Self::digestInput($request);         
        return userloginsservice::saveuserlogins($id, $request);
    }

	// Method to get all records
	public static function listuserlogins(Request $request)
	{
		$request = Self::digestInput($request);
        return userloginsservice::listuserlogins($request);
	}

    public function getuserlogins(Request $request)
    {
        $request = Self::digestInput($request);
        return userloginsservice::getuserlogins($request);
    }
}

