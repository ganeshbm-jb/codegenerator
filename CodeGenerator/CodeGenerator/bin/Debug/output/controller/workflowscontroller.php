<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\workflowsservice;


class workflowscontroller  extends ParentController
{
    //
    public function saveworkflows(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,workflowsservice::getworkflows($filter));           
        }        
        $request = Self::digestInput($request);         
        return workflowsservice::saveworkflows($id, $request);
    }

	// Method to get all records
	public static function listworkflows(Request $request)
	{
		$request = Self::digestInput($request);
        return workflowsservice::listworkflows($request);
	}

    public function getworkflows(Request $request)
    {
        $request = Self::digestInput($request);
        return workflowsservice::getworkflows($request);
    }
}

