<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\userrolesservice;


class userrolescontroller  extends ParentController
{
    //
    public function saveuserroles(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,userrolesservice::getuserroles($filter));           
        }        
        $request = Self::digestInput($request);         
        return userrolesservice::saveuserroles($id, $request);
    }

	// Method to get all records
	public static function listuserroles(Request $request)
	{
		$request = Self::digestInput($request);
        return userrolesservice::listuserroles($request);
	}

    public function getuserroles(Request $request)
    {
        $request = Self::digestInput($request);
        return userrolesservice::getuserroles($request);
    }
}

