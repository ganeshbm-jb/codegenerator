<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\workordersservice;


class workorderscontroller  extends ParentController
{
    //
    public function saveworkorders(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,workordersservice::getworkorders($filter));           
        }        
        $request = Self::digestInput($request);         
        return workordersservice::saveworkorders($id, $request);
    }

	// Method to get all records
	public static function listworkorders(Request $request)
	{
		$request = Self::digestInput($request);
        return workordersservice::listworkorders($request);
	}

    public function getworkorders(Request $request)
    {
        $request = Self::digestInput($request);
        return workordersservice::getworkorders($request);
    }
}

