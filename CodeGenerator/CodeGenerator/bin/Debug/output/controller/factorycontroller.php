<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\factoryservice;


class factorycontroller  extends ParentController
{
    //
    public function savefactory(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,factoryservice::getfactory($filter));           
        }        
        $request = Self::digestInput($request);         
        return factoryservice::savefactory($id, $request);
    }

	// Method to get all records
	public static function listfactory(Request $request)
	{
		$request = Self::digestInput($request);
        return factoryservice::listfactory($request);
	}

    public function getfactory(Request $request)
    {
        $request = Self::digestInput($request);
        return factoryservice::getfactory($request);
    }
}

