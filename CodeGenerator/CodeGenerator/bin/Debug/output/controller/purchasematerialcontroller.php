<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\purchasematerialservice;


class purchasematerialcontroller  extends ParentController
{
    //
    public function savepurchasematerial(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,purchasematerialservice::getpurchasematerial($filter));           
        }        
        $request = Self::digestInput($request);         
        return purchasematerialservice::savepurchasematerial($id, $request);
    }

	// Method to get all records
	public static function listpurchasematerial(Request $request)
	{
		$request = Self::digestInput($request);
        return purchasematerialservice::listpurchasematerial($request);
	}

    public function getpurchasematerial(Request $request)
    {
        $request = Self::digestInput($request);
        return purchasematerialservice::getpurchasematerial($request);
    }
}

