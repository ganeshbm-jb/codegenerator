<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\purchasesservice;


class purchasescontroller  extends ParentController
{
    //
    public function savepurchases(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,purchasesservice::getpurchases($filter));           
        }        
        $request = Self::digestInput($request);         
        return purchasesservice::savepurchases($id, $request);
    }

	// Method to get all records
	public static function listpurchases(Request $request)
	{
		$request = Self::digestInput($request);
        return purchasesservice::listpurchases($request);
	}

    public function getpurchases(Request $request)
    {
        $request = Self::digestInput($request);
        return purchasesservice::getpurchases($request);
    }
}

