<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\menusservice;


class menuscontroller  extends ParentController
{
    //
    public function savemenus(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,menusservice::getmenus($filter));           
        }        
        $request = Self::digestInput($request);         
        return menusservice::savemenus($id, $request);
    }

	// Method to get all records
	public static function listmenus(Request $request)
	{
		$request = Self::digestInput($request);
        return menusservice::listmenus($request);
	}

    public function getmenus(Request $request)
    {
        $request = Self::digestInput($request);
        return menusservice::getmenus($request);
    }
}

