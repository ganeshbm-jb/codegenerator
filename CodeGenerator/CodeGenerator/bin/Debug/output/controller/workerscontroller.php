<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\workersservice;


class workerscontroller  extends ParentController
{
    //
    public function saveworkers(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,workersservice::getworkers($filter));           
        }        
        $request = Self::digestInput($request);         
        return workersservice::saveworkers($id, $request);
    }

	// Method to get all records
	public static function listworkers(Request $request)
	{
		$request = Self::digestInput($request);
        return workersservice::listworkers($request);
	}

    public function getworkers(Request $request)
    {
        $request = Self::digestInput($request);
        return workersservice::getworkers($request);
    }
}

