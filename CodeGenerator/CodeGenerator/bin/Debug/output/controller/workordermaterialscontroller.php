<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\workordermaterialsservice;


class workordermaterialscontroller  extends ParentController
{
    //
    public function saveworkordermaterials(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,workordermaterialsservice::getworkordermaterials($filter));           
        }        
        $request = Self::digestInput($request);         
        return workordermaterialsservice::saveworkordermaterials($id, $request);
    }

	// Method to get all records
	public static function listworkordermaterials(Request $request)
	{
		$request = Self::digestInput($request);
        return workordermaterialsservice::listworkordermaterials($request);
	}

    public function getworkordermaterials(Request $request)
    {
        $request = Self::digestInput($request);
        return workordermaterialsservice::getworkordermaterials($request);
    }
}

