<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\codemasterservice;


class codemastercontroller  extends ParentController
{
    //
    public function savecodemaster(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,codemasterservice::getcodemaster($filter));           
        }        
        $request = Self::digestInput($request);         
        return codemasterservice::savecodemaster($id, $request);
    }

	// Method to get all records
	public static function listcodemaster(Request $request)
	{
		$request = Self::digestInput($request);
        return codemasterservice::listcodemaster($request);
	}

    public function getcodemaster(Request $request)
    {
        $request = Self::digestInput($request);
        return codemasterservice::getcodemaster($request);
    }
}

