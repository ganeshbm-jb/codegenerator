<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\rolesservice;


class rolescontroller  extends ParentController
{
    //
    public function saveroles(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,rolesservice::getroles($filter));           
        }        
        $request = Self::digestInput($request);         
        return rolesservice::saveroles($id, $request);
    }

	// Method to get all records
	public static function listroles(Request $request)
	{
		$request = Self::digestInput($request);
        return rolesservice::listroles($request);
	}

    public function getroles(Request $request)
    {
        $request = Self::digestInput($request);
        return rolesservice::getroles($request);
    }
}

