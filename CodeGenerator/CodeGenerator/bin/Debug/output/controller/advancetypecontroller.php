<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\advancetypeservice;


class advancetypecontroller  extends ParentController
{
    //
    public function saveadvancetype(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,advancetypeservice::getadvancetype($filter));           
        }        
        $request = Self::digestInput($request);         
        return advancetypeservice::saveadvancetype($id, $request);
    }

	// Method to get all records
	public static function listadvancetype(Request $request)
	{
		$request = Self::digestInput($request);
        return advancetypeservice::listadvancetype($request);
	}

    public function getadvancetype(Request $request)
    {
        $request = Self::digestInput($request);
        return advancetypeservice::getadvancetype($request);
    }
}

