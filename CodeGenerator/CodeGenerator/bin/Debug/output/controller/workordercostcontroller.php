<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\workordercostservice;


class workordercostcontroller  extends ParentController
{
    //
    public function saveworkordercost(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,workordercostservice::getworkordercost($filter));           
        }        
        $request = Self::digestInput($request);         
        return workordercostservice::saveworkordercost($id, $request);
    }

	// Method to get all records
	public static function listworkordercost(Request $request)
	{
		$request = Self::digestInput($request);
        return workordercostservice::listworkordercost($request);
	}

    public function getworkordercost(Request $request)
    {
        $request = Self::digestInput($request);
        return workordercostservice::getworkordercost($request);
    }
}

