<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\batchservice;


class batchcontroller  extends ParentController
{
    //
    public function savebatch(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,batchservice::getbatch($filter));           
        }        
        $request = Self::digestInput($request);         
        return batchservice::savebatch($id, $request);
    }

	// Method to get all records
	public static function listbatch(Request $request)
	{
		$request = Self::digestInput($request);
        return batchservice::listbatch($request);
	}

    public function getbatch(Request $request)
    {
        $request = Self::digestInput($request);
        return batchservice::getbatch($request);
    }
}

