<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\materialsservice;


class materialscontroller  extends ParentController
{
    //
    public function savematerials(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,materialsservice::getmaterials($filter));           
        }        
        $request = Self::digestInput($request);         
        return materialsservice::savematerials($id, $request);
    }

	// Method to get all records
	public static function listmaterials(Request $request)
	{
		$request = Self::digestInput($request);
        return materialsservice::listmaterials($request);
	}

    public function getmaterials(Request $request)
    {
        $request = Self::digestInput($request);
        return materialsservice::getmaterials($request);
    }
}

