<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\materialrequestservice;


class materialrequestcontroller  extends ParentController
{
    //
    public function savematerialrequest(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,materialrequestservice::getmaterialrequest($filter));           
        }        
        $request = Self::digestInput($request);         
        return materialrequestservice::savematerialrequest($id, $request);
    }

	// Method to get all records
	public static function listmaterialrequest(Request $request)
	{
		$request = Self::digestInput($request);
        return materialrequestservice::listmaterialrequest($request);
	}

    public function getmaterialrequest(Request $request)
    {
        $request = Self::digestInput($request);
        return materialrequestservice::getmaterialrequest($request);
    }
}

