<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\invoiceitemservice;


class invoiceitemcontroller  extends ParentController
{
    //
    public function saveinvoiceitem(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,invoiceitemservice::getinvoiceitem($filter));           
        }        
        $request = Self::digestInput($request);         
        return invoiceitemservice::saveinvoiceitem($id, $request);
    }

	// Method to get all records
	public static function listinvoiceitem(Request $request)
	{
		$request = Self::digestInput($request);
        return invoiceitemservice::listinvoiceitem($request);
	}

    public function getinvoiceitem(Request $request)
    {
        $request = Self::digestInput($request);
        return invoiceitemservice::getinvoiceitem($request);
    }
}

