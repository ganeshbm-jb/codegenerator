<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\cityservice;


class citycontroller  extends ParentController
{
    //
    public function savecity(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,cityservice::getcity($filter));           
        }        
        $request = Self::digestInput($request);         
        return cityservice::savecity($id, $request);
    }

	// Method to get all records
	public static function listcity(Request $request)
	{
		$request = Self::digestInput($request);
        return cityservice::listcity($request);
	}

    public function getcity(Request $request)
    {
        $request = Self::digestInput($request);
        return cityservice::getcity($request);
    }
}

