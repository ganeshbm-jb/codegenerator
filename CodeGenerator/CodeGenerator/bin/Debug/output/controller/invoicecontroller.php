<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\invoiceservice;


class invoicecontroller  extends ParentController
{
    //
    public function saveinvoice(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,invoiceservice::getinvoice($filter));           
        }        
        $request = Self::digestInput($request);         
        return invoiceservice::saveinvoice($id, $request);
    }

	// Method to get all records
	public static function listinvoice(Request $request)
	{
		$request = Self::digestInput($request);
        return invoiceservice::listinvoice($request);
	}

    public function getinvoice(Request $request)
    {
        $request = Self::digestInput($request);
        return invoiceservice::getinvoice($request);
    }
}

