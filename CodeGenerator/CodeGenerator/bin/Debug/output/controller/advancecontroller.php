<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\advanceservice;


class advancecontroller  extends ParentController
{
    //
    public function saveadvance(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,advanceservice::getadvance($filter));           
        }        
        $request = Self::digestInput($request);         
        return advanceservice::saveadvance($id, $request);
    }

	// Method to get all records
	public static function listadvance(Request $request)
	{
		$request = Self::digestInput($request);
        return advanceservice::listadvance($request);
	}

    public function getadvance(Request $request)
    {
        $request = Self::digestInput($request);
        return advanceservice::getadvance($request);
    }
}

