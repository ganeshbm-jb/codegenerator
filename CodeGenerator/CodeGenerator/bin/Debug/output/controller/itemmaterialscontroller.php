<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\itemmaterialsservice;


class itemmaterialscontroller  extends ParentController
{
    //
    public function saveitemmaterials(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,itemmaterialsservice::getitemmaterials($filter));           
        }        
        $request = Self::digestInput($request);         
        return itemmaterialsservice::saveitemmaterials($id, $request);
    }

	// Method to get all records
	public static function listitemmaterials(Request $request)
	{
		$request = Self::digestInput($request);
        return itemmaterialsservice::listitemmaterials($request);
	}

    public function getitemmaterials(Request $request)
    {
        $request = Self::digestInput($request);
        return itemmaterialsservice::getitemmaterials($request);
    }
}

