<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\suppliersservice;


class supplierscontroller  extends ParentController
{
    //
    public function savesuppliers(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,suppliersservice::getsuppliers($filter));           
        }        
        $request = Self::digestInput($request);         
        return suppliersservice::savesuppliers($id, $request);
    }

	// Method to get all records
	public static function listsuppliers(Request $request)
	{
		$request = Self::digestInput($request);
        return suppliersservice::listsuppliers($request);
	}

    public function getsuppliers(Request $request)
    {
        $request = Self::digestInput($request);
        return suppliersservice::getsuppliers($request);
    }
}

