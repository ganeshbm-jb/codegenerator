<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\requesttypeservice;


class requesttypecontroller  extends ParentController
{
    //
    public function saverequesttype(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,requesttypeservice::getrequesttype($filter));           
        }        
        $request = Self::digestInput($request);         
        return requesttypeservice::saverequesttype($id, $request);
    }

	// Method to get all records
	public static function listrequesttype(Request $request)
	{
		$request = Self::digestInput($request);
        return requesttypeservice::listrequesttype($request);
	}

    public function getrequesttype(Request $request)
    {
        $request = Self::digestInput($request);
        return requesttypeservice::getrequesttype($request);
    }
}

