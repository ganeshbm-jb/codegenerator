<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\workorderitemsservice;


class workorderitemscontroller  extends ParentController
{
    //
    public function saveworkorderitems(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,workorderitemsservice::getworkorderitems($filter));           
        }        
        $request = Self::digestInput($request);         
        return workorderitemsservice::saveworkorderitems($id, $request);
    }

	// Method to get all records
	public static function listworkorderitems(Request $request)
	{
		$request = Self::digestInput($request);
        return workorderitemsservice::listworkorderitems($request);
	}

    public function getworkorderitems(Request $request)
    {
        $request = Self::digestInput($request);
        return workorderitemsservice::getworkorderitems($request);
    }
}

