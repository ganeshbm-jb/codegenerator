<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\viewaccessservice;


class viewaccesscontroller  extends ParentController
{
    //
    public function saveviewaccess(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,viewaccessservice::getviewaccess($filter));           
        }        
        $request = Self::digestInput($request);         
        return viewaccessservice::saveviewaccess($id, $request);
    }

	// Method to get all records
	public static function listviewaccess(Request $request)
	{
		$request = Self::digestInput($request);
        return viewaccessservice::listviewaccess($request);
	}

    public function getviewaccess(Request $request)
    {
        $request = Self::digestInput($request);
        return viewaccessservice::getviewaccess($request);
    }
}

