<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\stocklocationqtyservice;


class stocklocationqtycontroller  extends ParentController
{
    //
    public function savestocklocationqty(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,stocklocationqtyservice::getstocklocationqty($filter));           
        }        
        $request = Self::digestInput($request);         
        return stocklocationqtyservice::savestocklocationqty($id, $request);
    }

	// Method to get all records
	public static function liststocklocationqty(Request $request)
	{
		$request = Self::digestInput($request);
        return stocklocationqtyservice::liststocklocationqty($request);
	}

    public function getstocklocationqty(Request $request)
    {
        $request = Self::digestInput($request);
        return stocklocationqtyservice::getstocklocationqty($request);
    }
}

