<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\materialrequestdetailsservice;


class materialrequestdetailscontroller  extends ParentController
{
    //
    public function savematerialrequestdetails(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,materialrequestdetailsservice::getmaterialrequestdetails($filter));           
        }        
        $request = Self::digestInput($request);         
        return materialrequestdetailsservice::savematerialrequestdetails($id, $request);
    }

	// Method to get all records
	public static function listmaterialrequestdetails(Request $request)
	{
		$request = Self::digestInput($request);
        return materialrequestdetailsservice::listmaterialrequestdetails($request);
	}

    public function getmaterialrequestdetails(Request $request)
    {
        $request = Self::digestInput($request);
        return materialrequestdetailsservice::getmaterialrequestdetails($request);
    }
}

