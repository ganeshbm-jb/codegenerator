<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\stockservice;


class stockcontroller  extends ParentController
{
    //
    public function savestock(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,stockservice::getstock($filter));           
        }        
        $request = Self::digestInput($request);         
        return stockservice::savestock($id, $request);
    }

	// Method to get all records
	public static function liststock(Request $request)
	{
		$request = Self::digestInput($request);
        return stockservice::liststock($request);
	}

    public function getstock(Request $request)
    {
        $request = Self::digestInput($request);
        return stockservice::getstock($request);
    }
}

