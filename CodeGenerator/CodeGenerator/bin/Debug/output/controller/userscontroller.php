<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\usersservice;


class userscontroller  extends ParentController
{
    //
    public function saveusers(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,usersservice::getusers($filter));           
        }        
        $request = Self::digestInput($request);         
        return usersservice::saveusers($id, $request);
    }

	// Method to get all records
	public static function listusers(Request $request)
	{
		$request = Self::digestInput($request);
        return usersservice::listusers($request);
	}

    public function getusers(Request $request)
    {
        $request = Self::digestInput($request);
        return usersservice::getusers($request);
    }
}

