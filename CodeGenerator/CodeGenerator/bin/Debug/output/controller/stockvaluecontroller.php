<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\stockvalueservice;


class stockvaluecontroller  extends ParentController
{
    //
    public function savestockvalue(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,stockvalueservice::getstockvalue($filter));           
        }        
        $request = Self::digestInput($request);         
        return stockvalueservice::savestockvalue($id, $request);
    }

	// Method to get all records
	public static function liststockvalue(Request $request)
	{
		$request = Self::digestInput($request);
        return stockvalueservice::liststockvalue($request);
	}

    public function getstockvalue(Request $request)
    {
        $request = Self::digestInput($request);
        return stockvalueservice::getstockvalue($request);
    }
}

