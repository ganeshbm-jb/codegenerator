<?php

namespace App\Services;

use App\Models\stocklocationqty;
use App\ServicesEx\stocklocationqtyserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class stocklocationqtyservice extends BaseService
{
	// Method to save data
	public static function savestocklocationqty($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = stocklocationqtyserviceex::savestocklocationqty_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = stocklocationqty::savestocklocationqty($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			stocklocationqtyserviceex::savestocklocationqty_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function liststocklocationqty($request)
	{
		// Pre Operation
		$request = stocklocationqtyserviceex::liststocklocationqty_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = stocklocationqty::liststocklocationqty($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		stocklocationqtyserviceex::liststocklocationqty_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getstocklocationqty($request)
	{
		// Pre Operation
		$request = stocklocationqtyserviceex::getstocklocationqty_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = stocklocationqty::getstocklocationqty($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		stocklocationqtyserviceex::getstocklocationqty_after($request);

		return $returnValue;		
	}
}
