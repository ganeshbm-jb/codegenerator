<?php

namespace App\Services;

use App\Models\codemaster;
use App\ServicesEx\codemasterserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class codemasterservice extends BaseService
{
	// Method to save data
	public static function savecodemaster($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = codemasterserviceex::savecodemaster_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = codemaster::savecodemaster($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			codemasterserviceex::savecodemaster_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listcodemaster($request)
	{
		// Pre Operation
		$request = codemasterserviceex::listcodemaster_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = codemaster::listcodemaster($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		codemasterserviceex::listcodemaster_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getcodemaster($request)
	{
		// Pre Operation
		$request = codemasterserviceex::getcodemaster_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = codemaster::getcodemaster($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		codemasterserviceex::getcodemaster_after($request);

		return $returnValue;		
	}
}
