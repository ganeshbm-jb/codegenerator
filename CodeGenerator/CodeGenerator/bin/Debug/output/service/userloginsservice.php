<?php

namespace App\Services;

use App\Models\userlogins;
use App\ServicesEx\userloginsserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class userloginsservice extends BaseService
{
	// Method to save data
	public static function saveuserlogins($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = userloginsserviceex::saveuserlogins_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = userlogins::saveuserlogins($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			userloginsserviceex::saveuserlogins_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listuserlogins($request)
	{
		// Pre Operation
		$request = userloginsserviceex::listuserlogins_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = userlogins::listuserlogins($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		userloginsserviceex::listuserlogins_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getuserlogins($request)
	{
		// Pre Operation
		$request = userloginsserviceex::getuserlogins_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = userlogins::getuserlogins($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		userloginsserviceex::getuserlogins_after($request);

		return $returnValue;		
	}
}
