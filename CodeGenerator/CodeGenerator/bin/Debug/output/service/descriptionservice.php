<?php

namespace App\Services;

use App\Models\description;
use App\ServicesEx\descriptionserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class descriptionservice extends BaseService
{
	// Method to save data
	public static function savedescription($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = descriptionserviceex::savedescription_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = description::savedescription($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			descriptionserviceex::savedescription_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listdescription($request)
	{
		// Pre Operation
		$request = descriptionserviceex::listdescription_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = description::listdescription($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		descriptionserviceex::listdescription_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getdescription($request)
	{
		// Pre Operation
		$request = descriptionserviceex::getdescription_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = description::getdescription($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		descriptionserviceex::getdescription_after($request);

		return $returnValue;		
	}
}
