<?php

namespace App\Services;

use App\Models\materials;
use App\ServicesEx\materialsserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class materialsservice extends BaseService
{
	// Method to save data
	public static function savematerials($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = materialsserviceex::savematerials_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = materials::savematerials($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			materialsserviceex::savematerials_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listmaterials($request)
	{
		// Pre Operation
		$request = materialsserviceex::listmaterials_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materials::listmaterials($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		materialsserviceex::listmaterials_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterials($request)
	{
		// Pre Operation
		$request = materialsserviceex::getmaterials_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materials::getmaterials($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		materialsserviceex::getmaterials_after($request);

		return $returnValue;		
	}
}
