<?php

namespace App\Services;

use App\Models\userroles;
use App\ServicesEx\userrolesserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class userrolesservice extends BaseService
{
	// Method to save data
	public static function saveuserroles($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = userrolesserviceex::saveuserroles_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = userroles::saveuserroles($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			userrolesserviceex::saveuserroles_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listuserroles($request)
	{
		// Pre Operation
		$request = userrolesserviceex::listuserroles_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = userroles::listuserroles($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		userrolesserviceex::listuserroles_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getuserroles($request)
	{
		// Pre Operation
		$request = userrolesserviceex::getuserroles_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = userroles::getuserroles($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		userrolesserviceex::getuserroles_after($request);

		return $returnValue;		
	}
}
