<?php

namespace App\Services;

use App\Models\workordermaterials;
use App\ServicesEx\workordermaterialsserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class workordermaterialsservice extends BaseService
{
	// Method to save data
	public static function saveworkordermaterials($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = workordermaterialsserviceex::saveworkordermaterials_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = workordermaterials::saveworkordermaterials($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			workordermaterialsserviceex::saveworkordermaterials_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listworkordermaterials($request)
	{
		// Pre Operation
		$request = workordermaterialsserviceex::listworkordermaterials_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workordermaterials::listworkordermaterials($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		workordermaterialsserviceex::listworkordermaterials_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkordermaterials($request)
	{
		// Pre Operation
		$request = workordermaterialsserviceex::getworkordermaterials_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workordermaterials::getworkordermaterials($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		workordermaterialsserviceex::getworkordermaterials_after($request);

		return $returnValue;		
	}
}
