<?php

namespace App\Services;

use App\Models\workflows;
use App\ServicesEx\workflowsserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class workflowsservice extends BaseService
{
	// Method to save data
	public static function saveworkflows($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = workflowsserviceex::saveworkflows_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = workflows::saveworkflows($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			workflowsserviceex::saveworkflows_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listworkflows($request)
	{
		// Pre Operation
		$request = workflowsserviceex::listworkflows_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workflows::listworkflows($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		workflowsserviceex::listworkflows_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkflows($request)
	{
		// Pre Operation
		$request = workflowsserviceex::getworkflows_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workflows::getworkflows($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		workflowsserviceex::getworkflows_after($request);

		return $returnValue;		
	}
}
