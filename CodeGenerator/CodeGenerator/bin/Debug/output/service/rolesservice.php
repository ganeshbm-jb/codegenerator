<?php

namespace App\Services;

use App\Models\roles;
use App\ServicesEx\rolesserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class rolesservice extends BaseService
{
	// Method to save data
	public static function saveroles($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = rolesserviceex::saveroles_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = roles::saveroles($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			rolesserviceex::saveroles_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listroles($request)
	{
		// Pre Operation
		$request = rolesserviceex::listroles_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = roles::listroles($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		rolesserviceex::listroles_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getroles($request)
	{
		// Pre Operation
		$request = rolesserviceex::getroles_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = roles::getroles($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		rolesserviceex::getroles_after($request);

		return $returnValue;		
	}
}
