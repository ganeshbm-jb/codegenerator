<?php

namespace App\Services;

use App\Models\purchaseorder;
use App\ServicesEx\purchaseorderserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class purchaseorderservice extends BaseService
{
	// Method to save data
	public static function savepurchaseorder($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = purchaseorderserviceex::savepurchaseorder_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = purchaseorder::savepurchaseorder($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			purchaseorderserviceex::savepurchaseorder_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listpurchaseorder($request)
	{
		// Pre Operation
		$request = purchaseorderserviceex::listpurchaseorder_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = purchaseorder::listpurchaseorder($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		purchaseorderserviceex::listpurchaseorder_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getpurchaseorder($request)
	{
		// Pre Operation
		$request = purchaseorderserviceex::getpurchaseorder_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = purchaseorder::getpurchaseorder($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		purchaseorderserviceex::getpurchaseorder_after($request);

		return $returnValue;		
	}
}
