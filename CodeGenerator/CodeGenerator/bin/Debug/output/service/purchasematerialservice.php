<?php

namespace App\Services;

use App\Models\purchasematerial;
use App\ServicesEx\purchasematerialserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class purchasematerialservice extends BaseService
{
	// Method to save data
	public static function savepurchasematerial($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = purchasematerialserviceex::savepurchasematerial_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = purchasematerial::savepurchasematerial($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			purchasematerialserviceex::savepurchasematerial_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listpurchasematerial($request)
	{
		// Pre Operation
		$request = purchasematerialserviceex::listpurchasematerial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = purchasematerial::listpurchasematerial($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		purchasematerialserviceex::listpurchasematerial_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getpurchasematerial($request)
	{
		// Pre Operation
		$request = purchasematerialserviceex::getpurchasematerial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = purchasematerial::getpurchasematerial($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		purchasematerialserviceex::getpurchasematerial_after($request);

		return $returnValue;		
	}
}
