<?php

namespace App\Services;

use App\Models\factory;
use App\ServicesEx\factoryserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class factoryservice extends BaseService
{
	// Method to save data
	public static function savefactory($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = factoryserviceex::savefactory_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = factory::savefactory($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			factoryserviceex::savefactory_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listfactory($request)
	{
		// Pre Operation
		$request = factoryserviceex::listfactory_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = factory::listfactory($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		factoryserviceex::listfactory_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getfactory($request)
	{
		// Pre Operation
		$request = factoryserviceex::getfactory_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = factory::getfactory($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		factoryserviceex::getfactory_after($request);

		return $returnValue;		
	}
}
