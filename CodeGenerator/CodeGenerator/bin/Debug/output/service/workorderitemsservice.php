<?php

namespace App\Services;

use App\Models\workorderitems;
use App\ServicesEx\workorderitemsserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class workorderitemsservice extends BaseService
{
	// Method to save data
	public static function saveworkorderitems($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = workorderitemsserviceex::saveworkorderitems_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = workorderitems::saveworkorderitems($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			workorderitemsserviceex::saveworkorderitems_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listworkorderitems($request)
	{
		// Pre Operation
		$request = workorderitemsserviceex::listworkorderitems_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workorderitems::listworkorderitems($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		workorderitemsserviceex::listworkorderitems_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkorderitems($request)
	{
		// Pre Operation
		$request = workorderitemsserviceex::getworkorderitems_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workorderitems::getworkorderitems($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		workorderitemsserviceex::getworkorderitems_after($request);

		return $returnValue;		
	}
}
