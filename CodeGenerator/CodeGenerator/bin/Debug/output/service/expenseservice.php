<?php

namespace App\Services;

use App\Models\expense;
use App\ServicesEx\expenseserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class expenseservice extends BaseService
{
	// Method to save data
	public static function saveexpense($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = expenseserviceex::saveexpense_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = expense::saveexpense($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			expenseserviceex::saveexpense_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listexpense($request)
	{
		// Pre Operation
		$request = expenseserviceex::listexpense_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = expense::listexpense($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		expenseserviceex::listexpense_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getexpense($request)
	{
		// Pre Operation
		$request = expenseserviceex::getexpense_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = expense::getexpense($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		expenseserviceex::getexpense_after($request);

		return $returnValue;		
	}
}
