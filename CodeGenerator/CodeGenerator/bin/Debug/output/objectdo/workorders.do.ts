import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class workordersDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "workorders";
    }
	static localStorage : string = "ls_workorders";
    // columns
    
projectid : string ='';
workorderdate : string ='';
completiondate : string ='';
remarks : string ='';
code : string ='';
status : string ='';
}

export class workordersFormGroup{

	constructor() { }
	formItems : string[] = ['id','projectid','workorderdate','completiondate','remarks','code','status',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
projectid : [''],
workorderdate : [''],
completiondate : [''],
remarks : [''],
code : [''],
status : [''],
		});
	}
}

