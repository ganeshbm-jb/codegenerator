import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class suppliersDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "suppliers";
    }
	static localStorage : string = "ls_suppliers";
    // columns
    
name : string ='';
address1 : string ='';
address2 : string ='';
city : string ='';
state : string ='';
pincode : string ='';
gstno : string ='';
sgst : string ='';
cgst : string ='';
igst : string ='';
phoneno : string ='';
contactperson : string ='';
mat1 : string ='';
mat2 : string ='';
mat3 : string ='';
mat4 : string ='';
mat5 : string ='';
}

export class suppliersFormGroup{

	constructor() { }
	formItems : string[] = ['id','name','address1','address2','city','state','pincode','gstno','sgst','cgst','igst','phoneno','contactperson','mat1','mat2','mat3','mat4','mat5',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
name : [''],
address1 : [''],
address2 : [''],
city : [''],
state : [''],
pincode : [''],
gstno : [''],
sgst : [''],
cgst : [''],
igst : [''],
phoneno : [''],
contactperson : [''],
mat1 : [''],
mat2 : [''],
mat3 : [''],
mat4 : [''],
mat5 : [''],
		});
	}
}

