import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class userloginsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "userlogins";
    }
	static localStorage : string = "ls_userlogins";
    // columns
    
userid : string ='';
username : string ='';
active : string ='';
source : string ='';
roles : string ='';
token : string ='';
lasttouchtime : string ='';
ipaddr : string ='';
}

export class userloginsFormGroup{

	constructor() { }
	formItems : string[] = ['id','userid','username','active','source','roles','token','lasttouchtime','ipaddr',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
userid : [''],
username : [''],
active : [''],
source : [''],
roles : [''],
token : [''],
lasttouchtime : [''],
ipaddr : [''],
		});
	}
}

