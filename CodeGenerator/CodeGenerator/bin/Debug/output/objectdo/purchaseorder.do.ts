import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class purchaseorderDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "purchaseorder";
    }
	static localStorage : string = "ls_purchaseorder";
    // columns
    
purchaseorderdate : string ='';
supplierid : string ='';
code : string ='';
billno : string ='';
billamt : string ='';
sgst : string ='';
cgst : string ='';
igst : string ='';
sgstamt : string ='';
cgstamt : string ='';
igstamt : string ='';
gstapplicable : string ='';
remarks : string ='';
status : string ='';
}

export class purchaseorderFormGroup{

	constructor() { }
	formItems : string[] = ['id','purchaseorderdate','supplierid','code','billno','billamt','sgst','cgst','igst','sgstamt','cgstamt','igstamt','gstapplicable','remarks','status',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
purchaseorderdate : [''],
supplierid : [''],
code : [''],
billno : [''],
billamt : [''],
sgst : [''],
cgst : [''],
igst : [''],
sgstamt : [''],
cgstamt : [''],
igstamt : [''],
gstapplicable : [''],
remarks : [''],
status : [''],
		});
	}
}

