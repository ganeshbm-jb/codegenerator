import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class purchasematerialDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "purchasematerial";
    }
	static localStorage : string = "ls_purchasematerial";
    // columns
    
purchaseid : string ='';
materialid : string ='';
qty : string ='';
unitprice : string ='';
price : string ='';
itemunit : string ='';
}

export class purchasematerialFormGroup{

	constructor() { }
	formItems : string[] = ['id','purchaseid','materialid','qty','unitprice','price','itemunit',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
purchaseid : [''],
materialid : [''],
qty : [''],
unitprice : [''],
price : [''],
itemunit : [''],
		});
	}
}

