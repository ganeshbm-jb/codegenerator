import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class materialrequestdetailsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "materialrequestdetails";
    }
	static localStorage : string = "ls_materialrequestdetails";
    // columns
    
materialid : string ='';
qty : string ='';
stockqty : string ='';
stockqtydate : string ='';
status : string ='';
materialrequestid : string ='';
}

export class materialrequestdetailsFormGroup{

	constructor() { }
	formItems : string[] = ['id','materialid','qty','stockqty','stockqtydate','status','materialrequestid',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
materialid : [''],
qty : [''],
stockqty : [''],
stockqtydate : [''],
status : [''],
materialrequestid : [''],
		});
	}
}

