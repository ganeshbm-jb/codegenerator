import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class projectsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "projects";
    }
	static localStorage : string = "ls_projects";
    // columns
    
name : string ='';
address1 : string ='';
address2 : string ='';
city : string ='';
pincode : string ='';
gstno : string ='';
phone : string ='';
contactperson : string ='';
}

export class projectsFormGroup{

	constructor() { }
	formItems : string[] = ['id','name','address1','address2','city','pincode','gstno','phone','contactperson',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
name : [''],
address1 : [''],
address2 : [''],
city : [''],
pincode : [''],
gstno : [''],
phone : [''],
contactperson : [''],
		});
	}
}

