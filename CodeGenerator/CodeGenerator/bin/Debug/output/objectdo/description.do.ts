import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class descriptionDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "description";
    }
	static localStorage : string = "ls_description";
    // columns
    
description : string ='';
atypeid : string ='';
amount : string ='';
status : string ='';
}

export class descriptionFormGroup{

	constructor() { }
	formItems : string[] = ['id','description','atypeid','amount','status',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
description : [''],
atypeid : [''],
amount : [''],
status : [''],
		});
	}
}

