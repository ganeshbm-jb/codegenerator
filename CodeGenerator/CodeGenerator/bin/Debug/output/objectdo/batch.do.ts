import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class batchDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "batch";
    }
	static localStorage : string = "ls_batch";
    // columns
    
batchname : string ='';
fdate : string ='';
tdate : string ='';
status : string ='';
cola : string ='';
colb : string ='';
colc : string ='';
coltotal : string ='';
batchno : string ='';
}

export class batchFormGroup{

	constructor() { }
	formItems : string[] = ['id','batchname','fdate','tdate','status','cola','colb','colc','coltotal','batchno',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
batchname : [''],
fdate : [''],
tdate : [''],
status : [''],
cola : [''],
colb : [''],
colc : [''],
coltotal : [''],
batchno : [''],
		});
	}
}

