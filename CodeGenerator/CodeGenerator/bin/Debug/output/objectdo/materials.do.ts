import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class materialsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "materials";
    }
	static localStorage : string = "ls_materials";
    // columns
    
code : string ='';
name : string ='';
unit : string ='';
hsncode : string ='';
sgst : string ='';
cgst : string ='';
}

export class materialsFormGroup{

	constructor() { }
	formItems : string[] = ['id','code','name','unit','hsncode','sgst','cgst',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
code : [''],
name : [''],
unit : [''],
hsncode : [''],
sgst : [''],
cgst : [''],
		});
	}
}

