import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class advanceDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "advance";
    }
	static localStorage : string = "ls_advance";
    // columns
    
description : string ='';
atypeid : string ='';
amount : string ='';
status : string ='';
advancedate : string ='';
}

export class advanceFormGroup{

	constructor() { }
	formItems : string[] = ['id','description','atypeid','amount','status','advancedate',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
description : [''],
atypeid : [''],
amount : [''],
status : [''],
advancedate : [''],
		});
	}
}

