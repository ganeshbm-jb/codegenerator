import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class requesttypeDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "requesttype";
    }
	static localStorage : string = "ls_requesttype";
    // columns
    
name : string ='';
}

export class requesttypeFormGroup{

	constructor() { }
	formItems : string[] = ['id','name',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
name : [''],
		});
	}
}

