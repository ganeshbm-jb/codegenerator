import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class menusDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "menus";
    }
	static localStorage : string = "ls_menus";
    // columns
    
code : string ='';
name : string ='';
menutype : string ='';
parent : string ='';
menuicon : string ='';
menuurl : string ='';
active : string ='';
morder : string ='';
role1 : string ='';
role2 : string ='';
role3 : string ='';
role4 : string ='';
role5 : string ='';
role6 : string ='';
}

export class menusFormGroup{

	constructor() { }
	formItems : string[] = ['id','code','name','menutype','parent','menuicon','menuurl','active','morder','role1','role2','role3','role4','role5','role6',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
code : [''],
name : [''],
menutype : [''],
parent : [''],
menuicon : [''],
menuurl : [''],
active : [''],
morder : [''],
role1 : [''],
role2 : [''],
role3 : [''],
role4 : [''],
role5 : [''],
role6 : [''],
		});
	}
}

