import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class codemasterDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "codemaster";
    }
	static localStorage : string = "ls_codemaster";
    // columns
    
module : string ='';
code : string ='';
part1 : string ='';
format : string ='';
count : string ='';
active : string ='';
}

export class codemasterFormGroup{

	constructor() { }
	formItems : string[] = ['id','module','code','part1','format','count','active',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
module : [''],
code : [''],
part1 : [''],
format : [''],
count : [''],
active : [''],
		});
	}
}

