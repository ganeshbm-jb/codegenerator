import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class workordermaterialsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "workordermaterials";
    }
	static localStorage : string = "ls_workordermaterials";
    // columns
    
workorderid : string ='';
workorderitemid : string ='';
materialid : string ='';
qty : string ='';
unitprice : string ='';
price : string ='';
unittype : string ='';
}

export class workordermaterialsFormGroup{

	constructor() { }
	formItems : string[] = ['id','workorderid','workorderitemid','materialid','qty','unitprice','price','unittype',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
workorderid : [''],
workorderitemid : [''],
materialid : [''],
qty : [''],
unitprice : [''],
price : [''],
unittype : [''],
		});
	}
}

