import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class invoiceothersDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "invoiceothers";
    }
	static localStorage : string = "ls_invoiceothers";
    // columns
    
invoiceid : string ='';
description : string ='';
price : string ='';
}

export class invoiceothersFormGroup{

	constructor() { }
	formItems : string[] = ['id','invoiceid','description','price',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
invoiceid : [''],
description : [''],
price : [''],
		});
	}
}

