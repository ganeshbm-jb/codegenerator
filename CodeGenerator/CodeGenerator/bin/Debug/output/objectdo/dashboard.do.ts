import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class dashboardDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "dashboard";
    }
	static localStorage : string = "ls_dashboard";
    // columns
    
batch_draft : string ='';
batch_rejected : string ='';
batch_pendingverification : string ='';
batch_pendingapproval : string ='';
batch_approved : string ='';
wo_draft : string ='';
wo_rejected : string ='';
wo_pendingverification : string ='';
wo_pendingapproval : string ='';
wo_approved : string ='';
invoice_draft : string ='';
invoice_rejected : string ='';
invoice_pendingverification : string ='';
invoice_pendingapproval : string ='';
invoice_approved : string ='';
expenses_draft : string ='';
expenses_rejected : string ='';
expenses_pendingverification : string ='';
expenses_pendingapproval : string ='';
expenses_approved : string ='';
advance_draft : string ='';
advance_rejected : string ='';
advance_pendingverification : string ='';
advance_pendingapproval : string ='';
advance_approved : string ='';
ledger_openingbalance : string ='';
}

export class dashboardFormGroup{

	constructor() { }
	formItems : string[] = ['id','batch_draft','batch_rejected','batch_pendingverification','batch_pendingapproval','batch_approved','wo_draft','wo_rejected','wo_pendingverification','wo_pendingapproval','wo_approved','invoice_draft','invoice_rejected','invoice_pendingverification','invoice_pendingapproval','invoice_approved','expenses_draft','expenses_rejected','expenses_pendingverification','expenses_pendingapproval','expenses_approved','advance_draft','advance_rejected','advance_pendingverification','advance_pendingapproval','advance_approved','ledger_openingbalance',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
batch_draft : [''],
batch_rejected : [''],
batch_pendingverification : [''],
batch_pendingapproval : [''],
batch_approved : [''],
wo_draft : [''],
wo_rejected : [''],
wo_pendingverification : [''],
wo_pendingapproval : [''],
wo_approved : [''],
invoice_draft : [''],
invoice_rejected : [''],
invoice_pendingverification : [''],
invoice_pendingapproval : [''],
invoice_approved : [''],
expenses_draft : [''],
expenses_rejected : [''],
expenses_pendingverification : [''],
expenses_pendingapproval : [''],
expenses_approved : [''],
advance_draft : [''],
advance_rejected : [''],
advance_pendingverification : [''],
advance_pendingapproval : [''],
advance_approved : [''],
ledger_openingbalance : [''],
		});
	}
}

