import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class workorderitemsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "workorderitems";
    }
	static localStorage : string = "ls_workorderitems";
    // columns
    
workorderid : string ='';
itemid : string ='';
qty : string ='';
unitprice : string ='';
price : string ='';
unittype : string ='';
}

export class workorderitemsFormGroup{

	constructor() { }
	formItems : string[] = ['id','workorderid','itemid','qty','unitprice','price','unittype',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
workorderid : [''],
itemid : [''],
qty : [''],
unitprice : [''],
price : [''],
unittype : [''],
		});
	}
}

