import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class stockvalueDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "stockvalue";
    }
	static localStorage : string = "ls_stockvalue";
    // columns
    
materialid : string ='';
stockdate : string ='';
avgprice : string ='';
}

export class stockvalueFormGroup{

	constructor() { }
	formItems : string[] = ['id','materialid','stockdate','avgprice',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
materialid : [''],
stockdate : [''],
avgprice : [''],
		});
	}
}

