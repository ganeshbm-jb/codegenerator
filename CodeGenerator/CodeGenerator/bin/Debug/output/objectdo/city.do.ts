import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class cityDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "city";
    }
	static localStorage : string = "ls_city";
    // columns
    
name : string ='';
}

export class cityFormGroup{

	constructor() { }
	formItems : string[] = ['id','name',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
name : [''],
		});
	}
}

