import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class rolesDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "roles";
    }
	static localStorage : string = "ls_roles";
    // columns
    
code : string ='';
name : string ='';
}

export class rolesFormGroup{

	constructor() { }
	formItems : string[] = ['id','code','name',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
code : [''],
name : [''],
		});
	}
}

