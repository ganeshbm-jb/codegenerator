import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class workflowsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "workflows";
    }
	static localStorage : string = "ls_workflows";
    // columns
    
code : string ='';
rcode : string ='';
source : string ='';
status : string ='';
pstatus : string ='';
nstatus : string ='';
active : string ='';
eof : string ='';
bof : string ='';
wforder : string ='';
}

export class workflowsFormGroup{

	constructor() { }
	formItems : string[] = ['id','code','rcode','source','status','pstatus','nstatus','active','eof','bof','wforder',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
code : [''],
rcode : [''],
source : [''],
status : [''],
pstatus : [''],
nstatus : [''],
active : [''],
eof : [''],
bof : [''],
wforder : [''],
		});
	}
}

