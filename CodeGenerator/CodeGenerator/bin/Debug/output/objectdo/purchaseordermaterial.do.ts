import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class purchaseordermaterialDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "purchaseordermaterial";
    }
	static localStorage : string = "ls_purchaseordermaterial";
    // columns
    
purchaseorderid : string ='';
materialid : string ='';
qty : string ='';
unitprice : string ='';
price : string ='';
itemunit : string ='';
}

export class purchaseordermaterialFormGroup{

	constructor() { }
	formItems : string[] = ['id','purchaseorderid','materialid','qty','unitprice','price','itemunit',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
purchaseorderid : [''],
materialid : [''],
qty : [''],
unitprice : [''],
price : [''],
itemunit : [''],
		});
	}
}

