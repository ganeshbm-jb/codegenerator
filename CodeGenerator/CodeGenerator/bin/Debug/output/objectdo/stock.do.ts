import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class stockDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "stock";
    }
	static localStorage : string = "ls_stock";
    // columns
    
parentid : string ='';
purchaseid : string ='';
materialid : string ='';
stockdate : string ='';
qty : string ='';
stocktype : string ='';
factoryid : string ='';
status : string ='';
unitprice : string ='';
}

export class stockFormGroup{

	constructor() { }
	formItems : string[] = ['id','parentid','purchaseid','materialid','stockdate','qty','stocktype','factoryid','status','unitprice',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
parentid : [''],
purchaseid : [''],
materialid : [''],
stockdate : [''],
qty : [''],
stocktype : [''],
factoryid : [''],
status : [''],
unitprice : [''],
		});
	}
}

