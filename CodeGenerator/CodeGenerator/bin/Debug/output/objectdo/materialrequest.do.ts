import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class materialrequestDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "materialrequest";
    }
	static localStorage : string = "ls_materialrequest";
    // columns
    
requestno : string ='';
requestdate : string ='';
duedate : string ='';
projectid : string ='';
factoryid : string ='';
requesttype : string ='';
status : string ='';
}

export class materialrequestFormGroup{

	constructor() { }
	formItems : string[] = ['id','requestno','requestdate','duedate','projectid','factoryid','requesttype','status',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
requestno : [''],
requestdate : [''],
duedate : [''],
projectid : [''],
factoryid : [''],
requesttype : [''],
status : [''],
		});
	}
}

