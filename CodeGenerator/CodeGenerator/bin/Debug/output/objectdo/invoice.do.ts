import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class invoiceDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "invoice";
    }
	static localStorage : string = "ls_invoice";
    // columns
    
projectid : string ='';
workorderid : string ='';
invoicedate : string ='';
duedate : string ='';
sgst : string ='';
sgstamt : string ='';
cgst : string ='';
cgstamt : string ='';
gsttotal : string ='';
itemtotal : string ='';
otherstotal : string ='';
total : string ='';
status : string ='';
}

export class invoiceFormGroup{

	constructor() { }
	formItems : string[] = ['id','projectid','workorderid','invoicedate','duedate','sgst','sgstamt','cgst','cgstamt','gsttotal','itemtotal','otherstotal','total','status',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
projectid : [''],
workorderid : [''],
invoicedate : [''],
duedate : [''],
sgst : [''],
sgstamt : [''],
cgst : [''],
cgstamt : [''],
gsttotal : [''],
itemtotal : [''],
otherstotal : [''],
total : [''],
status : [''],
		});
	}
}

