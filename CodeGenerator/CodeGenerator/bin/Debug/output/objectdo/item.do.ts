import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class itemDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "item";
    }
	static localStorage : string = "ls_item";
    // columns
    
itemname : string ='';
}

export class itemFormGroup{

	constructor() { }
	formItems : string[] = ['id','itemname',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
itemname : [''],
		});
	}
}

