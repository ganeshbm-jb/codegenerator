import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class usersDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "users";
    }
	static localStorage : string = "ls_users";
    // columns
    
userid : string ='';
password : string ='';
active : string ='';
firstname : string ='';
lastname : string ='';
mobile : string ='';
email : string ='';
role : string ='';
}

export class usersFormGroup{

	constructor() { }
	formItems : string[] = ['id','userid','password','active','firstname','lastname','mobile','email','role',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
userid : [''],
password : [''],
active : [''],
firstname : [''],
lastname : [''],
mobile : [''],
email : [''],
role : [''],
		});
	}
}

