import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class stocklocationqtyDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "stocklocationqty";
    }
	static localStorage : string = "ls_stocklocationqty";
    // columns
    
parentid : string ='';
materialid : string ='';
factoryid : string ='';
inqty : string ='';
outqty : string ='';
totalqty : string ='';
lowstock : string ='';
}

export class stocklocationqtyFormGroup{

	constructor() { }
	formItems : string[] = ['id','parentid','materialid','factoryid','inqty','outqty','totalqty','lowstock',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
parentid : [''],
materialid : [''],
factoryid : [''],
inqty : [''],
outqty : [''],
totalqty : [''],
lowstock : [''],
		});
	}
}

