import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class ledgerDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "ledger";
    }
	static localStorage : string = "ls_ledger";
    // columns
    
srcid : string ='';
src : string ='';
tdate : string ='';
cramt : string ='';
dramt : string ='';
batchid : string ='';
batchstatus : string ='';
srctype : string ='';
status : string ='';
description : string ='';
}

export class ledgerFormGroup{

	constructor() { }
	formItems : string[] = ['id','srcid','src','tdate','cramt','dramt','batchid','batchstatus','srctype','status','description',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
srcid : [''],
src : [''],
tdate : [''],
cramt : [''],
dramt : [''],
batchid : [''],
batchstatus : [''],
srctype : [''],
status : [''],
description : [''],
		});
	}
}

