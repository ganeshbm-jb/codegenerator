import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class expenseDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "expense";
    }
	static localStorage : string = "ls_expense";
    // columns
    
edate : string ='';
description : string ='';
amount : string ='';
pid : string ='';
source : string ='';
status : string ='';
pstatus : string ='';
nstatus : string ='';
active : string ='';
typeid : string ='';
}

export class expenseFormGroup{

	constructor() { }
	formItems : string[] = ['id','edate','description','amount','pid','source','status','pstatus','nstatus','active','typeid',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
edate : [''],
description : [''],
amount : [''],
pid : [''],
source : [''],
status : [''],
pstatus : [''],
nstatus : [''],
active : [''],
typeid : [''],
		});
	}
}

