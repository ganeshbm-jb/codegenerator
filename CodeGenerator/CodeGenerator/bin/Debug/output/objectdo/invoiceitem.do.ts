import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class invoiceitemDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "invoiceitem";
    }
	static localStorage : string = "ls_invoiceitem";
    // columns
    
invoiceid : string ='';
itemid : string ='';
qty : string ='';
unitprice : string ='';
price : string ='';
}

export class invoiceitemFormGroup{

	constructor() { }
	formItems : string[] = ['id','invoiceid','itemid','qty','unitprice','price',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
invoiceid : [''],
itemid : [''],
qty : [''],
unitprice : [''],
price : [''],
		});
	}
}

