import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class stockqtyDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "stockqty";
    }
	static localStorage : string = "ls_stockqty";
    // columns
    
parentid : string ='';
materialid : string ='';
opqty : string ='';
inqty : string ='';
outqty : string ='';
prqty : string ='';
frqty : string ='';
damageqty : string ='';
totalqty : string ='';
reorderqty : string ='';
reorder : string ='';
}

export class stockqtyFormGroup{

	constructor() { }
	formItems : string[] = ['id','parentid','materialid','opqty','inqty','outqty','prqty','frqty','damageqty','totalqty','reorderqty','reorder',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
parentid : [''],
materialid : [''],
opqty : [''],
inqty : [''],
outqty : [''],
prqty : [''],
frqty : [''],
damageqty : [''],
totalqty : [''],
reorderqty : [''],
reorder : [''],
		});
	}
}

