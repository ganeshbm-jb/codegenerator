import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class viewaccessDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "viewaccess";
    }
	static localStorage : string = "ls_viewaccess";
    // columns
    
code : string ='';
access : string ='';
role : string ='';
status : string ='';
}

export class viewaccessFormGroup{

	constructor() { }
	formItems : string[] = ['id','code','access','role','status',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
code : [''],
access : [''],
role : [''],
status : [''],
		});
	}
}

