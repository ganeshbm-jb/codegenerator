import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class purchasesDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "purchases";
    }
	static localStorage : string = "ls_purchases";
    // columns
    
purchasedate : string ='';
supplierid : string ='';
code : string ='';
billno : string ='';
billamt : string ='';
sgst : string ='';
cgst : string ='';
igst : string ='';
sgstamt : string ='';
cgstamt : string ='';
igstamt : string ='';
gstapplicable : string ='';
remarks : string ='';
status : string ='';
}

export class purchasesFormGroup{

	constructor() { }
	formItems : string[] = ['id','purchasedate','supplierid','code','billno','billamt','sgst','cgst','igst','sgstamt','cgstamt','igstamt','gstapplicable','remarks','status',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
purchasedate : [''],
supplierid : [''],
code : [''],
billno : [''],
billamt : [''],
sgst : [''],
cgst : [''],
igst : [''],
sgstamt : [''],
cgstamt : [''],
igstamt : [''],
gstapplicable : [''],
remarks : [''],
status : [''],
		});
	}
}

