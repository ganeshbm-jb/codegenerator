import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class workersDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "workers";
    }
	static localStorage : string = "ls_workers";
    // columns
    
name : string ='';
phoneno : string ='';
salary : string ='';
worktype : string ='';
salarytype : string ='';
}

export class workersFormGroup{

	constructor() { }
	formItems : string[] = ['id','name','phoneno','salary','worktype','salarytype',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
name : [''],
phoneno : [''],
salary : [''],
worktype : [''],
salarytype : [''],
		});
	}
}

