import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class expensetypeDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "expensetype";
    }
	static localStorage : string = "ls_expensetype";
    // columns
    
name : string ='';
}

export class expensetypeFormGroup{

	constructor() { }
	formItems : string[] = ['id','name',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			
name : [''],
		});
	}
}

