CREATE TABLE `dashboard` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `dashboard` add column `batch_draft` int  default 0;
alter table `dashboard` add column `batch_rejected` int  default 0;
alter table `dashboard` add column `batch_pendingverification` int  default 0;
alter table `dashboard` add column `batch_pendingapproval` int  default 0;
alter table `dashboard` add column `batch_approved` int  default 0;
alter table `dashboard` add column `wo_draft` int  default 0;
alter table `dashboard` add column `wo_rejected` int  default 0;
alter table `dashboard` add column `wo_pendingverification` int  default 0;
alter table `dashboard` add column `wo_pendingapproval` int  default 0;
alter table `dashboard` add column `wo_approved` int  default 0;
alter table `dashboard` add column `invoice_draft` int  default 0;
alter table `dashboard` add column `invoice_rejected` int  default 0;
alter table `dashboard` add column `invoice_pendingverification` int  default 0;
alter table `dashboard` add column `invoice_pendingapproval` int  default 0;
alter table `dashboard` add column `invoice_approved` int  default 0;
alter table `dashboard` add column `expenses_draft` int  default 0;
alter table `dashboard` add column `expenses_rejected` int  default 0;
alter table `dashboard` add column `expenses_pendingverification` int  default 0;
alter table `dashboard` add column `expenses_pendingapproval` int  default 0;
alter table `dashboard` add column `expenses_approved` int  default 0;
alter table `dashboard` add column `advance_draft` int  default 0;
alter table `dashboard` add column `advance_rejected` int  default 0;
alter table `dashboard` add column `advance_pendingverification` int  default 0;
alter table `dashboard` add column `advance_pendingapproval` int  default 0;
alter table `dashboard` add column `advance_approved` int  default 0;
alter table `dashboard` add column `ledger_openingbalance` int  default 0;

