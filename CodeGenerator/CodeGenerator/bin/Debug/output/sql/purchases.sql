CREATE TABLE `purchases` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `purchases` add column `purchasedate` date  ;
alter table `purchases` add column `supplierid` varchar(255)  default "";
alter table `purchases` add column `code` varchar(255) unique not null default "";
alter table `purchases` add column `billno` varchar(255)  default "";
alter table `purchases` add column `billamt` decimal(18,2)  default 0;
alter table `purchases` add column `sgst` decimal(18,2)  default 0;
alter table `purchases` add column `cgst` decimal(18,2)  default 0;
alter table `purchases` add column `igst` decimal(18,2)  default 0;
alter table `purchases` add column `sgstamt` decimal(18,2)  default 0;
alter table `purchases` add column `cgstamt` decimal(18,2)  default 0;
alter table `purchases` add column `igstamt` decimal(18,2)  default 0;
alter table `purchases` add column `gstapplicable` tinyint  default 1;
alter table `purchases` add column `remarks` varchar(255)  default "";
alter table `purchases` add column `status` varchar(255)  default "";

