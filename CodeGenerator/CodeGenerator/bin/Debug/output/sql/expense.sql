CREATE TABLE `expense` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `expense` add column `edate` date not null ;
alter table `expense` add column `description` varchar(255) not null ;
alter table `expense` add column `amount` decimal(18,2)  default 0;
alter table `expense` add column `pid` varchar(255)  default "";
alter table `expense` add column `source` varchar(255)  default "";
alter table `expense` add column `status` varchar(255) not null default "";
alter table `expense` add column `pstatus` varchar(255)  default "";
alter table `expense` add column `nstatus` varchar(255)  default "";
alter table `expense` add column `active` tinyint  default 1;
alter table `expense` add column `typeid` varchar(255) not null ;

