CREATE TABLE `codemaster` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `codemaster` add column `module` varchar(255)  ;
alter table `codemaster` add column `code` varchar(255) not null default "";
alter table `codemaster` add column `part1` varchar(255) not null default "";
alter table `codemaster` add column `format` varchar(255) not null default "";
alter table `codemaster` add column `count` smallint  default 0;
alter table `codemaster` add column `active` tinyint  default 1;

