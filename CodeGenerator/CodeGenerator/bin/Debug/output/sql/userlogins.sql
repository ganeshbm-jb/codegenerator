CREATE TABLE `userlogins` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `userlogins` add column `userid` varchar(255) not null ;
alter table `userlogins` add column `username` varchar(255) not null ;
alter table `userlogins` add column `active` tinyint  default 0;
alter table `userlogins` add column `source` varchar(255)  default "";
alter table `userlogins` add column `roles` varchar(255)  default "";
alter table `userlogins` add column `token` varchar(255) unique not null ;
alter table `userlogins` add column `lasttouchtime` timestamp null default null;
alter table `userlogins` add column `ipaddr` varchar(255)  default "";

