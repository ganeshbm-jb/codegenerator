CREATE TABLE `materialrequest` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `materialrequest` add column `requestno` varchar(255) unique not null ;
alter table `materialrequest` add column `requestdate` date not null ;
alter table `materialrequest` add column `duedate` date  ;
alter table `materialrequest` add column `projectid` varchar(255)  default "";
alter table `materialrequest` add column `factoryid` varchar(255)  default "";
alter table `materialrequest` add column `requesttype` varchar(255)  default "";
alter table `materialrequest` add column `status` varchar(255) not null ;

