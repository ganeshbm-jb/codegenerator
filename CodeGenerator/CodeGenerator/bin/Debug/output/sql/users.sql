CREATE TABLE `users` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `users` add column `userid` varchar(255) unique not null ;
alter table `users` add column `password` varchar(255) not null ;
alter table `users` add column `active` tinyint  default 0;
alter table `users` add column `firstname` varchar(255) not null default "";
alter table `users` add column `lastname` varchar(255)  default "";
alter table `users` add column `mobile` varchar(255)  default "";
alter table `users` add column `email` varchar(255)  default "";
alter table `users` add column `role` varchar(255)  default "";

