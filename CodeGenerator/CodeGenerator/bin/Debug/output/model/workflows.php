<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class workflows extends Model
{
	protected $table = 'workflows';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveworkflows($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = workflows::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = workflows::where('id', $id)->first();
			if (empty($record)) {
				$record = new workflows;
			}

			$record->id = $input['id'];
			if(isset($input['code']))
			$record->code = $input['code'];
		if(isset($input['rcode']))
			$record->rcode = $input['rcode'];
		if(isset($input['source']))
			$record->source = $input['source'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['pstatus']))
			$record->pstatus = $input['pstatus'];
		if(isset($input['nstatus']))
			$record->nstatus = $input['nstatus'];
		if(isset($input['active']))
			$record->active = $input['active'];
		if(isset($input['eof']))
			$record->eof = $input['eof'];
		if(isset($input['bof']))
			$record->bof = $input['bof'];
		if(isset($input['wforder']))
			$record->wforder = $input['wforder'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getworkflows('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listworkflows($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workflows::all();
		else
		{
			$result = workflows::select('workflows.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getworkflows($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workflows::where($col, $value)->get();
		else
		{
			$result = workflows::select('workflows.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
