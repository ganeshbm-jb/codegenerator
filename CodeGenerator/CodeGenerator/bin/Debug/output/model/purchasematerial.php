<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class purchasematerial extends Model
{
	protected $table = 'purchasematerial';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savepurchasematerial($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = purchasematerial::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = purchasematerial::where('id', $id)->first();
			if (empty($record)) {
				$record = new purchasematerial;
			}

			$record->id = $input['id'];
			if(isset($input['purchaseid']))
			$record->purchaseid = $input['purchaseid'];
		if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		if(isset($input['qty']))
			$record->qty = $input['qty'];
		if(isset($input['unitprice']))
			$record->unitprice = $input['unitprice'];
		if(isset($input['price']))
			$record->price = $input['price'];
		if(isset($input['itemunit']))
			$record->itemunit = $input['itemunit'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getpurchasematerial('purchasematerial.id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listpurchasematerial($request)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = purchasematerial::all();
		else
		{
			$result = purchasematerial::select("purchasematerial.*","materials.name as materialname")->join("materials","materials.id","=","purchasematerial.materialid")->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getpurchasematerial($col, $value)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = purchasematerial::where($col, $value)->get();
		else
		{
			$result = purchasematerial::select("purchasematerial.*","materials.name as materialname")->join("materials","materials.id","=","purchasematerial.materialid")->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
