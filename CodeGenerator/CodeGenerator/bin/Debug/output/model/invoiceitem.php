<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class invoiceitem extends Model
{
	protected $table = 'invoiceitem';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveinvoiceitem($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = invoiceitem::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = invoiceitem::where('id', $id)->first();
			if (empty($record)) {
				$record = new invoiceitem;
			}

			$record->id = $input['id'];
			if(isset($input['invoiceid']))
			$record->invoiceid = $input['invoiceid'];
		if(isset($input['itemid']))
			$record->itemid = $input['itemid'];
		if(isset($input['qty']))
			$record->qty = $input['qty'];
		if(isset($input['unitprice']))
			$record->unitprice = $input['unitprice'];
		if(isset($input['price']))
			$record->price = $input['price'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getinvoiceitem('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listinvoiceitem($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = invoiceitem::all();
		else
		{
			$result = invoiceitem::select('invoiceitem.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getinvoiceitem($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = invoiceitem::where($col, $value)->get();
		else
		{
			$result = invoiceitem::select('invoiceitem.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
