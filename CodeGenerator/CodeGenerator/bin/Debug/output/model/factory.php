<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class factory extends Model
{
	protected $table = 'factory';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savefactory($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = factory::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = factory::where('id', $id)->first();
			if (empty($record)) {
				$record = new factory;
			}

			$record->id = $input['id'];
			if(isset($input['name']))
			$record->name = $input['name'];
		if(isset($input['address1']))
			$record->address1 = $input['address1'];
		if(isset($input['address2']))
			$record->address2 = $input['address2'];
		if(isset($input['city']))
			$record->city = $input['city'];
		if(isset($input['pincode']))
			$record->pincode = $input['pincode'];
		if(isset($input['gstno']))
			$record->gstno = $input['gstno'];
		if(isset($input['phone']))
			$record->phone = $input['phone'];
		if(isset($input['contactperson']))
			$record->contactperson = $input['contactperson'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getfactory('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listfactory($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = factory::all();
		else
		{
			$result = factory::select('factory.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getfactory($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = factory::where($col, $value)->get();
		else
		{
			$result = factory::select('factory.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
