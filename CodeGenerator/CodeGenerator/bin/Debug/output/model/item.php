<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class item extends Model
{
	protected $table = 'item';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveitem($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = item::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = item::where('id', $id)->first();
			if (empty($record)) {
				$record = new item;
			}

			$record->id = $input['id'];
			if(isset($input['itemname']))
			$record->itemname = $input['itemname'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getitem('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listitem($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = item::all();
		else
		{
			$result = item::select('item.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getitem($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = item::where($col, $value)->get();
		else
		{
			$result = item::select('item.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
