<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class workordermaterials extends Model
{
	protected $table = 'workordermaterials';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveworkordermaterials($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = workordermaterials::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = workordermaterials::where('id', $id)->first();
			if (empty($record)) {
				$record = new workordermaterials;
			}

			$record->id = $input['id'];
			if(isset($input['workorderid']))
			$record->workorderid = $input['workorderid'];
		if(isset($input['workorderitemid']))
			$record->workorderitemid = $input['workorderitemid'];
		if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		if(isset($input['qty']))
			$record->qty = $input['qty'];
		if(isset($input['unitprice']))
			$record->unitprice = $input['unitprice'];
		if(isset($input['price']))
			$record->price = $input['price'];
		if(isset($input['unittype']))
			$record->unittype = $input['unittype'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getworkordermaterials('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listworkordermaterials($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workordermaterials::all();
		else
		{
			$result = workordermaterials::select('workordermaterials.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getworkordermaterials($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workordermaterials::where($col, $value)->get();
		else
		{
			$result = workordermaterials::select('workordermaterials.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
