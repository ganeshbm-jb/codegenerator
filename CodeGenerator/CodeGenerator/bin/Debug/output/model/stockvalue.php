<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class stockvalue extends Model
{
	protected $table = 'stockvalue';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savestockvalue($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = stockvalue::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = stockvalue::where('id', $id)->first();
			if (empty($record)) {
				$record = new stockvalue;
			}

			$record->id = $input['id'];
			if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		if(isset($input['stockdate']))
			$record->stockdate = $input['stockdate'];
		if(isset($input['avgprice']))
			$record->avgprice = $input['avgprice'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getstockvalue('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function liststockvalue($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = stockvalue::all();
		else
		{
			$result = stockvalue::select('stockvalue.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getstockvalue($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = stockvalue::where($col, $value)->get();
		else
		{
			$result = stockvalue::select('stockvalue.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
