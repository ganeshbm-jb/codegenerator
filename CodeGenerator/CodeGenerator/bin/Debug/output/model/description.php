<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class description extends Model
{
	protected $table = 'description';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savedescription($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = description::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = description::where('id', $id)->first();
			if (empty($record)) {
				$record = new description;
			}

			$record->id = $input['id'];
			if(isset($input['description']))
			$record->description = $input['description'];
		if(isset($input['atypeid']))
			$record->atypeid = $input['atypeid'];
		if(isset($input['amount']))
			$record->amount = $input['amount'];
		if(isset($input['status']))
			$record->status = $input['status'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getdescription('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listdescription($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = description::all();
		else
		{
			$result = description::select('description.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getdescription($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = description::where($col, $value)->get();
		else
		{
			$result = description::select('description.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
