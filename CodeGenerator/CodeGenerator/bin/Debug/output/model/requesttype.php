<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class requesttype extends Model
{
	protected $table = 'requesttype';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saverequesttype($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = requesttype::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = requesttype::where('id', $id)->first();
			if (empty($record)) {
				$record = new requesttype;
			}

			$record->id = $input['id'];
			if(isset($input['name']))
			$record->name = $input['name'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getrequesttype('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listrequesttype($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = requesttype::all();
		else
		{
			$result = requesttype::select('requesttype.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getrequesttype($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = requesttype::where($col, $value)->get();
		else
		{
			$result = requesttype::select('requesttype.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
