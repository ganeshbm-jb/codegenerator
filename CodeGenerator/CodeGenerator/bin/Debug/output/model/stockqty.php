<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class stockqty extends Model
{
	protected $table = 'stockqty';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savestockqty($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = stockqty::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = stockqty::where('id', $id)->first();
			if (empty($record)) {
				$record = new stockqty;
			}

			$record->id = $input['id'];
			if(isset($input['parentid']))
			$record->parentid = $input['parentid'];
		if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		if(isset($input['opqty']))
			$record->opqty = $input['opqty'];
		if(isset($input['inqty']))
			$record->inqty = $input['inqty'];
		if(isset($input['outqty']))
			$record->outqty = $input['outqty'];
		if(isset($input['prqty']))
			$record->prqty = $input['prqty'];
		if(isset($input['frqty']))
			$record->frqty = $input['frqty'];
		if(isset($input['damageqty']))
			$record->damageqty = $input['damageqty'];
		if(isset($input['totalqty']))
			$record->totalqty = $input['totalqty'];
		if(isset($input['reorderqty']))
			$record->reorderqty = $input['reorderqty'];
		if(isset($input['reorder']))
			$record->reorder = $input['reorder'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getstockqty('stockqty.id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function liststockqty($request)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = stockqty::all();
		else
		{
			$result = stockqty::select("stockqty.*", "materials.name", "materials.unit", "materials.id as materialid")->join("materials", "materials.id", "=", "stockqty.materialid")->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getstockqty($col, $value)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = stockqty::where($col, $value)->get();
		else
		{
			$result = stockqty::select("stockqty.*", "materials.name", "materials.unit", "materials.id as materialid")->join("materials", "materials.id", "=", "stockqty.materialid")->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
