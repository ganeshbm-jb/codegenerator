<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class purchaseorder extends Model
{
	protected $table = 'purchaseorder';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savepurchaseorder($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = purchaseorder::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = purchaseorder::where('id', $id)->first();
			if (empty($record)) {
				$record = new purchaseorder;
			}

			$record->id = $input['id'];
			if(isset($input['purchaseorderdate']))
			$record->purchaseorderdate = $input['purchaseorderdate'];
		if(isset($input['supplierid']))
			$record->supplierid = $input['supplierid'];
		if(isset($input['code']))
			$record->code = $input['code'];
		if(isset($input['billno']))
			$record->billno = $input['billno'];
		if(isset($input['billamt']))
			$record->billamt = $input['billamt'];
		if(isset($input['sgst']))
			$record->sgst = $input['sgst'];
		if(isset($input['cgst']))
			$record->cgst = $input['cgst'];
		if(isset($input['igst']))
			$record->igst = $input['igst'];
		if(isset($input['sgstamt']))
			$record->sgstamt = $input['sgstamt'];
		if(isset($input['cgstamt']))
			$record->cgstamt = $input['cgstamt'];
		if(isset($input['igstamt']))
			$record->igstamt = $input['igstamt'];
		if(isset($input['gstapplicable']))
			$record->gstapplicable = $input['gstapplicable'];
		if(isset($input['remarks']))
			$record->remarks = $input['remarks'];
		if(isset($input['status']))
			$record->status = $input['status'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getpurchaseorder('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listpurchaseorder($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = purchaseorder::all();
		else
		{
			$result = purchaseorder::select('purchaseorder.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getpurchaseorder($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = purchaseorder::where($col, $value)->get();
		else
		{
			$result = purchaseorder::select('purchaseorder.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
