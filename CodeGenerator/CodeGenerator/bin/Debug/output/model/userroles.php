<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class userroles extends Model
{
	protected $table = 'userroles';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveuserroles($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = userroles::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = userroles::where('id', $id)->first();
			if (empty($record)) {
				$record = new userroles;
			}

			$record->id = $input['id'];
			if(isset($input['userid']))
			$record->userid = $input['userid'];
		if(isset($input['roleid']))
			$record->roleid = $input['roleid'];
		if(isset($input['active']))
			$record->active = $input['active'];
		if(isset($input['prole']))
			$record->prole = $input['prole'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getuserroles('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listuserroles($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = userroles::all();
		else
		{
			$result = userroles::select('userroles.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getuserroles($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = userroles::where($col, $value)->get();
		else
		{
			$result = userroles::select('userroles.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
