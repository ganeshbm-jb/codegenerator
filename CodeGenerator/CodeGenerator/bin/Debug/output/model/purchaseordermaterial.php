<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class purchaseordermaterial extends Model
{
	protected $table = 'purchaseordermaterial';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savepurchaseordermaterial($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = purchaseordermaterial::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = purchaseordermaterial::where('id', $id)->first();
			if (empty($record)) {
				$record = new purchaseordermaterial;
			}

			$record->id = $input['id'];
			if(isset($input['purchaseorderid']))
			$record->purchaseorderid = $input['purchaseorderid'];
		if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		if(isset($input['qty']))
			$record->qty = $input['qty'];
		if(isset($input['unitprice']))
			$record->unitprice = $input['unitprice'];
		if(isset($input['price']))
			$record->price = $input['price'];
		if(isset($input['itemunit']))
			$record->itemunit = $input['itemunit'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getpurchaseordermaterial('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listpurchaseordermaterial($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = purchaseordermaterial::all();
		else
		{
			$result = purchaseordermaterial::select('purchaseordermaterial.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getpurchaseordermaterial($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = purchaseordermaterial::where($col, $value)->get();
		else
		{
			$result = purchaseordermaterial::select('purchaseordermaterial.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
