<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class batch extends Model
{
	protected $table = 'batch';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savebatch($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = batch::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = batch::where('id', $id)->first();
			if (empty($record)) {
				$record = new batch;
			}

			$record->id = $input['id'];
			if(isset($input['batchname']))
			$record->batchname = $input['batchname'];
		if(isset($input['fdate']))
			$record->fdate = $input['fdate'];
		if(isset($input['tdate']))
			$record->tdate = $input['tdate'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['cola']))
			$record->cola = $input['cola'];
		if(isset($input['colb']))
			$record->colb = $input['colb'];
		if(isset($input['colc']))
			$record->colc = $input['colc'];
		if(isset($input['coltotal']))
			$record->coltotal = $input['coltotal'];
		if(isset($input['batchno']))
			$record->batchno = $input['batchno'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getbatch('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listbatch($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = batch::all();
		else
		{
			$result = batch::select('batch.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getbatch($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = batch::where($col, $value)->get();
		else
		{
			$result = batch::select('batch.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
