<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class dashboard extends Model
{
	protected $table = 'dashboard';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savedashboard($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = dashboard::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = dashboard::where('id', $id)->first();
			if (empty($record)) {
				$record = new dashboard;
			}

			$record->id = $input['id'];
			if(isset($input['batch_draft']))
			$record->batch_draft = $input['batch_draft'];
		if(isset($input['batch_rejected']))
			$record->batch_rejected = $input['batch_rejected'];
		if(isset($input['batch_pendingverification']))
			$record->batch_pendingverification = $input['batch_pendingverification'];
		if(isset($input['batch_pendingapproval']))
			$record->batch_pendingapproval = $input['batch_pendingapproval'];
		if(isset($input['batch_approved']))
			$record->batch_approved = $input['batch_approved'];
		if(isset($input['wo_draft']))
			$record->wo_draft = $input['wo_draft'];
		if(isset($input['wo_rejected']))
			$record->wo_rejected = $input['wo_rejected'];
		if(isset($input['wo_pendingverification']))
			$record->wo_pendingverification = $input['wo_pendingverification'];
		if(isset($input['wo_pendingapproval']))
			$record->wo_pendingapproval = $input['wo_pendingapproval'];
		if(isset($input['wo_approved']))
			$record->wo_approved = $input['wo_approved'];
		if(isset($input['invoice_draft']))
			$record->invoice_draft = $input['invoice_draft'];
		if(isset($input['invoice_rejected']))
			$record->invoice_rejected = $input['invoice_rejected'];
		if(isset($input['invoice_pendingverification']))
			$record->invoice_pendingverification = $input['invoice_pendingverification'];
		if(isset($input['invoice_pendingapproval']))
			$record->invoice_pendingapproval = $input['invoice_pendingapproval'];
		if(isset($input['invoice_approved']))
			$record->invoice_approved = $input['invoice_approved'];
		if(isset($input['expenses_draft']))
			$record->expenses_draft = $input['expenses_draft'];
		if(isset($input['expenses_rejected']))
			$record->expenses_rejected = $input['expenses_rejected'];
		if(isset($input['expenses_pendingverification']))
			$record->expenses_pendingverification = $input['expenses_pendingverification'];
		if(isset($input['expenses_pendingapproval']))
			$record->expenses_pendingapproval = $input['expenses_pendingapproval'];
		if(isset($input['expenses_approved']))
			$record->expenses_approved = $input['expenses_approved'];
		if(isset($input['advance_draft']))
			$record->advance_draft = $input['advance_draft'];
		if(isset($input['advance_rejected']))
			$record->advance_rejected = $input['advance_rejected'];
		if(isset($input['advance_pendingverification']))
			$record->advance_pendingverification = $input['advance_pendingverification'];
		if(isset($input['advance_pendingapproval']))
			$record->advance_pendingapproval = $input['advance_pendingapproval'];
		if(isset($input['advance_approved']))
			$record->advance_approved = $input['advance_approved'];
		if(isset($input['ledger_openingbalance']))
			$record->ledger_openingbalance = $input['ledger_openingbalance'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getdashboard('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listdashboard($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = dashboard::all();
		else
		{
			$result = dashboard::select('dashboard.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getdashboard($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = dashboard::where($col, $value)->get();
		else
		{
			$result = dashboard::select('dashboard.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
