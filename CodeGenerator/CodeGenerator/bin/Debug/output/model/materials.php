<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class materials extends Model
{
	protected $table = 'materials';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savematerials($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = materials::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = materials::where('id', $id)->first();
			if (empty($record)) {
				$record = new materials;
			}

			$record->id = $input['id'];
			if(isset($input['code']))
			$record->code = $input['code'];
		if(isset($input['name']))
			$record->name = $input['name'];
		if(isset($input['unit']))
			$record->unit = $input['unit'];
		if(isset($input['hsncode']))
			$record->hsncode = $input['hsncode'];
		if(isset($input['sgst']))
			$record->sgst = $input['sgst'];
		if(isset($input['cgst']))
			$record->cgst = $input['cgst'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getmaterials('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listmaterials($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = materials::all();
		else
		{
			$result = materials::select('materials.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getmaterials($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = materials::where($col, $value)->get();
		else
		{
			$result = materials::select('materials.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
