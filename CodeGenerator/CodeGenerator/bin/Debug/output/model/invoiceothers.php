<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class invoiceothers extends Model
{
	protected $table = 'invoiceothers';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveinvoiceothers($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = invoiceothers::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = invoiceothers::where('id', $id)->first();
			if (empty($record)) {
				$record = new invoiceothers;
			}

			$record->id = $input['id'];
			if(isset($input['invoiceid']))
			$record->invoiceid = $input['invoiceid'];
		if(isset($input['description']))
			$record->description = $input['description'];
		if(isset($input['price']))
			$record->price = $input['price'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getinvoiceothers('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listinvoiceothers($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = invoiceothers::all();
		else
		{
			$result = invoiceothers::select('invoiceothers.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getinvoiceothers($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = invoiceothers::where($col, $value)->get();
		else
		{
			$result = invoiceothers::select('invoiceothers.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
