<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class workorderitems extends Model
{
	protected $table = 'workorderitems';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveworkorderitems($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = workorderitems::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = workorderitems::where('id', $id)->first();
			if (empty($record)) {
				$record = new workorderitems;
			}

			$record->id = $input['id'];
			if(isset($input['workorderid']))
			$record->workorderid = $input['workorderid'];
		if(isset($input['itemid']))
			$record->itemid = $input['itemid'];
		if(isset($input['qty']))
			$record->qty = $input['qty'];
		if(isset($input['unitprice']))
			$record->unitprice = $input['unitprice'];
		if(isset($input['price']))
			$record->price = $input['price'];
		if(isset($input['unittype']))
			$record->unittype = $input['unittype'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getworkorderitems('workorderitems.id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listworkorderitems($request)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = workorderitems::all();
		else
		{
			$result = workorderitems::select("workorderitems.*", "item.itemname")->join("item", "item.id", "=", "workorderitems.itemid")->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getworkorderitems($col, $value)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = workorderitems::where($col, $value)->get();
		else
		{
			$result = workorderitems::select("workorderitems.*", "item.itemname")->join("item", "item.id", "=", "workorderitems.itemid")->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
