<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class users extends Model
{
	protected $table = 'users';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveusers($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = users::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = users::where('id', $id)->first();
			if (empty($record)) {
				$record = new users;
			}

			$record->id = $input['id'];
			if(isset($input['userid']))
			$record->userid = $input['userid'];
		if(isset($input['password']))
			$record->password = $input['password'];
		if(isset($input['active']))
			$record->active = $input['active'];
		if(isset($input['firstname']))
			$record->firstname = $input['firstname'];
		if(isset($input['lastname']))
			$record->lastname = $input['lastname'];
		if(isset($input['mobile']))
			$record->mobile = $input['mobile'];
		if(isset($input['email']))
			$record->email = $input['email'];
		if(isset($input['role']))
			$record->role = $input['role'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getusers('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listusers($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = users::all();
		else
		{
			$result = users::select('users.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getusers($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = users::where($col, $value)->get();
		else
		{
			$result = users::select('users.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
