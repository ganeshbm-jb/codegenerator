<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class viewaccess extends Model
{
	protected $table = 'viewaccess';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveviewaccess($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = viewaccess::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = viewaccess::where('id', $id)->first();
			if (empty($record)) {
				$record = new viewaccess;
			}

			$record->id = $input['id'];
			if(isset($input['code']))
			$record->code = $input['code'];
		if(isset($input['access']))
			$record->access = $input['access'];
		if(isset($input['role']))
			$record->role = $input['role'];
		if(isset($input['status']))
			$record->status = $input['status'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getviewaccess('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listviewaccess($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = viewaccess::all();
		else
		{
			$result = viewaccess::select('viewaccess.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getviewaccess($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = viewaccess::where($col, $value)->get();
		else
		{
			$result = viewaccess::select('viewaccess.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
