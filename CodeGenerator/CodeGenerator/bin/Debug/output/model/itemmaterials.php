<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class itemmaterials extends Model
{
	protected $table = 'itemmaterials';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveitemmaterials($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = itemmaterials::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = itemmaterials::where('id', $id)->first();
			if (empty($record)) {
				$record = new itemmaterials;
			}

			$record->id = $input['id'];
			if(isset($input['itemid']))
			$record->itemid = $input['itemid'];
		if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getitemmaterials('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listitemmaterials($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = itemmaterials::all();
		else
		{
			$result = itemmaterials::select('itemmaterials.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getitemmaterials($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = itemmaterials::where($col, $value)->get();
		else
		{
			$result = itemmaterials::select('itemmaterials.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
