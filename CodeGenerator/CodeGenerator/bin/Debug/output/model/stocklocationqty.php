<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class stocklocationqty extends Model
{
	protected $table = 'stocklocationqty';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savestocklocationqty($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = stocklocationqty::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = stocklocationqty::where('id', $id)->first();
			if (empty($record)) {
				$record = new stocklocationqty;
			}

			$record->id = $input['id'];
			if(isset($input['parentid']))
			$record->parentid = $input['parentid'];
		if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		if(isset($input['factoryid']))
			$record->factoryid = $input['factoryid'];
		if(isset($input['inqty']))
			$record->inqty = $input['inqty'];
		if(isset($input['outqty']))
			$record->outqty = $input['outqty'];
		if(isset($input['totalqty']))
			$record->totalqty = $input['totalqty'];
		if(isset($input['lowstock']))
			$record->lowstock = $input['lowstock'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getstocklocationqty('stocklocationqty.id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function liststocklocationqty($request)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = stocklocationqty::all();
		else
		{
			$result = stocklocationqty::select("stocklocationqty.*", "materials.name", "materials.unit", "materials.id as materialid", "factory.id as factoryid", "factory.name as factoryname")->join("materials", "materials.id", "=", "stocklocationqty.materialid")->join("factory", "factory.id", "=", "stocklocationqty.factoryid")->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getstocklocationqty($col, $value)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = stocklocationqty::where($col, $value)->get();
		else
		{
			$result = stocklocationqty::select("stocklocationqty.*", "materials.name", "materials.unit", "materials.id as materialid", "factory.id as factoryid", "factory.name as factoryname")->join("materials", "materials.id", "=", "stocklocationqty.materialid")->join("factory", "factory.id", "=", "stocklocationqty.factoryid")->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
