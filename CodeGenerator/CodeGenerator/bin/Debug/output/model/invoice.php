<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class invoice extends Model
{
	protected $table = 'invoice';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveinvoice($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = invoice::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = invoice::where('id', $id)->first();
			if (empty($record)) {
				$record = new invoice;
			}

			$record->id = $input['id'];
			if(isset($input['projectid']))
			$record->projectid = $input['projectid'];
		if(isset($input['workorderid']))
			$record->workorderid = $input['workorderid'];
		if(isset($input['invoicedate']))
			$record->invoicedate = $input['invoicedate'];
		if(isset($input['duedate']))
			$record->duedate = $input['duedate'];
		if(isset($input['sgst']))
			$record->sgst = $input['sgst'];
		if(isset($input['sgstamt']))
			$record->sgstamt = $input['sgstamt'];
		if(isset($input['cgst']))
			$record->cgst = $input['cgst'];
		if(isset($input['cgstamt']))
			$record->cgstamt = $input['cgstamt'];
		if(isset($input['gsttotal']))
			$record->gsttotal = $input['gsttotal'];
		if(isset($input['itemtotal']))
			$record->itemtotal = $input['itemtotal'];
		if(isset($input['otherstotal']))
			$record->otherstotal = $input['otherstotal'];
		if(isset($input['total']))
			$record->total = $input['total'];
		if(isset($input['status']))
			$record->status = $input['status'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getinvoice('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listinvoice($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = invoice::all();
		else
		{
			$result = invoice::select('invoice.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getinvoice($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = invoice::where($col, $value)->get();
		else
		{
			$result = invoice::select('invoice.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
