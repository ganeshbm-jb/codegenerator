<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class advancetype extends Model
{
	protected $table = 'advancetype';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveadvancetype($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = advancetype::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = advancetype::where('id', $id)->first();
			if (empty($record)) {
				$record = new advancetype;
			}

			$record->id = $input['id'];
			if(isset($input['name']))
			$record->name = $input['name'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getadvancetype('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listadvancetype($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = advancetype::all();
		else
		{
			$result = advancetype::select('advancetype.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getadvancetype($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = advancetype::where($col, $value)->get();
		else
		{
			$result = advancetype::select('advancetype.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
