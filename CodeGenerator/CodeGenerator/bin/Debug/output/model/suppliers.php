<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class suppliers extends Model
{
	protected $table = 'suppliers';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savesuppliers($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = suppliers::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = suppliers::where('id', $id)->first();
			if (empty($record)) {
				$record = new suppliers;
			}

			$record->id = $input['id'];
			if(isset($input['name']))
			$record->name = $input['name'];
		if(isset($input['address1']))
			$record->address1 = $input['address1'];
		if(isset($input['address2']))
			$record->address2 = $input['address2'];
		if(isset($input['city']))
			$record->city = $input['city'];
		if(isset($input['state']))
			$record->state = $input['state'];
		if(isset($input['pincode']))
			$record->pincode = $input['pincode'];
		if(isset($input['gstno']))
			$record->gstno = $input['gstno'];
		if(isset($input['sgst']))
			$record->sgst = $input['sgst'];
		if(isset($input['cgst']))
			$record->cgst = $input['cgst'];
		if(isset($input['igst']))
			$record->igst = $input['igst'];
		if(isset($input['phoneno']))
			$record->phoneno = $input['phoneno'];
		if(isset($input['contactperson']))
			$record->contactperson = $input['contactperson'];
		if(isset($input['mat1']))
			$record->mat1 = $input['mat1'];
		if(isset($input['mat2']))
			$record->mat2 = $input['mat2'];
		if(isset($input['mat3']))
			$record->mat3 = $input['mat3'];
		if(isset($input['mat4']))
			$record->mat4 = $input['mat4'];
		if(isset($input['mat5']))
			$record->mat5 = $input['mat5'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getsuppliers('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listsuppliers($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = suppliers::all();
		else
		{
			$result = suppliers::select('suppliers.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getsuppliers($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = suppliers::where($col, $value)->get();
		else
		{
			$result = suppliers::select('suppliers.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
