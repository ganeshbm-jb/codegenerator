<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class advance extends Model
{
	protected $table = 'advance';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveadvance($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = advance::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = advance::where('id', $id)->first();
			if (empty($record)) {
				$record = new advance;
			}

			$record->id = $input['id'];
			if(isset($input['description']))
			$record->description = $input['description'];
		if(isset($input['atypeid']))
			$record->atypeid = $input['atypeid'];
		if(isset($input['amount']))
			$record->amount = $input['amount'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['advancedate']))
			$record->advancedate = $input['advancedate'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getadvance('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listadvance($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = advance::all();
		else
		{
			$result = advance::select('advance.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getadvance($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = advance::where($col, $value)->get();
		else
		{
			$result = advance::select('advance.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
