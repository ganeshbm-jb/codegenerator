<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class materialrequest extends Model
{
	protected $table = 'materialrequest';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savematerialrequest($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = materialrequest::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = materialrequest::where('id', $id)->first();
			if (empty($record)) {
				$record = new materialrequest;
			}

			$record->id = $input['id'];
			if(isset($input['requestno']))
			$record->requestno = $input['requestno'];
		if(isset($input['requestdate']))
			$record->requestdate = $input['requestdate'];
		if(isset($input['duedate']))
			$record->duedate = $input['duedate'];
		if(isset($input['projectid']))
			$record->projectid = $input['projectid'];
		if(isset($input['factoryid']))
			$record->factoryid = $input['factoryid'];
		if(isset($input['requesttype']))
			$record->requesttype = $input['requesttype'];
		if(isset($input['status']))
			$record->status = $input['status'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getmaterialrequest('materialrequest.id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listmaterialrequest($request)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = materialrequest::all();
		else
		{
			$result = materialrequest::select("materialrequest.*","projects.name as projectname", "factory.name as factoryname","requesttype.name as requesttypename")->join("projects","projects.id","=","materialrequest.projectid")->join("factory","factory.id","=","materialrequest.factoryid")->join("requesttype","requesttype.id","=","materialrequest.requesttype")->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getmaterialrequest($col, $value)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = materialrequest::where($col, $value)->get();
		else
		{
			$result = materialrequest::select("materialrequest.*","projects.name as projectname", "factory.name as factoryname","requesttype.name as requesttypename")->join("projects","projects.id","=","materialrequest.projectid")->join("factory","factory.id","=","materialrequest.factoryid")->join("requesttype","requesttype.id","=","materialrequest.requesttype")->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
