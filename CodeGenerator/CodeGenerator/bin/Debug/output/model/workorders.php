<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class workorders extends Model
{
	protected $table = 'workorders';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveworkorders($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = workorders::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = workorders::where('id', $id)->first();
			if (empty($record)) {
				$record = new workorders;
			}

			$record->id = $input['id'];
			if(isset($input['projectid']))
			$record->projectid = $input['projectid'];
		if(isset($input['workorderdate']))
			$record->workorderdate = $input['workorderdate'];
		if(isset($input['completiondate']))
			$record->completiondate = $input['completiondate'];
		if(isset($input['remarks']))
			$record->remarks = $input['remarks'];
		if(isset($input['code']))
			$record->code = $input['code'];
		if(isset($input['status']))
			$record->status = $input['status'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getworkorders('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listworkorders($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workorders::all();
		else
		{
			$result = workorders::select('workorders.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getworkorders($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workorders::where($col, $value)->get();
		else
		{
			$result = workorders::select('workorders.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
