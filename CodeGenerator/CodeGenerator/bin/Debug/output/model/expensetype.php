<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class expensetype extends Model
{
	protected $table = 'expensetype';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveexpensetype($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = expensetype::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = expensetype::where('id', $id)->first();
			if (empty($record)) {
				$record = new expensetype;
			}

			$record->id = $input['id'];
			if(isset($input['name']))
			$record->name = $input['name'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getexpensetype('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listexpensetype($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = expensetype::all();
		else
		{
			$result = expensetype::select('expensetype.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getexpensetype($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = expensetype::where($col, $value)->get();
		else
		{
			$result = expensetype::select('expensetype.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
