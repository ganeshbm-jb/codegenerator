<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class stock extends Model
{
	protected $table = 'stock';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savestock($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = stock::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = stock::where('id', $id)->first();
			if (empty($record)) {
				$record = new stock;
			}

			$record->id = $input['id'];
			if(isset($input['parentid']))
			$record->parentid = $input['parentid'];
		if(isset($input['purchaseid']))
			$record->purchaseid = $input['purchaseid'];
		if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		if(isset($input['stockdate']))
			$record->stockdate = $input['stockdate'];
		if(isset($input['qty']))
			$record->qty = $input['qty'];
		if(isset($input['stocktype']))
			$record->stocktype = $input['stocktype'];
		if(isset($input['factoryid']))
			$record->factoryid = $input['factoryid'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['unitprice']))
			$record->unitprice = $input['unitprice'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getstock('stock.id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function liststock($request)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = stock::all();
		else
		{
			$result = stock::select("stock.*","materials.name","materials.id as materialsid","materials.name as materialname","factory.name as factoryname")->join("materials", "materials.id", "=", "stock.materialid")->join("factory","factory.id","=","stock.factoryid")->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getstock($col, $value)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = stock::where($col, $value)->get();
		else
		{
			$result = stock::select("stock.*","materials.name","materials.id as materialsid","materials.name as materialname","factory.name as factoryname")->join("materials", "materials.id", "=", "stock.materialid")->join("factory","factory.id","=","stock.factoryid")->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
