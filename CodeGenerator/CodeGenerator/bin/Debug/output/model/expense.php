<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class expense extends Model
{
	protected $table = 'expense';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveexpense($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = expense::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = expense::where('id', $id)->first();
			if (empty($record)) {
				$record = new expense;
			}

			$record->id = $input['id'];
			if(isset($input['edate']))
			$record->edate = $input['edate'];
		if(isset($input['description']))
			$record->description = $input['description'];
		if(isset($input['amount']))
			$record->amount = $input['amount'];
		if(isset($input['pid']))
			$record->pid = $input['pid'];
		if(isset($input['source']))
			$record->source = $input['source'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['pstatus']))
			$record->pstatus = $input['pstatus'];
		if(isset($input['nstatus']))
			$record->nstatus = $input['nstatus'];
		if(isset($input['active']))
			$record->active = $input['active'];
		if(isset($input['typeid']))
			$record->typeid = $input['typeid'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getexpense('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listexpense($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = expense::all();
		else
		{
			$result = expense::select('expense.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getexpense($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = expense::where($col, $value)->get();
		else
		{
			$result = expense::select('expense.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
