<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class materialrequestdetails extends Model
{
	protected $table = 'materialrequestdetails';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savematerialrequestdetails($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = materialrequestdetails::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = materialrequestdetails::where('id', $id)->first();
			if (empty($record)) {
				$record = new materialrequestdetails;
			}

			$record->id = $input['id'];
			if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		if(isset($input['qty']))
			$record->qty = $input['qty'];
		if(isset($input['stockqty']))
			$record->stockqty = $input['stockqty'];
		if(isset($input['stockqtydate']))
			$record->stockqtydate = $input['stockqtydate'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['materialrequestid']))
			$record->materialrequestid = $input['materialrequestid'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getmaterialrequestdetails('materialrequestdetails.id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listmaterialrequestdetails($request)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = materialrequestdetails::all();
		else
		{
			$result = materialrequestdetails::select("materialrequestdetails.*", "materials.name as materialname")->join("materials", "materials.id", "=", "materialrequestdetails.materialid")->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getmaterialrequestdetails($col, $value)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = materialrequestdetails::where($col, $value)->get();
		else
		{
			$result = materialrequestdetails::select("materialrequestdetails.*", "materials.name as materialname")->join("materials", "materials.id", "=", "materialrequestdetails.materialid")->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
