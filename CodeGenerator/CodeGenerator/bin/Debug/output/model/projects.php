<?php

namespace App\Models;
use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class projects extends Model
{
	protected $table = 'projects';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveprojects($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = projects::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = projects::where('id', $id)->first();
			if (empty($record)) {
				$record = new projects;
			}

			$record->id = $input['id'];
			if(isset($input['name']))
			$record->name = $input['name'];
		if(isset($input['address1']))
			$record->address1 = $input['address1'];
		if(isset($input['address2']))
			$record->address2 = $input['address2'];
		if(isset($input['city']))
			$record->city = $input['city'];
		if(isset($input['pincode']))
			$record->pincode = $input['pincode'];
		if(isset($input['gstno']))
			$record->gstno = $input['gstno'];
		if(isset($input['phone']))
			$record->phone = $input['phone'];
		if(isset($input['contactperson']))
			$record->contactperson = $input['contactperson'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getprojects('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listprojects($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = projects::all();
		else
		{
			$result = projects::select('projects.*')->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}


	// Method to get a record
	public static function getprojects($col, $value)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = projects::where($col, $value)->get();
		else
		{
			$result = projects::select('projects.*')->where($col, $value)->get();
		}
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return $result;
	}
}
