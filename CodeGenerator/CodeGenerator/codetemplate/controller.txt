﻿<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\@tablenameservice;
use App\Services\customservice;


class @tablenamecontroller  extends ParentController
{
    //
    public static function save@tablename(Request $request)
    {        
		$request = Self::tagInput("@tablename", "providerid", $request);
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,@tablenameservice::get@tablename($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return @tablenameservice::save@tablename($id, $request);
    }

	// Method to get all records
	public static function list@tablename(Request $request)
	{
		$request = Self::digestInput($request);
        return Self::filterResult("@tablename", "providerid", $request,@tablenameservice::list@tablename($request));
	}

    public static function get@tablename(Request $request)
    {
        $request = Self::digestInput($request);
        return @tablenameservice::get@tablename($request);
    }

	// Method to get all records
	public static function list@tablenamecount(Request $request)
	{
		$request = Self::digestInput($request);
        return @tablenameservice::list@tablenamecount($request);
	}

    public static function get@tablenamecount(Request $request)
    {
        $request = Self::digestInput($request);
        return @tablenameservice::get@tablenamecount($request);
    }
}
