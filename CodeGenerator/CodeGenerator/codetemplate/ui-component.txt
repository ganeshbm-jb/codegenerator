﻿**********************************************
FORM
**********************************************
export class @utablenameFormComponent extends CustomFormComponent implements OnInit {
  @Input() @tablename: any;
  @Input() editenabled: boolean = false;
  @Output() @tablenameObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.@tablenameForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: @tablenameFormGroup = new @tablenameFormGroup();
  @tablenameForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.@tablenameForm, this.formGroup, this.@tablename);
  }

  validationResult: any;
  @tablenameFormSave() {
    let payload: any = new @tablenameDO(PostOperation.Save);
    payload = this.getForm(this.@tablenameForm, this.formGroup, new @tablenameDO(PostOperation.Save));
    // validate
    this.validationResult = new @tablenameValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.apiService.doDataPost(payload).subscribe((result: any) => {
        if (!this.alertNrefresh(result, payload?.apiref, this.uiSvc)) { return; }
        this.@tablename = result['data'];
        this.@tablenameObjectChanged.emit(this.@tablename);
      });
  }


}

**********************************************
LIST
**********************************************
export class @utablenameListComponent extends CustomListComponent implements OnInit {
  @Input() @tablenameList: any[] = [];
  @Output() @tablenameItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(@tablename: any) {
    this.@tablenameItemSelected.emit(@tablename);
  }
}

**********************************************
LIST - PAGE
**********************************************
export class @utablenameListPageComponent extends CustomPageComponent implements OnInit {

  constructor(public apiService: ApiService, public localStorage: LocalstorageService, public uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
    this.list@tablename();
  }

  // properties
  @tablenameList: @tablenameDO[] = [];
  @tablename: @tablenameDO = new @tablenameDO("");
  view@tablename(item: any) {
    // if (item == "" || item == undefined) item = new materialrequestDO(PostOperation.Save);
    this.localStorage.set(@tablenameDO.localStorage, item);
    this.localStorage.set(this.originatedfrom, window.location.href);
    this.redirectTo(PageLinks.@tablename);
  }

  list@tablename() {
    // let ob: OrderByObject = new OrderByObject("batchno", OrderByObject.SortTypeDesc);
    let payload = new @tablenameDO(PostOperation.List);
    //payload.orderby.push(ob);
    let filters = this.prepareFilterObject("status", "=", this.getViewAccess('VW_@tablename_LIST')[0]?.status);
    if (filters?.length > 0) {
      payload.filters.push(this.applyOrFilter(new OrFilterObject("", ""), filters));
    }
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.@tablenameList = result['data'];
      // this.@tablenameList = this.applyFilter(this.@tablenameList, this.getViewAccess('VW_@tablename_LIST')[0]?.status);
    });
  }
 

}



**********************************************
PAGE
**********************************************
export class @utablenamePageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('app@tablenameform') @tablenameFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  // oninit
  ngOnInit(): void {
    // Init Masters
    this.@tablename = this.localStorage.get(@tablenameDO.localStorage);
    if (this.@tablename == "" || this.@tablename == undefined) {
      this.@tablename = new @tablenameDO(PostOperation.Save);      
      this.@tablename = this.setDefaultValues(new @tablenameFormGroup(), this.@tablename, new @tablenameDefault());      
      this.editenabled = true;
    } else this.get@tablename();
  }

  
  get@tablename() {
    let payload: any = new @tablenameDO(PostOperation.Get);
    payload.col = "@tablename.id";
    payload.value = this.@tablename.id;
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.reLoadParent(result['data'][0]);
    });
  }

  reLoadParent(data: any) {
    this.localStorage.set(@tablenameDO.localStorage, data);
    this.@tablename = data;
    this.@tablenameFormComponent.loadForm();
  }

  @tablename: any;
  @tablenameObjectChanged(data: any) {
    this.reLoadParent(data);
  }

  @tablenameSubmit(event: any) {
    this.submitenabled = false;
    // do submit    
    if (event != "") {
      let payload: any = new @tablenameDO(PostOperation.Save);
      this.doSubmitAction(payload, this.@tablename, event, this.apiService);
    }
  }


}

**********************************************
REPORT PAGE
**********************************************
export class @utablenamereportsPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('app@tablenamereportform') @tablenameFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  showFilter : boolean = false;

  showHideFilterForm()
  {
	this.showFilter = !this.showFilter;
  }
  // oninit
  ngOnInit(): void {
    this.@tablename = new @tablenameDO("");      
    this.@tablename = this.setDefaultValues(new @tablenameFormGroup(), this.@tablename, new @tablenameDefault());      
    this.editenabled = true;
	this.@tablenameFormComponent?.loadForm();
  }


  @tablename: any;
  @tablenameObjectChanged(payload: any) {
    // do Filter
	this.applyFilter(payload);
  }

  @tablenameList: @tablenameDO[] = [];
  applyFilter(payload:any)
  {
	// set filter logic
  } 
}

**********************************************
REPORT FORM
**********************************************
export class @utablenamereportsFormComponent extends CustomFormComponent implements OnInit {
  @Input() @tablename: any;
  @Input() editenabled: boolean = false;
  @Output() @tablenameObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.@tablenameForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: @tablenameFormGroup = new @tablenameFormGroup();
  @tablenameForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.@tablenameForm, this.formGroup, this.@tablename);
  }

  validationResult: any;
  @tablenameFormSearch() {
    let payload: any = new @tablenameDO(PostOperation.List);
    payload = this.getForm(this.@tablenameForm, this.formGroup, new @tablenameDO(PostOperation.List));
    // validate
    this.validationResult = new @tablenameReportValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.@tablenameObjectChanged.emit(payload);
  }


}

**********************************************
REPORT LIST
**********************************************
export class @utablenamereportsListComponent extends CustomListComponent implements OnInit {
  @Input() @tablenameList: any[] = [];
  @Output() @tablenameItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(@tablename: any) {
    this.@tablenameItemSelected.emit(@tablename);
  }
}