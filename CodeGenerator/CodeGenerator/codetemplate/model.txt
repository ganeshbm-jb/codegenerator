﻿<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class @tablename extends Model
{
	protected $table = '@tableprefix@tablename';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function save@tablename($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = @tablename::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = @tablename::where('id', $id)->first();
			if (empty($record)) {
				$record = new @tablename;
			}

			$record->id = $input['id'];
			@saveitems        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::get@tablename('@hasjoinid', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function list@tablename($request)
	{
		$result = [];
		$relatedTableResult = @relatedTableResult;

		if ($relatedTableResult == 0) {
            if (isset($request['showonly']) && count($request['showonly']) > 0) {               
                $result = @tablename::select($request['showonly'])->get();
            } else {
                $result = @tablename::all();
            }
        } else {
            $result = @relatedTableContent->get();

        }

		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function get@tablename($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = @relatedTableResult;
		if($relatedTableResult == 0){
			if (isset($request['showonly']) && count($request['showonly']) > 0) {
                $result = @tablename::select($request['showonly'])->where($col, $value)->get();
            } else {
			$result = @tablename::where($col, $value)->get();
			}
			}
		else
		{
			$result = @relatedTableContent->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = @parenttablescount;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			@parenttablesresult

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = @childtablescount;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			@childtablesresult

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}