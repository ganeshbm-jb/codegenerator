﻿namespace CodeGenerator
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tvSchema = new System.Windows.Forms.TreeView();
            this.cmsSchema = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiTable = new System.Windows.Forms.ToolStripMenuItem();
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.projectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tcSchema = new System.Windows.Forms.TabControl();
            this.tpProperties = new System.Windows.Forms.TabPage();
            this.pgProperties = new System.Windows.Forms.PropertyGrid();
            this.cmsTableSave = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiTableSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tpSQL = new System.Windows.Forms.TabPage();
            this.tbSQL = new System.Windows.Forms.TextBox();
            this.tpModel = new System.Windows.Forms.TabPage();
            this.tbModel = new System.Windows.Forms.TextBox();
            this.tpService = new System.Windows.Forms.TabPage();
            this.tbService = new System.Windows.Forms.TextBox();
            this.tpController = new System.Windows.Forms.TabPage();
            this.tbController = new System.Windows.Forms.TextBox();
            this.fbDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tpSettings = new System.Windows.Forms.TabPage();
            this.pgSettings = new System.Windows.Forms.PropertyGrid();
            this.cmsSettings = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.cmsSchema.SuspendLayout();
            this.msMain.SuspendLayout();
            this.tcSchema.SuspendLayout();
            this.tpProperties.SuspendLayout();
            this.cmsTableSave.SuspendLayout();
            this.tpSQL.SuspendLayout();
            this.tpModel.SuspendLayout();
            this.tpService.SuspendLayout();
            this.tpController.SuspendLayout();
            this.tpSettings.SuspendLayout();
            this.cmsSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.AutoSize = true;
            this.pnlMain.Controls.Add(this.splitContainer1);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(896, 460);
            this.pnlMain.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.Controls.Add(this.tvSchema);
            this.splitContainer1.Panel1.Controls.Add(this.msMain);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.tcSchema);
            this.splitContainer1.Size = new System.Drawing.Size(896, 460);
            this.splitContainer1.SplitterDistance = 298;
            this.splitContainer1.TabIndex = 1;
            // 
            // tvSchema
            // 
            this.tvSchema.ContextMenuStrip = this.cmsSchema;
            this.tvSchema.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvSchema.FullRowSelect = true;
            this.tvSchema.Location = new System.Drawing.Point(0, 24);
            this.tvSchema.Name = "tvSchema";
            this.tvSchema.Size = new System.Drawing.Size(298, 436);
            this.tvSchema.TabIndex = 0;
            this.tvSchema.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvSchema_NodeMouseClick);
            this.tvSchema.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvSchema_NodeMouseDoubleClick);
            this.tvSchema.DoubleClick += new System.EventHandler(this.tvSchema_DoubleClick);
            // 
            // cmsSchema
            // 
            this.cmsSchema.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiTable,
            this.settingsToolStripMenuItem});
            this.cmsSchema.Name = "cmsSchema";
            this.cmsSchema.Size = new System.Drawing.Size(117, 48);
            // 
            // tsmiTable
            // 
            this.tsmiTable.Name = "tsmiTable";
            this.tsmiTable.Size = new System.Drawing.Size(116, 22);
            this.tsmiTable.Text = "Table";
            this.tsmiTable.Click += new System.EventHandler(this.tsmiTable_Click);
            // 
            // msMain
            // 
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.projectToolStripMenuItem,
            this.generateToolStripMenuItem});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(298, 24);
            this.msMain.TabIndex = 1;
            this.msMain.Text = "menuStrip1";
            this.msMain.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.msMain_ItemClicked);
            // 
            // projectToolStripMenuItem
            // 
            this.projectToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem});
            this.projectToolStripMenuItem.Name = "projectToolStripMenuItem";
            this.projectToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.projectToolStripMenuItem.Text = "Project";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Visible = false;
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // generateToolStripMenuItem
            // 
            this.generateToolStripMenuItem.Name = "generateToolStripMenuItem";
            this.generateToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.generateToolStripMenuItem.Text = "Generate";
            this.generateToolStripMenuItem.Click += new System.EventHandler(this.generateToolStripMenuItem_Click);
            // 
            // tcSchema
            // 
            this.tcSchema.Controls.Add(this.tpProperties);
            this.tcSchema.Controls.Add(this.tpSQL);
            this.tcSchema.Controls.Add(this.tpModel);
            this.tcSchema.Controls.Add(this.tpService);
            this.tcSchema.Controls.Add(this.tpController);
            this.tcSchema.Controls.Add(this.tpSettings);
            this.tcSchema.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcSchema.Location = new System.Drawing.Point(0, 0);
            this.tcSchema.Name = "tcSchema";
            this.tcSchema.SelectedIndex = 0;
            this.tcSchema.Size = new System.Drawing.Size(594, 460);
            this.tcSchema.TabIndex = 0;
            this.tcSchema.SelectedIndexChanged += new System.EventHandler(this.tcSchema_SelectedIndexChanged);
            // 
            // tpProperties
            // 
            this.tpProperties.Controls.Add(this.pgProperties);
            this.tpProperties.Location = new System.Drawing.Point(4, 22);
            this.tpProperties.Name = "tpProperties";
            this.tpProperties.Padding = new System.Windows.Forms.Padding(3);
            this.tpProperties.Size = new System.Drawing.Size(586, 434);
            this.tpProperties.TabIndex = 4;
            this.tpProperties.Text = "Properties";
            this.tpProperties.UseVisualStyleBackColor = true;
            // 
            // pgProperties
            // 
            this.pgProperties.ContextMenuStrip = this.cmsTableSave;
            this.pgProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgProperties.Location = new System.Drawing.Point(3, 3);
            this.pgProperties.Name = "pgProperties";
            this.pgProperties.Size = new System.Drawing.Size(580, 428);
            this.pgProperties.TabIndex = 0;
            // 
            // cmsTableSave
            // 
            this.cmsTableSave.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiTableSave});
            this.cmsTableSave.Name = "cmsTableSave";
            this.cmsTableSave.Size = new System.Drawing.Size(129, 26);
            // 
            // tsmiTableSave
            // 
            this.tsmiTableSave.Name = "tsmiTableSave";
            this.tsmiTableSave.Size = new System.Drawing.Size(128, 22);
            this.tsmiTableSave.Text = "Save Table";
            this.tsmiTableSave.Click += new System.EventHandler(this.tsmiTableSave_Click);
            // 
            // tpSQL
            // 
            this.tpSQL.Controls.Add(this.tbSQL);
            this.tpSQL.Location = new System.Drawing.Point(4, 22);
            this.tpSQL.Name = "tpSQL";
            this.tpSQL.Padding = new System.Windows.Forms.Padding(3);
            this.tpSQL.Size = new System.Drawing.Size(586, 434);
            this.tpSQL.TabIndex = 0;
            this.tpSQL.Text = "SQL";
            this.tpSQL.UseVisualStyleBackColor = true;
            // 
            // tbSQL
            // 
            this.tbSQL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.tbSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbSQL.Location = new System.Drawing.Point(3, 3);
            this.tbSQL.Multiline = true;
            this.tbSQL.Name = "tbSQL";
            this.tbSQL.ReadOnly = true;
            this.tbSQL.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbSQL.Size = new System.Drawing.Size(580, 428);
            this.tbSQL.TabIndex = 0;
            // 
            // tpModel
            // 
            this.tpModel.Controls.Add(this.tbModel);
            this.tpModel.Location = new System.Drawing.Point(4, 22);
            this.tpModel.Name = "tpModel";
            this.tpModel.Padding = new System.Windows.Forms.Padding(3);
            this.tpModel.Size = new System.Drawing.Size(586, 434);
            this.tpModel.TabIndex = 1;
            this.tpModel.Text = "Model";
            this.tpModel.UseVisualStyleBackColor = true;
            // 
            // tbModel
            // 
            this.tbModel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.tbModel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbModel.Location = new System.Drawing.Point(3, 3);
            this.tbModel.Multiline = true;
            this.tbModel.Name = "tbModel";
            this.tbModel.ReadOnly = true;
            this.tbModel.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbModel.Size = new System.Drawing.Size(580, 428);
            this.tbModel.TabIndex = 1;
            // 
            // tpService
            // 
            this.tpService.Controls.Add(this.tbService);
            this.tpService.Location = new System.Drawing.Point(4, 22);
            this.tpService.Name = "tpService";
            this.tpService.Size = new System.Drawing.Size(586, 434);
            this.tpService.TabIndex = 2;
            this.tpService.Text = "Service";
            this.tpService.UseVisualStyleBackColor = true;
            // 
            // tbService
            // 
            this.tbService.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbService.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbService.Location = new System.Drawing.Point(0, 0);
            this.tbService.Multiline = true;
            this.tbService.Name = "tbService";
            this.tbService.ReadOnly = true;
            this.tbService.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbService.Size = new System.Drawing.Size(586, 434);
            this.tbService.TabIndex = 1;
            // 
            // tpController
            // 
            this.tpController.Controls.Add(this.tbController);
            this.tpController.Location = new System.Drawing.Point(4, 22);
            this.tpController.Name = "tpController";
            this.tpController.Size = new System.Drawing.Size(586, 434);
            this.tpController.TabIndex = 3;
            this.tpController.Text = "Controller";
            this.tpController.UseVisualStyleBackColor = true;
            // 
            // tbController
            // 
            this.tbController.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.tbController.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbController.Location = new System.Drawing.Point(0, 0);
            this.tbController.Multiline = true;
            this.tbController.Name = "tbController";
            this.tbController.ReadOnly = true;
            this.tbController.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbController.Size = new System.Drawing.Size(586, 434);
            this.tbController.TabIndex = 1;
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // tpSettings
            // 
            this.tpSettings.Controls.Add(this.pgSettings);
            this.tpSettings.Location = new System.Drawing.Point(4, 22);
            this.tpSettings.Name = "tpSettings";
            this.tpSettings.Size = new System.Drawing.Size(586, 434);
            this.tpSettings.TabIndex = 5;
            this.tpSettings.Text = "Settings";
            this.tpSettings.UseVisualStyleBackColor = true;
            // 
            // pgSettings
            // 
            this.pgSettings.ContextMenuStrip = this.cmsSettings;
            this.pgSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgSettings.Location = new System.Drawing.Point(0, 0);
            this.pgSettings.Name = "pgSettings";
            this.pgSettings.Size = new System.Drawing.Size(586, 434);
            this.pgSettings.TabIndex = 1;
            // 
            // cmsSettings
            // 
            this.cmsSettings.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSettings});
            this.cmsSettings.Name = "cmsSettings";
            this.cmsSettings.Size = new System.Drawing.Size(144, 26);
            // 
            // tsmiSettings
            // 
            this.tsmiSettings.Name = "tsmiSettings";
            this.tsmiSettings.Size = new System.Drawing.Size(152, 22);
            this.tsmiSettings.Text = "Save Settings";
            this.tsmiSettings.Click += new System.EventHandler(this.tsmiSettings_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 460);
            this.Controls.Add(this.pnlMain);
            this.MainMenuStrip = this.msMain;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.Text = "Code Generator";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.pnlMain.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.cmsSchema.ResumeLayout(false);
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.tcSchema.ResumeLayout(false);
            this.tpProperties.ResumeLayout(false);
            this.cmsTableSave.ResumeLayout(false);
            this.tpSQL.ResumeLayout(false);
            this.tpSQL.PerformLayout();
            this.tpModel.ResumeLayout(false);
            this.tpModel.PerformLayout();
            this.tpService.ResumeLayout(false);
            this.tpService.PerformLayout();
            this.tpController.ResumeLayout(false);
            this.tpController.PerformLayout();
            this.tpSettings.ResumeLayout(false);
            this.cmsSettings.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView tvSchema;
        private System.Windows.Forms.TabControl tcSchema;
        private System.Windows.Forms.TabPage tpSQL;
        private System.Windows.Forms.TextBox tbSQL;
        private System.Windows.Forms.TabPage tpModel;
        private System.Windows.Forms.TextBox tbModel;
        private System.Windows.Forms.TabPage tpService;
        private System.Windows.Forms.TextBox tbService;
        private System.Windows.Forms.TabPage tpController;
        private System.Windows.Forms.TextBox tbController;
        private System.Windows.Forms.ContextMenuStrip cmsSchema;
        private System.Windows.Forms.ToolStripMenuItem tsmiTable;
        private System.Windows.Forms.TabPage tpProperties;
        private System.Windows.Forms.PropertyGrid pgProperties;
        private System.Windows.Forms.ContextMenuStrip cmsTableSave;
        private System.Windows.Forms.ToolStripMenuItem tsmiTableSave;
        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem generateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog fbDialog;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.TabPage tpSettings;
        private System.Windows.Forms.PropertyGrid pgSettings;
        private System.Windows.Forms.ContextMenuStrip cmsSettings;
        private System.Windows.Forms.ToolStripMenuItem tsmiSettings;
    }
}

