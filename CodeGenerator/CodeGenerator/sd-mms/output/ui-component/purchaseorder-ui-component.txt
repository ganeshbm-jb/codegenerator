**********************************************
FORM
**********************************************
export class PurchaseorderFormComponent extends CustomFormComponent implements OnInit {
  @Input() purchaseorder: any;
  @Input() editenabled: boolean = false;
  @Output() purchaseorderObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.purchaseorderForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: purchaseorderFormGroup = new purchaseorderFormGroup();
  purchaseorderForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.purchaseorderForm, this.formGroup, this.purchaseorder);
  }

  validationResult: any;
  purchaseorderFormSave() {
    let payload: any = new purchaseorderDO(PostOperation.Save);
    payload = this.getForm(this.purchaseorderForm, this.formGroup, new purchaseorderDO(PostOperation.Save));
    // validate
    this.validationResult = new purchaseorderValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.apiService.doDataPost(payload).subscribe((result: any) => {
        if (!this.alertNrefresh(result, payload?.apiref, this.uiSvc)) { return; }
        this.purchaseorder = result['data'];
        this.purchaseorderObjectChanged.emit(this.purchaseorder);
      });
  }


}

**********************************************
LIST
**********************************************
export class PurchaseorderListComponent extends CustomListComponent implements OnInit {
  @Input() purchaseorderList: any[] = [];
  @Output() purchaseorderItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(purchaseorder: any) {
    this.purchaseorderItemSelected.emit(purchaseorder);
  }
}

**********************************************
LIST - PAGE
**********************************************
export class PurchaseorderListPageComponent extends CustomPageComponent implements OnInit {

  constructor(public apiService: ApiService, public localStorage: LocalstorageService, public uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
    this.listpurchaseorder();
  }

  // properties
  purchaseorderList: purchaseorderDO[] = [];
  purchaseorder: purchaseorderDO = new purchaseorderDO("");
  viewpurchaseorder(item: any) {
    // if (item == "" || item == undefined) item = new materialrequestDO(PostOperation.Save);
    this.localStorage.set(purchaseorderDO.localStorage, item);
    this.localStorage.set(this.originatedfrom, window.location.href);
    this.redirectTo(PageLinks.purchaseorder);
  }

  listpurchaseorder() {
    // let ob: OrderByObject = new OrderByObject("batchno", OrderByObject.SortTypeDesc);
    let payload = new purchaseorderDO(PostOperation.List);
    //payload.orderby.push(ob);
    let filters = this.prepareFilterObject("status", "=", this.getViewAccess('VW_purchaseorder_LIST')[0]?.status);
    if (filters?.length > 0) {
      payload.filters.push(this.applyOrFilter(new OrFilterObject("", ""), filters));
    }
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.purchaseorderList = result['data'];
      // this.purchaseorderList = this.applyFilter(this.purchaseorderList, this.getViewAccess('VW_purchaseorder_LIST')[0]?.status);
    });
  }
 

}



**********************************************
PAGE
**********************************************
export class PurchaseorderPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('apppurchaseorderform') purchaseorderFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  // oninit
  ngOnInit(): void {
    // Init Masters
    this.purchaseorder = this.localStorage.get(purchaseorderDO.localStorage);
    if (this.purchaseorder == "" || this.purchaseorder == undefined) {
      this.purchaseorder = new purchaseorderDO(PostOperation.Save);      
      this.purchaseorder = this.setDefaultValues(new purchaseorderFormGroup(), this.purchaseorder, new purchaseorderDefault());      
      this.editenabled = true;
    } else this.getpurchaseorder();
  }

  
  getpurchaseorder() {
    let payload: any = new purchaseorderDO(PostOperation.Get);
    payload.col = "purchaseorder.id";
    payload.value = this.purchaseorder.id;
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.reLoadParent(result['data'][0]);
    });
  }

  reLoadParent(data: any) {
    this.localStorage.set(purchaseorderDO.localStorage, data);
    this.purchaseorder = data;
    this.purchaseorderFormComponent.loadForm();
  }

  purchaseorder: any;
  purchaseorderObjectChanged(data: any) {
    this.reLoadParent(data);
  }

  purchaseorderSubmit(event: any) {
    this.submitenabled = false;
    // do submit    
    if (event != "") {
      let payload: any = new purchaseorderDO(PostOperation.Save);
      this.doSubmitAction(payload, this.purchaseorder, event, this.apiService);
    }
  }


}

**********************************************
REPORT PAGE
**********************************************
export class PurchaseorderreportsPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('apppurchaseorderreportform') purchaseorderFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  showFilter : boolean = false;

  showHideFilterForm()
  {
	this.showFilter = !this.showFilter;
  }
  // oninit
  ngOnInit(): void {
    this.purchaseorder = new purchaseorderDO("");      
    this.purchaseorder = this.setDefaultValues(new purchaseorderFormGroup(), this.purchaseorder, new purchaseorderDefault());      
    this.editenabled = true;
	this.purchaseorderFormComponent?.loadForm();
  }


  purchaseorder: any;
  purchaseorderObjectChanged(payload: any) {
    // do Filter
	this.applyFilter(payload);
  }

  purchaseorderList: purchaseorderDO[] = [];
  applyFilter(payload:any)
  {
	// set filter logic
  } 
}

**********************************************
REPORT FORM
**********************************************
export class PurchaseorderreportsFormComponent extends CustomFormComponent implements OnInit {
  @Input() purchaseorder: any;
  @Input() editenabled: boolean = false;
  @Output() purchaseorderObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.purchaseorderForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: purchaseorderFormGroup = new purchaseorderFormGroup();
  purchaseorderForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.purchaseorderForm, this.formGroup, this.purchaseorder);
  }

  validationResult: any;
  purchaseorderFormSearch() {
    let payload: any = new purchaseorderDO(PostOperation.List);
    payload = this.getForm(this.purchaseorderForm, this.formGroup, new purchaseorderDO(PostOperation.List));
    // validate
    this.validationResult = new purchaseorderReportValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.purchaseorderObjectChanged.emit(payload);
  }


}

**********************************************
REPORT LIST
**********************************************
export class PurchaseorderreportsListComponent extends CustomListComponent implements OnInit {
  @Input() purchaseorderList: any[] = [];
  @Output() purchaseorderItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(purchaseorder: any) {
    this.purchaseorderItemSelected.emit(purchaseorder);
  }
}
