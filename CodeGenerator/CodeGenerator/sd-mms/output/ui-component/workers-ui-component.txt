**********************************************
FORM
**********************************************
export class WorkersFormComponent extends CustomFormComponent implements OnInit {
  @Input() workers: any;
  @Input() editenabled: boolean = false;
  @Output() workersObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.workersForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: workersFormGroup = new workersFormGroup();
  workersForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.workersForm, this.formGroup, this.workers);
  }

  validationResult: any;
  workersFormSave() {
    let payload: any = new workersDO(PostOperation.Save);
    payload = this.getForm(this.workersForm, this.formGroup, new workersDO(PostOperation.Save));
    // validate
    this.validationResult = new workersValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.apiService.doDataPost(payload).subscribe((result: any) => {
        if (!this.alertNrefresh(result, payload?.apiref, this.uiSvc)) { return; }
        this.workers = result['data'];
        this.workersObjectChanged.emit(this.workers);
      });
  }


}

**********************************************
LIST
**********************************************
export class WorkersListComponent extends CustomListComponent implements OnInit {
  @Input() workersList: any[] = [];
  @Output() workersItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(workers: any) {
    this.workersItemSelected.emit(workers);
  }
}

**********************************************
LIST - PAGE
**********************************************
export class WorkersListPageComponent extends CustomPageComponent implements OnInit {

  constructor(public apiService: ApiService, public localStorage: LocalstorageService, public uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
    this.listworkers();
  }

  // properties
  workersList: workersDO[] = [];
  workers: workersDO = new workersDO("");
  viewworkers(item: any) {
    // if (item == "" || item == undefined) item = new materialrequestDO(PostOperation.Save);
    this.localStorage.set(workersDO.localStorage, item);
    this.localStorage.set(this.originatedfrom, window.location.href);
    this.redirectTo(PageLinks.workers);
  }

  listworkers() {
    // let ob: OrderByObject = new OrderByObject("batchno", OrderByObject.SortTypeDesc);
    let payload = new workersDO(PostOperation.List);
    //payload.orderby.push(ob);
    let filters = this.prepareFilterObject("status", "=", this.getViewAccess('VW_workers_LIST')[0]?.status);
    if (filters?.length > 0) {
      payload.filters.push(this.applyOrFilter(new OrFilterObject("", ""), filters));
    }
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.workersList = result['data'];
      // this.workersList = this.applyFilter(this.workersList, this.getViewAccess('VW_workers_LIST')[0]?.status);
    });
  }
 

}



**********************************************
PAGE
**********************************************
export class WorkersPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appworkersform') workersFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  // oninit
  ngOnInit(): void {
    // Init Masters
    this.workers = this.localStorage.get(workersDO.localStorage);
    if (this.workers == "" || this.workers == undefined) {
      this.workers = new workersDO(PostOperation.Save);      
      this.workers = this.setDefaultValues(new workersFormGroup(), this.workers, new workersDefault());      
      this.editenabled = true;
    } else this.getworkers();
  }

  
  getworkers() {
    let payload: any = new workersDO(PostOperation.Get);
    payload.col = "workers.id";
    payload.value = this.workers.id;
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.reLoadParent(result['data'][0]);
    });
  }

  reLoadParent(data: any) {
    this.localStorage.set(workersDO.localStorage, data);
    this.workers = data;
    this.workersFormComponent.loadForm();
  }

  workers: any;
  workersObjectChanged(data: any) {
    this.reLoadParent(data);
  }

  workersSubmit(event: any) {
    this.submitenabled = false;
    // do submit    
    if (event != "") {
      let payload: any = new workersDO(PostOperation.Save);
      this.doSubmitAction(payload, this.workers, event, this.apiService);
    }
  }


}

**********************************************
REPORT PAGE
**********************************************
export class WorkersreportsPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appworkersreportform') workersFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  showFilter : boolean = false;

  showHideFilterForm()
  {
	this.showFilter = !this.showFilter;
  }
  // oninit
  ngOnInit(): void {
    this.workers = new workersDO("");      
    this.workers = this.setDefaultValues(new workersFormGroup(), this.workers, new workersDefault());      
    this.editenabled = true;
	this.workersFormComponent?.loadForm();
  }


  workers: any;
  workersObjectChanged(payload: any) {
    // do Filter
	this.applyFilter(payload);
  }

  workersList: workersDO[] = [];
  applyFilter(payload:any)
  {
	// set filter logic
  } 
}

**********************************************
REPORT FORM
**********************************************
export class WorkersreportsFormComponent extends CustomFormComponent implements OnInit {
  @Input() workers: any;
  @Input() editenabled: boolean = false;
  @Output() workersObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.workersForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: workersFormGroup = new workersFormGroup();
  workersForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.workersForm, this.formGroup, this.workers);
  }

  validationResult: any;
  workersFormSearch() {
    let payload: any = new workersDO(PostOperation.List);
    payload = this.getForm(this.workersForm, this.formGroup, new workersDO(PostOperation.List));
    // validate
    this.validationResult = new workersReportValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.workersObjectChanged.emit(payload);
  }


}

**********************************************
REPORT LIST
**********************************************
export class WorkersreportsListComponent extends CustomListComponent implements OnInit {
  @Input() workersList: any[] = [];
  @Output() workersItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(workers: any) {
    this.workersItemSelected.emit(workers);
  }
}
