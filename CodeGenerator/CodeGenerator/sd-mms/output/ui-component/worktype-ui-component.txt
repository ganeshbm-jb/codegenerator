**********************************************
FORM
**********************************************
export class WorktypeFormComponent extends CustomFormComponent implements OnInit {
  @Input() worktype: any;
  @Input() editenabled: boolean = false;
  @Output() worktypeObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.worktypeForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: worktypeFormGroup = new worktypeFormGroup();
  worktypeForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.worktypeForm, this.formGroup, this.worktype);
  }

  validationResult: any;
  worktypeFormSave() {
    let payload: any = new worktypeDO(PostOperation.Save);
    payload = this.getForm(this.worktypeForm, this.formGroup, new worktypeDO(PostOperation.Save));
    // validate
    this.validationResult = new worktypeValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.apiService.doDataPost(payload).subscribe((result: any) => {
        if (!this.alertNrefresh(result, payload?.apiref, this.uiSvc)) { return; }
        this.worktype = result['data'];
        this.worktypeObjectChanged.emit(this.worktype);
      });
  }


}

**********************************************
LIST
**********************************************
export class WorktypeListComponent extends CustomListComponent implements OnInit {
  @Input() worktypeList: any[] = [];
  @Output() worktypeItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(worktype: any) {
    this.worktypeItemSelected.emit(worktype);
  }
}

**********************************************
LIST - PAGE
**********************************************
export class WorktypeListPageComponent extends CustomPageComponent implements OnInit {

  constructor(public apiService: ApiService, public localStorage: LocalstorageService, public uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
    this.listworktype();
  }

  // properties
  worktypeList: worktypeDO[] = [];
  worktype: worktypeDO = new worktypeDO("");
  viewworktype(item: any) {
    // if (item == "" || item == undefined) item = new materialrequestDO(PostOperation.Save);
    this.localStorage.set(worktypeDO.localStorage, item);
    this.localStorage.set(this.originatedfrom, window.location.href);
    this.redirectTo(PageLinks.worktype);
  }

  listworktype() {
    // let ob: OrderByObject = new OrderByObject("batchno", OrderByObject.SortTypeDesc);
    let payload = new worktypeDO(PostOperation.List);
    //payload.orderby.push(ob);
    let filters = this.prepareFilterObject("status", "=", this.getViewAccess('VW_worktype_LIST')[0]?.status);
    if (filters?.length > 0) {
      payload.filters.push(this.applyOrFilter(new OrFilterObject("", ""), filters));
    }
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.worktypeList = result['data'];
      // this.worktypeList = this.applyFilter(this.worktypeList, this.getViewAccess('VW_worktype_LIST')[0]?.status);
    });
  }
 

}



**********************************************
PAGE
**********************************************
export class WorktypePageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appworktypeform') worktypeFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  // oninit
  ngOnInit(): void {
    // Init Masters
    this.worktype = this.localStorage.get(worktypeDO.localStorage);
    if (this.worktype == "" || this.worktype == undefined) {
      this.worktype = new worktypeDO(PostOperation.Save);      
      this.worktype = this.setDefaultValues(new worktypeFormGroup(), this.worktype, new worktypeDefault());      
      this.editenabled = true;
    } else this.getworktype();
  }

  
  getworktype() {
    let payload: any = new worktypeDO(PostOperation.Get);
    payload.col = "worktype.id";
    payload.value = this.worktype.id;
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.reLoadParent(result['data'][0]);
    });
  }

  reLoadParent(data: any) {
    this.localStorage.set(worktypeDO.localStorage, data);
    this.worktype = data;
    this.worktypeFormComponent.loadForm();
  }

  worktype: any;
  worktypeObjectChanged(data: any) {
    this.reLoadParent(data);
  }

  worktypeSubmit(event: any) {
    this.submitenabled = false;
    // do submit    
    if (event != "") {
      let payload: any = new worktypeDO(PostOperation.Save);
      this.doSubmitAction(payload, this.worktype, event, this.apiService);
    }
  }


}

**********************************************
REPORT PAGE
**********************************************
export class WorktypereportsPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appworktypereportform') worktypeFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  showFilter : boolean = false;

  showHideFilterForm()
  {
	this.showFilter = !this.showFilter;
  }
  // oninit
  ngOnInit(): void {
    this.worktype = new worktypeDO("");      
    this.worktype = this.setDefaultValues(new worktypeFormGroup(), this.worktype, new worktypeDefault());      
    this.editenabled = true;
	this.worktypeFormComponent?.loadForm();
  }


  worktype: any;
  worktypeObjectChanged(payload: any) {
    // do Filter
	this.applyFilter(payload);
  }

  worktypeList: worktypeDO[] = [];
  applyFilter(payload:any)
  {
	// set filter logic
  } 
}

**********************************************
REPORT FORM
**********************************************
export class WorktypereportsFormComponent extends CustomFormComponent implements OnInit {
  @Input() worktype: any;
  @Input() editenabled: boolean = false;
  @Output() worktypeObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.worktypeForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: worktypeFormGroup = new worktypeFormGroup();
  worktypeForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.worktypeForm, this.formGroup, this.worktype);
  }

  validationResult: any;
  worktypeFormSearch() {
    let payload: any = new worktypeDO(PostOperation.List);
    payload = this.getForm(this.worktypeForm, this.formGroup, new worktypeDO(PostOperation.List));
    // validate
    this.validationResult = new worktypeReportValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.worktypeObjectChanged.emit(payload);
  }


}

**********************************************
REPORT LIST
**********************************************
export class WorktypereportsListComponent extends CustomListComponent implements OnInit {
  @Input() worktypeList: any[] = [];
  @Output() worktypeItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(worktype: any) {
    this.worktypeItemSelected.emit(worktype);
  }
}
