**********************************************
FORM
**********************************************
export class DashboardFormComponent extends CustomFormComponent implements OnInit {
  @Input() dashboard: any;
  @Input() editenabled: boolean = false;
  @Output() dashboardObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.dashboardForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: dashboardFormGroup = new dashboardFormGroup();
  dashboardForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.dashboardForm, this.formGroup, this.dashboard);
  }

  validationResult: any;
  dashboardFormSave() {
    let payload: any = new dashboardDO(PostOperation.Save);
    payload = this.getForm(this.dashboardForm, this.formGroup, new dashboardDO(PostOperation.Save));
    // validate
    this.validationResult = new dashboardValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.apiService.doDataPost(payload).subscribe((result: any) => {
        if (!this.alertNrefresh(result, payload?.apiref, this.uiSvc)) { return; }
        this.dashboard = result['data'];
        this.dashboardObjectChanged.emit(this.dashboard);
      });
  }


}

**********************************************
LIST
**********************************************
export class DashboardListComponent extends CustomListComponent implements OnInit {
  @Input() dashboardList: any[] = [];
  @Output() dashboardItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(dashboard: any) {
    this.dashboardItemSelected.emit(dashboard);
  }
}

**********************************************
LIST - PAGE
**********************************************
export class DashboardListPageComponent extends CustomPageComponent implements OnInit {

  constructor(public apiService: ApiService, public localStorage: LocalstorageService, public uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
    this.listdashboard();
  }

  // properties
  dashboardList: dashboardDO[] = [];
  dashboard: dashboardDO = new dashboardDO("");
  viewdashboard(item: any) {
    // if (item == "" || item == undefined) item = new materialrequestDO(PostOperation.Save);
    this.localStorage.set(dashboardDO.localStorage, item);
    this.localStorage.set(this.originatedfrom, window.location.href);
    this.redirectTo(PageLinks.dashboard);
  }

  listdashboard() {
    // let ob: OrderByObject = new OrderByObject("batchno", OrderByObject.SortTypeDesc);
    let payload = new dashboardDO(PostOperation.List);
    //payload.orderby.push(ob);
    let filters = this.prepareFilterObject("status", "=", this.getViewAccess('VW_dashboard_LIST')[0]?.status);
    if (filters?.length > 0) {
      payload.filters.push(this.applyOrFilter(new OrFilterObject("", ""), filters));
    }
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.dashboardList = result['data'];
      // this.dashboardList = this.applyFilter(this.dashboardList, this.getViewAccess('VW_dashboard_LIST')[0]?.status);
    });
  }
 

}



**********************************************
PAGE
**********************************************
export class DashboardPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appdashboardform') dashboardFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  // oninit
  ngOnInit(): void {
    // Init Masters
    this.dashboard = this.localStorage.get(dashboardDO.localStorage);
    if (this.dashboard == "" || this.dashboard == undefined) {
      this.dashboard = new dashboardDO(PostOperation.Save);      
      this.dashboard = this.setDefaultValues(new dashboardFormGroup(), this.dashboard, new dashboardDefault());      
      this.editenabled = true;
    } else this.getdashboard();
  }

  
  getdashboard() {
    let payload: any = new dashboardDO(PostOperation.Get);
    payload.col = "dashboard.id";
    payload.value = this.dashboard.id;
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.reLoadParent(result['data'][0]);
    });
  }

  reLoadParent(data: any) {
    this.localStorage.set(dashboardDO.localStorage, data);
    this.dashboard = data;
    this.dashboardFormComponent.loadForm();
  }

  dashboard: any;
  dashboardObjectChanged(data: any) {
    this.reLoadParent(data);
  }

  dashboardSubmit(event: any) {
    this.submitenabled = false;
    // do submit    
    if (event != "") {
      let payload: any = new dashboardDO(PostOperation.Save);
      this.doSubmitAction(payload, this.dashboard, event, this.apiService);
    }
  }


}

**********************************************
REPORT PAGE
**********************************************
export class DashboardreportsPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appdashboardreportform') dashboardFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  showFilter : boolean = false;

  showHideFilterForm()
  {
	this.showFilter = !this.showFilter;
  }
  // oninit
  ngOnInit(): void {
    this.dashboard = new dashboardDO("");      
    this.dashboard = this.setDefaultValues(new dashboardFormGroup(), this.dashboard, new dashboardDefault());      
    this.editenabled = true;
	this.dashboardFormComponent?.loadForm();
  }


  dashboard: any;
  dashboardObjectChanged(payload: any) {
    // do Filter
	this.applyFilter(payload);
  }

  dashboardList: dashboardDO[] = [];
  applyFilter(payload:any)
  {
	// set filter logic
  } 
}

**********************************************
REPORT FORM
**********************************************
export class DashboardreportsFormComponent extends CustomFormComponent implements OnInit {
  @Input() dashboard: any;
  @Input() editenabled: boolean = false;
  @Output() dashboardObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.dashboardForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: dashboardFormGroup = new dashboardFormGroup();
  dashboardForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.dashboardForm, this.formGroup, this.dashboard);
  }

  validationResult: any;
  dashboardFormSearch() {
    let payload: any = new dashboardDO(PostOperation.List);
    payload = this.getForm(this.dashboardForm, this.formGroup, new dashboardDO(PostOperation.List));
    // validate
    this.validationResult = new dashboardReportValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.dashboardObjectChanged.emit(payload);
  }


}

**********************************************
REPORT LIST
**********************************************
export class DashboardreportsListComponent extends CustomListComponent implements OnInit {
  @Input() dashboardList: any[] = [];
  @Output() dashboardItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(dashboard: any) {
    this.dashboardItemSelected.emit(dashboard);
  }
}
