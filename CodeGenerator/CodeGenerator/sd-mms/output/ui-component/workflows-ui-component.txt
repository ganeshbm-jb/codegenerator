**********************************************
FORM
**********************************************
export class WorkflowsFormComponent extends CustomFormComponent implements OnInit {
  @Input() workflows: any;
  @Input() editenabled: boolean = false;
  @Output() workflowsObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.workflowsForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: workflowsFormGroup = new workflowsFormGroup();
  workflowsForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.workflowsForm, this.formGroup, this.workflows);
  }

  validationResult: any;
  workflowsFormSave() {
    let payload: any = new workflowsDO(PostOperation.Save);
    payload = this.getForm(this.workflowsForm, this.formGroup, new workflowsDO(PostOperation.Save));
    // validate
    this.validationResult = new workflowsValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.apiService.doDataPost(payload).subscribe((result: any) => {
        if (!this.alertNrefresh(result, payload?.apiref, this.uiSvc)) { return; }
        this.workflows = result['data'];
        this.workflowsObjectChanged.emit(this.workflows);
      });
  }


}

**********************************************
LIST
**********************************************
export class WorkflowsListComponent extends CustomListComponent implements OnInit {
  @Input() workflowsList: any[] = [];
  @Output() workflowsItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(workflows: any) {
    this.workflowsItemSelected.emit(workflows);
  }
}

**********************************************
LIST - PAGE
**********************************************
export class WorkflowsListPageComponent extends CustomPageComponent implements OnInit {

  constructor(public apiService: ApiService, public localStorage: LocalstorageService, public uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
    this.listworkflows();
  }

  // properties
  workflowsList: workflowsDO[] = [];
  workflows: workflowsDO = new workflowsDO("");
  viewworkflows(item: any) {
    // if (item == "" || item == undefined) item = new materialrequestDO(PostOperation.Save);
    this.localStorage.set(workflowsDO.localStorage, item);
    this.localStorage.set(this.originatedfrom, window.location.href);
    this.redirectTo(PageLinks.workflows);
  }

  listworkflows() {
    // let ob: OrderByObject = new OrderByObject("batchno", OrderByObject.SortTypeDesc);
    let payload = new workflowsDO(PostOperation.List);
    //payload.orderby.push(ob);
    let filters = this.prepareFilterObject("status", "=", this.getViewAccess('VW_workflows_LIST')[0]?.status);
    if (filters?.length > 0) {
      payload.filters.push(this.applyOrFilter(new OrFilterObject("", ""), filters));
    }
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.workflowsList = result['data'];
      // this.workflowsList = this.applyFilter(this.workflowsList, this.getViewAccess('VW_workflows_LIST')[0]?.status);
    });
  }
 

}



**********************************************
PAGE
**********************************************
export class WorkflowsPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appworkflowsform') workflowsFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  // oninit
  ngOnInit(): void {
    // Init Masters
    this.workflows = this.localStorage.get(workflowsDO.localStorage);
    if (this.workflows == "" || this.workflows == undefined) {
      this.workflows = new workflowsDO(PostOperation.Save);      
      this.workflows = this.setDefaultValues(new workflowsFormGroup(), this.workflows, new workflowsDefault());      
      this.editenabled = true;
    } else this.getworkflows();
  }

  
  getworkflows() {
    let payload: any = new workflowsDO(PostOperation.Get);
    payload.col = "workflows.id";
    payload.value = this.workflows.id;
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.reLoadParent(result['data'][0]);
    });
  }

  reLoadParent(data: any) {
    this.localStorage.set(workflowsDO.localStorage, data);
    this.workflows = data;
    this.workflowsFormComponent.loadForm();
  }

  workflows: any;
  workflowsObjectChanged(data: any) {
    this.reLoadParent(data);
  }

  workflowsSubmit(event: any) {
    this.submitenabled = false;
    // do submit    
    if (event != "") {
      let payload: any = new workflowsDO(PostOperation.Save);
      this.doSubmitAction(payload, this.workflows, event, this.apiService);
    }
  }


}

**********************************************
REPORT PAGE
**********************************************
export class WorkflowsreportsPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appworkflowsreportform') workflowsFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  showFilter : boolean = false;

  showHideFilterForm()
  {
	this.showFilter = !this.showFilter;
  }
  // oninit
  ngOnInit(): void {
    this.workflows = new workflowsDO("");      
    this.workflows = this.setDefaultValues(new workflowsFormGroup(), this.workflows, new workflowsDefault());      
    this.editenabled = true;
	this.workflowsFormComponent?.loadForm();
  }


  workflows: any;
  workflowsObjectChanged(payload: any) {
    // do Filter
	this.applyFilter(payload);
  }

  workflowsList: workflowsDO[] = [];
  applyFilter(payload:any)
  {
	// set filter logic
  } 
}

**********************************************
REPORT FORM
**********************************************
export class WorkflowsreportsFormComponent extends CustomFormComponent implements OnInit {
  @Input() workflows: any;
  @Input() editenabled: boolean = false;
  @Output() workflowsObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.workflowsForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: workflowsFormGroup = new workflowsFormGroup();
  workflowsForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.workflowsForm, this.formGroup, this.workflows);
  }

  validationResult: any;
  workflowsFormSearch() {
    let payload: any = new workflowsDO(PostOperation.List);
    payload = this.getForm(this.workflowsForm, this.formGroup, new workflowsDO(PostOperation.List));
    // validate
    this.validationResult = new workflowsReportValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.workflowsObjectChanged.emit(payload);
  }


}

**********************************************
REPORT LIST
**********************************************
export class WorkflowsreportsListComponent extends CustomListComponent implements OnInit {
  @Input() workflowsList: any[] = [];
  @Output() workflowsItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(workflows: any) {
    this.workflowsItemSelected.emit(workflows);
  }
}
