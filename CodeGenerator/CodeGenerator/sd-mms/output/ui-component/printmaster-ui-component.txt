**********************************************
FORM
**********************************************
export class PrintmasterFormComponent extends CustomFormComponent implements OnInit {
  @Input() printmaster: any;
  @Input() editenabled: boolean = false;
  @Output() printmasterObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.printmasterForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: printmasterFormGroup = new printmasterFormGroup();
  printmasterForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.printmasterForm, this.formGroup, this.printmaster);
  }

  validationResult: any;
  printmasterFormSave() {
    let payload: any = new printmasterDO(PostOperation.Save);
    payload = this.getForm(this.printmasterForm, this.formGroup, new printmasterDO(PostOperation.Save));
    // validate
    this.validationResult = new printmasterValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.apiService.doDataPost(payload).subscribe((result: any) => {
        if (!this.alertNrefresh(result, payload?.apiref, this.uiSvc)) { return; }
        this.printmaster = result['data'];
        this.printmasterObjectChanged.emit(this.printmaster);
      });
  }


}

**********************************************
LIST
**********************************************
export class PrintmasterListComponent extends CustomListComponent implements OnInit {
  @Input() printmasterList: any[] = [];
  @Output() printmasterItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(printmaster: any) {
    this.printmasterItemSelected.emit(printmaster);
  }
}

**********************************************
LIST - PAGE
**********************************************
export class PrintmasterListPageComponent extends CustomPageComponent implements OnInit {

  constructor(public apiService: ApiService, public localStorage: LocalstorageService, public uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
    this.listprintmaster();
  }

  // properties
  printmasterList: printmasterDO[] = [];
  printmaster: printmasterDO = new printmasterDO("");
  viewprintmaster(item: any) {
    // if (item == "" || item == undefined) item = new materialrequestDO(PostOperation.Save);
    this.localStorage.set(printmasterDO.localStorage, item);
    this.localStorage.set(this.originatedfrom, window.location.href);
    this.redirectTo(PageLinks.printmaster);
  }

  listprintmaster() {
    // let ob: OrderByObject = new OrderByObject("batchno", OrderByObject.SortTypeDesc);
    let payload = new printmasterDO(PostOperation.List);
    //payload.orderby.push(ob);
    let filters = this.prepareFilterObject("status", "=", this.getViewAccess('VW_printmaster_LIST')[0]?.status);
    if (filters?.length > 0) {
      payload.filters.push(this.applyOrFilter(new OrFilterObject("", ""), filters));
    }
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.printmasterList = result['data'];
      // this.printmasterList = this.applyFilter(this.printmasterList, this.getViewAccess('VW_printmaster_LIST')[0]?.status);
    });
  }
 

}



**********************************************
PAGE
**********************************************
export class PrintmasterPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appprintmasterform') printmasterFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  // oninit
  ngOnInit(): void {
    // Init Masters
    this.printmaster = this.localStorage.get(printmasterDO.localStorage);
    if (this.printmaster == "" || this.printmaster == undefined) {
      this.printmaster = new printmasterDO(PostOperation.Save);      
      this.printmaster = this.setDefaultValues(new printmasterFormGroup(), this.printmaster, new printmasterDefault());      
      this.editenabled = true;
    } else this.getprintmaster();
  }

  
  getprintmaster() {
    let payload: any = new printmasterDO(PostOperation.Get);
    payload.col = "printmaster.id";
    payload.value = this.printmaster.id;
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.reLoadParent(result['data'][0]);
    });
  }

  reLoadParent(data: any) {
    this.localStorage.set(printmasterDO.localStorage, data);
    this.printmaster = data;
    this.printmasterFormComponent.loadForm();
  }

  printmaster: any;
  printmasterObjectChanged(data: any) {
    this.reLoadParent(data);
  }

  printmasterSubmit(event: any) {
    this.submitenabled = false;
    // do submit    
    if (event != "") {
      let payload: any = new printmasterDO(PostOperation.Save);
      this.doSubmitAction(payload, this.printmaster, event, this.apiService);
    }
  }


}

**********************************************
REPORT PAGE
**********************************************
export class PrintmasterreportsPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appprintmasterreportform') printmasterFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  showFilter : boolean = false;

  showHideFilterForm()
  {
	this.showFilter = !this.showFilter;
  }
  // oninit
  ngOnInit(): void {
    this.printmaster = new printmasterDO("");      
    this.printmaster = this.setDefaultValues(new printmasterFormGroup(), this.printmaster, new printmasterDefault());      
    this.editenabled = true;
	this.printmasterFormComponent?.loadForm();
  }


  printmaster: any;
  printmasterObjectChanged(payload: any) {
    // do Filter
	this.applyFilter(payload);
  }

  printmasterList: printmasterDO[] = [];
  applyFilter(payload:any)
  {
	// set filter logic
  } 
}

**********************************************
REPORT FORM
**********************************************
export class PrintmasterreportsFormComponent extends CustomFormComponent implements OnInit {
  @Input() printmaster: any;
  @Input() editenabled: boolean = false;
  @Output() printmasterObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.printmasterForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: printmasterFormGroup = new printmasterFormGroup();
  printmasterForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.printmasterForm, this.formGroup, this.printmaster);
  }

  validationResult: any;
  printmasterFormSearch() {
    let payload: any = new printmasterDO(PostOperation.List);
    payload = this.getForm(this.printmasterForm, this.formGroup, new printmasterDO(PostOperation.List));
    // validate
    this.validationResult = new printmasterReportValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.printmasterObjectChanged.emit(payload);
  }


}

**********************************************
REPORT LIST
**********************************************
export class PrintmasterreportsListComponent extends CustomListComponent implements OnInit {
  @Input() printmasterList: any[] = [];
  @Output() printmasterItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(printmaster: any) {
    this.printmasterItemSelected.emit(printmaster);
  }
}
