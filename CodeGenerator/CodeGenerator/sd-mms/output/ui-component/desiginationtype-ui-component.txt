**********************************************
FORM
**********************************************
export class DesiginationtypeFormComponent extends CustomFormComponent implements OnInit {
  @Input() desiginationtype: any;
  @Input() editenabled: boolean = false;
  @Output() desiginationtypeObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.desiginationtypeForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: desiginationtypeFormGroup = new desiginationtypeFormGroup();
  desiginationtypeForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.desiginationtypeForm, this.formGroup, this.desiginationtype);
  }

  validationResult: any;
  desiginationtypeFormSave() {
    let payload: any = new desiginationtypeDO(PostOperation.Save);
    payload = this.getForm(this.desiginationtypeForm, this.formGroup, new desiginationtypeDO(PostOperation.Save));
    // validate
    this.validationResult = new desiginationtypeValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.apiService.doDataPost(payload).subscribe((result: any) => {
        if (!this.alertNrefresh(result, payload?.apiref, this.uiSvc)) { return; }
        this.desiginationtype = result['data'];
        this.desiginationtypeObjectChanged.emit(this.desiginationtype);
      });
  }


}

**********************************************
LIST
**********************************************
export class DesiginationtypeListComponent extends CustomListComponent implements OnInit {
  @Input() desiginationtypeList: any[] = [];
  @Output() desiginationtypeItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(desiginationtype: any) {
    this.desiginationtypeItemSelected.emit(desiginationtype);
  }
}

**********************************************
LIST - PAGE
**********************************************
export class DesiginationtypeListPageComponent extends CustomPageComponent implements OnInit {

  constructor(public apiService: ApiService, public localStorage: LocalstorageService, public uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
    this.listdesiginationtype();
  }

  // properties
  desiginationtypeList: desiginationtypeDO[] = [];
  desiginationtype: desiginationtypeDO = new desiginationtypeDO("");
  viewdesiginationtype(item: any) {
    // if (item == "" || item == undefined) item = new materialrequestDO(PostOperation.Save);
    this.localStorage.set(desiginationtypeDO.localStorage, item);
    this.localStorage.set(this.originatedfrom, window.location.href);
    this.redirectTo(PageLinks.desiginationtype);
  }

  listdesiginationtype() {
    // let ob: OrderByObject = new OrderByObject("batchno", OrderByObject.SortTypeDesc);
    let payload = new desiginationtypeDO(PostOperation.List);
    //payload.orderby.push(ob);
    let filters = this.prepareFilterObject("status", "=", this.getViewAccess('VW_desiginationtype_LIST')[0]?.status);
    if (filters?.length > 0) {
      payload.filters.push(this.applyOrFilter(new OrFilterObject("", ""), filters));
    }
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.desiginationtypeList = result['data'];
      // this.desiginationtypeList = this.applyFilter(this.desiginationtypeList, this.getViewAccess('VW_desiginationtype_LIST')[0]?.status);
    });
  }
 

}



**********************************************
PAGE
**********************************************
export class DesiginationtypePageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appdesiginationtypeform') desiginationtypeFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  // oninit
  ngOnInit(): void {
    // Init Masters
    this.desiginationtype = this.localStorage.get(desiginationtypeDO.localStorage);
    if (this.desiginationtype == "" || this.desiginationtype == undefined) {
      this.desiginationtype = new desiginationtypeDO(PostOperation.Save);      
      this.desiginationtype = this.setDefaultValues(new desiginationtypeFormGroup(), this.desiginationtype, new desiginationtypeDefault());      
      this.editenabled = true;
    } else this.getdesiginationtype();
  }

  
  getdesiginationtype() {
    let payload: any = new desiginationtypeDO(PostOperation.Get);
    payload.col = "desiginationtype.id";
    payload.value = this.desiginationtype.id;
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.reLoadParent(result['data'][0]);
    });
  }

  reLoadParent(data: any) {
    this.localStorage.set(desiginationtypeDO.localStorage, data);
    this.desiginationtype = data;
    this.desiginationtypeFormComponent.loadForm();
  }

  desiginationtype: any;
  desiginationtypeObjectChanged(data: any) {
    this.reLoadParent(data);
  }

  desiginationtypeSubmit(event: any) {
    this.submitenabled = false;
    // do submit    
    if (event != "") {
      let payload: any = new desiginationtypeDO(PostOperation.Save);
      this.doSubmitAction(payload, this.desiginationtype, event, this.apiService);
    }
  }


}

**********************************************
REPORT PAGE
**********************************************
export class DesiginationtypereportsPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appdesiginationtypereportform') desiginationtypeFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  showFilter : boolean = false;

  showHideFilterForm()
  {
	this.showFilter = !this.showFilter;
  }
  // oninit
  ngOnInit(): void {
    this.desiginationtype = new desiginationtypeDO("");      
    this.desiginationtype = this.setDefaultValues(new desiginationtypeFormGroup(), this.desiginationtype, new desiginationtypeDefault());      
    this.editenabled = true;
	this.desiginationtypeFormComponent?.loadForm();
  }


  desiginationtype: any;
  desiginationtypeObjectChanged(payload: any) {
    // do Filter
	this.applyFilter(payload);
  }

  desiginationtypeList: desiginationtypeDO[] = [];
  applyFilter(payload:any)
  {
	// set filter logic
  } 
}

**********************************************
REPORT FORM
**********************************************
export class DesiginationtypereportsFormComponent extends CustomFormComponent implements OnInit {
  @Input() desiginationtype: any;
  @Input() editenabled: boolean = false;
  @Output() desiginationtypeObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.desiginationtypeForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: desiginationtypeFormGroup = new desiginationtypeFormGroup();
  desiginationtypeForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.desiginationtypeForm, this.formGroup, this.desiginationtype);
  }

  validationResult: any;
  desiginationtypeFormSearch() {
    let payload: any = new desiginationtypeDO(PostOperation.List);
    payload = this.getForm(this.desiginationtypeForm, this.formGroup, new desiginationtypeDO(PostOperation.List));
    // validate
    this.validationResult = new desiginationtypeReportValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.desiginationtypeObjectChanged.emit(payload);
  }


}

**********************************************
REPORT LIST
**********************************************
export class DesiginationtypereportsListComponent extends CustomListComponent implements OnInit {
  @Input() desiginationtypeList: any[] = [];
  @Output() desiginationtypeItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(desiginationtype: any) {
    this.desiginationtypeItemSelected.emit(desiginationtype);
  }
}
