**********************************************
FORM
**********************************************
export class WorkordermaterialsFormComponent extends CustomFormComponent implements OnInit {
  @Input() workordermaterials: any;
  @Input() editenabled: boolean = false;
  @Output() workordermaterialsObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.workordermaterialsForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: workordermaterialsFormGroup = new workordermaterialsFormGroup();
  workordermaterialsForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.workordermaterialsForm, this.formGroup, this.workordermaterials);
  }

  validationResult: any;
  workordermaterialsFormSave() {
    let payload: any = new workordermaterialsDO(PostOperation.Save);
    payload = this.getForm(this.workordermaterialsForm, this.formGroup, new workordermaterialsDO(PostOperation.Save));
    // validate
    this.validationResult = new workordermaterialsValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.apiService.doDataPost(payload).subscribe((result: any) => {
        if (!this.alertNrefresh(result, payload?.apiref, this.uiSvc)) { return; }
        this.workordermaterials = result['data'];
        this.workordermaterialsObjectChanged.emit(this.workordermaterials);
      });
  }


}

**********************************************
LIST
**********************************************
export class WorkordermaterialsListComponent extends CustomListComponent implements OnInit {
  @Input() workordermaterialsList: any[] = [];
  @Output() workordermaterialsItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(workordermaterials: any) {
    this.workordermaterialsItemSelected.emit(workordermaterials);
  }
}

**********************************************
LIST - PAGE
**********************************************
export class WorkordermaterialsListPageComponent extends CustomPageComponent implements OnInit {

  constructor(public apiService: ApiService, public localStorage: LocalstorageService, public uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
    this.listworkordermaterials();
  }

  // properties
  workordermaterialsList: workordermaterialsDO[] = [];
  workordermaterials: workordermaterialsDO = new workordermaterialsDO("");
  viewworkordermaterials(item: any) {
    // if (item == "" || item == undefined) item = new materialrequestDO(PostOperation.Save);
    this.localStorage.set(workordermaterialsDO.localStorage, item);
    this.localStorage.set(this.originatedfrom, window.location.href);
    this.redirectTo(PageLinks.workordermaterials);
  }

  listworkordermaterials() {
    // let ob: OrderByObject = new OrderByObject("batchno", OrderByObject.SortTypeDesc);
    let payload = new workordermaterialsDO(PostOperation.List);
    //payload.orderby.push(ob);
    let filters = this.prepareFilterObject("status", "=", this.getViewAccess('VW_workordermaterials_LIST')[0]?.status);
    if (filters?.length > 0) {
      payload.filters.push(this.applyOrFilter(new OrFilterObject("", ""), filters));
    }
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.workordermaterialsList = result['data'];
      // this.workordermaterialsList = this.applyFilter(this.workordermaterialsList, this.getViewAccess('VW_workordermaterials_LIST')[0]?.status);
    });
  }
 

}



**********************************************
PAGE
**********************************************
export class WorkordermaterialsPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appworkordermaterialsform') workordermaterialsFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  // oninit
  ngOnInit(): void {
    // Init Masters
    this.workordermaterials = this.localStorage.get(workordermaterialsDO.localStorage);
    if (this.workordermaterials == "" || this.workordermaterials == undefined) {
      this.workordermaterials = new workordermaterialsDO(PostOperation.Save);      
      this.workordermaterials = this.setDefaultValues(new workordermaterialsFormGroup(), this.workordermaterials, new workordermaterialsDefault());      
      this.editenabled = true;
    } else this.getworkordermaterials();
  }

  
  getworkordermaterials() {
    let payload: any = new workordermaterialsDO(PostOperation.Get);
    payload.col = "workordermaterials.id";
    payload.value = this.workordermaterials.id;
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.reLoadParent(result['data'][0]);
    });
  }

  reLoadParent(data: any) {
    this.localStorage.set(workordermaterialsDO.localStorage, data);
    this.workordermaterials = data;
    this.workordermaterialsFormComponent.loadForm();
  }

  workordermaterials: any;
  workordermaterialsObjectChanged(data: any) {
    this.reLoadParent(data);
  }

  workordermaterialsSubmit(event: any) {
    this.submitenabled = false;
    // do submit    
    if (event != "") {
      let payload: any = new workordermaterialsDO(PostOperation.Save);
      this.doSubmitAction(payload, this.workordermaterials, event, this.apiService);
    }
  }


}

**********************************************
REPORT PAGE
**********************************************
export class WorkordermaterialsreportsPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appworkordermaterialsreportform') workordermaterialsFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  showFilter : boolean = false;

  showHideFilterForm()
  {
	this.showFilter = !this.showFilter;
  }
  // oninit
  ngOnInit(): void {
    this.workordermaterials = new workordermaterialsDO("");      
    this.workordermaterials = this.setDefaultValues(new workordermaterialsFormGroup(), this.workordermaterials, new workordermaterialsDefault());      
    this.editenabled = true;
	this.workordermaterialsFormComponent?.loadForm();
  }


  workordermaterials: any;
  workordermaterialsObjectChanged(payload: any) {
    // do Filter
	this.applyFilter(payload);
  }

  workordermaterialsList: workordermaterialsDO[] = [];
  applyFilter(payload:any)
  {
	// set filter logic
  } 
}

**********************************************
REPORT FORM
**********************************************
export class WorkordermaterialsreportsFormComponent extends CustomFormComponent implements OnInit {
  @Input() workordermaterials: any;
  @Input() editenabled: boolean = false;
  @Output() workordermaterialsObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.workordermaterialsForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: workordermaterialsFormGroup = new workordermaterialsFormGroup();
  workordermaterialsForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.workordermaterialsForm, this.formGroup, this.workordermaterials);
  }

  validationResult: any;
  workordermaterialsFormSearch() {
    let payload: any = new workordermaterialsDO(PostOperation.List);
    payload = this.getForm(this.workordermaterialsForm, this.formGroup, new workordermaterialsDO(PostOperation.List));
    // validate
    this.validationResult = new workordermaterialsReportValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.workordermaterialsObjectChanged.emit(payload);
  }


}

**********************************************
REPORT LIST
**********************************************
export class WorkordermaterialsreportsListComponent extends CustomListComponent implements OnInit {
  @Input() workordermaterialsList: any[] = [];
  @Output() workordermaterialsItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(workordermaterials: any) {
    this.workordermaterialsItemSelected.emit(workordermaterials);
  }
}
