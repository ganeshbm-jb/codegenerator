**********************************************
FORM
**********************************************
export class ItemmaterialsFormComponent extends CustomFormComponent implements OnInit {
  @Input() itemmaterials: any;
  @Input() editenabled: boolean = false;
  @Output() itemmaterialsObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.itemmaterialsForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: itemmaterialsFormGroup = new itemmaterialsFormGroup();
  itemmaterialsForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.itemmaterialsForm, this.formGroup, this.itemmaterials);
  }

  validationResult: any;
  itemmaterialsFormSave() {
    let payload: any = new itemmaterialsDO(PostOperation.Save);
    payload = this.getForm(this.itemmaterialsForm, this.formGroup, new itemmaterialsDO(PostOperation.Save));
    // validate
    this.validationResult = new itemmaterialsValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.apiService.doDataPost(payload).subscribe((result: any) => {
        if (!this.alertNrefresh(result, payload?.apiref, this.uiSvc)) { return; }
        this.itemmaterials = result['data'];
        this.itemmaterialsObjectChanged.emit(this.itemmaterials);
      });
  }


}

**********************************************
LIST
**********************************************
export class ItemmaterialsListComponent extends CustomListComponent implements OnInit {
  @Input() itemmaterialsList: any[] = [];
  @Output() itemmaterialsItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(itemmaterials: any) {
    this.itemmaterialsItemSelected.emit(itemmaterials);
  }
}

**********************************************
LIST - PAGE
**********************************************
export class ItemmaterialsListPageComponent extends CustomPageComponent implements OnInit {

  constructor(public apiService: ApiService, public localStorage: LocalstorageService, public uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
    this.listitemmaterials();
  }

  // properties
  itemmaterialsList: itemmaterialsDO[] = [];
  itemmaterials: itemmaterialsDO = new itemmaterialsDO("");
  viewitemmaterials(item: any) {
    // if (item == "" || item == undefined) item = new materialrequestDO(PostOperation.Save);
    this.localStorage.set(itemmaterialsDO.localStorage, item);
    this.localStorage.set(this.originatedfrom, window.location.href);
    this.redirectTo(PageLinks.itemmaterials);
  }

  listitemmaterials() {
    // let ob: OrderByObject = new OrderByObject("batchno", OrderByObject.SortTypeDesc);
    let payload = new itemmaterialsDO(PostOperation.List);
    //payload.orderby.push(ob);
    let filters = this.prepareFilterObject("status", "=", this.getViewAccess('VW_itemmaterials_LIST')[0]?.status);
    if (filters?.length > 0) {
      payload.filters.push(this.applyOrFilter(new OrFilterObject("", ""), filters));
    }
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.itemmaterialsList = result['data'];
      // this.itemmaterialsList = this.applyFilter(this.itemmaterialsList, this.getViewAccess('VW_itemmaterials_LIST')[0]?.status);
    });
  }
 

}



**********************************************
PAGE
**********************************************
export class ItemmaterialsPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appitemmaterialsform') itemmaterialsFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  // oninit
  ngOnInit(): void {
    // Init Masters
    this.itemmaterials = this.localStorage.get(itemmaterialsDO.localStorage);
    if (this.itemmaterials == "" || this.itemmaterials == undefined) {
      this.itemmaterials = new itemmaterialsDO(PostOperation.Save);      
      this.itemmaterials = this.setDefaultValues(new itemmaterialsFormGroup(), this.itemmaterials, new itemmaterialsDefault());      
      this.editenabled = true;
    } else this.getitemmaterials();
  }

  
  getitemmaterials() {
    let payload: any = new itemmaterialsDO(PostOperation.Get);
    payload.col = "itemmaterials.id";
    payload.value = this.itemmaterials.id;
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.reLoadParent(result['data'][0]);
    });
  }

  reLoadParent(data: any) {
    this.localStorage.set(itemmaterialsDO.localStorage, data);
    this.itemmaterials = data;
    this.itemmaterialsFormComponent.loadForm();
  }

  itemmaterials: any;
  itemmaterialsObjectChanged(data: any) {
    this.reLoadParent(data);
  }

  itemmaterialsSubmit(event: any) {
    this.submitenabled = false;
    // do submit    
    if (event != "") {
      let payload: any = new itemmaterialsDO(PostOperation.Save);
      this.doSubmitAction(payload, this.itemmaterials, event, this.apiService);
    }
  }


}

**********************************************
REPORT PAGE
**********************************************
export class ItemmaterialsreportsPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('appitemmaterialsreportform') itemmaterialsFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  showFilter : boolean = false;

  showHideFilterForm()
  {
	this.showFilter = !this.showFilter;
  }
  // oninit
  ngOnInit(): void {
    this.itemmaterials = new itemmaterialsDO("");      
    this.itemmaterials = this.setDefaultValues(new itemmaterialsFormGroup(), this.itemmaterials, new itemmaterialsDefault());      
    this.editenabled = true;
	this.itemmaterialsFormComponent?.loadForm();
  }


  itemmaterials: any;
  itemmaterialsObjectChanged(payload: any) {
    // do Filter
	this.applyFilter(payload);
  }

  itemmaterialsList: itemmaterialsDO[] = [];
  applyFilter(payload:any)
  {
	// set filter logic
  } 
}

**********************************************
REPORT FORM
**********************************************
export class ItemmaterialsreportsFormComponent extends CustomFormComponent implements OnInit {
  @Input() itemmaterials: any;
  @Input() editenabled: boolean = false;
  @Output() itemmaterialsObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.itemmaterialsForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: itemmaterialsFormGroup = new itemmaterialsFormGroup();
  itemmaterialsForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.itemmaterialsForm, this.formGroup, this.itemmaterials);
  }

  validationResult: any;
  itemmaterialsFormSearch() {
    let payload: any = new itemmaterialsDO(PostOperation.List);
    payload = this.getForm(this.itemmaterialsForm, this.formGroup, new itemmaterialsDO(PostOperation.List));
    // validate
    this.validationResult = new itemmaterialsReportValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.itemmaterialsObjectChanged.emit(payload);
  }


}

**********************************************
REPORT LIST
**********************************************
export class ItemmaterialsreportsListComponent extends CustomListComponent implements OnInit {
  @Input() itemmaterialsList: any[] = [];
  @Output() itemmaterialsItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(itemmaterials: any) {
    this.itemmaterialsItemSelected.emit(itemmaterials);
  }
}
