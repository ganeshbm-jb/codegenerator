**********************************************
FORM
**********************************************
export class RequesttypeFormComponent extends CustomFormComponent implements OnInit {
  @Input() requesttype: any;
  @Input() editenabled: boolean = false;
  @Output() requesttypeObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.requesttypeForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: requesttypeFormGroup = new requesttypeFormGroup();
  requesttypeForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.requesttypeForm, this.formGroup, this.requesttype);
  }

  validationResult: any;
  requesttypeFormSave() {
    let payload: any = new requesttypeDO(PostOperation.Save);
    payload = this.getForm(this.requesttypeForm, this.formGroup, new requesttypeDO(PostOperation.Save));
    // validate
    this.validationResult = new requesttypeValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.apiService.doDataPost(payload).subscribe((result: any) => {
        if (!this.alertNrefresh(result, payload?.apiref, this.uiSvc)) { return; }
        this.requesttype = result['data'];
        this.requesttypeObjectChanged.emit(this.requesttype);
      });
  }


}

**********************************************
LIST
**********************************************
export class RequesttypeListComponent extends CustomListComponent implements OnInit {
  @Input() requesttypeList: any[] = [];
  @Output() requesttypeItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(requesttype: any) {
    this.requesttypeItemSelected.emit(requesttype);
  }
}

**********************************************
LIST - PAGE
**********************************************
export class RequesttypeListPageComponent extends CustomPageComponent implements OnInit {

  constructor(public apiService: ApiService, public localStorage: LocalstorageService, public uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
    this.listrequesttype();
  }

  // properties
  requesttypeList: requesttypeDO[] = [];
  requesttype: requesttypeDO = new requesttypeDO("");
  viewrequesttype(item: any) {
    // if (item == "" || item == undefined) item = new materialrequestDO(PostOperation.Save);
    this.localStorage.set(requesttypeDO.localStorage, item);
    this.localStorage.set(this.originatedfrom, window.location.href);
    this.redirectTo(PageLinks.requesttype);
  }

  listrequesttype() {
    // let ob: OrderByObject = new OrderByObject("batchno", OrderByObject.SortTypeDesc);
    let payload = new requesttypeDO(PostOperation.List);
    //payload.orderby.push(ob);
    let filters = this.prepareFilterObject("status", "=", this.getViewAccess('VW_requesttype_LIST')[0]?.status);
    if (filters?.length > 0) {
      payload.filters.push(this.applyOrFilter(new OrFilterObject("", ""), filters));
    }
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.requesttypeList = result['data'];
      // this.requesttypeList = this.applyFilter(this.requesttypeList, this.getViewAccess('VW_requesttype_LIST')[0]?.status);
    });
  }
 

}



**********************************************
PAGE
**********************************************
export class RequesttypePageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('apprequesttypeform') requesttypeFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  // oninit
  ngOnInit(): void {
    // Init Masters
    this.requesttype = this.localStorage.get(requesttypeDO.localStorage);
    if (this.requesttype == "" || this.requesttype == undefined) {
      this.requesttype = new requesttypeDO(PostOperation.Save);      
      this.requesttype = this.setDefaultValues(new requesttypeFormGroup(), this.requesttype, new requesttypeDefault());      
      this.editenabled = true;
    } else this.getrequesttype();
  }

  
  getrequesttype() {
    let payload: any = new requesttypeDO(PostOperation.Get);
    payload.col = "requesttype.id";
    payload.value = this.requesttype.id;
    this.apiService.doDataPost(payload).subscribe((result: any) => {
      this.reLoadParent(result['data'][0]);
    });
  }

  reLoadParent(data: any) {
    this.localStorage.set(requesttypeDO.localStorage, data);
    this.requesttype = data;
    this.requesttypeFormComponent.loadForm();
  }

  requesttype: any;
  requesttypeObjectChanged(data: any) {
    this.reLoadParent(data);
  }

  requesttypeSubmit(event: any) {
    this.submitenabled = false;
    // do submit    
    if (event != "") {
      let payload: any = new requesttypeDO(PostOperation.Save);
      this.doSubmitAction(payload, this.requesttype, event, this.apiService);
    }
  }


}

**********************************************
REPORT PAGE
**********************************************
export class RequesttypereportsPageComponent extends CustomPageComponent implements OnInit, AfterViewInit {
  @ViewChild('apprequesttypereportform') requesttypeFormComponent: any;
  // constructor
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }
  ngAfterViewInit(): void {

  }

  showFilter : boolean = false;

  showHideFilterForm()
  {
	this.showFilter = !this.showFilter;
  }
  // oninit
  ngOnInit(): void {
    this.requesttype = new requesttypeDO("");      
    this.requesttype = this.setDefaultValues(new requesttypeFormGroup(), this.requesttype, new requesttypeDefault());      
    this.editenabled = true;
	this.requesttypeFormComponent?.loadForm();
  }


  requesttype: any;
  requesttypeObjectChanged(payload: any) {
    // do Filter
	this.applyFilter(payload);
  }

  requesttypeList: requesttypeDO[] = [];
  applyFilter(payload:any)
  {
	// set filter logic
  } 
}

**********************************************
REPORT FORM
**********************************************
export class RequesttypereportsFormComponent extends CustomFormComponent implements OnInit {
  @Input() requesttype: any;
  @Input() editenabled: boolean = false;
  @Output() requesttypeObjectChanged = new EventEmitter();
  constructor(private formBuilder: FormBuilder, public apiService: ApiService, public localStorage: LocalstorageService, private uiSvc: UiUtilsService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
    // dynamic control load
    this.requesttypeForm = this.formGroup.initializeForm(this.formBuilder);
  }

  // properties
  formGroup: requesttypeFormGroup = new requesttypeFormGroup();
  requesttypeForm: FormGroup;
  idx: number = 0;

  // methods
  ngOnInit(): void {
    this.loadForm();
  }

  loadForm() {
    this.bindForm(this.requesttypeForm, this.formGroup, this.requesttype);
  }

  validationResult: any;
  requesttypeFormSearch() {
    let payload: any = new requesttypeDO(PostOperation.List);
    payload = this.getForm(this.requesttypeForm, this.formGroup, new requesttypeDO(PostOperation.List));
    // validate
    this.validationResult = new requesttypeReportValidator(payload).validate();
    if (!this.validationResult.validated) {
      return;
    }
    else
      this.requesttypeObjectChanged.emit(payload);
  }


}

**********************************************
REPORT LIST
**********************************************
export class RequesttypereportsListComponent extends CustomListComponent implements OnInit {
  @Input() requesttypeList: any[] = [];
  @Output() requesttypeItemSelected = new EventEmitter();
  constructor(public apiService: ApiService, public localStorage: LocalstorageService) {
    super(apiService);
    this.initMasters(this.apiService, this.localStorage, false);
  }

  ngOnInit(): void {
  }

  viewClicked(requesttype: any) {
    this.requesttypeItemSelected.emit(requesttype);
  }
}
