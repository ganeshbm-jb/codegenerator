<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\usermenus;
use DB;
use Webpatser\Uuid\Uuid;

class usermenusserviceex extends BaseService
{
	// Method to save data
	public static function saveusermenus_before($id, $input)
	{
		return $input;
	}
	public static function saveusermenus_after($id, $input)
	{
	}

	// Method to get all records
	public static function listusermenus_before($request)
	{		
		return $request;
	}
	public static function listusermenus_after($request)
	{		
	}


	// Method to get a record
	public static function getusermenus_before($request)
	{	
		return $request;	
	}
	public static function getusermenus_after($request)
	{		
	}
}
