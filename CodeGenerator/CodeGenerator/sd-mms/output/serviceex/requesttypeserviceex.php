<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\requesttype;
use DB;
use Webpatser\Uuid\Uuid;

class requesttypeserviceex extends BaseService
{
	// Method to save data
	public static function saverequesttype_before($id, $input)
	{
		return $input;
	}
	public static function saverequesttype_after($id, $input)
	{
	}

	// Method to get all records
	public static function listrequesttype_before($request)
	{		
		return $request;
	}
	public static function listrequesttype_after($request)
	{		
	}


	// Method to get a record
	public static function getrequesttype_before($request)
	{	
		return $request;	
	}
	public static function getrequesttype_after($request)
	{		
	}
}
