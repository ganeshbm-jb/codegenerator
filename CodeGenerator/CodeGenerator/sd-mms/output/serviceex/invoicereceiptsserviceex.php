<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\invoicereceipts;
use DB;
use Webpatser\Uuid\Uuid;

class invoicereceiptsserviceex extends BaseService
{
	// Method to save data
	public static function saveinvoicereceipts_before($id, $input)
	{
		return $input;
	}
	public static function saveinvoicereceipts_after($id, $input)
	{
	}

	// Method to get all records
	public static function listinvoicereceipts_before($request)
	{		
		return $request;
	}
	public static function listinvoicereceipts_after($request)
	{		
	}


	// Method to get a record
	public static function getinvoicereceipts_before($request)
	{	
		return $request;	
	}
	public static function getinvoicereceipts_after($request)
	{		
	}
}
