<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\advance;
use DB;
use Webpatser\Uuid\Uuid;

class advanceserviceex extends BaseService
{
	// Method to save data
	public static function saveadvance_before($id, $input)
	{
		return $input;
	}
	public static function saveadvance_after($id, $input)
	{
	}

	// Method to get all records
	public static function listadvance_before($request)
	{		
		return $request;
	}
	public static function listadvance_after($request)
	{		
	}


	// Method to get a record
	public static function getadvance_before($request)
	{	
		return $request;	
	}
	public static function getadvance_after($request)
	{		
	}
}
