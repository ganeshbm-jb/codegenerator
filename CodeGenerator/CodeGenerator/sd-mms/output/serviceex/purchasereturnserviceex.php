<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\purchasereturn;
use DB;
use Webpatser\Uuid\Uuid;

class purchasereturnserviceex extends BaseService
{
	// Method to save data
	public static function savepurchasereturn_before($id, $input)
	{
		return $input;
	}
	public static function savepurchasereturn_after($id, $input)
	{
	}

	// Method to get all records
	public static function listpurchasereturn_before($request)
	{		
		return $request;
	}
	public static function listpurchasereturn_after($request)
	{		
	}


	// Method to get a record
	public static function getpurchasereturn_before($request)
	{	
		return $request;	
	}
	public static function getpurchasereturn_after($request)
	{		
	}
}
