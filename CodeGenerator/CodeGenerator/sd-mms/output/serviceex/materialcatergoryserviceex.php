<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\materialcatergory;
use DB;
use Webpatser\Uuid\Uuid;

class materialcatergoryserviceex extends BaseService
{
	// Method to save data
	public static function savematerialcatergory_before($id, $input)
	{
		return $input;
	}
	public static function savematerialcatergory_after($id, $input)
	{
	}

	// Method to get all records
	public static function listmaterialcatergory_before($request)
	{		
		return $request;
	}
	public static function listmaterialcatergory_after($request)
	{		
	}


	// Method to get a record
	public static function getmaterialcatergory_before($request)
	{	
		return $request;	
	}
	public static function getmaterialcatergory_after($request)
	{		
	}
}
