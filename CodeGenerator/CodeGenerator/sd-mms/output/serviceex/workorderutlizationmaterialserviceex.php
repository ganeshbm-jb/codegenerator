<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\workorderutlizationmaterial;
use DB;
use Webpatser\Uuid\Uuid;

class workorderutlizationmaterialserviceex extends BaseService
{
	// Method to save data
	public static function saveworkorderutlizationmaterial_before($id, $input)
	{
		return $input;
	}
	public static function saveworkorderutlizationmaterial_after($id, $input)
	{
	}

	// Method to get all records
	public static function listworkorderutlizationmaterial_before($request)
	{		
		return $request;
	}
	public static function listworkorderutlizationmaterial_after($request)
	{		
	}


	// Method to get a record
	public static function getworkorderutlizationmaterial_before($request)
	{	
		return $request;	
	}
	public static function getworkorderutlizationmaterial_after($request)
	{		
	}
}
