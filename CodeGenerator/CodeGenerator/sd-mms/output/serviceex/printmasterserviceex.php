<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\printmaster;
use DB;
use Webpatser\Uuid\Uuid;

class printmasterserviceex extends BaseService
{
	// Method to save data
	public static function saveprintmaster_before($id, $input)
	{
		return $input;
	}
	public static function saveprintmaster_after($id, $input)
	{
	}

	// Method to get all records
	public static function listprintmaster_before($request)
	{		
		return $request;
	}
	public static function listprintmaster_after($request)
	{		
	}


	// Method to get a record
	public static function getprintmaster_before($request)
	{	
		return $request;	
	}
	public static function getprintmaster_after($request)
	{		
	}
}
