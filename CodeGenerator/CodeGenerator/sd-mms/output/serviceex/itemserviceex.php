<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\item;
use DB;
use Webpatser\Uuid\Uuid;

class itemserviceex extends BaseService
{
	// Method to save data
	public static function saveitem_before($id, $input)
	{
		return $input;
	}
	public static function saveitem_after($id, $input)
	{
	}

	// Method to get all records
	public static function listitem_before($request)
	{		
		return $request;
	}
	public static function listitem_after($request)
	{		
	}


	// Method to get a record
	public static function getitem_before($request)
	{	
		return $request;	
	}
	public static function getitem_after($request)
	{		
	}
}
