<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\ledger;
use DB;
use Webpatser\Uuid\Uuid;

class ledgerserviceex extends BaseService
{
	// Method to save data
	public static function saveledger_before($id, $input)
	{
		return $input;
	}
	public static function saveledger_after($id, $input)
	{
	}

	// Method to get all records
	public static function listledger_before($request)
	{		
		return $request;
	}
	public static function listledger_after($request)
	{		
	}


	// Method to get a record
	public static function getledger_before($request)
	{	
		return $request;	
	}
	public static function getledger_after($request)
	{		
	}
}
