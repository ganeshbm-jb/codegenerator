<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\purchasematerial;
use DB;
use Webpatser\Uuid\Uuid;

class purchasematerialserviceex extends BaseService
{
	// Method to save data
	public static function savepurchasematerial_before($id, $input)
	{
		return $input;
	}
	public static function savepurchasematerial_after($id, $input)
	{
	}

	// Method to get all records
	public static function listpurchasematerial_before($request)
	{		
		return $request;
	}
	public static function listpurchasematerial_after($request)
	{		
	}


	// Method to get a record
	public static function getpurchasematerial_before($request)
	{	
		return $request;	
	}
	public static function getpurchasematerial_after($request)
	{		
	}
}
