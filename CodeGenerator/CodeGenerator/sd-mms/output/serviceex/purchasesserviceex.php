<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\purchases;
use DB;
use Webpatser\Uuid\Uuid;

class purchasesserviceex extends BaseService
{
	// Method to save data
	public static function savepurchases_before($id, $input)
	{
		return $input;
	}
	public static function savepurchases_after($id, $input)
	{
	}

	// Method to get all records
	public static function listpurchases_before($request)
	{		
		return $request;
	}
	public static function listpurchases_after($request)
	{		
	}


	// Method to get a record
	public static function getpurchases_before($request)
	{	
		return $request;	
	}
	public static function getpurchases_after($request)
	{		
	}
}
