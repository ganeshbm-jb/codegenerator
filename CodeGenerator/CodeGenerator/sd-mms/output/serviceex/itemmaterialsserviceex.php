<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\itemmaterials;
use DB;
use Webpatser\Uuid\Uuid;

class itemmaterialsserviceex extends BaseService
{
	// Method to save data
	public static function saveitemmaterials_before($id, $input)
	{
		return $input;
	}
	public static function saveitemmaterials_after($id, $input)
	{
	}

	// Method to get all records
	public static function listitemmaterials_before($request)
	{		
		return $request;
	}
	public static function listitemmaterials_after($request)
	{		
	}


	// Method to get a record
	public static function getitemmaterials_before($request)
	{	
		return $request;	
	}
	public static function getitemmaterials_after($request)
	{		
	}
}
