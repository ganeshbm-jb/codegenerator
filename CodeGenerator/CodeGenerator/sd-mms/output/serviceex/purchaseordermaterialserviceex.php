<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\purchaseordermaterial;
use DB;
use Webpatser\Uuid\Uuid;

class purchaseordermaterialserviceex extends BaseService
{
	// Method to save data
	public static function savepurchaseordermaterial_before($id, $input)
	{
		return $input;
	}
	public static function savepurchaseordermaterial_after($id, $input)
	{
	}

	// Method to get all records
	public static function listpurchaseordermaterial_before($request)
	{		
		return $request;
	}
	public static function listpurchaseordermaterial_after($request)
	{		
	}


	// Method to get a record
	public static function getpurchaseordermaterial_before($request)
	{	
		return $request;	
	}
	public static function getpurchaseordermaterial_after($request)
	{		
	}
}
