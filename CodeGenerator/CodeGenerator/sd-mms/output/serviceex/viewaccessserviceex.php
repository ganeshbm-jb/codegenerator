<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\viewaccess;
use DB;
use Webpatser\Uuid\Uuid;

class viewaccessserviceex extends BaseService
{
	// Method to save data
	public static function saveviewaccess_before($id, $input)
	{
		return $input;
	}
	public static function saveviewaccess_after($id, $input)
	{
	}

	// Method to get all records
	public static function listviewaccess_before($request)
	{		
		return $request;
	}
	public static function listviewaccess_after($request)
	{		
	}


	// Method to get a record
	public static function getviewaccess_before($request)
	{	
		return $request;	
	}
	public static function getviewaccess_after($request)
	{		
	}
}
