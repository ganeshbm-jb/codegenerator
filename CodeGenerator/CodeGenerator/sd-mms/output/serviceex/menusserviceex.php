<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\menus;
use DB;
use Webpatser\Uuid\Uuid;

class menusserviceex extends BaseService
{
	// Method to save data
	public static function savemenus_before($id, $input)
	{
		return $input;
	}
	public static function savemenus_after($id, $input)
	{
	}

	// Method to get all records
	public static function listmenus_before($request)
	{		
		return $request;
	}
	public static function listmenus_after($request)
	{		
	}


	// Method to get a record
	public static function getmenus_before($request)
	{	
		return $request;	
	}
	public static function getmenus_after($request)
	{		
	}
}
