<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\materialtype;
use DB;
use Webpatser\Uuid\Uuid;

class materialtypeserviceex extends BaseService
{
	// Method to save data
	public static function savematerialtype_before($id, $input)
	{
		return $input;
	}
	public static function savematerialtype_after($id, $input)
	{
	}

	// Method to get all records
	public static function listmaterialtype_before($request)
	{		
		return $request;
	}
	public static function listmaterialtype_after($request)
	{		
	}


	// Method to get a record
	public static function getmaterialtype_before($request)
	{	
		return $request;	
	}
	public static function getmaterialtype_after($request)
	{		
	}
}
