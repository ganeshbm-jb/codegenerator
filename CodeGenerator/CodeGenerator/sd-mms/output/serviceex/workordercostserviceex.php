<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\workordercost;
use DB;
use Webpatser\Uuid\Uuid;

class workordercostserviceex extends BaseService
{
	// Method to save data
	public static function saveworkordercost_before($id, $input)
	{
		return $input;
	}
	public static function saveworkordercost_after($id, $input)
	{
	}

	// Method to get all records
	public static function listworkordercost_before($request)
	{		
		return $request;
	}
	public static function listworkordercost_after($request)
	{		
	}


	// Method to get a record
	public static function getworkordercost_before($request)
	{	
		return $request;	
	}
	public static function getworkordercost_after($request)
	{		
	}
}
