<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\stocklocationqty;
use DB;
use Webpatser\Uuid\Uuid;

class stocklocationqtyserviceex extends BaseService
{
	// Method to save data
	public static function savestocklocationqty_before($id, $input)
	{
		return $input;
	}
	public static function savestocklocationqty_after($id, $input)
	{
	}

	// Method to get all records
	public static function liststocklocationqty_before($request)
	{		
		return $request;
	}
	public static function liststocklocationqty_after($request)
	{		
	}


	// Method to get a record
	public static function getstocklocationqty_before($request)
	{	
		return $request;	
	}
	public static function getstocklocationqty_after($request)
	{		
	}
}
