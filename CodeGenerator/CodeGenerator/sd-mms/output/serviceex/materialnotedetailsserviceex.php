<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\materialnotedetails;
use DB;
use Webpatser\Uuid\Uuid;

class materialnotedetailsserviceex extends BaseService
{
	// Method to save data
	public static function savematerialnotedetails_before($id, $input)
	{
		return $input;
	}
	public static function savematerialnotedetails_after($id, $input)
	{
	}

	// Method to get all records
	public static function listmaterialnotedetails_before($request)
	{		
		return $request;
	}
	public static function listmaterialnotedetails_after($request)
	{		
	}


	// Method to get a record
	public static function getmaterialnotedetails_before($request)
	{	
		return $request;	
	}
	public static function getmaterialnotedetails_after($request)
	{		
	}
}
