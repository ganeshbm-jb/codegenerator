<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\materialcategory;
use DB;
use Webpatser\Uuid\Uuid;

class materialcategoryserviceex extends BaseService
{
	// Method to save data
	public static function savematerialcategory_before($id, $input)
	{
		return $input;
	}
	public static function savematerialcategory_after($id, $input)
	{
	}

	// Method to get all records
	public static function listmaterialcategory_before($request)
	{		
		return $request;
	}
	public static function listmaterialcategory_after($request)
	{		
	}


	// Method to get a record
	public static function getmaterialcategory_before($request)
	{	
		return $request;	
	}
	public static function getmaterialcategory_after($request)
	{		
	}
}
