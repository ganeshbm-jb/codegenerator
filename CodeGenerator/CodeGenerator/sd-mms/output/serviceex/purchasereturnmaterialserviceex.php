<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\purchasereturnmaterial;
use DB;
use Webpatser\Uuid\Uuid;

class purchasereturnmaterialserviceex extends BaseService
{
	// Method to save data
	public static function savepurchasereturnmaterial_before($id, $input)
	{
		return $input;
	}
	public static function savepurchasereturnmaterial_after($id, $input)
	{
	}

	// Method to get all records
	public static function listpurchasereturnmaterial_before($request)
	{		
		return $request;
	}
	public static function listpurchasereturnmaterial_after($request)
	{		
	}


	// Method to get a record
	public static function getpurchasereturnmaterial_before($request)
	{	
		return $request;	
	}
	public static function getpurchasereturnmaterial_after($request)
	{		
	}
}
