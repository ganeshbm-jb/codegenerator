<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\advancetype;
use DB;
use Webpatser\Uuid\Uuid;

class advancetypeserviceex extends BaseService
{
	// Method to save data
	public static function saveadvancetype_before($id, $input)
	{
		return $input;
	}
	public static function saveadvancetype_after($id, $input)
	{
	}

	// Method to get all records
	public static function listadvancetype_before($request)
	{		
		return $request;
	}
	public static function listadvancetype_after($request)
	{		
	}


	// Method to get a record
	public static function getadvancetype_before($request)
	{	
		return $request;	
	}
	public static function getadvancetype_after($request)
	{		
	}
}
