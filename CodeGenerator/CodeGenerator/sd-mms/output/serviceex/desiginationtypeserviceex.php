<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\desiginationtype;
use DB;
use Webpatser\Uuid\Uuid;

class desiginationtypeserviceex extends BaseService
{
	// Method to save data
	public static function savedesiginationtype_before($id, $input)
	{
		return $input;
	}
	public static function savedesiginationtype_after($id, $input)
	{
	}

	// Method to get all records
	public static function listdesiginationtype_before($request)
	{		
		return $request;
	}
	public static function listdesiginationtype_after($request)
	{		
	}


	// Method to get a record
	public static function getdesiginationtype_before($request)
	{	
		return $request;	
	}
	public static function getdesiginationtype_after($request)
	{		
	}
}
