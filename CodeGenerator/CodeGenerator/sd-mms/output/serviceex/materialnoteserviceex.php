<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\materialnote;
use DB;
use Webpatser\Uuid\Uuid;

class materialnoteserviceex extends BaseService
{
	// Method to save data
	public static function savematerialnote_before($id, $input)
	{
		return $input;
	}
	public static function savematerialnote_after($id, $input)
	{
	}

	// Method to get all records
	public static function listmaterialnote_before($request)
	{		
		return $request;
	}
	public static function listmaterialnote_after($request)
	{		
	}


	// Method to get a record
	public static function getmaterialnote_before($request)
	{	
		return $request;	
	}
	public static function getmaterialnote_after($request)
	{		
	}
}
