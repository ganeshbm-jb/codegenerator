<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\stock;
use DB;
use Webpatser\Uuid\Uuid;

class stockserviceex extends BaseService
{
	// Method to save data
	public static function savestock_before($id, $input)
	{
		return $input;
	}
	public static function savestock_after($id, $input)
	{
	}

	// Method to get all records
	public static function liststock_before($request)
	{		
		return $request;
	}
	public static function liststock_after($request)
	{		
	}


	// Method to get a record
	public static function getstock_before($request)
	{	
		return $request;	
	}
	public static function getstock_after($request)
	{		
	}
}
