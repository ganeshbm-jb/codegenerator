<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\codemaster;
use DB;
use Webpatser\Uuid\Uuid;

class codemasterserviceex extends BaseService
{
	// Method to save data
	public static function savecodemaster_before($id, $input)
	{
		return $input;
	}
	public static function savecodemaster_after($id, $input)
	{
	}

	// Method to get all records
	public static function listcodemaster_before($request)
	{		
		return $request;
	}
	public static function listcodemaster_after($request)
	{		
	}


	// Method to get a record
	public static function getcodemaster_before($request)
	{	
		return $request;	
	}
	public static function getcodemaster_after($request)
	{		
	}
}
