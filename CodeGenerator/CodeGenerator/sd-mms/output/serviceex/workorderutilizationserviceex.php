<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\workorderutilization;
use DB;
use Webpatser\Uuid\Uuid;

class workorderutilizationserviceex extends BaseService
{
	// Method to save data
	public static function saveworkorderutilization_before($id, $input)
	{
		return $input;
	}
	public static function saveworkorderutilization_after($id, $input)
	{
	}

	// Method to get all records
	public static function listworkorderutilization_before($request)
	{		
		return $request;
	}
	public static function listworkorderutilization_after($request)
	{		
	}


	// Method to get a record
	public static function getworkorderutilization_before($request)
	{	
		return $request;	
	}
	public static function getworkorderutilization_after($request)
	{		
	}
}
