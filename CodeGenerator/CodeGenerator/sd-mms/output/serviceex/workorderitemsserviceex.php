<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\workorderitems;
use DB;
use Webpatser\Uuid\Uuid;

class workorderitemsserviceex extends BaseService
{
	// Method to save data
	public static function saveworkorderitems_before($id, $input)
	{
		return $input;
	}
	public static function saveworkorderitems_after($id, $input)
	{
	}

	// Method to get all records
	public static function listworkorderitems_before($request)
	{		
		return $request;
	}
	public static function listworkorderitems_after($request)
	{		
	}


	// Method to get a record
	public static function getworkorderitems_before($request)
	{	
		return $request;	
	}
	public static function getworkorderitems_after($request)
	{		
	}
}
