<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\materialrequest;
use DB;
use Webpatser\Uuid\Uuid;

class materialrequestserviceex extends BaseService
{
	// Method to save data
	public static function savematerialrequest_before($id, $input)
	{
		return $input;
	}
	public static function savematerialrequest_after($id, $input)
	{
	}

	// Method to get all records
	public static function listmaterialrequest_before($request)
	{		
		return $request;
	}
	public static function listmaterialrequest_after($request)
	{		
	}


	// Method to get a record
	public static function getmaterialrequest_before($request)
	{	
		return $request;	
	}
	public static function getmaterialrequest_after($request)
	{		
	}
}
