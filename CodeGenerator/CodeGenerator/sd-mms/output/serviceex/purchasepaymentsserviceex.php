<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\purchasepayments;
use DB;
use Webpatser\Uuid\Uuid;

class purchasepaymentsserviceex extends BaseService
{
	// Method to save data
	public static function savepurchasepayments_before($id, $input)
	{
		return $input;
	}
	public static function savepurchasepayments_after($id, $input)
	{
	}

	// Method to get all records
	public static function listpurchasepayments_before($request)
	{		
		return $request;
	}
	public static function listpurchasepayments_after($request)
	{		
	}


	// Method to get a record
	public static function getpurchasepayments_before($request)
	{	
		return $request;	
	}
	public static function getpurchasepayments_after($request)
	{		
	}
}
