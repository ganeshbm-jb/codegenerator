<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\stockvalue;
use DB;
use Webpatser\Uuid\Uuid;

class stockvalueserviceex extends BaseService
{
	// Method to save data
	public static function savestockvalue_before($id, $input)
	{
		return $input;
	}
	public static function savestockvalue_after($id, $input)
	{
	}

	// Method to get all records
	public static function liststockvalue_before($request)
	{		
		return $request;
	}
	public static function liststockvalue_after($request)
	{		
	}


	// Method to get a record
	public static function getstockvalue_before($request)
	{	
		return $request;	
	}
	public static function getstockvalue_after($request)
	{		
	}
}
