<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\workers;
use DB;
use Webpatser\Uuid\Uuid;

class workersserviceex extends BaseService
{
	// Method to save data
	public static function saveworkers_before($id, $input)
	{
		return $input;
	}
	public static function saveworkers_after($id, $input)
	{
	}

	// Method to get all records
	public static function listworkers_before($request)
	{		
		return $request;
	}
	public static function listworkers_after($request)
	{		
	}


	// Method to get a record
	public static function getworkers_before($request)
	{	
		return $request;	
	}
	public static function getworkers_after($request)
	{		
	}
}
