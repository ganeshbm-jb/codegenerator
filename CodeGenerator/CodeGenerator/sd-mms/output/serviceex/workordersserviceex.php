<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\workorders;
use DB;
use Webpatser\Uuid\Uuid;

class workordersserviceex extends BaseService
{
	// Method to save data
	public static function saveworkorders_before($id, $input)
	{
		return $input;
	}
	public static function saveworkorders_after($id, $input)
	{
	}

	// Method to get all records
	public static function listworkorders_before($request)
	{		
		return $request;
	}
	public static function listworkorders_after($request)
	{		
	}


	// Method to get a record
	public static function getworkorders_before($request)
	{	
		return $request;	
	}
	public static function getworkorders_after($request)
	{		
	}
}
