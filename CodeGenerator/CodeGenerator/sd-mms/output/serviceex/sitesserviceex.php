<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\sites;
use DB;
use Webpatser\Uuid\Uuid;

class sitesserviceex extends BaseService
{
	// Method to save data
	public static function savesites_before($id, $input)
	{
		return $input;
	}
	public static function savesites_after($id, $input)
	{
	}

	// Method to get all records
	public static function listsites_before($request)
	{		
		return $request;
	}
	public static function listsites_after($request)
	{		
	}


	// Method to get a record
	public static function getsites_before($request)
	{	
		return $request;	
	}
	public static function getsites_after($request)
	{		
	}
}
