<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\company;
use DB;
use Webpatser\Uuid\Uuid;

class companyserviceex extends BaseService
{
	// Method to save data
	public static function savecompany_before($id, $input)
	{
		return $input;
	}
	public static function savecompany_after($id, $input)
	{
	}

	// Method to get all records
	public static function listcompany_before($request)
	{		
		return $request;
	}
	public static function listcompany_after($request)
	{		
	}


	// Method to get a record
	public static function getcompany_before($request)
	{	
		return $request;	
	}
	public static function getcompany_after($request)
	{		
	}
}
