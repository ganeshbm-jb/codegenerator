<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\invoiceitem;
use DB;
use Webpatser\Uuid\Uuid;

class invoiceitemserviceex extends BaseService
{
	// Method to save data
	public static function saveinvoiceitem_before($id, $input)
	{
		return $input;
	}
	public static function saveinvoiceitem_after($id, $input)
	{
	}

	// Method to get all records
	public static function listinvoiceitem_before($request)
	{		
		return $request;
	}
	public static function listinvoiceitem_after($request)
	{		
	}


	// Method to get a record
	public static function getinvoiceitem_before($request)
	{	
		return $request;	
	}
	public static function getinvoiceitem_after($request)
	{		
	}
}
