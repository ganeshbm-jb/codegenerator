<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\salarytype;
use DB;
use Webpatser\Uuid\Uuid;

class salarytypeserviceex extends BaseService
{
	// Method to save data
	public static function savesalarytype_before($id, $input)
	{
		return $input;
	}
	public static function savesalarytype_after($id, $input)
	{
	}

	// Method to get all records
	public static function listsalarytype_before($request)
	{		
		return $request;
	}
	public static function listsalarytype_after($request)
	{		
	}


	// Method to get a record
	public static function getsalarytype_before($request)
	{	
		return $request;	
	}
	public static function getsalarytype_after($request)
	{		
	}
}
