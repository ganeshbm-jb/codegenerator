<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\stockqty;
use DB;
use Webpatser\Uuid\Uuid;

class stockqtyserviceex extends BaseService
{
	// Method to save data
	public static function savestockqty_before($id, $input)
	{
		return $input;
	}
	public static function savestockqty_after($id, $input)
	{
	}

	// Method to get all records
	public static function liststockqty_before($request)
	{		
		return $request;
	}
	public static function liststockqty_after($request)
	{		
	}


	// Method to get a record
	public static function getstockqty_before($request)
	{	
		return $request;	
	}
	public static function getstockqty_after($request)
	{		
	}
}
