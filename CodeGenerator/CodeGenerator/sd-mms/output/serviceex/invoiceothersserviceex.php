<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\invoiceothers;
use DB;
use Webpatser\Uuid\Uuid;

class invoiceothersserviceex extends BaseService
{
	// Method to save data
	public static function saveinvoiceothers_before($id, $input)
	{
		return $input;
	}
	public static function saveinvoiceothers_after($id, $input)
	{
	}

	// Method to get all records
	public static function listinvoiceothers_before($request)
	{		
		return $request;
	}
	public static function listinvoiceothers_after($request)
	{		
	}


	// Method to get a record
	public static function getinvoiceothers_before($request)
	{	
		return $request;	
	}
	public static function getinvoiceothers_after($request)
	{		
	}
}
