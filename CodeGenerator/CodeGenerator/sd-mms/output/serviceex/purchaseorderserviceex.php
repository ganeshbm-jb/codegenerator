<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\purchaseorder;
use DB;
use Webpatser\Uuid\Uuid;

class purchaseorderserviceex extends BaseService
{
	// Method to save data
	public static function savepurchaseorder_before($id, $input)
	{
		return $input;
	}
	public static function savepurchaseorder_after($id, $input)
	{
	}

	// Method to get all records
	public static function listpurchaseorder_before($request)
	{		
		return $request;
	}
	public static function listpurchaseorder_after($request)
	{		
	}


	// Method to get a record
	public static function getpurchaseorder_before($request)
	{	
		return $request;	
	}
	public static function getpurchaseorder_after($request)
	{		
	}
}
