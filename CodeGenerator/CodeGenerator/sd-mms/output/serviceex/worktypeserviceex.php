<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\worktype;
use DB;
use Webpatser\Uuid\Uuid;

class worktypeserviceex extends BaseService
{
	// Method to save data
	public static function saveworktype_before($id, $input)
	{
		return $input;
	}
	public static function saveworktype_after($id, $input)
	{
	}

	// Method to get all records
	public static function listworktype_before($request)
	{		
		return $request;
	}
	public static function listworktype_after($request)
	{		
	}


	// Method to get a record
	public static function getworktype_before($request)
	{	
		return $request;	
	}
	public static function getworktype_after($request)
	{		
	}
}
