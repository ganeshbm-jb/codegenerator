<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\units;
use DB;
use Webpatser\Uuid\Uuid;

class unitsserviceex extends BaseService
{
	// Method to save data
	public static function saveunits_before($id, $input)
	{
		return $input;
	}
	public static function saveunits_after($id, $input)
	{
	}

	// Method to get all records
	public static function listunits_before($request)
	{		
		return $request;
	}
	public static function listunits_after($request)
	{		
	}


	// Method to get a record
	public static function getunits_before($request)
	{	
		return $request;	
	}
	public static function getunits_after($request)
	{		
	}
}
