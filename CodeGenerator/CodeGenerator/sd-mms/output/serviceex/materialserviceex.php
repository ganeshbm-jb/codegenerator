<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\material;
use DB;
use Webpatser\Uuid\Uuid;

class materialserviceex extends BaseService
{
	// Method to save data
	public static function savematerial_before($id, $input)
	{
		return $input;
	}
	public static function savematerial_after($id, $input)
	{
	}

	// Method to get all records
	public static function listmaterial_before($request)
	{		
		return $request;
	}
	public static function listmaterial_after($request)
	{		
	}


	// Method to get a record
	public static function getmaterial_before($request)
	{	
		return $request;	
	}
	public static function getmaterial_after($request)
	{		
	}
}
