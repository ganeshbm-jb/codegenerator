<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\userviewaccess;
use DB;
use Webpatser\Uuid\Uuid;

class userviewaccessserviceex extends BaseService
{
	// Method to save data
	public static function saveuserviewaccess_before($id, $input)
	{
		return $input;
	}
	public static function saveuserviewaccess_after($id, $input)
	{
	}

	// Method to get all records
	public static function listuserviewaccess_before($request)
	{		
		return $request;
	}
	public static function listuserviewaccess_after($request)
	{		
	}


	// Method to get a record
	public static function getuserviewaccess_before($request)
	{	
		return $request;	
	}
	public static function getuserviewaccess_after($request)
	{		
	}
}
