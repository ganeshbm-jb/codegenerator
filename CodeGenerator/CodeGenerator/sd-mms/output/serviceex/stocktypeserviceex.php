<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\stocktype;
use DB;
use Webpatser\Uuid\Uuid;

class stocktypeserviceex extends BaseService
{
	// Method to save data
	public static function savestocktype_before($id, $input)
	{
		return $input;
	}
	public static function savestocktype_after($id, $input)
	{
	}

	// Method to get all records
	public static function liststocktype_before($request)
	{		
		return $request;
	}
	public static function liststocktype_after($request)
	{		
	}


	// Method to get a record
	public static function getstocktype_before($request)
	{	
		return $request;	
	}
	public static function getstocktype_after($request)
	{		
	}
}
