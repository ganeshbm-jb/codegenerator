<?php

namespace App\ServicesEx;

use App\Services\BaseService;
use App\Models\workerstimesheet;
use DB;
use Webpatser\Uuid\Uuid;

class workerstimesheetserviceex extends BaseService
{
	// Method to save data
	public static function saveworkerstimesheet_before($id, $input)
	{
		return $input;
	}
	public static function saveworkerstimesheet_after($id, $input)
	{
	}

	// Method to get all records
	public static function listworkerstimesheet_before($request)
	{		
		return $request;
	}
	public static function listworkerstimesheet_after($request)
	{		
	}


	// Method to get a record
	public static function getworkerstimesheet_before($request)
	{	
		return $request;	
	}
	public static function getworkerstimesheet_after($request)
	{		
	}
}
