import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class sitesDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "sites";
    }
	static localStorage : string = "ls_sites";
    // columns
    
projectid : string ='';
name : string ='';
}

export class sitesFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','projectid','name',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
projectid : [''],
name : [''],
		});
	}
}

