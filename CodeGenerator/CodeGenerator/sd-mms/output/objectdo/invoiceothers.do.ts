import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class invoiceothersDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "invoiceothers";
    }
	static localStorage : string = "ls_invoiceothers";
    // columns
    
invoiceid : string ='';
description : string ='';
price : string ='';
materialid : string ='';
qty : string ='';
unitprice : string ='';
itemunit : string ='';
sgst : string ='';
cgst : string ='';
sgstamount : string ='';
cgstamount : string ='';
totalbeforegst : string ='';
totalaftergst : string ='';
filtermaterialsParent : boolean = false;
checkmaterialsParentExists : boolean = false;
materialsparentobject : any = '';
}

export class invoiceothersFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','invoiceid','description','price','materialid','qty','unitprice','itemunit','sgst','cgst','sgstamount','cgstamount','totalbeforegst','totalaftergst',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
invoiceid : [''],
description : [''],
price : [''],
materialid : [''],
qty : [''],
unitprice : [''],
itemunit : [''],
sgst : [''],
cgst : [''],
sgstamount : [''],
cgstamount : [''],
totalbeforegst : [''],
totalaftergst : [''],
		});
	}
}

