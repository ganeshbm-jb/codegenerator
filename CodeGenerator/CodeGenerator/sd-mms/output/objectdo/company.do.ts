import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class companyDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "company";
    }
	static localStorage : string = "ls_company";
    // columns
    
name : string ='';
address1 : string ='';
address2 : string ='';
city : string ='';
state : string ='';
pincode : string ='';
gstno : string ='';
sgst : string ='';
cgst : string ='';
igst : string ='';
phoneno : string ='';
contactperson : string ='';
email : string ='';
country : string ='';
}

export class companyFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','name','address1','address2','city','state','pincode','gstno','sgst','cgst','igst','phoneno','contactperson','email','country',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
name : [''],
address1 : [''],
address2 : [''],
city : [''],
state : [''],
pincode : [''],
gstno : [''],
sgst : [''],
cgst : [''],
igst : [''],
phoneno : [''],
contactperson : [''],
email : [''],
country : [''],
		});
	}
}

