import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class advanceDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "advance";
    }
	static localStorage : string = "ls_advance";
    // columns
    
description : string ='';
atypeid : string ='';
amount : string ='';
status : string ='';
advancedate : string ='';
advancedate2 : string ='';
source : string ='';
sourceid : string ='';
filteradvancetypeParent : boolean = false;
checkadvancetypeParentExists : boolean = false;
advancetypeparentobject : any = '';
}

export class advanceFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','description','atypeid','amount','status','advancedate','advancedate2','source','sourceid',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
description : [''],
atypeid : [''],
amount : [''],
status : [''],
advancedate : [''],
advancedate2 : [''],
source : [''],
sourceid : [''],
		});
	}
}

