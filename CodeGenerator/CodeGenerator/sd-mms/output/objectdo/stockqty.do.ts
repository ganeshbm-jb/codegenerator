import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class stockqtyDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "stockqty";
    }
	static localStorage : string = "ls_stockqty";
    // columns
    
parentid : string ='';
materialid : string ='';
opqty : string ='';
inqty : string ='';
outqty : string ='';
prqty : string ='';
frqty : string ='';
damageqty : string ='';
totalqty : string ='';
reorderqty : string ='';
reorder : string ='';
opqtyprice : string ='';
inqtyprice : string ='';
outqtyprice : string ='';
prqtyprice : string ='';
frqtyprice : string ='';
damageqtyprice : string ='';
totalqtyprice : string ='';
loadstockList : boolean = false;
checkstockExists : boolean = false;
stockobject : any = '';
loadstocklocationqtyList : boolean = false;
checkstocklocationqtyExists : boolean = false;
stocklocationqtyobject : any = '';
filtermaterialsParent : boolean = false;
checkmaterialsParentExists : boolean = false;
materialsparentobject : any = '';
}

export class stockqtyFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','parentid','materialid','opqty','inqty','outqty','prqty','frqty','damageqty','totalqty','reorderqty','reorder','opqtyprice','inqtyprice','outqtyprice','prqtyprice','frqtyprice','damageqtyprice','totalqtyprice',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
parentid : [''],
materialid : [''],
opqty : [''],
inqty : [''],
outqty : [''],
prqty : [''],
frqty : [''],
damageqty : [''],
totalqty : [''],
reorderqty : [''],
reorder : [''],
opqtyprice : [''],
inqtyprice : [''],
outqtyprice : [''],
prqtyprice : [''],
frqtyprice : [''],
damageqtyprice : [''],
totalqtyprice : [''],
		});
	}
}

