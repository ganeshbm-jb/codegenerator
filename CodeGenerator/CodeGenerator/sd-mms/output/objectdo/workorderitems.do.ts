import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class workorderitemsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "workorderitems";
    }
	static localStorage : string = "ls_workorderitems";
    // columns
    
workorderid : string ='';
itemid : string ='';
qty : string ='';
unitprice : string ='';
price : string ='';
unittype : string ='';
itemunit : string ='';
sgst : string ='';
cgst : string ='';
sgstamount : string ='';
cgstamount : string ='';
totalbeforegst : string ='';
totalaftergst : string ='';
filterworkordersParent : boolean = false;
checkworkordersParentExists : boolean = false;
workordersparentobject : any = '';
filtermaterialsParent : boolean = false;
checkmaterialsParentExists : boolean = false;
materialsparentobject : any = '';
}

export class workorderitemsFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','workorderid','itemid','qty','unitprice','price','unittype','itemunit','sgst','cgst','sgstamount','cgstamount','totalbeforegst','totalaftergst',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
workorderid : [''],
itemid : [''],
qty : [''],
unitprice : [''],
price : [''],
unittype : [''],
itemunit : [''],
sgst : [''],
cgst : [''],
sgstamount : [''],
cgstamount : [''],
totalbeforegst : [''],
totalaftergst : [''],
		});
	}
}

