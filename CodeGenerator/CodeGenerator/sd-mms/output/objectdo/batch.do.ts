import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class batchDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "batch";
    }
	static localStorage : string = "ls_batch";
    // columns
    
batchname : string ='';
fdate : string ='';
fdate2 : string ='';
tdate : string ='';
tdate2 : string ='';
status : string ='';
cola : string ='';
colb : string ='';
colc : string ='';
coltotal : string ='';
batchno : string ='';
}

export class batchFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','batchname','fdate','fdate2','tdate','tdate2','status','cola','colb','colc','coltotal','batchno',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
batchname : [''],
fdate : [''],
fdate2 : [''],
tdate : [''],
tdate2 : [''],
status : [''],
cola : [''],
colb : [''],
colc : [''],
coltotal : [''],
batchno : [''],
		});
	}
}

