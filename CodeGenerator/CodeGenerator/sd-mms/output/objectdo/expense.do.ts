import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class expenseDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "expense";
    }
	static localStorage : string = "ls_expense";
    // columns
    
edate : string ='';
edate2 : string ='';
description : string ='';
amount : string ='';
pid : string ='';
source : string ='';
status : string ='';
pstatus : string ='';
nstatus : string ='';
active : string ='';
typeid : string ='';
filterexpensetypeParent : boolean = false;
checkexpensetypeParentExists : boolean = false;
expensetypeparentobject : any = '';
}

export class expenseFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','edate','edate2','description','amount','pid','source','status','pstatus','nstatus','active','typeid',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
edate : [''],
edate2 : [''],
description : [''],
amount : [''],
pid : [''],
source : [''],
status : [''],
pstatus : [''],
nstatus : [''],
active : [''],
typeid : [''],
		});
	}
}

