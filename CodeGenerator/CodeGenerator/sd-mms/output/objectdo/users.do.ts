import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class usersDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "users";
    }
	static localStorage : string = "ls_users";
    // columns
    
userid : string ='';
password : string ='';
active : string ='';
firstname : string ='';
lastname : string ='';
mobile : string ='';
email : string ='';
role : string ='';
filterrolesParent : boolean = false;
checkrolesParentExists : boolean = false;
rolesparentobject : any = '';
}

export class usersFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','userid','password','active','firstname','lastname','mobile','email','role',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
userid : [''],
password : [''],
active : [''],
firstname : [''],
lastname : [''],
mobile : [''],
email : [''],
role : [''],
		});
	}
}

