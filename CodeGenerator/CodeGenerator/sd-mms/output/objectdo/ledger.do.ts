import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class ledgerDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "ledger";
    }
	static localStorage : string = "ls_ledger";
    // columns
    
srcid : string ='';
src : string ='';
tdate : string ='';
tdate2 : string ='';
cramt : string ='';
dramt : string ='';
batchid : string ='';
batchstatus : string ='';
srctype : string ='';
status : string ='';
description : string ='';
}

export class ledgerFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','srcid','src','tdate','tdate2','cramt','dramt','batchid','batchstatus','srctype','status','description',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
srcid : [''],
src : [''],
tdate : [''],
tdate2 : [''],
cramt : [''],
dramt : [''],
batchid : [''],
batchstatus : [''],
srctype : [''],
status : [''],
description : [''],
		});
	}
}

