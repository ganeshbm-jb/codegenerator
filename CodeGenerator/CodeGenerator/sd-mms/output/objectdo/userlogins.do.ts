import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class userloginsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "userlogins";
    }
	static localStorage : string = "ls_userlogins";
    // columns
    
userid : string ='';
username : string ='';
active : string ='';
source : string ='';
roles : string ='';
token : string ='';
lasttouchtime : string ='';
lasttouchtime2 : string ='';
ipaddr : string ='';
}

export class userloginsFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','userid','username','active','source','roles','token','lasttouchtime','lasttouchtime2','ipaddr',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
userid : [''],
username : [''],
active : [''],
source : [''],
roles : [''],
token : [''],
lasttouchtime : [''],
lasttouchtime2 : [''],
ipaddr : [''],
		});
	}
}

