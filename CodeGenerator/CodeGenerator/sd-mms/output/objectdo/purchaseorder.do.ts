import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class purchaseorderDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "purchaseorder";
    }
	static localStorage : string = "ls_purchaseorder";
    // columns
    
purchaseorderdate : string ='';
purchaseorderdate2 : string ='';
supplierid : string ='';
code : string ='';
billamt : string ='';
sgstamt : string ='';
cgstamt : string ='';
igstamt : string ='';
gstapplicable : string ='';
remarks : string ='';
status : string ='';
projectid : string ='';
totalbeforegst : string ='';
paymentmode : string ='';
totalaftergst : string ='';
totalitems : string ='';
paidamount : string ='';
balanceamount : string ='';
loadpurchaseordermaterialList : boolean = false;
checkpurchaseordermaterialExists : boolean = false;
purchaseordermaterialobject : any = '';
loadpurchasesList : boolean = false;
checkpurchasesExists : boolean = false;
purchasesobject : any = '';
filtersuppliersParent : boolean = false;
checksuppliersParentExists : boolean = false;
suppliersparentobject : any = '';
filterprojectsParent : boolean = false;
checkprojectsParentExists : boolean = false;
projectsparentobject : any = '';
}

export class purchaseorderFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','purchaseorderdate','purchaseorderdate2','supplierid','code','billamt','sgstamt','cgstamt','igstamt','gstapplicable','remarks','status','projectid','totalbeforegst','paymentmode','totalaftergst','totalitems','paidamount','balanceamount',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
purchaseorderdate : [''],
purchaseorderdate2 : [''],
supplierid : [''],
code : [''],
billamt : [''],
sgstamt : [''],
cgstamt : [''],
igstamt : [''],
gstapplicable : [''],
remarks : [''],
status : [''],
projectid : [''],
totalbeforegst : [''],
paymentmode : [''],
totalaftergst : [''],
totalitems : [''],
paidamount : [''],
balanceamount : [''],
		});
	}
}

