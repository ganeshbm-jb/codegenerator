import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class workordersDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "workorders";
    }
	static localStorage : string = "ls_workorders";
    // columns
    
projectid : string ='';
workorderdate : string ='';
workorderdate2 : string ='';
completiondate : string ='';
completiondate2 : string ='';
remarks : string ='';
code : string ='';
status : string ='';
totalaftergst : string ='';
totalbeforegst : string ='';
sgstamt : string ='';
cgstamt : string ='';
igstamt : string ='';
billamt : string ='';
startdate : string ='';
startdate2 : string ='';
factoryid : string ='';
totalitems : string ='';
paidamount : string ='';
balanceamount : string ='';
filterprojectsParent : boolean = false;
checkprojectsParentExists : boolean = false;
projectsparentobject : any = '';
filterfactoryParent : boolean = false;
checkfactoryParentExists : boolean = false;
factoryparentobject : any = '';
}

export class workordersFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','projectid','workorderdate','workorderdate2','completiondate','completiondate2','remarks','code','status','totalaftergst','totalbeforegst','sgstamt','cgstamt','igstamt','billamt','startdate','startdate2','factoryid','totalitems','paidamount','balanceamount',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
projectid : [''],
workorderdate : [''],
workorderdate2 : [''],
completiondate : [''],
completiondate2 : [''],
remarks : [''],
code : [''],
status : [''],
totalaftergst : [''],
totalbeforegst : [''],
sgstamt : [''],
cgstamt : [''],
igstamt : [''],
billamt : [''],
startdate : [''],
startdate2 : [''],
factoryid : [''],
totalitems : [''],
paidamount : [''],
balanceamount : [''],
		});
	}
}

