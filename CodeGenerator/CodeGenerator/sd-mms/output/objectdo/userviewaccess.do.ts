import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class userviewaccessDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "userviewaccess";
    }
	static localStorage : string = "ls_userviewaccess";
    // columns
    
viewaccessid : string ='';
active : string ='';
}

export class userviewaccessFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','viewaccessid','active',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
viewaccessid : [''],
active : [''],
		});
	}
}

