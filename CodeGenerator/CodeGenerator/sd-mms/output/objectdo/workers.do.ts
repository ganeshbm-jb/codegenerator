import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class workersDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "workers";
    }
	static localStorage : string = "ls_workers";
    // columns
    
name : string ='';
phoneno : string ='';
salary : string ='';
worktype : string ='';
salarytype : string ='';
desiginationtype : string ='';
filterworktypeParent : boolean = false;
checkworktypeParentExists : boolean = false;
worktypeparentobject : any = '';
filtersalarytypeParent : boolean = false;
checksalarytypeParentExists : boolean = false;
salarytypeparentobject : any = '';
filterdesiginationtypeParent : boolean = false;
checkdesiginationtypeParentExists : boolean = false;
desiginationtypeparentobject : any = '';
}

export class workersFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','name','phoneno','salary','worktype','salarytype','desiginationtype',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
name : [''],
phoneno : [''],
salary : [''],
worktype : [''],
salarytype : [''],
desiginationtype : [''],
		});
	}
}

