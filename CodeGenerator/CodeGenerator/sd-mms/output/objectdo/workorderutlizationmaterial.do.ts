import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class workorderutlizationmaterialDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "workorderutlizationmaterial";
    }
	static localStorage : string = "ls_workorderutlizationmaterial";
    // columns
    
woutilizationid : string ='';
materialid : string ='';
qty : string ='';
price : string ='';
filterworkorderutilizationParent : boolean = false;
checkworkorderutilizationParentExists : boolean = false;
workorderutilizationparentobject : any = '';
filtermaterialsParent : boolean = false;
checkmaterialsParentExists : boolean = false;
materialsparentobject : any = '';
}

export class workorderutlizationmaterialFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','woutilizationid','materialid','qty','price',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
woutilizationid : [''],
materialid : [''],
qty : [''],
price : [''],
		});
	}
}

