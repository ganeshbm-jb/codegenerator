import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class desiginationtypeDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "desiginationtype";
    }
	static localStorage : string = "ls_desiginationtype";
    // columns
    
name : string ='';
}

export class desiginationtypeFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','name',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
name : [''],
		});
	}
}

