import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class stockvalueDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "stockvalue";
    }
	static localStorage : string = "ls_stockvalue";
    // columns
    
materialid : string ='';
stockdate : string ='';
stockdate2 : string ='';
avgprice : string ='';
}

export class stockvalueFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','materialid','stockdate','stockdate2','avgprice',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
materialid : [''],
stockdate : [''],
stockdate2 : [''],
avgprice : [''],
		});
	}
}

