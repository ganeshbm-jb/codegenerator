import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class workordercostDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "workordercost";
    }
	static localStorage : string = "ls_workordercost";
    // columns
    
amount : string ='';
costtype : string ='';
description : string ='';
}

export class workordercostFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','amount','costtype','description',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
amount : [''],
costtype : [''],
description : [''],
		});
	}
}

