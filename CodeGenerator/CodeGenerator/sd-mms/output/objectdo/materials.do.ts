import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class materialsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "materials";
    }
	static localStorage : string = "ls_materials";
    // columns
    
code : string ='';
name : string ='';
unit : string ='';
hsncode : string ='';
sgst : string ='';
cgst : string ='';
materialtype : string ='';
materialcategoryid : string ='';
reorderqty : string ='';
filterunitsParent : boolean = false;
checkunitsParentExists : boolean = false;
unitsparentobject : any = '';
filtermaterialtypeParent : boolean = false;
checkmaterialtypeParentExists : boolean = false;
materialtypeparentobject : any = '';
filtermaterialcategoryParent : boolean = false;
checkmaterialcategoryParentExists : boolean = false;
materialcategoryparentobject : any = '';
}

export class materialsFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','code','name','unit','hsncode','sgst','cgst','materialtype','materialcategoryid','reorderqty',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
code : [''],
name : [''],
unit : [''],
hsncode : [''],
sgst : [''],
cgst : [''],
materialtype : [''],
materialcategoryid : [''],
reorderqty : [''],
		});
	}
}

