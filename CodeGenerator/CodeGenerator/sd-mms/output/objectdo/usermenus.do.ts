import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class usermenusDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "usermenus";
    }
	static localStorage : string = "ls_usermenus";
    // columns
    
userid : string ='';
menuid : string ='';
active : string ='';
}

export class usermenusFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','userid','menuid','active',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
userid : [''],
menuid : [''],
active : [''],
		});
	}
}

