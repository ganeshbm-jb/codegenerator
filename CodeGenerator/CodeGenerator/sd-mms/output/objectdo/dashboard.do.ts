import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class dashboardDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "dashboard";
    }
	static localStorage : string = "ls_dashboard";
    // columns
    
source : string ='';
status : string ='';
count : string ='';
}

export class dashboardFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','source','status','count',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
source : [''],
status : [''],
count : [''],
		});
	}
}

