import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class purchasereturnDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "purchasereturn";
    }
	static localStorage : string = "ls_purchasereturn";
    // columns
    
purchaseorderid : string ='';
purchasereturndate : string ='';
purchasereturndate2 : string ='';
status : string ='';
reason : string ='';
factoryid : string ='';
filterfactoryParent : boolean = false;
checkfactoryParentExists : boolean = false;
factoryparentobject : any = '';
}

export class purchasereturnFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','purchaseorderid','purchasereturndate','purchasereturndate2','status','reason','factoryid',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
purchaseorderid : [''],
purchasereturndate : [''],
purchasereturndate2 : [''],
status : [''],
reason : [''],
factoryid : [''],
		});
	}
}

