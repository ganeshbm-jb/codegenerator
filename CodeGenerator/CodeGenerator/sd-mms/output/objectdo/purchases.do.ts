import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class purchasesDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "purchases";
    }
	static localStorage : string = "ls_purchases";
    // columns
    
purchasedate : string ='';
purchasedate2 : string ='';
supplierid : string ='';
code : string ='';
billno : string ='';
billamt : string ='';
sgst : string ='';
cgst : string ='';
igst : string ='';
sgstamt : string ='';
cgstamt : string ='';
igstamt : string ='';
gstapplicable : string ='';
remarks : string ='';
status : string ='';
purchaseorderid : string ='';
totalbeforegst : string ='';
totalaftergst : string ='';
totalitems : string ='';
transportname : string ='';
vehicleno : string ='';
factoryid : string ='';
loadpurchasematerialList : boolean = false;
checkpurchasematerialExists : boolean = false;
purchasematerialobject : any = '';
filtersuppliersParent : boolean = false;
checksuppliersParentExists : boolean = false;
suppliersparentobject : any = '';
filterfactoryParent : boolean = false;
checkfactoryParentExists : boolean = false;
factoryparentobject : any = '';
}

export class purchasesFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','purchasedate','purchasedate2','supplierid','code','billno','billamt','sgst','cgst','igst','sgstamt','cgstamt','igstamt','gstapplicable','remarks','status','purchaseorderid','totalbeforegst','totalaftergst','totalitems','transportname','vehicleno','factoryid',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
purchasedate : [''],
purchasedate2 : [''],
supplierid : [''],
code : [''],
billno : [''],
billamt : [''],
sgst : [''],
cgst : [''],
igst : [''],
sgstamt : [''],
cgstamt : [''],
igstamt : [''],
gstapplicable : [''],
remarks : [''],
status : [''],
purchaseorderid : [''],
totalbeforegst : [''],
totalaftergst : [''],
totalitems : [''],
transportname : [''],
vehicleno : [''],
factoryid : [''],
		});
	}
}

