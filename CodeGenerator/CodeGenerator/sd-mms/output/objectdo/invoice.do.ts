import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class invoiceDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "invoice";
    }
	static localStorage : string = "ls_invoice";
    // columns
    
projectid : string ='';
workorderid : string ='';
invoicedate : string ='';
invoicedate2 : string ='';
duedate : string ='';
duedate2 : string ='';
sgst : string ='';
sgstamt : string ='';
cgst : string ='';
cgstamt : string ='';
gsttotal : string ='';
itemtotal : string ='';
otherstotal : string ='';
total : string ='';
status : string ='';
totalbeforegst : string ='';
totalaftergst : string ='';
code : string ='';
filterprojectsParent : boolean = false;
checkprojectsParentExists : boolean = false;
projectsparentobject : any = '';
filterworkordersParent : boolean = false;
checkworkordersParentExists : boolean = false;
workordersparentobject : any = '';
}

export class invoiceFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','projectid','workorderid','invoicedate','invoicedate2','duedate','duedate2','sgst','sgstamt','cgst','cgstamt','gsttotal','itemtotal','otherstotal','total','status','totalbeforegst','totalaftergst','code',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
projectid : [''],
workorderid : [''],
invoicedate : [''],
invoicedate2 : [''],
duedate : [''],
duedate2 : [''],
sgst : [''],
sgstamt : [''],
cgst : [''],
cgstamt : [''],
gsttotal : [''],
itemtotal : [''],
otherstotal : [''],
total : [''],
status : [''],
totalbeforegst : [''],
totalaftergst : [''],
code : [''],
		});
	}
}

