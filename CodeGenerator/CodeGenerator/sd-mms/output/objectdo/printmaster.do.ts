import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class printmasterDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "printmaster";
    }
	static localStorage : string = "ls_printmaster";
    // columns
    
printdata : string ='';
apiref : string ='';
}

export class printmasterFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','printdata','apiref',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
printdata : [''],
apiref : [''],
		});
	}
}

