import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class purchaseordermaterialDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "purchaseordermaterial";
    }
	static localStorage : string = "ls_purchaseordermaterial";
    // columns
    
purchaseorderid : string ='';
materialid : string ='';
qty : string ='';
unitprice : string ='';
price : string ='';
itemunit : string ='';
sgst : string ='';
cgst : string ='';
sgstamount : string ='';
cgstamount : string ='';
totalbeforegst : string ='';
totalaftergst : string ='';
inqty : string ='';
balqty : string ='';
filtermaterialsParent : boolean = false;
checkmaterialsParentExists : boolean = false;
materialsparentobject : any = '';
}

export class purchaseordermaterialFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','purchaseorderid','materialid','qty','unitprice','price','itemunit','sgst','cgst','sgstamount','cgstamount','totalbeforegst','totalaftergst','inqty','balqty',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
purchaseorderid : [''],
materialid : [''],
qty : [''],
unitprice : [''],
price : [''],
itemunit : [''],
sgst : [''],
cgst : [''],
sgstamount : [''],
cgstamount : [''],
totalbeforegst : [''],
totalaftergst : [''],
inqty : [''],
balqty : [''],
		});
	}
}

