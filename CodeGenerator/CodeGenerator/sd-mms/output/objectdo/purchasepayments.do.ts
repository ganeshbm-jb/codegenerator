import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class purchasepaymentsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "purchasepayments";
    }
	static localStorage : string = "ls_purchasepayments";
    // columns
    
poid : string ='';
paymentdate : string ='';
paymentdate2 : string ='';
amount : string ='';
paymentmode : string ='';
txndate : string ='';
txndate2 : string ='';
txnno : string ='';
txnbank : string ='';
status : string ='';
txnstatus : string ='';
duedate : string ='';
duedate2 : string ='';
code : string ='';
filterpurchaseorderParent : boolean = false;
checkpurchaseorderParentExists : boolean = false;
purchaseorderparentobject : any = '';
}

export class purchasepaymentsFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','poid','paymentdate','paymentdate2','amount','paymentmode','txndate','txndate2','txnno','txnbank','status','txnstatus','duedate','duedate2','code',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
poid : [''],
paymentdate : [''],
paymentdate2 : [''],
amount : [''],
paymentmode : [''],
txndate : [''],
txndate2 : [''],
txnno : [''],
txnbank : [''],
status : [''],
txnstatus : [''],
duedate : [''],
duedate2 : [''],
code : [''],
		});
	}
}

