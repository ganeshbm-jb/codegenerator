import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class purchasereturnmaterialDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "purchasereturnmaterial";
    }
	static localStorage : string = "ls_purchasereturnmaterial";
    // columns
    
purchasereturnid : string ='';
materialid : string ='';
qty : string ='';
price : string ='';
filtermaterialsParent : boolean = false;
checkmaterialsParentExists : boolean = false;
materialsparentobject : any = '';
}

export class purchasereturnmaterialFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','purchasereturnid','materialid','qty','price',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
purchasereturnid : [''],
materialid : [''],
qty : [''],
price : [''],
		});
	}
}

