import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class purchasematerialDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "purchasematerial";
    }
	static localStorage : string = "ls_purchasematerial";
    // columns
    
purchaseid : string ='';
materialid : string ='';
qty : string ='';
unitprice : string ='';
price : string ='';
itemunit : string ='';
purchaseorderid : string ='';
sgstamt : string ='';
cgstamt : string ='';
igstamt : string ='';
totalbeforegst : string ='';
totalaftergst : string ='';
totalitems : string ='';
sgst : string ='';
cgst : string ='';
filtermaterialsParent : boolean = false;
checkmaterialsParentExists : boolean = false;
materialsparentobject : any = '';
}

export class purchasematerialFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','purchaseid','materialid','qty','unitprice','price','itemunit','purchaseorderid','sgstamt','cgstamt','igstamt','totalbeforegst','totalaftergst','totalitems','sgst','cgst',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
purchaseid : [''],
materialid : [''],
qty : [''],
unitprice : [''],
price : [''],
itemunit : [''],
purchaseorderid : [''],
sgstamt : [''],
cgstamt : [''],
igstamt : [''],
totalbeforegst : [''],
totalaftergst : [''],
totalitems : [''],
sgst : [''],
cgst : [''],
		});
	}
}

