import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class viewaccessDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "viewaccess";
    }
	static localStorage : string = "ls_viewaccess";
    // columns
    
code : string ='';
access : string ='';
role : string ='';
status : string ='';
}

export class viewaccessFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','code','access','role','status',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
code : [''],
access : [''],
role : [''],
status : [''],
		});
	}
}

