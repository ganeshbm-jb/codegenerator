import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class invoicereceiptsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "invoicereceipts";
    }
	static localStorage : string = "ls_invoicereceipts";
    // columns
    
woid : string ='';
receiptsdate : string ='';
receiptsdate2 : string ='';
amount : string ='';
paymentmode : string ='';
txndate : string ='';
txndate2 : string ='';
txnno : string ='';
txnbank : string ='';
status : string ='';
txnstatus : string ='';
duedate : string ='';
duedate2 : string ='';
code : string ='';
filterworkordersParent : boolean = false;
checkworkordersParentExists : boolean = false;
workordersparentobject : any = '';
}

export class invoicereceiptsFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','woid','receiptsdate','receiptsdate2','amount','paymentmode','txndate','txndate2','txnno','txnbank','status','txnstatus','duedate','duedate2','code',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
woid : [''],
receiptsdate : [''],
receiptsdate2 : [''],
amount : [''],
paymentmode : [''],
txndate : [''],
txndate2 : [''],
txnno : [''],
txnbank : [''],
status : [''],
txnstatus : [''],
duedate : [''],
duedate2 : [''],
code : [''],
		});
	}
}

