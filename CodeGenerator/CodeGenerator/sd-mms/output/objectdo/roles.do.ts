import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class rolesDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "roles";
    }
	static localStorage : string = "ls_roles";
    // columns
    
code : string ='';
name : string ='';
}

export class rolesFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','code','name',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
code : [''],
name : [''],
		});
	}
}

