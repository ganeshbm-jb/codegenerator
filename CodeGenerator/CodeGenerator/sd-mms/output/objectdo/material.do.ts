import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class materialDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "material";
    }
	static localStorage : string = "ls_material";
    // columns
    
mnid : string ='';
materialid : string ='';
qty : string ='';
}

export class materialFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','mnid','materialid','qty',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
mnid : [''],
materialid : [''],
qty : [''],
		});
	}
}

