import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class stocklocationqtyDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "stocklocationqty";
    }
	static localStorage : string = "ls_stocklocationqty";
    // columns
    
parentid : string ='';
materialid : string ='';
factoryid : string ='';
inqty : string ='';
outqty : string ='';
totalqty : string ='';
lowstock : string ='';
inqtyprice : string ='';
outqtyprice : string ='';
totalqtyprice : string ='';
opqty : string ='';
opqtyprice : string ='';
prqty : string ='';
prqtyprice : string ='';
frqty : string ='';
frqtyprice : string ='';
damageqty : string ='';
damageqtyprice : string ='';
filtermaterialsParent : boolean = false;
checkmaterialsParentExists : boolean = false;
materialsparentobject : any = '';
filterfactoryParent : boolean = false;
checkfactoryParentExists : boolean = false;
factoryparentobject : any = '';
}

export class stocklocationqtyFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','parentid','materialid','factoryid','inqty','outqty','totalqty','lowstock','inqtyprice','outqtyprice','totalqtyprice','opqty','opqtyprice','prqty','prqtyprice','frqty','frqtyprice','damageqty','damageqtyprice',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
parentid : [''],
materialid : [''],
factoryid : [''],
inqty : [''],
outqty : [''],
totalqty : [''],
lowstock : [''],
inqtyprice : [''],
outqtyprice : [''],
totalqtyprice : [''],
opqty : [''],
opqtyprice : [''],
prqty : [''],
prqtyprice : [''],
frqty : [''],
frqtyprice : [''],
damageqty : [''],
damageqtyprice : [''],
		});
	}
}

