import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class codemasterDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "codemaster";
    }
	static localStorage : string = "ls_codemaster";
    // columns
    
module : string ='';
code : string ='';
part1 : string ='';
format : string ='';
count : string ='';
active : string ='';
}

export class codemasterFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','module','code','part1','format','count','active',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
module : [''],
code : [''],
part1 : [''],
format : [''],
count : [''],
active : [''],
		});
	}
}

