import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class suppliersDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "suppliers";
    }
	static localStorage : string = "ls_suppliers";
    // columns
    
name : string ='';
address1 : string ='';
address2 : string ='';
city : string ='';
state : string ='';
pincode : string ='';
gstno : string ='';
sgst : string ='';
cgst : string ='';
igst : string ='';
phoneno : string ='';
contactperson : string ='';
country : string ='';
email : string ='';
}

export class suppliersFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','name','address1','address2','city','state','pincode','gstno','sgst','cgst','igst','phoneno','contactperson','country','email',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
name : [''],
address1 : [''],
address2 : [''],
city : [''],
state : [''],
pincode : [''],
gstno : [''],
sgst : [''],
cgst : [''],
igst : [''],
phoneno : [''],
contactperson : [''],
country : [''],
email : [''],
		});
	}
}

