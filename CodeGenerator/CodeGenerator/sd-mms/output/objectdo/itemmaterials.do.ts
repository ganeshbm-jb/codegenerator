import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class itemmaterialsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "itemmaterials";
    }
	static localStorage : string = "ls_itemmaterials";
    // columns
    
itemid : string ='';
materialid : string ='';
}

export class itemmaterialsFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','itemid','materialid',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
itemid : [''],
materialid : [''],
		});
	}
}

