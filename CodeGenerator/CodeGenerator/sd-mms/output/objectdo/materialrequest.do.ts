import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class materialrequestDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "materialrequest";
    }
	static localStorage : string = "ls_materialrequest";
    // columns
    
requestno : string ='';
requestdate : string ='';
requestdate2 : string ='';
duedate : string ='';
duedate2 : string ='';
projectid : string ='';
factoryid : string ='';
requesttype : string ='';
status : string ='';
filterprojectsParent : boolean = false;
checkprojectsParentExists : boolean = false;
projectsparentobject : any = '';
filterfactoryParent : boolean = false;
checkfactoryParentExists : boolean = false;
factoryparentobject : any = '';
filterrequesttypeParent : boolean = false;
checkrequesttypeParentExists : boolean = false;
requesttypeparentobject : any = '';
}

export class materialrequestFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','requestno','requestdate','requestdate2','duedate','duedate2','projectid','factoryid','requesttype','status',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
requestno : [''],
requestdate : [''],
requestdate2 : [''],
duedate : [''],
duedate2 : [''],
projectid : [''],
factoryid : [''],
requesttype : [''],
status : [''],
		});
	}
}

