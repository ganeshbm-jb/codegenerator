import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class workordermaterialsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "workordermaterials";
    }
	static localStorage : string = "ls_workordermaterials";
    // columns
    
workorderid : string ='';
workorderitemid : string ='';
materialid : string ='';
qty : string ='';
unitprice : string ='';
price : string ='';
unittype : string ='';
itemunit : string ='';
sgst : string ='';
cgst : string ='';
sgstamount : string ='';
cgstamount : string ='';
totalbeforegst : string ='';
totalaftergst : string ='';
outqty : string ='';
balqty : string ='';
filterworkordersParent : boolean = false;
checkworkordersParentExists : boolean = false;
workordersparentobject : any = '';
filtermaterialsParent : boolean = false;
checkmaterialsParentExists : boolean = false;
materialsparentobject : any = '';
}

export class workordermaterialsFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','workorderid','workorderitemid','materialid','qty','unitprice','price','unittype','itemunit','sgst','cgst','sgstamount','cgstamount','totalbeforegst','totalaftergst','outqty','balqty',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
workorderid : [''],
workorderitemid : [''],
materialid : [''],
qty : [''],
unitprice : [''],
price : [''],
unittype : [''],
itemunit : [''],
sgst : [''],
cgst : [''],
sgstamount : [''],
cgstamount : [''],
totalbeforegst : [''],
totalaftergst : [''],
outqty : [''],
balqty : [''],
		});
	}
}

