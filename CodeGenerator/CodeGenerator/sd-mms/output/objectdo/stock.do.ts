import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class stockDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "stock";
    }
	static localStorage : string = "ls_stock";
    // columns
    
parentid : string ='';
purchaseid : string ='';
materialid : string ='';
stockdate : string ='';
stockdate2 : string ='';
qty : string ='';
stocktypeid : string ='';
factoryid : string ='';
status : string ='';
unitprice : string ='';
projectid : string ='';
purchaseorderid : string ='';
purchaseordercode : string ='';
purchaseorderproject : string ='';
sgst : string ='';
cgst : string ='';
sgstamount : string ='';
cgstamount : string ='';
totalprice : string ='';
factoryid2 : string ='';
projectid2 : string ='';
description : string ='';
filterpurchasesParent : boolean = false;
checkpurchasesParentExists : boolean = false;
purchasesparentobject : any = '';
filtermaterialsParent : boolean = false;
checkmaterialsParentExists : boolean = false;
materialsparentobject : any = '';
filterstocktypeParent : boolean = false;
checkstocktypeParentExists : boolean = false;
stocktypeparentobject : any = '';
filterfactoryParent : boolean = false;
checkfactoryParentExists : boolean = false;
factoryparentobject : any = '';
filterprojectsParent : boolean = false;
checkprojectsParentExists : boolean = false;
projectsparentobject : any = '';
filterpurchaseorderParent : boolean = false;
checkpurchaseorderParentExists : boolean = false;
purchaseorderparentobject : any = '';
}

export class stockFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','parentid','purchaseid','materialid','stockdate','stockdate2','qty','stocktypeid','factoryid','status','unitprice','projectid','purchaseorderid','purchaseordercode','purchaseorderproject','sgst','cgst','sgstamount','cgstamount','totalprice','factoryid2','projectid2','description',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
parentid : [''],
purchaseid : [''],
materialid : [''],
stockdate : [''],
stockdate2 : [''],
qty : [''],
stocktypeid : [''],
factoryid : [''],
status : [''],
unitprice : [''],
projectid : [''],
purchaseorderid : [''],
purchaseordercode : [''],
purchaseorderproject : [''],
sgst : [''],
cgst : [''],
sgstamount : [''],
cgstamount : [''],
totalprice : [''],
factoryid2 : [''],
projectid2 : [''],
description : [''],
		});
	}
}

