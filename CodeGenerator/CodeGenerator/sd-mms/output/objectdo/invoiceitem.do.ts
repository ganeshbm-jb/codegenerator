import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class invoiceitemDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "invoiceitem";
    }
	static localStorage : string = "ls_invoiceitem";
    // columns
    
invoiceid : string ='';
itemid : string ='';
qty : string ='';
unitprice : string ='';
price : string ='';
itemunit : string ='';
sgst : string ='';
cgst : string ='';
sgstamount : string ='';
cgstamount : string ='';
totalbeforegst : string ='';
totalaftergst : string ='';
filtermaterialsParent : boolean = false;
checkmaterialsParentExists : boolean = false;
materialsparentobject : any = '';
}

export class invoiceitemFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','invoiceid','itemid','qty','unitprice','price','itemunit','sgst','cgst','sgstamount','cgstamount','totalbeforegst','totalaftergst',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
invoiceid : [''],
itemid : [''],
qty : [''],
unitprice : [''],
price : [''],
itemunit : [''],
sgst : [''],
cgst : [''],
sgstamount : [''],
cgstamount : [''],
totalbeforegst : [''],
totalaftergst : [''],
		});
	}
}

