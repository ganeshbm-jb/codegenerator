import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class materialnotedetailsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "materialnotedetails";
    }
	static localStorage : string = "ls_materialnotedetails";
    // columns
    
mnid : string ='';
materialid : string ='';
qty : string ='';
filtermaterialsParent : boolean = false;
checkmaterialsParentExists : boolean = false;
materialsparentobject : any = '';
}

export class materialnotedetailsFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','mnid','materialid','qty',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
mnid : [''],
materialid : [''],
qty : [''],
		});
	}
}

