import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class salarytypeDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "salarytype";
    }
	static localStorage : string = "ls_salarytype";
    // columns
    
name : string ='';
}

export class salarytypeFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','name',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
name : [''],
		});
	}
}

