import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class workorderutilizationDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "workorderutilization";
    }
	static localStorage : string = "ls_workorderutilization";
    // columns
    
workorderid : string ='';
udate : string ='';
udate2 : string ='';
status : string ='';
}

export class workorderutilizationFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','workorderid','udate','udate2','status',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
workorderid : [''],
udate : [''],
udate2 : [''],
status : [''],
		});
	}
}

