import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class materialrequestdetailsDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "materialrequestdetails";
    }
	static localStorage : string = "ls_materialrequestdetails";
    // columns
    
materialid : string ='';
qty : string ='';
stockqty : string ='';
stockqtydate : string ='';
stockqtydate2 : string ='';
status : string ='';
materialrequestid : string ='';
filtermaterialsParent : boolean = false;
checkmaterialsParentExists : boolean = false;
materialsparentobject : any = '';
}

export class materialrequestdetailsFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','materialid','qty','stockqty','stockqtydate','stockqtydate2','status','materialrequestid',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
materialid : [''],
qty : [''],
stockqty : [''],
stockqtydate : [''],
stockqtydate2 : [''],
status : [''],
materialrequestid : [''],
		});
	}
}

