import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class materialnoteDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "materialnote";
    }
	static localStorage : string = "ls_materialnote";
    // columns
    
code : string ='';
mndate : string ='';
mndate2 : string ='';
vehicleno : string ='';
contactperson : string ='';
phone : string ='';
fromlocation : string ='';
tolocation : string ='';
status : string ='';
}

export class materialnoteFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','code','mndate','mndate2','vehicleno','contactperson','phone','fromlocation','tolocation','status',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
code : [''],
mndate : [''],
mndate2 : [''],
vehicleno : [''],
contactperson : [''],
phone : [''],
fromlocation : [''],
tolocation : [''],
status : [''],
		});
	}
}

