import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class workerstimesheetDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "workerstimesheet";
    }
	static localStorage : string = "ls_workerstimesheet";
    // columns
    
workersid : string ='';
tdate : string ='';
tdate2 : string ='';
timein : string ='';
timeout : string ='';
totalhours : string ='';
description : string ='';
filterworkersParent : boolean = false;
checkworkersParentExists : boolean = false;
workersparentobject : any = '';
}

export class workerstimesheetFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','workersid','tdate','tdate2','timein','timeout','totalhours','description',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
workersid : [''],
tdate : [''],
tdate2 : [''],
timein : [''],
timeout : [''],
totalhours : [''],
description : [''],
		});
	}
}

