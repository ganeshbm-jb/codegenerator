import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class userrolesDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "userroles";
    }
	static localStorage : string = "ls_userroles";
    // columns
    
userid : string ='';
roleid : string ='';
active : string ='';
prole : string ='';
}

export class userrolesFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','userid','roleid','active','prole',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
userid : [''],
roleid : [''],
active : [''],
prole : [''],
		});
	}
}

