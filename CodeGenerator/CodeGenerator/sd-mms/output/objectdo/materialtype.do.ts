import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class materialtypeDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "materialtype";
    }
	static localStorage : string = "ls_materialtype";
    // columns
    
name : string ='';
}

export class materialtypeFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','name',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
name : [''],
		});
	}
}

