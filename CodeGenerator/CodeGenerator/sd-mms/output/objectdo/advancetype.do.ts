import { FormBuilder } from "@angular/forms";
import { baseDO } from "../base.do";

export class advancetypeDO extends baseDO {
    constructor(_postoperation : string) {
        super();
		this.postoperation = _postoperation;
        this.apiref = "advancetype";
    }
	static localStorage : string = "ls_advancetype";
    // columns
    
name : string ='';
}

export class advancetypeFormGroup{

	constructor() { }
	formItems : string[] = ['misccol1','misccol2','misccol3','misccol4','misccol5','id','name',];
	initializeForm(_formbuilder : FormBuilder)
	{
		return _formbuilder.group({
			id : [''],
			misccol1 : [''],
			misccol2 : [''],
			misccol3 : [''],
			misccol4 : [''],
			misccol5 : [''],
			
name : [''],
		});
	}
}

