CREATE TABLE `workorders` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `workorders` add column `projectid` varchar(255) not null default "";
alter table `workorders` add column `workorderdate` date not null ;
alter table `workorders` add column `completiondate` date not null ;
alter table `workorders` add column `remarks` varchar(255)  default "";
alter table `workorders` add column `code` varchar(255) unique not null ;
alter table `workorders` add column `status` varchar(255) not null default "";
alter table `workorders` add column `totalaftergst` decimal(18,2)  default 0;
alter table `workorders` add column `totalbeforegst` decimal(18,2)  default 0;
alter table `workorders` add column `sgstamt` decimal(18,2)  default 0;
alter table `workorders` add column `cgstamt` decimal(18,2)  default 0;
alter table `workorders` add column `igstamt` decimal(18,2)  default 0;
alter table `workorders` add column `billamt` decimal(18,2)  default 0;
alter table `workorders` add column `startdate` date null ;
alter table `workorders` add column `factoryid` varchar(255)  ;
alter table `workorders` add column `totalitems` int  default 0;
alter table `workorders` add column `paidamount` decimal(18,2)  default 0;
alter table `workorders` add column `balanceamount` decimal(18,2)  default 0;

