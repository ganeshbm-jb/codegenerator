CREATE TABLE `invoice` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `invoice` add column `projectid` varchar(255) not null default "";
alter table `invoice` add column `workorderid` varchar(255) not null default "";
alter table `invoice` add column `invoicedate` date  ;
alter table `invoice` add column `duedate` date  ;
alter table `invoice` add column `sgst` decimal(18,2)  default 0;
alter table `invoice` add column `sgstamt` decimal(18,2)  default 0;
alter table `invoice` add column `cgst` decimal(18,2)  default 0;
alter table `invoice` add column `cgstamt` decimal(18,2)  default 0;
alter table `invoice` add column `gsttotal` decimal(18,2)  default 0;
alter table `invoice` add column `itemtotal` decimal(18,2)  default 0;
alter table `invoice` add column `otherstotal` decimal(18,2)  default 0;
alter table `invoice` add column `total` decimal(18,2)  default 0;
alter table `invoice` add column `status` varchar(255)  default "";
alter table `invoice` add column `totalbeforegst` decimal(18,2)  default 0;
alter table `invoice` add column `totalaftergst` decimal(18,2)  default 0;
alter table `invoice` add column `code` varchar(255) unique not null default "";

