CREATE TABLE `batch` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `batch` add column `batchname` varchar(255) unique ;
alter table `batch` add column `fdate` date  ;
alter table `batch` add column `tdate` date  ;
alter table `batch` add column `status` varchar(255) not null ;
alter table `batch` add column `cola` decimal(18,2)  default 0;
alter table `batch` add column `colb` decimal(18,2)  default 0;
alter table `batch` add column `colc` decimal(18,2)  default 0;
alter table `batch` add column `coltotal` decimal(18,2)  default 0;
alter table `batch` add column `batchno` varchar(255) unique not null ;

