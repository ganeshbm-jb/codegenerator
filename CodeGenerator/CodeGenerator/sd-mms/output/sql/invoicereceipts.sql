CREATE TABLE `invoicereceipts` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `invoicereceipts` add column `woid` varchar(255)  ;
alter table `invoicereceipts` add column `receiptsdate` date  ;
alter table `invoicereceipts` add column `amount` int  ;
alter table `invoicereceipts` add column `paymentmode` varchar(255)  ;
alter table `invoicereceipts` add column `txndate` date  ;
alter table `invoicereceipts` add column `txnno` varchar(255)  ;
alter table `invoicereceipts` add column `txnbank` varchar(255)  ;
alter table `invoicereceipts` add column `status` varchar(255)  ;
alter table `invoicereceipts` add column `txnstatus` varchar(255)  default "";
alter table `invoicereceipts` add column `duedate` date  ;
alter table `invoicereceipts` add column `code` varchar(255) unique not null default "";

