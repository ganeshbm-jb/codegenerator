CREATE TABLE `stockqty` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `stockqty` add column `parentid` varchar(255) not null default "";
alter table `stockqty` add column `materialid` varchar(255) not null default "";
alter table `stockqty` add column `opqty` decimal(18,2)  default 0;
alter table `stockqty` add column `inqty` decimal(18,2)  default 0;
alter table `stockqty` add column `outqty` decimal(18,2)  default 0;
alter table `stockqty` add column `prqty` decimal(18,2)  default 0;
alter table `stockqty` add column `frqty` decimal(18,2)  default 0;
alter table `stockqty` add column `damageqty` decimal(18,2)  default 0;
alter table `stockqty` add column `totalqty` decimal(18,2)  default 0;
alter table `stockqty` add column `reorderqty` decimal(18,2)  default 0;
alter table `stockqty` add column `reorder` int  default 0;
alter table `stockqty` add column `opqtyprice` decimal(18,2)  default 0;
alter table `stockqty` add column `inqtyprice` decimal(18,2)  default 0;
alter table `stockqty` add column `outqtyprice` decimal(18,2)  default 0;
alter table `stockqty` add column `prqtyprice` decimal(18,2)  default 0;
alter table `stockqty` add column `frqtyprice` decimal(18,2)  default 0;
alter table `stockqty` add column `damageqtyprice` decimal(18,2)  default 0;
alter table `stockqty` add column `totalqtyprice` decimal(18,2)  default 0;

