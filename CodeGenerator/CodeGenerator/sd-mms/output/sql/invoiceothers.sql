CREATE TABLE `invoiceothers` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `invoiceothers` add column `invoiceid` varchar(255) not null default "";
alter table `invoiceothers` add column `description` varchar(255) not null default "";
alter table `invoiceothers` add column `price` decimal(18,2)  default 0;
alter table `invoiceothers` add column `materialid` varchar(255)  ;
alter table `invoiceothers` add column `qty` decimal(18,2)  default 0;
alter table `invoiceothers` add column `unitprice` decimal(18,2)  default 0;
alter table `invoiceothers` add column `itemunit` varchar(255)  default "";
alter table `invoiceothers` add column `sgst` decimal(18,2)  default 0;
alter table `invoiceothers` add column `cgst` decimal(18,2)  default 0;
alter table `invoiceothers` add column `sgstamount` decimal(18,2)  default 0;
alter table `invoiceothers` add column `cgstamount` decimal(18,2)  default 0;
alter table `invoiceothers` add column `totalbeforegst` decimal(18,2)  default 0;
alter table `invoiceothers` add column `totalaftergst` decimal(18,2)  default 0;

