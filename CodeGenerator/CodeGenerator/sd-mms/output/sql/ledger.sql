CREATE TABLE `ledger` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `ledger` add column `srcid` varchar(255) not null ;
alter table `ledger` add column `src` varchar(255) not null ;
alter table `ledger` add column `tdate` date not null ;
alter table `ledger` add column `cramt` decimal(18,2)  default 0;
alter table `ledger` add column `dramt` decimal(18,2)  default 0;
alter table `ledger` add column `batchid` varchar(255)  default "";
alter table `ledger` add column `batchstatus` varchar(255)  default "";
alter table `ledger` add column `srctype` varchar(255)  default "";
alter table `ledger` add column `status` varchar(255)  default "";
alter table `ledger` add column `description` varchar(255)  default "";

