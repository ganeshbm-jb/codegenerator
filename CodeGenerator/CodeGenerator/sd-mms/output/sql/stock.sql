CREATE TABLE `stock` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `stock` add column `parentid` varchar(255) null default "";
alter table `stock` add column `purchaseid` varchar(255) null default "";
alter table `stock` add column `materialid` varchar(255) not null default "";
alter table `stock` add column `stockdate` date not null ;
alter table `stock` add column `qty` decimal(18,2)  default 0;
alter table `stock` add column `stocktypeid` varchar(255) not null default "";
alter table `stock` add column `factoryid` varchar(255) null default "";
alter table `stock` add column `status` varchar(255) not null default "";
alter table `stock` add column `unitprice` decimal(18,2)  default 0;
alter table `stock` add column `projectid` varchar(255) null default "";
alter table `stock` add column `purchaseorderid` varchar(255) null default "";
alter table `stock` add column `purchaseordercode` varchar(255) null default "";
alter table `stock` add column `purchaseorderproject` varchar(255) null default "";
alter table `stock` add column `sgst` decimal(18,2)  default 0;
alter table `stock` add column `cgst` decimal(18,2)  default 0;
alter table `stock` add column `sgstamount` decimal(18,2)  default 0;
alter table `stock` add column `cgstamount` decimal(18,2)  default 0;
alter table `stock` add column `totalprice` decimal(18,2)  default 0;
alter table `stock` add column `factoryid2` varchar(255)  default "";
alter table `stock` add column `projectid2` varchar(255)  default "";
alter table `stock` add column `description` varchar(255) null ;

