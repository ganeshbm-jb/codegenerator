CREATE TABLE `menus` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `menus` add column `code` varchar(255) not null default "";
alter table `menus` add column `name` varchar(255) not null default "";
alter table `menus` add column `menutype` varchar(255) not null default "";
alter table `menus` add column `parent` varchar(255) not null default "";
alter table `menus` add column `menuicon` varchar(255)  default "";
alter table `menus` add column `menuurl` varchar(255) not null default "";
alter table `menus` add column `active` tinyint  default 0;
alter table `menus` add column `morder` tinyint  default 0;
alter table `menus` add column `role1` varchar(255)  default "";
alter table `menus` add column `role2` varchar(255)  default "";
alter table `menus` add column `role3` varchar(255)  default "";
alter table `menus` add column `role4` varchar(255)  default "";
alter table `menus` add column `role5` varchar(255)  default "";
alter table `menus` add column `role6` varchar(255)  default "";

