CREATE TABLE `workordermaterials` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `workordermaterials` add column `workorderid` varchar(255) not null default "";
alter table `workordermaterials` add column `workorderitemid` varchar(255) not null default "";
alter table `workordermaterials` add column `materialid` varchar(255) not null default "";
alter table `workordermaterials` add column `qty` decimal(18,2)  default 0;
alter table `workordermaterials` add column `unitprice` decimal(18,2)  default 0;
alter table `workordermaterials` add column `price` decimal(18,2)  default 0;
alter table `workordermaterials` add column `unittype` varchar(255)  default "";
alter table `workordermaterials` add column `itemunit` varchar(255) not null default "";
alter table `workordermaterials` add column `sgst` decimal(18,2)  default 0;
alter table `workordermaterials` add column `cgst` decimal(18,2)  default 0;
alter table `workordermaterials` add column `sgstamount` decimal(18,2)  default 0;
alter table `workordermaterials` add column `cgstamount` decimal(18,2)  default 0;
alter table `workordermaterials` add column `totalbeforegst` decimal(18,2)  default 0;
alter table `workordermaterials` add column `totalaftergst` decimal(18,2)  default 0;
alter table `workordermaterials` add column `outqty` decimal(18,2)  default 0;
alter table `workordermaterials` add column `balqty` decimal(18,2)  default 0;

