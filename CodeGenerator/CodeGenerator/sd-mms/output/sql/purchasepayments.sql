CREATE TABLE `purchasepayments` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `purchasepayments` add column `poid` varchar(255)  default "";
alter table `purchasepayments` add column `paymentdate` date  ;
alter table `purchasepayments` add column `amount` decimal(18,2)  default 0;
alter table `purchasepayments` add column `paymentmode` varchar(255)  ;
alter table `purchasepayments` add column `txndate` date  ;
alter table `purchasepayments` add column `txnno` varchar(255)  ;
alter table `purchasepayments` add column `txnbank` varchar(255)  ;
alter table `purchasepayments` add column `status` varchar(255) null ;
alter table `purchasepayments` add column `txnstatus` varchar(255)  default "";
alter table `purchasepayments` add column `duedate` date  ;
alter table `purchasepayments` add column `code` varchar(255) unique not null default "";

