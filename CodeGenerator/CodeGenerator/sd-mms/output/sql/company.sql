CREATE TABLE `company` (`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`created_by` varchar(255)  DEFAULT NULL,
`updated_by` varchar(255)  DEFAULT NULL,
`created_at` timestamp NULL DEFAULT NULL,
`updated_at` timestamp NULL DEFAULT NULL, 
`deleted_at` timestamp NULL DEFAULT NULL, 
`vby` varchar(255) null,
`cby` varchar(255) null,
`aby` varchar(255) null,
`rby` varchar(255) null,
`vdate` timestamp NULL DEFAULT NULL,
`cdate` timestamp NULL DEFAULT NULL,
`adate` timestamp NULL DEFAULT NULL,
`rdate` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table `company` add column `name` varchar(255) unique not null ;
alter table `company` add column `address1` varchar(255)  default "";
alter table `company` add column `address2` varchar(255)  default "";
alter table `company` add column `city` varchar(255)  default "";
alter table `company` add column `state` varchar(255)  default "";
alter table `company` add column `pincode` varchar(255)  default "";
alter table `company` add column `gstno` varchar(255)  default "";
alter table `company` add column `sgst` decimal(18,2)  default 0;
alter table `company` add column `cgst` decimal(18,2)  default 0;
alter table `company` add column `igst` decimal(18,2)  default 0;
alter table `company` add column `phoneno` varchar(255)  default "";
alter table `company` add column `contactperson` varchar(255)  default "";
alter table `company` add column `email` varchar(255)  default "";
alter table `company` add column `country` varchar(255)  default "";

