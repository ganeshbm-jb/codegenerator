<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class workorderutlizationmaterial extends Model
{
	protected $table = 'workorderutlizationmaterial';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveworkorderutlizationmaterial($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = workorderutlizationmaterial::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = workorderutlizationmaterial::where('id', $id)->first();
			if (empty($record)) {
				$record = new workorderutlizationmaterial;
			}

			$record->id = $input['id'];
			if(isset($input['woutilizationid']))
			$record->woutilizationid = $input['woutilizationid'];
		if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		if(isset($input['qty']))
			$record->qty = $input['qty'];
		if(isset($input['price']))
			$record->price = $input['price'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getworkorderutlizationmaterial('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listworkorderutlizationmaterial($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workorderutlizationmaterial::all();
		else
		{
			$result = workorderutlizationmaterial::select('workorderutlizationmaterial.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getworkorderutlizationmaterial($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workorderutlizationmaterial::where($col, $value)->get();
		else
		{
			$result = workorderutlizationmaterial::select('workorderutlizationmaterial.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 2;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $workorderutilizationParent = []; 
    if ($pushrecord == 1 && isset($request['filterworkorderutilizationParent']) && !empty($request['filterworkorderutilizationParent']))
       { 
     $workorderutilizationParent = workorderutilization::getworkorderutilization('id',$record['woutilizationid'], $request['workorderutilizationparentobject']); 
        if (isset($request['checkworkorderutilizationParentExists']) && !empty($request['checkworkorderutilizationParentExists'])) 
 {
        if(count($workorderutilizationParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $workorderutilizationParent = workorderutilization::getworkorderutilization('id',$record['woutilizationid']); } 
 if (($pushrecord == 1) && (count($workorderutilizationParent) > 0)) $record['workorderutilizationParent'] = $workorderutilizationParent[0]; 

    $materialsParent = []; 
    if ($pushrecord == 1 && isset($request['filtermaterialsParent']) && !empty($request['filtermaterialsParent']))
       { 
     $materialsParent = materials::getmaterials('id',$record['materialid'], $request['materialsparentobject']); 
        if (isset($request['checkmaterialsParentExists']) && !empty($request['checkmaterialsParentExists'])) 
 {
        if(count($materialsParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $materialsParent = materials::getmaterials('id',$record['materialid']); } 
 if (($pushrecord == 1) && (count($materialsParent) > 0)) $record['materialsParent'] = $materialsParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
