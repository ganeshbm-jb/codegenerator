<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class material extends Model
{
	protected $table = 'material';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savematerial($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = material::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = material::where('id', $id)->first();
			if (empty($record)) {
				$record = new material;
			}

			$record->id = $input['id'];
			if(isset($input['mnid']))
			$record->mnid = $input['mnid'];
		if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		if(isset($input['qty']))
			$record->qty = $input['qty'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getmaterial('material.id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listmaterial($request)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = material::all();
		else
		{
			$result = material::select("material.*", "materials.name as materialname","materials.code as materialcode","units.name as unitname")->join("materials", "materials.id", "=", "material.materialid")->join("units","units.id","=","materials.unit")->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getmaterial($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = material::where($col, $value)->get();
		else
		{
			$result = material::select("material.*", "materials.name as materialname","materials.code as materialcode","units.name as unitname")->join("materials", "materials.id", "=", "material.materialid")->join("units","units.id","=","materials.unit")->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 0;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
