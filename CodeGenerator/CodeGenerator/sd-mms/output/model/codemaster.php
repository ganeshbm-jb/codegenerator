<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class codemaster extends Model
{
	protected $table = 'codemaster';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savecodemaster($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = codemaster::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = codemaster::where('id', $id)->first();
			if (empty($record)) {
				$record = new codemaster;
			}

			$record->id = $input['id'];
			if(isset($input['module']))
			$record->module = $input['module'];
		if(isset($input['code']))
			$record->code = $input['code'];
		if(isset($input['part1']))
			$record->part1 = $input['part1'];
		if(isset($input['format']))
			$record->format = $input['format'];
		if(isset($input['count']))
			$record->count = $input['count'];
		if(isset($input['active']))
			$record->active = $input['active'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getcodemaster('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listcodemaster($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = codemaster::all();
		else
		{
			$result = codemaster::select('codemaster.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getcodemaster($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = codemaster::where($col, $value)->get();
		else
		{
			$result = codemaster::select('codemaster.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 0;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
