<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class workorders extends Model
{
	protected $table = 'workorders';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveworkorders($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = workorders::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = workorders::where('id', $id)->first();
			if (empty($record)) {
				$record = new workorders;
			}

			$record->id = $input['id'];
			if(isset($input['projectid']))
			$record->projectid = $input['projectid'];
		if(isset($input['workorderdate']))
			$record->workorderdate = $input['workorderdate'];
		if(isset($input['completiondate']))
			$record->completiondate = $input['completiondate'];
		if(isset($input['remarks']))
			$record->remarks = $input['remarks'];
		if(isset($input['code']))
			$record->code = $input['code'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['totalaftergst']))
			$record->totalaftergst = $input['totalaftergst'];
		if(isset($input['totalbeforegst']))
			$record->totalbeforegst = $input['totalbeforegst'];
		if(isset($input['sgstamt']))
			$record->sgstamt = $input['sgstamt'];
		if(isset($input['cgstamt']))
			$record->cgstamt = $input['cgstamt'];
		if(isset($input['igstamt']))
			$record->igstamt = $input['igstamt'];
		if(isset($input['billamt']))
			$record->billamt = $input['billamt'];
		if(isset($input['startdate']))
			$record->startdate = $input['startdate'];
		if(isset($input['factoryid']))
			$record->factoryid = $input['factoryid'];
		if(isset($input['totalitems']))
			$record->totalitems = $input['totalitems'];
		if(isset($input['paidamount']))
			$record->paidamount = $input['paidamount'];
		if(isset($input['balanceamount']))
			$record->balanceamount = $input['balanceamount'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getworkorders('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listworkorders($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workorders::all();
		else
		{
			$result = workorders::select('workorders.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getworkorders($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workorders::where($col, $value)->get();
		else
		{
			$result = workorders::select('workorders.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 2;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $projectsParent = []; 
    if ($pushrecord == 1 && isset($request['filterprojectsParent']) && !empty($request['filterprojectsParent']))
       { 
     $projectsParent = projects::getprojects('id',$record['projectid'], $request['projectsparentobject']); 
        if (isset($request['checkprojectsParentExists']) && !empty($request['checkprojectsParentExists'])) 
 {
        if(count($projectsParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $projectsParent = projects::getprojects('id',$record['projectid']); } 
 if (($pushrecord == 1) && (count($projectsParent) > 0)) $record['projectsParent'] = $projectsParent[0]; 

    $factoryParent = []; 
    if ($pushrecord == 1 && isset($request['filterfactoryParent']) && !empty($request['filterfactoryParent']))
       { 
     $factoryParent = factory::getfactory('id',$record['factoryid'], $request['factoryparentobject']); 
        if (isset($request['checkfactoryParentExists']) && !empty($request['checkfactoryParentExists'])) 
 {
        if(count($factoryParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $factoryParent = factory::getfactory('id',$record['factoryid']); } 
 if (($pushrecord == 1) && (count($factoryParent) > 0)) $record['factoryParent'] = $factoryParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
