<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class workflows extends Model
{
	protected $table = 'workflows';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveworkflows($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = workflows::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = workflows::where('id', $id)->first();
			if (empty($record)) {
				$record = new workflows;
			}

			$record->id = $input['id'];
			if(isset($input['code']))
			$record->code = $input['code'];
		if(isset($input['rcode']))
			$record->rcode = $input['rcode'];
		if(isset($input['source']))
			$record->source = $input['source'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['pstatus']))
			$record->pstatus = $input['pstatus'];
		if(isset($input['nstatus']))
			$record->nstatus = $input['nstatus'];
		if(isset($input['active']))
			$record->active = $input['active'];
		if(isset($input['eof']))
			$record->eof = $input['eof'];
		if(isset($input['bof']))
			$record->bof = $input['bof'];
		if(isset($input['wforder']))
			$record->wforder = $input['wforder'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getworkflows('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listworkflows($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workflows::all();
		else
		{
			$result = workflows::select('workflows.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getworkflows($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workflows::where($col, $value)->get();
		else
		{
			$result = workflows::select('workflows.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 0;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
