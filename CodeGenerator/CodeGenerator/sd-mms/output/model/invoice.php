<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class invoice extends Model
{
	protected $table = 'invoice';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveinvoice($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = invoice::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = invoice::where('id', $id)->first();
			if (empty($record)) {
				$record = new invoice;
			}

			$record->id = $input['id'];
			if(isset($input['projectid']))
			$record->projectid = $input['projectid'];
		if(isset($input['workorderid']))
			$record->workorderid = $input['workorderid'];
		if(isset($input['invoicedate']))
			$record->invoicedate = $input['invoicedate'];
		if(isset($input['duedate']))
			$record->duedate = $input['duedate'];
		if(isset($input['sgst']))
			$record->sgst = $input['sgst'];
		if(isset($input['sgstamt']))
			$record->sgstamt = $input['sgstamt'];
		if(isset($input['cgst']))
			$record->cgst = $input['cgst'];
		if(isset($input['cgstamt']))
			$record->cgstamt = $input['cgstamt'];
		if(isset($input['gsttotal']))
			$record->gsttotal = $input['gsttotal'];
		if(isset($input['itemtotal']))
			$record->itemtotal = $input['itemtotal'];
		if(isset($input['otherstotal']))
			$record->otherstotal = $input['otherstotal'];
		if(isset($input['total']))
			$record->total = $input['total'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['totalbeforegst']))
			$record->totalbeforegst = $input['totalbeforegst'];
		if(isset($input['totalaftergst']))
			$record->totalaftergst = $input['totalaftergst'];
		if(isset($input['code']))
			$record->code = $input['code'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getinvoice('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listinvoice($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = invoice::all();
		else
		{
			$result = invoice::select('invoice.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getinvoice($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = invoice::where($col, $value)->get();
		else
		{
			$result = invoice::select('invoice.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 2;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $projectsParent = []; 
    if ($pushrecord == 1 && isset($request['filterprojectsParent']) && !empty($request['filterprojectsParent']))
       { 
     $projectsParent = projects::getprojects('id',$record['projectid'], $request['projectsparentobject']); 
        if (isset($request['checkprojectsParentExists']) && !empty($request['checkprojectsParentExists'])) 
 {
        if(count($projectsParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $projectsParent = projects::getprojects('id',$record['projectid']); } 
 if (($pushrecord == 1) && (count($projectsParent) > 0)) $record['projectsParent'] = $projectsParent[0]; 

    $workordersParent = []; 
    if ($pushrecord == 1 && isset($request['filterworkordersParent']) && !empty($request['filterworkordersParent']))
       { 
     $workordersParent = workorders::getworkorders('id',$record['workorderid'], $request['workordersparentobject']); 
        if (isset($request['checkworkordersParentExists']) && !empty($request['checkworkordersParentExists'])) 
 {
        if(count($workordersParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $workordersParent = workorders::getworkorders('id',$record['workorderid']); } 
 if (($pushrecord == 1) && (count($workordersParent) > 0)) $record['workordersParent'] = $workordersParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
