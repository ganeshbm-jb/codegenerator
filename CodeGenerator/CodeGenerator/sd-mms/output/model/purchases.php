<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class purchases extends Model
{
	protected $table = 'purchases';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savepurchases($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = purchases::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = purchases::where('id', $id)->first();
			if (empty($record)) {
				$record = new purchases;
			}

			$record->id = $input['id'];
			if(isset($input['purchasedate']))
			$record->purchasedate = $input['purchasedate'];
		if(isset($input['supplierid']))
			$record->supplierid = $input['supplierid'];
		if(isset($input['code']))
			$record->code = $input['code'];
		if(isset($input['billno']))
			$record->billno = $input['billno'];
		if(isset($input['billamt']))
			$record->billamt = $input['billamt'];
		if(isset($input['sgst']))
			$record->sgst = $input['sgst'];
		if(isset($input['cgst']))
			$record->cgst = $input['cgst'];
		if(isset($input['igst']))
			$record->igst = $input['igst'];
		if(isset($input['sgstamt']))
			$record->sgstamt = $input['sgstamt'];
		if(isset($input['cgstamt']))
			$record->cgstamt = $input['cgstamt'];
		if(isset($input['igstamt']))
			$record->igstamt = $input['igstamt'];
		if(isset($input['gstapplicable']))
			$record->gstapplicable = $input['gstapplicable'];
		if(isset($input['remarks']))
			$record->remarks = $input['remarks'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['purchaseorderid']))
			$record->purchaseorderid = $input['purchaseorderid'];
		if(isset($input['totalbeforegst']))
			$record->totalbeforegst = $input['totalbeforegst'];
		if(isset($input['totalaftergst']))
			$record->totalaftergst = $input['totalaftergst'];
		if(isset($input['totalitems']))
			$record->totalitems = $input['totalitems'];
		if(isset($input['transportname']))
			$record->transportname = $input['transportname'];
		if(isset($input['vehicleno']))
			$record->vehicleno = $input['vehicleno'];
		if(isset($input['factoryid']))
			$record->factoryid = $input['factoryid'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getpurchases('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listpurchases($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = purchases::all();
		else
		{
			$result = purchases::select('purchases.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getpurchases($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = purchases::where($col, $value)->get();
		else
		{
			$result = purchases::select('purchases.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 2;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $suppliersParent = []; 
    if ($pushrecord == 1 && isset($request['filtersuppliersParent']) && !empty($request['filtersuppliersParent']))
       { 
     $suppliersParent = suppliers::getsuppliers('id',$record['supplierid'], $request['suppliersparentobject']); 
        if (isset($request['checksuppliersParentExists']) && !empty($request['checksuppliersParentExists'])) 
 {
        if(count($suppliersParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $suppliersParent = suppliers::getsuppliers('id',$record['supplierid']); } 
 if (($pushrecord == 1) && (count($suppliersParent) > 0)) $record['suppliersParent'] = $suppliersParent[0]; 

    $factoryParent = []; 
    if ($pushrecord == 1 && isset($request['filterfactoryParent']) && !empty($request['filterfactoryParent']))
       { 
     $factoryParent = factory::getfactory('id',$record['factoryid'], $request['factoryparentobject']); 
        if (isset($request['checkfactoryParentExists']) && !empty($request['checkfactoryParentExists'])) 
 {
        if(count($factoryParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $factoryParent = factory::getfactory('id',$record['factoryid']); } 
 if (($pushrecord == 1) && (count($factoryParent) > 0)) $record['factoryParent'] = $factoryParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 1;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			
        if ($pushrecord == 1 && isset($request['loadpurchasematerialList']) && !empty($request['loadpurchasematerialList']))
        { 
    $record['purchasematerialList'] = purchasematerial::getpurchasematerial('purchaseid',$record['id'], $request['purchasematerialobject']); 
        if (isset($request['checkpurchasematerialExists']) && !empty($request['checkpurchasematerialExists'])) 
 {
        if(count($record['purchasematerialList']) == 0) $pushrecord = 0; 
  } 
 }

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
