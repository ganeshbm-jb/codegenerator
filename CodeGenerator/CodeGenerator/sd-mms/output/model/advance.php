<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class advance extends Model
{
	protected $table = 'advance';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveadvance($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = advance::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = advance::where('id', $id)->first();
			if (empty($record)) {
				$record = new advance;
			}

			$record->id = $input['id'];
			if(isset($input['description']))
			$record->description = $input['description'];
		if(isset($input['atypeid']))
			$record->atypeid = $input['atypeid'];
		if(isset($input['amount']))
			$record->amount = $input['amount'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['advancedate']))
			$record->advancedate = $input['advancedate'];
		if(isset($input['source']))
			$record->source = $input['source'];
		if(isset($input['sourceid']))
			$record->sourceid = $input['sourceid'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getadvance('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listadvance($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = advance::all();
		else
		{
			$result = advance::select('advance.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getadvance($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = advance::where($col, $value)->get();
		else
		{
			$result = advance::select('advance.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 1;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $advancetypeParent = []; 
    if ($pushrecord == 1 && isset($request['filteradvancetypeParent']) && !empty($request['filteradvancetypeParent']))
       { 
     $advancetypeParent = advancetype::getadvancetype('id',$record['atypeid'], $request['advancetypeparentobject']); 
        if (isset($request['checkadvancetypeParentExists']) && !empty($request['checkadvancetypeParentExists'])) 
 {
        if(count($advancetypeParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $advancetypeParent = advancetype::getadvancetype('id',$record['atypeid']); } 
 if (($pushrecord == 1) && (count($advancetypeParent) > 0)) $record['advancetypeParent'] = $advancetypeParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
