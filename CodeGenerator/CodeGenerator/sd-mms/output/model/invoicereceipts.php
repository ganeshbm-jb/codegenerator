<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class invoicereceipts extends Model
{
	protected $table = 'invoicereceipts';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveinvoicereceipts($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = invoicereceipts::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = invoicereceipts::where('id', $id)->first();
			if (empty($record)) {
				$record = new invoicereceipts;
			}

			$record->id = $input['id'];
			if(isset($input['woid']))
			$record->woid = $input['woid'];
		if(isset($input['receiptsdate']))
			$record->receiptsdate = $input['receiptsdate'];
		if(isset($input['amount']))
			$record->amount = $input['amount'];
		if(isset($input['paymentmode']))
			$record->paymentmode = $input['paymentmode'];
		if(isset($input['txndate']))
			$record->txndate = $input['txndate'];
		if(isset($input['txnno']))
			$record->txnno = $input['txnno'];
		if(isset($input['txnbank']))
			$record->txnbank = $input['txnbank'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['txnstatus']))
			$record->txnstatus = $input['txnstatus'];
		if(isset($input['duedate']))
			$record->duedate = $input['duedate'];
		if(isset($input['code']))
			$record->code = $input['code'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getinvoicereceipts('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listinvoicereceipts($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = invoicereceipts::all();
		else
		{
			$result = invoicereceipts::select('invoicereceipts.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getinvoicereceipts($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = invoicereceipts::where($col, $value)->get();
		else
		{
			$result = invoicereceipts::select('invoicereceipts.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 1;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $workordersParent = []; 
    if ($pushrecord == 1 && isset($request['filterworkordersParent']) && !empty($request['filterworkordersParent']))
       { 
     $workordersParent = workorders::getworkorders('id',$record['woid'], $request['workordersparentobject']); 
        if (isset($request['checkworkordersParentExists']) && !empty($request['checkworkordersParentExists'])) 
 {
        if(count($workordersParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $workordersParent = workorders::getworkorders('id',$record['woid']); } 
 if (($pushrecord == 1) && (count($workordersParent) > 0)) $record['workordersParent'] = $workordersParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
