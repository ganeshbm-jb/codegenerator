<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class workers extends Model
{
	protected $table = 'workers';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveworkers($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = workers::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = workers::where('id', $id)->first();
			if (empty($record)) {
				$record = new workers;
			}

			$record->id = $input['id'];
			if(isset($input['name']))
			$record->name = $input['name'];
		if(isset($input['phoneno']))
			$record->phoneno = $input['phoneno'];
		if(isset($input['salary']))
			$record->salary = $input['salary'];
		if(isset($input['worktype']))
			$record->worktype = $input['worktype'];
		if(isset($input['salarytype']))
			$record->salarytype = $input['salarytype'];
		if(isset($input['desiginationtype']))
			$record->desiginationtype = $input['desiginationtype'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getworkers('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listworkers($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workers::all();
		else
		{
			$result = workers::select('workers.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getworkers($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workers::where($col, $value)->get();
		else
		{
			$result = workers::select('workers.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 3;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $worktypeParent = []; 
    if ($pushrecord == 1 && isset($request['filterworktypeParent']) && !empty($request['filterworktypeParent']))
       { 
     $worktypeParent = worktype::getworktype('worktype.id',$record['worktype'], $request['worktypeparentobject']); 
        if (isset($request['checkworktypeParentExists']) && !empty($request['checkworktypeParentExists'])) 
 {
        if(count($worktypeParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $worktypeParent = worktype::getworktype('worktype.id',$record['worktype']); } 
 if (($pushrecord == 1) && (count($worktypeParent) > 0)) $record['worktypeParent'] = $worktypeParent[0]; 

    $salarytypeParent = []; 
    if ($pushrecord == 1 && isset($request['filtersalarytypeParent']) && !empty($request['filtersalarytypeParent']))
       { 
     $salarytypeParent = salarytype::getsalarytype('salarytype.id',$record['salarytype'], $request['salarytypeparentobject']); 
        if (isset($request['checksalarytypeParentExists']) && !empty($request['checksalarytypeParentExists'])) 
 {
        if(count($salarytypeParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $salarytypeParent = salarytype::getsalarytype('salarytype.id',$record['salarytype']); } 
 if (($pushrecord == 1) && (count($salarytypeParent) > 0)) $record['salarytypeParent'] = $salarytypeParent[0]; 

    $desiginationtypeParent = []; 
    if ($pushrecord == 1 && isset($request['filterdesiginationtypeParent']) && !empty($request['filterdesiginationtypeParent']))
       { 
     $desiginationtypeParent = desiginationtype::getdesiginationtype('desiginationtype.id',$record['desiginationtype'], $request['desiginationtypeparentobject']); 
        if (isset($request['checkdesiginationtypeParentExists']) && !empty($request['checkdesiginationtypeParentExists'])) 
 {
        if(count($desiginationtypeParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $desiginationtypeParent = desiginationtype::getdesiginationtype('desiginationtype.id',$record['desiginationtype']); } 
 if (($pushrecord == 1) && (count($desiginationtypeParent) > 0)) $record['desiginationtypeParent'] = $desiginationtypeParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
