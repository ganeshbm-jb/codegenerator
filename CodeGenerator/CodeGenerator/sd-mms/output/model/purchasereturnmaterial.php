<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class purchasereturnmaterial extends Model
{
	protected $table = 'purchasereturnmaterial';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savepurchasereturnmaterial($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = purchasereturnmaterial::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = purchasereturnmaterial::where('id', $id)->first();
			if (empty($record)) {
				$record = new purchasereturnmaterial;
			}

			$record->id = $input['id'];
			if(isset($input['purchasereturnid']))
			$record->purchasereturnid = $input['purchasereturnid'];
		if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		if(isset($input['qty']))
			$record->qty = $input['qty'];
		if(isset($input['price']))
			$record->price = $input['price'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getpurchasereturnmaterial('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listpurchasereturnmaterial($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = purchasereturnmaterial::all();
		else
		{
			$result = purchasereturnmaterial::select('purchasereturnmaterial.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getpurchasereturnmaterial($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = purchasereturnmaterial::where($col, $value)->get();
		else
		{
			$result = purchasereturnmaterial::select('purchasereturnmaterial.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 1;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $materialsParent = []; 
    if ($pushrecord == 1 && isset($request['filtermaterialsParent']) && !empty($request['filtermaterialsParent']))
       { 
     $materialsParent = materials::getmaterials('id',$record['materialid'], $request['materialsparentobject']); 
        if (isset($request['checkmaterialsParentExists']) && !empty($request['checkmaterialsParentExists'])) 
 {
        if(count($materialsParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $materialsParent = materials::getmaterials('id',$record['materialid']); } 
 if (($pushrecord == 1) && (count($materialsParent) > 0)) $record['materialsParent'] = $materialsParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
