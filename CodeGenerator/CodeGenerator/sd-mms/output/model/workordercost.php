<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class workordercost extends Model
{
	protected $table = 'workordercost';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveworkordercost($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = workordercost::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = workordercost::where('id', $id)->first();
			if (empty($record)) {
				$record = new workordercost;
			}

			$record->id = $input['id'];
			if(isset($input['amount']))
			$record->amount = $input['amount'];
		if(isset($input['costtype']))
			$record->costtype = $input['costtype'];
		if(isset($input['description']))
			$record->description = $input['description'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getworkordercost('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listworkordercost($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workordercost::all();
		else
		{
			$result = workordercost::select('workordercost.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getworkordercost($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workordercost::where($col, $value)->get();
		else
		{
			$result = workordercost::select('workordercost.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 0;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
