<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class userlogins extends Model
{
	protected $table = 'userlogins';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveuserlogins($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = userlogins::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = userlogins::where('id', $id)->first();
			if (empty($record)) {
				$record = new userlogins;
			}

			$record->id = $input['id'];
			if(isset($input['userid']))
			$record->userid = $input['userid'];
		if(isset($input['username']))
			$record->username = $input['username'];
		if(isset($input['active']))
			$record->active = $input['active'];
		if(isset($input['source']))
			$record->source = $input['source'];
		if(isset($input['roles']))
			$record->roles = $input['roles'];
		if(isset($input['token']))
			$record->token = $input['token'];
		if(isset($input['lasttouchtime']))
			$record->lasttouchtime = $input['lasttouchtime'];
		if(isset($input['ipaddr']))
			$record->ipaddr = $input['ipaddr'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getuserlogins('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listuserlogins($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = userlogins::all();
		else
		{
			$result = userlogins::select('userlogins.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getuserlogins($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = userlogins::where($col, $value)->get();
		else
		{
			$result = userlogins::select('userlogins.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 0;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
