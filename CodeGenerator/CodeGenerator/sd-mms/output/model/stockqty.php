<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class stockqty extends Model
{
	protected $table = 'stockqty';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savestockqty($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = stockqty::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = stockqty::where('id', $id)->first();
			if (empty($record)) {
				$record = new stockqty;
			}

			$record->id = $input['id'];
			if(isset($input['parentid']))
			$record->parentid = $input['parentid'];
		if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		if(isset($input['opqty']))
			$record->opqty = $input['opqty'];
		if(isset($input['inqty']))
			$record->inqty = $input['inqty'];
		if(isset($input['outqty']))
			$record->outqty = $input['outqty'];
		if(isset($input['prqty']))
			$record->prqty = $input['prqty'];
		if(isset($input['frqty']))
			$record->frqty = $input['frqty'];
		if(isset($input['damageqty']))
			$record->damageqty = $input['damageqty'];
		if(isset($input['totalqty']))
			$record->totalqty = $input['totalqty'];
		if(isset($input['reorderqty']))
			$record->reorderqty = $input['reorderqty'];
		if(isset($input['reorder']))
			$record->reorder = $input['reorder'];
		if(isset($input['opqtyprice']))
			$record->opqtyprice = $input['opqtyprice'];
		if(isset($input['inqtyprice']))
			$record->inqtyprice = $input['inqtyprice'];
		if(isset($input['outqtyprice']))
			$record->outqtyprice = $input['outqtyprice'];
		if(isset($input['prqtyprice']))
			$record->prqtyprice = $input['prqtyprice'];
		if(isset($input['frqtyprice']))
			$record->frqtyprice = $input['frqtyprice'];
		if(isset($input['damageqtyprice']))
			$record->damageqtyprice = $input['damageqtyprice'];
		if(isset($input['totalqtyprice']))
			$record->totalqtyprice = $input['totalqtyprice'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getstockqty('stockqty.id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function liststockqty($request)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = stockqty::all();
		else
		{
			$result = stockqty::select("stockqty.*", "materials.name", "materials.unit", "materials.id as materialid")->join("materials", "materials.id", "=", "stockqty.materialid")->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getstockqty($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = stockqty::where($col, $value)->get();
		else
		{
			$result = stockqty::select("stockqty.*", "materials.name", "materials.unit", "materials.id as materialid")->join("materials", "materials.id", "=", "stockqty.materialid")->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 1;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $materialsParent = []; 
    if ($pushrecord == 1 && isset($request['filtermaterialsParent']) && !empty($request['filtermaterialsParent']))
       { 
     $materialsParent = materials::getmaterials('id',$record['materialid'], $request['materialsparentobject']); 
        if (isset($request['checkmaterialsParentExists']) && !empty($request['checkmaterialsParentExists'])) 
 {
        if(count($materialsParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $materialsParent = materials::getmaterials('id',$record['materialid']); } 
 if (($pushrecord == 1) && (count($materialsParent) > 0)) $record['materialsParent'] = $materialsParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 2;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			
        if ($pushrecord == 1 && isset($request['loadstockList']) && !empty($request['loadstockList']))
        { 
    $record['stockList'] = stock::getstock('materialid',$record['id'], $request['stockobject']); 
        if (isset($request['checkstockExists']) && !empty($request['checkstockExists'])) 
 {
        if(count($record['stockList']) == 0) $pushrecord = 0; 
  } 
 }
        if ($pushrecord == 1 && isset($request['loadstocklocationqtyList']) && !empty($request['loadstocklocationqtyList']))
        { 
    $record['stocklocationqtyList'] = stocklocationqty::getstocklocationqty('materialid',$record['id'], $request['stocklocationqtyobject']); 
        if (isset($request['checkstocklocationqtyExists']) && !empty($request['checkstocklocationqtyExists'])) 
 {
        if(count($record['stocklocationqtyList']) == 0) $pushrecord = 0; 
  } 
 }

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
