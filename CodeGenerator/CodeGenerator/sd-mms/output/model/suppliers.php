<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class suppliers extends Model
{
	protected $table = 'suppliers';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savesuppliers($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = suppliers::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = suppliers::where('id', $id)->first();
			if (empty($record)) {
				$record = new suppliers;
			}

			$record->id = $input['id'];
			if(isset($input['name']))
			$record->name = $input['name'];
		if(isset($input['address1']))
			$record->address1 = $input['address1'];
		if(isset($input['address2']))
			$record->address2 = $input['address2'];
		if(isset($input['city']))
			$record->city = $input['city'];
		if(isset($input['state']))
			$record->state = $input['state'];
		if(isset($input['pincode']))
			$record->pincode = $input['pincode'];
		if(isset($input['gstno']))
			$record->gstno = $input['gstno'];
		if(isset($input['sgst']))
			$record->sgst = $input['sgst'];
		if(isset($input['cgst']))
			$record->cgst = $input['cgst'];
		if(isset($input['igst']))
			$record->igst = $input['igst'];
		if(isset($input['phoneno']))
			$record->phoneno = $input['phoneno'];
		if(isset($input['contactperson']))
			$record->contactperson = $input['contactperson'];
		if(isset($input['country']))
			$record->country = $input['country'];
		if(isset($input['email']))
			$record->email = $input['email'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getsuppliers('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listsuppliers($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = suppliers::all();
		else
		{
			$result = suppliers::select('suppliers.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getsuppliers($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = suppliers::where($col, $value)->get();
		else
		{
			$result = suppliers::select('suppliers.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 0;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
