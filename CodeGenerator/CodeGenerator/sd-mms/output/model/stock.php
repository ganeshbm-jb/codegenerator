<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class stock extends Model
{
	protected $table = 'stock';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savestock($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = stock::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = stock::where('id', $id)->first();
			if (empty($record)) {
				$record = new stock;
			}

			$record->id = $input['id'];
			if(isset($input['parentid']))
			$record->parentid = $input['parentid'];
		if(isset($input['purchaseid']))
			$record->purchaseid = $input['purchaseid'];
		if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		if(isset($input['stockdate']))
			$record->stockdate = $input['stockdate'];
		if(isset($input['qty']))
			$record->qty = $input['qty'];
		if(isset($input['stocktypeid']))
			$record->stocktypeid = $input['stocktypeid'];
		if(isset($input['factoryid']))
			$record->factoryid = $input['factoryid'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['unitprice']))
			$record->unitprice = $input['unitprice'];
		if(isset($input['projectid']))
			$record->projectid = $input['projectid'];
		if(isset($input['purchaseorderid']))
			$record->purchaseorderid = $input['purchaseorderid'];
		if(isset($input['purchaseordercode']))
			$record->purchaseordercode = $input['purchaseordercode'];
		if(isset($input['purchaseorderproject']))
			$record->purchaseorderproject = $input['purchaseorderproject'];
		if(isset($input['sgst']))
			$record->sgst = $input['sgst'];
		if(isset($input['cgst']))
			$record->cgst = $input['cgst'];
		if(isset($input['sgstamount']))
			$record->sgstamount = $input['sgstamount'];
		if(isset($input['cgstamount']))
			$record->cgstamount = $input['cgstamount'];
		if(isset($input['totalprice']))
			$record->totalprice = $input['totalprice'];
		if(isset($input['factoryid2']))
			$record->factoryid2 = $input['factoryid2'];
		if(isset($input['projectid2']))
			$record->projectid2 = $input['projectid2'];
		if(isset($input['description']))
			$record->description = $input['description'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getstock('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function liststock($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = stock::all();
		else
		{
			$result = stock::select('stock.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getstock($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = stock::where($col, $value)->get();
		else
		{
			$result = stock::select('stock.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 6;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $purchasesParent = []; 
    if ($pushrecord == 1 && isset($request['filterpurchasesParent']) && !empty($request['filterpurchasesParent']))
       { 
     $purchasesParent = purchases::getpurchases('id',$record['purchaseid'], $request['purchasesparentobject']); 
        if (isset($request['checkpurchasesParentExists']) && !empty($request['checkpurchasesParentExists'])) 
 {
        if(count($purchasesParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $purchasesParent = purchases::getpurchases('id',$record['purchaseid']); } 
 if (($pushrecord == 1) && (count($purchasesParent) > 0)) $record['purchasesParent'] = $purchasesParent[0]; 

    $materialsParent = []; 
    if ($pushrecord == 1 && isset($request['filtermaterialsParent']) && !empty($request['filtermaterialsParent']))
       { 
     $materialsParent = materials::getmaterials('id',$record['materialid'], $request['materialsparentobject']); 
        if (isset($request['checkmaterialsParentExists']) && !empty($request['checkmaterialsParentExists'])) 
 {
        if(count($materialsParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $materialsParent = materials::getmaterials('id',$record['materialid']); } 
 if (($pushrecord == 1) && (count($materialsParent) > 0)) $record['materialsParent'] = $materialsParent[0]; 

    $stocktypeParent = []; 
    if ($pushrecord == 1 && isset($request['filterstocktypeParent']) && !empty($request['filterstocktypeParent']))
       { 
     $stocktypeParent = stocktype::getstocktype('id',$record['stocktypeid'], $request['stocktypeparentobject']); 
        if (isset($request['checkstocktypeParentExists']) && !empty($request['checkstocktypeParentExists'])) 
 {
        if(count($stocktypeParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $stocktypeParent = stocktype::getstocktype('id',$record['stocktypeid']); } 
 if (($pushrecord == 1) && (count($stocktypeParent) > 0)) $record['stocktypeParent'] = $stocktypeParent[0]; 

    $factoryParent = []; 
    if ($pushrecord == 1 && isset($request['filterfactoryParent']) && !empty($request['filterfactoryParent']))
       { 
     $factoryParent = factory::getfactory('id',$record['factoryid'], $request['factoryparentobject']); 
        if (isset($request['checkfactoryParentExists']) && !empty($request['checkfactoryParentExists'])) 
 {
        if(count($factoryParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $factoryParent = factory::getfactory('id',$record['factoryid']); } 
 if (($pushrecord == 1) && (count($factoryParent) > 0)) $record['factoryParent'] = $factoryParent[0]; 

    $projectsParent = []; 
    if ($pushrecord == 1 && isset($request['filterprojectsParent']) && !empty($request['filterprojectsParent']))
       { 
     $projectsParent = projects::getprojects('id',$record['projectid'], $request['projectsparentobject']); 
        if (isset($request['checkprojectsParentExists']) && !empty($request['checkprojectsParentExists'])) 
 {
        if(count($projectsParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $projectsParent = projects::getprojects('id',$record['projectid']); } 
 if (($pushrecord == 1) && (count($projectsParent) > 0)) $record['projectsParent'] = $projectsParent[0]; 

    $purchaseorderParent = []; 
    if ($pushrecord == 1 && isset($request['filterpurchaseorderParent']) && !empty($request['filterpurchaseorderParent']))
       { 
     $purchaseorderParent = purchaseorder::getpurchaseorder('id',$record['purchaseorderid'], $request['purchaseorderparentobject']); 
        if (isset($request['checkpurchaseorderParentExists']) && !empty($request['checkpurchaseorderParentExists'])) 
 {
        if(count($purchaseorderParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $purchaseorderParent = purchaseorder::getpurchaseorder('id',$record['purchaseorderid']); } 
 if (($pushrecord == 1) && (count($purchaseorderParent) > 0)) $record['purchaseorderParent'] = $purchaseorderParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
