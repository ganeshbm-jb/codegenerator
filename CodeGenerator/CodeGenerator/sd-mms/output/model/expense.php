<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class expense extends Model
{
	protected $table = 'expense';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveexpense($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = expense::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = expense::where('id', $id)->first();
			if (empty($record)) {
				$record = new expense;
			}

			$record->id = $input['id'];
			if(isset($input['edate']))
			$record->edate = $input['edate'];
		if(isset($input['description']))
			$record->description = $input['description'];
		if(isset($input['amount']))
			$record->amount = $input['amount'];
		if(isset($input['pid']))
			$record->pid = $input['pid'];
		if(isset($input['source']))
			$record->source = $input['source'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['pstatus']))
			$record->pstatus = $input['pstatus'];
		if(isset($input['nstatus']))
			$record->nstatus = $input['nstatus'];
		if(isset($input['active']))
			$record->active = $input['active'];
		if(isset($input['typeid']))
			$record->typeid = $input['typeid'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getexpense('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listexpense($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = expense::all();
		else
		{
			$result = expense::select('expense.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getexpense($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = expense::where($col, $value)->get();
		else
		{
			$result = expense::select('expense.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 1;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $expensetypeParent = []; 
    if ($pushrecord == 1 && isset($request['filterexpensetypeParent']) && !empty($request['filterexpensetypeParent']))
       { 
     $expensetypeParent = expensetype::getexpensetype('id',$record['typeid'], $request['expensetypeparentobject']); 
        if (isset($request['checkexpensetypeParentExists']) && !empty($request['checkexpensetypeParentExists'])) 
 {
        if(count($expensetypeParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $expensetypeParent = expensetype::getexpensetype('id',$record['typeid']); } 
 if (($pushrecord == 1) && (count($expensetypeParent) > 0)) $record['expensetypeParent'] = $expensetypeParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
