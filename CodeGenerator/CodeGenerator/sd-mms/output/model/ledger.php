<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class ledger extends Model
{
	protected $table = 'ledger';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveledger($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = ledger::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = ledger::where('id', $id)->first();
			if (empty($record)) {
				$record = new ledger;
			}

			$record->id = $input['id'];
			if(isset($input['srcid']))
			$record->srcid = $input['srcid'];
		if(isset($input['src']))
			$record->src = $input['src'];
		if(isset($input['tdate']))
			$record->tdate = $input['tdate'];
		if(isset($input['cramt']))
			$record->cramt = $input['cramt'];
		if(isset($input['dramt']))
			$record->dramt = $input['dramt'];
		if(isset($input['batchid']))
			$record->batchid = $input['batchid'];
		if(isset($input['batchstatus']))
			$record->batchstatus = $input['batchstatus'];
		if(isset($input['srctype']))
			$record->srctype = $input['srctype'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['description']))
			$record->description = $input['description'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getledger('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listledger($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = ledger::all();
		else
		{
			$result = ledger::select('ledger.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getledger($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = ledger::where($col, $value)->get();
		else
		{
			$result = ledger::select('ledger.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 0;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
