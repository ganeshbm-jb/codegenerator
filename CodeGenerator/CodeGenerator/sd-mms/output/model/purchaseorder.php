<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class purchaseorder extends Model
{
	protected $table = 'purchaseorder';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savepurchaseorder($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = purchaseorder::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = purchaseorder::where('id', $id)->first();
			if (empty($record)) {
				$record = new purchaseorder;
			}

			$record->id = $input['id'];
			if(isset($input['purchaseorderdate']))
			$record->purchaseorderdate = $input['purchaseorderdate'];
		if(isset($input['supplierid']))
			$record->supplierid = $input['supplierid'];
		if(isset($input['code']))
			$record->code = $input['code'];
		if(isset($input['billamt']))
			$record->billamt = $input['billamt'];
		if(isset($input['sgstamt']))
			$record->sgstamt = $input['sgstamt'];
		if(isset($input['cgstamt']))
			$record->cgstamt = $input['cgstamt'];
		if(isset($input['igstamt']))
			$record->igstamt = $input['igstamt'];
		if(isset($input['gstapplicable']))
			$record->gstapplicable = $input['gstapplicable'];
		if(isset($input['remarks']))
			$record->remarks = $input['remarks'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['projectid']))
			$record->projectid = $input['projectid'];
		if(isset($input['totalbeforegst']))
			$record->totalbeforegst = $input['totalbeforegst'];
		if(isset($input['paymentmode']))
			$record->paymentmode = $input['paymentmode'];
		if(isset($input['totalaftergst']))
			$record->totalaftergst = $input['totalaftergst'];
		if(isset($input['totalitems']))
			$record->totalitems = $input['totalitems'];
		if(isset($input['paidamount']))
			$record->paidamount = $input['paidamount'];
		if(isset($input['balanceamount']))
			$record->balanceamount = $input['balanceamount'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getpurchaseorder('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listpurchaseorder($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = purchaseorder::all();
		else
		{
			$result = purchaseorder::select('purchaseorder.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getpurchaseorder($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = purchaseorder::where($col, $value)->get();
		else
		{
			$result = purchaseorder::select('purchaseorder.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 2;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $suppliersParent = []; 
    if ($pushrecord == 1 && isset($request['filtersuppliersParent']) && !empty($request['filtersuppliersParent']))
       { 
     $suppliersParent = suppliers::getsuppliers('id',$record['supplierid'], $request['suppliersparentobject']); 
        if (isset($request['checksuppliersParentExists']) && !empty($request['checksuppliersParentExists'])) 
 {
        if(count($suppliersParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $suppliersParent = suppliers::getsuppliers('id',$record['supplierid']); } 
 if (($pushrecord == 1) && (count($suppliersParent) > 0)) $record['suppliersParent'] = $suppliersParent[0]; 

    $projectsParent = []; 
    if ($pushrecord == 1 && isset($request['filterprojectsParent']) && !empty($request['filterprojectsParent']))
       { 
     $projectsParent = projects::getprojects('id',$record['projectid'], $request['projectsparentobject']); 
        if (isset($request['checkprojectsParentExists']) && !empty($request['checkprojectsParentExists'])) 
 {
        if(count($projectsParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $projectsParent = projects::getprojects('id',$record['projectid']); } 
 if (($pushrecord == 1) && (count($projectsParent) > 0)) $record['projectsParent'] = $projectsParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 2;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			
        if ($pushrecord == 1 && isset($request['loadpurchaseordermaterialList']) && !empty($request['loadpurchaseordermaterialList']))
        { 
    $record['purchaseordermaterialList'] = purchaseordermaterial::getpurchaseordermaterial('purchaseorderid',$record['id'], $request['purchaseordermaterialobject']); 
        if (isset($request['checkpurchaseordermaterialExists']) && !empty($request['checkpurchaseordermaterialExists'])) 
 {
        if(count($record['purchaseordermaterialList']) == 0) $pushrecord = 0; 
  } 
 }
        if ($pushrecord == 1 && isset($request['loadpurchasesList']) && !empty($request['loadpurchasesList']))
        { 
    $record['purchasesList'] = purchases::getpurchases('purchaseorderid',$record['id'], $request['purchasesobject']); 
        if (isset($request['checkpurchasesExists']) && !empty($request['checkpurchasesExists'])) 
 {
        if(count($record['purchasesList']) == 0) $pushrecord = 0; 
  } 
 }

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
