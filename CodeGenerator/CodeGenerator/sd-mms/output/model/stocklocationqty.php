<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class stocklocationqty extends Model
{
	protected $table = 'stocklocationqty';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savestocklocationqty($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = stocklocationqty::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = stocklocationqty::where('id', $id)->first();
			if (empty($record)) {
				$record = new stocklocationqty;
			}

			$record->id = $input['id'];
			if(isset($input['parentid']))
			$record->parentid = $input['parentid'];
		if(isset($input['materialid']))
			$record->materialid = $input['materialid'];
		if(isset($input['factoryid']))
			$record->factoryid = $input['factoryid'];
		if(isset($input['inqty']))
			$record->inqty = $input['inqty'];
		if(isset($input['outqty']))
			$record->outqty = $input['outqty'];
		if(isset($input['totalqty']))
			$record->totalqty = $input['totalqty'];
		if(isset($input['lowstock']))
			$record->lowstock = $input['lowstock'];
		if(isset($input['inqtyprice']))
			$record->inqtyprice = $input['inqtyprice'];
		if(isset($input['outqtyprice']))
			$record->outqtyprice = $input['outqtyprice'];
		if(isset($input['totalqtyprice']))
			$record->totalqtyprice = $input['totalqtyprice'];
		if(isset($input['opqty']))
			$record->opqty = $input['opqty'];
		if(isset($input['opqtyprice']))
			$record->opqtyprice = $input['opqtyprice'];
		if(isset($input['prqty']))
			$record->prqty = $input['prqty'];
		if(isset($input['prqtyprice']))
			$record->prqtyprice = $input['prqtyprice'];
		if(isset($input['frqty']))
			$record->frqty = $input['frqty'];
		if(isset($input['frqtyprice']))
			$record->frqtyprice = $input['frqtyprice'];
		if(isset($input['damageqty']))
			$record->damageqty = $input['damageqty'];
		if(isset($input['damageqtyprice']))
			$record->damageqtyprice = $input['damageqtyprice'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getstocklocationqty('stocklocationqty.id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function liststocklocationqty($request)
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = stocklocationqty::all();
		else
		{
			$result = stocklocationqty::select("stocklocationqty.*", "materials.name", "materials.unit", "materials.id as materialid", "factory.id as factoryid", "factory.name as factoryname")->join("materials", "materials.id", "=", "stocklocationqty.materialid")->join("factory", "factory.id", "=", "stocklocationqty.factoryid")->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getstocklocationqty($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 1;
		if($relatedTableResult == 0)
			$result = stocklocationqty::where($col, $value)->get();
		else
		{
			$result = stocklocationqty::select("stocklocationqty.*", "materials.name", "materials.unit", "materials.id as materialid", "factory.id as factoryid", "factory.name as factoryname")->join("materials", "materials.id", "=", "stocklocationqty.materialid")->join("factory", "factory.id", "=", "stocklocationqty.factoryid")->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 2;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $materialsParent = []; 
    if ($pushrecord == 1 && isset($request['filtermaterialsParent']) && !empty($request['filtermaterialsParent']))
       { 
     $materialsParent = materials::getmaterials('id',$record['materialid'], $request['materialsparentobject']); 
        if (isset($request['checkmaterialsParentExists']) && !empty($request['checkmaterialsParentExists'])) 
 {
        if(count($materialsParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $materialsParent = materials::getmaterials('id',$record['materialid']); } 
 if (($pushrecord == 1) && (count($materialsParent) > 0)) $record['materialsParent'] = $materialsParent[0]; 

    $factoryParent = []; 
    if ($pushrecord == 1 && isset($request['filterfactoryParent']) && !empty($request['filterfactoryParent']))
       { 
     $factoryParent = factory::getfactory('id',$record['factoryid'], $request['factoryparentobject']); 
        if (isset($request['checkfactoryParentExists']) && !empty($request['checkfactoryParentExists'])) 
 {
        if(count($factoryParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $factoryParent = factory::getfactory('id',$record['factoryid']); } 
 if (($pushrecord == 1) && (count($factoryParent) > 0)) $record['factoryParent'] = $factoryParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
