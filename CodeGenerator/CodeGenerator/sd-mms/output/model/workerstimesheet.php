<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class workerstimesheet extends Model
{
	protected $table = 'workerstimesheet';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveworkerstimesheet($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = workerstimesheet::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = workerstimesheet::where('id', $id)->first();
			if (empty($record)) {
				$record = new workerstimesheet;
			}

			$record->id = $input['id'];
			if(isset($input['workersid']))
			$record->workersid = $input['workersid'];
		if(isset($input['tdate']))
			$record->tdate = $input['tdate'];
		if(isset($input['timein']))
			$record->timein = $input['timein'];
		if(isset($input['timeout']))
			$record->timeout = $input['timeout'];
		if(isset($input['totalhours']))
			$record->totalhours = $input['totalhours'];
		if(isset($input['description']))
			$record->description = $input['description'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getworkerstimesheet('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listworkerstimesheet($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workerstimesheet::all();
		else
		{
			$result = workerstimesheet::select('workerstimesheet.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getworkerstimesheet($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workerstimesheet::where($col, $value)->get();
		else
		{
			$result = workerstimesheet::select('workerstimesheet.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 1;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $workersParent = []; 
    if ($pushrecord == 1 && isset($request['filterworkersParent']) && !empty($request['filterworkersParent']))
       { 
     $workersParent = workers::getworkers('id',$record['workersid'], $request['workersparentobject']); 
        if (isset($request['checkworkersParentExists']) && !empty($request['checkworkersParentExists'])) 
 {
        if(count($workersParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $workersParent = workers::getworkers('id',$record['workersid']); } 
 if (($pushrecord == 1) && (count($workersParent) > 0)) $record['workersParent'] = $workersParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
