<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class materials extends Model
{
	protected $table = 'materials';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savematerials($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = materials::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = materials::where('id', $id)->first();
			if (empty($record)) {
				$record = new materials;
			}

			$record->id = $input['id'];
			if(isset($input['code']))
			$record->code = $input['code'];
		if(isset($input['name']))
			$record->name = $input['name'];
		if(isset($input['unit']))
			$record->unit = $input['unit'];
		if(isset($input['hsncode']))
			$record->hsncode = $input['hsncode'];
		if(isset($input['sgst']))
			$record->sgst = $input['sgst'];
		if(isset($input['cgst']))
			$record->cgst = $input['cgst'];
		if(isset($input['materialtype']))
			$record->materialtype = $input['materialtype'];
		if(isset($input['materialcategoryid']))
			$record->materialcategoryid = $input['materialcategoryid'];
		if(isset($input['reorderqty']))
			$record->reorderqty = $input['reorderqty'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getmaterials('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listmaterials($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = materials::all();
		else
		{
			$result = materials::select('materials.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getmaterials($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = materials::where($col, $value)->get();
		else
		{
			$result = materials::select('materials.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 3;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $unitsParent = []; 
    if ($pushrecord == 1 && isset($request['filterunitsParent']) && !empty($request['filterunitsParent']))
       { 
     $unitsParent = units::getunits('id',$record['unit'], $request['unitsparentobject']); 
        if (isset($request['checkunitsParentExists']) && !empty($request['checkunitsParentExists'])) 
 {
        if(count($unitsParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $unitsParent = units::getunits('id',$record['unit']); } 
 if (($pushrecord == 1) && (count($unitsParent) > 0)) $record['unitsParent'] = $unitsParent[0]; 

    $materialtypeParent = []; 
    if ($pushrecord == 1 && isset($request['filtermaterialtypeParent']) && !empty($request['filtermaterialtypeParent']))
       { 
     $materialtypeParent = materialtype::getmaterialtype('id',$record['materialtype'], $request['materialtypeparentobject']); 
        if (isset($request['checkmaterialtypeParentExists']) && !empty($request['checkmaterialtypeParentExists'])) 
 {
        if(count($materialtypeParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $materialtypeParent = materialtype::getmaterialtype('id',$record['materialtype']); } 
 if (($pushrecord == 1) && (count($materialtypeParent) > 0)) $record['materialtypeParent'] = $materialtypeParent[0]; 

    $materialcategoryParent = []; 
    if ($pushrecord == 1 && isset($request['filtermaterialcategoryParent']) && !empty($request['filtermaterialcategoryParent']))
       { 
     $materialcategoryParent = materialcategory::getmaterialcategory('id',$record['materialcategoryid'], $request['materialcategoryparentobject']); 
        if (isset($request['checkmaterialcategoryParentExists']) && !empty($request['checkmaterialcategoryParentExists'])) 
 {
        if(count($materialcategoryParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $materialcategoryParent = materialcategory::getmaterialcategory('id',$record['materialcategoryid']); } 
 if (($pushrecord == 1) && (count($materialcategoryParent) > 0)) $record['materialcategoryParent'] = $materialcategoryParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
