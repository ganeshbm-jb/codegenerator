<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class menus extends Model
{
	protected $table = 'menus';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savemenus($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = menus::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = menus::where('id', $id)->first();
			if (empty($record)) {
				$record = new menus;
			}

			$record->id = $input['id'];
			if(isset($input['code']))
			$record->code = $input['code'];
		if(isset($input['name']))
			$record->name = $input['name'];
		if(isset($input['menutype']))
			$record->menutype = $input['menutype'];
		if(isset($input['parent']))
			$record->parent = $input['parent'];
		if(isset($input['menuicon']))
			$record->menuicon = $input['menuicon'];
		if(isset($input['menuurl']))
			$record->menuurl = $input['menuurl'];
		if(isset($input['active']))
			$record->active = $input['active'];
		if(isset($input['morder']))
			$record->morder = $input['morder'];
		if(isset($input['role1']))
			$record->role1 = $input['role1'];
		if(isset($input['role2']))
			$record->role2 = $input['role2'];
		if(isset($input['role3']))
			$record->role3 = $input['role3'];
		if(isset($input['role4']))
			$record->role4 = $input['role4'];
		if(isset($input['role5']))
			$record->role5 = $input['role5'];
		if(isset($input['role6']))
			$record->role6 = $input['role6'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getmenus('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listmenus($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = menus::all();
		else
		{
			$result = menus::select('menus.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getmenus($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = menus::where($col, $value)->get();
		else
		{
			$result = menus::select('menus.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 0;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
