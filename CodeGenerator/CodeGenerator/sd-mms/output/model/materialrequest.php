<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class materialrequest extends Model
{
	protected $table = 'materialrequest';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savematerialrequest($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = materialrequest::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = materialrequest::where('id', $id)->first();
			if (empty($record)) {
				$record = new materialrequest;
			}

			$record->id = $input['id'];
			if(isset($input['requestno']))
			$record->requestno = $input['requestno'];
		if(isset($input['requestdate']))
			$record->requestdate = $input['requestdate'];
		if(isset($input['duedate']))
			$record->duedate = $input['duedate'];
		if(isset($input['projectid']))
			$record->projectid = $input['projectid'];
		if(isset($input['factoryid']))
			$record->factoryid = $input['factoryid'];
		if(isset($input['requesttype']))
			$record->requesttype = $input['requesttype'];
		if(isset($input['status']))
			$record->status = $input['status'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getmaterialrequest('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listmaterialrequest($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = materialrequest::all();
		else
		{
			$result = materialrequest::select('materialrequest.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getmaterialrequest($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = materialrequest::where($col, $value)->get();
		else
		{
			$result = materialrequest::select('materialrequest.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 3;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $projectsParent = []; 
    if ($pushrecord == 1 && isset($request['filterprojectsParent']) && !empty($request['filterprojectsParent']))
       { 
     $projectsParent = projects::getprojects('id',$record['projectid'], $request['projectsparentobject']); 
        if (isset($request['checkprojectsParentExists']) && !empty($request['checkprojectsParentExists'])) 
 {
        if(count($projectsParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $projectsParent = projects::getprojects('id',$record['projectid']); } 
 if (($pushrecord == 1) && (count($projectsParent) > 0)) $record['projectsParent'] = $projectsParent[0]; 

    $factoryParent = []; 
    if ($pushrecord == 1 && isset($request['filterfactoryParent']) && !empty($request['filterfactoryParent']))
       { 
     $factoryParent = factory::getfactory('id',$record['factoryid'], $request['factoryparentobject']); 
        if (isset($request['checkfactoryParentExists']) && !empty($request['checkfactoryParentExists'])) 
 {
        if(count($factoryParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $factoryParent = factory::getfactory('id',$record['factoryid']); } 
 if (($pushrecord == 1) && (count($factoryParent) > 0)) $record['factoryParent'] = $factoryParent[0]; 

    $requesttypeParent = []; 
    if ($pushrecord == 1 && isset($request['filterrequesttypeParent']) && !empty($request['filterrequesttypeParent']))
       { 
     $requesttypeParent = requesttype::getrequesttype('id',$record['requesttype'], $request['requesttypeparentobject']); 
        if (isset($request['checkrequesttypeParentExists']) && !empty($request['checkrequesttypeParentExists'])) 
 {
        if(count($requesttypeParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $requesttypeParent = requesttype::getrequesttype('id',$record['requesttype']); } 
 if (($pushrecord == 1) && (count($requesttypeParent) > 0)) $record['requesttypeParent'] = $requesttypeParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
