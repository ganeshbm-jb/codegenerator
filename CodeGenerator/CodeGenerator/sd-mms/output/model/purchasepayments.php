<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class purchasepayments extends Model
{
	protected $table = 'purchasepayments';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function savepurchasepayments($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = purchasepayments::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = purchasepayments::where('id', $id)->first();
			if (empty($record)) {
				$record = new purchasepayments;
			}

			$record->id = $input['id'];
			if(isset($input['poid']))
			$record->poid = $input['poid'];
		if(isset($input['paymentdate']))
			$record->paymentdate = $input['paymentdate'];
		if(isset($input['amount']))
			$record->amount = $input['amount'];
		if(isset($input['paymentmode']))
			$record->paymentmode = $input['paymentmode'];
		if(isset($input['txndate']))
			$record->txndate = $input['txndate'];
		if(isset($input['txnno']))
			$record->txnno = $input['txnno'];
		if(isset($input['txnbank']))
			$record->txnbank = $input['txnbank'];
		if(isset($input['status']))
			$record->status = $input['status'];
		if(isset($input['txnstatus']))
			$record->txnstatus = $input['txnstatus'];
		if(isset($input['duedate']))
			$record->duedate = $input['duedate'];
		if(isset($input['code']))
			$record->code = $input['code'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getpurchasepayments('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listpurchasepayments($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = purchasepayments::all();
		else
		{
			$result = purchasepayments::select('purchasepayments.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getpurchasepayments($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = purchasepayments::where($col, $value)->get();
		else
		{
			$result = purchasepayments::select('purchasepayments.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 1;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $purchaseorderParent = []; 
    if ($pushrecord == 1 && isset($request['filterpurchaseorderParent']) && !empty($request['filterpurchaseorderParent']))
       { 
     $purchaseorderParent = purchaseorder::getpurchaseorder('id',$record['poid'], $request['purchaseorderparentobject']); 
        if (isset($request['checkpurchaseorderParentExists']) && !empty($request['checkpurchaseorderParentExists'])) 
 {
        if(count($purchaseorderParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $purchaseorderParent = purchaseorder::getpurchaseorder('id',$record['poid']); } 
 if (($pushrecord == 1) && (count($purchaseorderParent) > 0)) $record['purchaseorderParent'] = $purchaseorderParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
