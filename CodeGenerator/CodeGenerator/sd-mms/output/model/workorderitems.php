<?php

namespace App\Models;
//use App\Models\DataHandler;
use Illuminate\Database\Eloquent\Model;

class workorderitems extends Model
{
	protected $table = 'workorderitems';
    protected $casts = ['id' => 'string'];
    public $incrementing = false;

	// Method to save data
	public static function saveworkorderitems($id, $input)
	{
		if(isset($input['delete']) && ($input['delete'] == '1'))
		{
			 $record = workorderitems::where('id', $id)->firstorfail()->delete();
		}
		else
		{
			$input['id'] = $id;
			$record = workorderitems::where('id', $id)->first();
			if (empty($record)) {
				$record = new workorderitems;
			}

			$record->id = $input['id'];
			if(isset($input['workorderid']))
			$record->workorderid = $input['workorderid'];
		if(isset($input['itemid']))
			$record->itemid = $input['itemid'];
		if(isset($input['qty']))
			$record->qty = $input['qty'];
		if(isset($input['unitprice']))
			$record->unitprice = $input['unitprice'];
		if(isset($input['price']))
			$record->price = $input['price'];
		if(isset($input['unittype']))
			$record->unittype = $input['unittype'];
		if(isset($input['itemunit']))
			$record->itemunit = $input['itemunit'];
		if(isset($input['sgst']))
			$record->sgst = $input['sgst'];
		if(isset($input['cgst']))
			$record->cgst = $input['cgst'];
		if(isset($input['sgstamount']))
			$record->sgstamount = $input['sgstamount'];
		if(isset($input['cgstamount']))
			$record->cgstamount = $input['cgstamount'];
		if(isset($input['totalbeforegst']))
			$record->totalbeforegst = $input['totalbeforegst'];
		if(isset($input['totalaftergst']))
			$record->totalaftergst = $input['totalaftergst'];
		        
			$record = DBHandler::updateUsers($record, $input);
			$record->save();     
			$record = Self::getworkorderitems('id', $input['id'])[0];
		}   
        return $record;
	}

	// Method to get all records
	public static function listworkorderitems($request)
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workorderitems::all();
		else
		{
			$result = workorderitems::select('workorderitems.*')->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}


	// Method to get a record
	public static function getworkorderitems($col, $value, $request = [])
	{
		$result = [];
		$relatedTableResult = 0;
		if($relatedTableResult == 0)
			$result = workorderitems::where($col, $value)->get();
		else
		{
			$result = workorderitems::select('workorderitems.*')->where($col, $value)->get();
		}
		$result->makeHidden(['created_by','updated_by','created_at','updated_at','deleted_at','vby','cby','aby','rby','vdate','cdate','rdate','adate']);
		if(!empty($request['filters']))
		{
			$result = DataHandler::filterData($request['filters'], $result);
		}
		if(!empty($request['orderby']))
		{
			$result = DataHandler::sortData($request['orderby'], $result);
		}
		return Self::digestResult($result, $request);
	}

	// Method to digest result
	private static function digestResult($result, $request = [])
	{
		// parent table
		$tempresult = [];
		$pushrecord = 1;
		$parenttablescount = 2;
		foreach($result as $record)
		{
			if($parenttablescount == 0) break;
			
    $workordersParent = []; 
    if ($pushrecord == 1 && isset($request['filterworkordersParent']) && !empty($request['filterworkordersParent']))
       { 
     $workordersParent = workorders::getworkorders('workorders.id',$record['workorderid'], $request['workordersparentobject']); 
        if (isset($request['checkworkordersParentExists']) && !empty($request['checkworkordersParentExists'])) 
 {
        if(count($workordersParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $workordersParent = workorders::getworkorders('workorders.id',$record['workorderid']); } 
 if (($pushrecord == 1) && (count($workordersParent) > 0)) $record['workordersParent'] = $workordersParent[0]; 

    $materialsParent = []; 
    if ($pushrecord == 1 && isset($request['filtermaterialsParent']) && !empty($request['filtermaterialsParent']))
       { 
     $materialsParent = materials::getmaterials('id',$record['itemid'], $request['materialsparentobject']); 
        if (isset($request['checkmaterialsParentExists']) && !empty($request['checkmaterialsParentExists'])) 
 {
        if(count($materialsParent) == 0) $pushrecord = 0; 
  } 
 }
 else 
 { 
 $materialsParent = materials::getmaterials('id',$record['itemid']); } 
 if (($pushrecord == 1) && (count($materialsParent) > 0)) $record['materialsParent'] = $materialsParent[0]; 


			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		// child table include
		$tempresult = [];
		$pushrecord = 1;
		$childtablescount = 0;
		foreach($result as $record)
		{
			if($childtablescount == 0) break;
			

			if($pushrecord == 1)
				array_push($tempresult, $record);
			$pushrecord = 1;
			$result = $tempresult;
		}		
		
		return $result;
	}
}
