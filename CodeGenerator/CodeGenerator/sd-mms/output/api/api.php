Route::group(['namespace' => 'meta', 'prefix' => 'advance'], function () { 
Route::any('save', 'advancecontroller@saveadvance');
Route::any('get', 'advancecontroller@getadvance');
Route::any('list', 'advancecontroller@listadvance');
Route::any('listcount', 'advancecontroller@listadvancecount');
Route::any('getcount', 'advancecontroller@getadvancecount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'advancetype'], function () { 
Route::any('save', 'advancetypecontroller@saveadvancetype');
Route::any('get', 'advancetypecontroller@getadvancetype');
Route::any('list', 'advancetypecontroller@listadvancetype');
Route::any('listcount', 'advancetypecontroller@listadvancetypecount');
Route::any('getcount', 'advancetypecontroller@getadvancetypecount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'batch'], function () { 
Route::any('save', 'batchcontroller@savebatch');
Route::any('get', 'batchcontroller@getbatch');
Route::any('list', 'batchcontroller@listbatch');
Route::any('listcount', 'batchcontroller@listbatchcount');
Route::any('getcount', 'batchcontroller@getbatchcount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'city'], function () { 
Route::any('save', 'citycontroller@savecity');
Route::any('get', 'citycontroller@getcity');
Route::any('list', 'citycontroller@listcity');
Route::any('listcount', 'citycontroller@listcitycount');
Route::any('getcount', 'citycontroller@getcitycount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'codemaster'], function () { 
Route::any('save', 'codemastercontroller@savecodemaster');
Route::any('get', 'codemastercontroller@getcodemaster');
Route::any('list', 'codemastercontroller@listcodemaster');
Route::any('listcount', 'codemastercontroller@listcodemastercount');
Route::any('getcount', 'codemastercontroller@getcodemastercount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'company'], function () { 
Route::any('save', 'companycontroller@savecompany');
Route::any('get', 'companycontroller@getcompany');
Route::any('list', 'companycontroller@listcompany');
Route::any('listcount', 'companycontroller@listcompanycount');
Route::any('getcount', 'companycontroller@getcompanycount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'dashboard'], function () { 
Route::any('save', 'dashboardcontroller@savedashboard');
Route::any('get', 'dashboardcontroller@getdashboard');
Route::any('list', 'dashboardcontroller@listdashboard');
Route::any('listcount', 'dashboardcontroller@listdashboardcount');
Route::any('getcount', 'dashboardcontroller@getdashboardcount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'description'], function () { 
Route::any('save', 'descriptioncontroller@savedescription');
Route::any('get', 'descriptioncontroller@getdescription');
Route::any('list', 'descriptioncontroller@listdescription');
Route::any('listcount', 'descriptioncontroller@listdescriptioncount');
Route::any('getcount', 'descriptioncontroller@getdescriptioncount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'desiginationtype'], function () { 
Route::any('save', 'desiginationtypecontroller@savedesiginationtype');
Route::any('get', 'desiginationtypecontroller@getdesiginationtype');
Route::any('list', 'desiginationtypecontroller@listdesiginationtype');
Route::any('listcount', 'desiginationtypecontroller@listdesiginationtypecount');
Route::any('getcount', 'desiginationtypecontroller@getdesiginationtypecount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'expense'], function () { 
Route::any('save', 'expensecontroller@saveexpense');
Route::any('get', 'expensecontroller@getexpense');
Route::any('list', 'expensecontroller@listexpense');
Route::any('listcount', 'expensecontroller@listexpensecount');
Route::any('getcount', 'expensecontroller@getexpensecount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'expensetype'], function () { 
Route::any('save', 'expensetypecontroller@saveexpensetype');
Route::any('get', 'expensetypecontroller@getexpensetype');
Route::any('list', 'expensetypecontroller@listexpensetype');
Route::any('listcount', 'expensetypecontroller@listexpensetypecount');
Route::any('getcount', 'expensetypecontroller@getexpensetypecount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'factory'], function () { 
Route::any('save', 'factorycontroller@savefactory');
Route::any('get', 'factorycontroller@getfactory');
Route::any('list', 'factorycontroller@listfactory');
Route::any('listcount', 'factorycontroller@listfactorycount');
Route::any('getcount', 'factorycontroller@getfactorycount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'invoice'], function () { 
Route::any('save', 'invoicecontroller@saveinvoice');
Route::any('get', 'invoicecontroller@getinvoice');
Route::any('list', 'invoicecontroller@listinvoice');
Route::any('listcount', 'invoicecontroller@listinvoicecount');
Route::any('getcount', 'invoicecontroller@getinvoicecount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'invoiceitem'], function () { 
Route::any('save', 'invoiceitemcontroller@saveinvoiceitem');
Route::any('get', 'invoiceitemcontroller@getinvoiceitem');
Route::any('list', 'invoiceitemcontroller@listinvoiceitem');
Route::any('listcount', 'invoiceitemcontroller@listinvoiceitemcount');
Route::any('getcount', 'invoiceitemcontroller@getinvoiceitemcount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'invoiceothers'], function () { 
Route::any('save', 'invoiceotherscontroller@saveinvoiceothers');
Route::any('get', 'invoiceotherscontroller@getinvoiceothers');
Route::any('list', 'invoiceotherscontroller@listinvoiceothers');
Route::any('listcount', 'invoiceotherscontroller@listinvoiceotherscount');
Route::any('getcount', 'invoiceotherscontroller@getinvoiceotherscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'invoicereceipts'], function () { 
Route::any('save', 'invoicereceiptscontroller@saveinvoicereceipts');
Route::any('get', 'invoicereceiptscontroller@getinvoicereceipts');
Route::any('list', 'invoicereceiptscontroller@listinvoicereceipts');
Route::any('listcount', 'invoicereceiptscontroller@listinvoicereceiptscount');
Route::any('getcount', 'invoicereceiptscontroller@getinvoicereceiptscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'item'], function () { 
Route::any('save', 'itemcontroller@saveitem');
Route::any('get', 'itemcontroller@getitem');
Route::any('list', 'itemcontroller@listitem');
Route::any('listcount', 'itemcontroller@listitemcount');
Route::any('getcount', 'itemcontroller@getitemcount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'itemmaterials'], function () { 
Route::any('save', 'itemmaterialscontroller@saveitemmaterials');
Route::any('get', 'itemmaterialscontroller@getitemmaterials');
Route::any('list', 'itemmaterialscontroller@listitemmaterials');
Route::any('listcount', 'itemmaterialscontroller@listitemmaterialscount');
Route::any('getcount', 'itemmaterialscontroller@getitemmaterialscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'ledger'], function () { 
Route::any('save', 'ledgercontroller@saveledger');
Route::any('get', 'ledgercontroller@getledger');
Route::any('list', 'ledgercontroller@listledger');
Route::any('listcount', 'ledgercontroller@listledgercount');
Route::any('getcount', 'ledgercontroller@getledgercount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'material'], function () { 
Route::any('save', 'materialcontroller@savematerial');
Route::any('get', 'materialcontroller@getmaterial');
Route::any('list', 'materialcontroller@listmaterial');
Route::any('listcount', 'materialcontroller@listmaterialcount');
Route::any('getcount', 'materialcontroller@getmaterialcount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'materialcategory'], function () { 
Route::any('save', 'materialcategorycontroller@savematerialcategory');
Route::any('get', 'materialcategorycontroller@getmaterialcategory');
Route::any('list', 'materialcategorycontroller@listmaterialcategory');
Route::any('listcount', 'materialcategorycontroller@listmaterialcategorycount');
Route::any('getcount', 'materialcategorycontroller@getmaterialcategorycount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'materialcatergory'], function () { 
Route::any('save', 'materialcatergorycontroller@savematerialcatergory');
Route::any('get', 'materialcatergorycontroller@getmaterialcatergory');
Route::any('list', 'materialcatergorycontroller@listmaterialcatergory');
Route::any('listcount', 'materialcatergorycontroller@listmaterialcatergorycount');
Route::any('getcount', 'materialcatergorycontroller@getmaterialcatergorycount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'materialnote'], function () { 
Route::any('save', 'materialnotecontroller@savematerialnote');
Route::any('get', 'materialnotecontroller@getmaterialnote');
Route::any('list', 'materialnotecontroller@listmaterialnote');
Route::any('listcount', 'materialnotecontroller@listmaterialnotecount');
Route::any('getcount', 'materialnotecontroller@getmaterialnotecount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'materialnotedetails'], function () { 
Route::any('save', 'materialnotedetailscontroller@savematerialnotedetails');
Route::any('get', 'materialnotedetailscontroller@getmaterialnotedetails');
Route::any('list', 'materialnotedetailscontroller@listmaterialnotedetails');
Route::any('listcount', 'materialnotedetailscontroller@listmaterialnotedetailscount');
Route::any('getcount', 'materialnotedetailscontroller@getmaterialnotedetailscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'materialrequest'], function () { 
Route::any('save', 'materialrequestcontroller@savematerialrequest');
Route::any('get', 'materialrequestcontroller@getmaterialrequest');
Route::any('list', 'materialrequestcontroller@listmaterialrequest');
Route::any('listcount', 'materialrequestcontroller@listmaterialrequestcount');
Route::any('getcount', 'materialrequestcontroller@getmaterialrequestcount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'materialrequestdetails'], function () { 
Route::any('save', 'materialrequestdetailscontroller@savematerialrequestdetails');
Route::any('get', 'materialrequestdetailscontroller@getmaterialrequestdetails');
Route::any('list', 'materialrequestdetailscontroller@listmaterialrequestdetails');
Route::any('listcount', 'materialrequestdetailscontroller@listmaterialrequestdetailscount');
Route::any('getcount', 'materialrequestdetailscontroller@getmaterialrequestdetailscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'materials'], function () { 
Route::any('save', 'materialscontroller@savematerials');
Route::any('get', 'materialscontroller@getmaterials');
Route::any('list', 'materialscontroller@listmaterials');
Route::any('listcount', 'materialscontroller@listmaterialscount');
Route::any('getcount', 'materialscontroller@getmaterialscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'materialtype'], function () { 
Route::any('save', 'materialtypecontroller@savematerialtype');
Route::any('get', 'materialtypecontroller@getmaterialtype');
Route::any('list', 'materialtypecontroller@listmaterialtype');
Route::any('listcount', 'materialtypecontroller@listmaterialtypecount');
Route::any('getcount', 'materialtypecontroller@getmaterialtypecount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'menus'], function () { 
Route::any('save', 'menuscontroller@savemenus');
Route::any('get', 'menuscontroller@getmenus');
Route::any('list', 'menuscontroller@listmenus');
Route::any('listcount', 'menuscontroller@listmenuscount');
Route::any('getcount', 'menuscontroller@getmenuscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'printmaster'], function () { 
Route::any('save', 'printmastercontroller@saveprintmaster');
Route::any('get', 'printmastercontroller@getprintmaster');
Route::any('list', 'printmastercontroller@listprintmaster');
Route::any('listcount', 'printmastercontroller@listprintmastercount');
Route::any('getcount', 'printmastercontroller@getprintmastercount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'projects'], function () { 
Route::any('save', 'projectscontroller@saveprojects');
Route::any('get', 'projectscontroller@getprojects');
Route::any('list', 'projectscontroller@listprojects');
Route::any('listcount', 'projectscontroller@listprojectscount');
Route::any('getcount', 'projectscontroller@getprojectscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'purchasematerial'], function () { 
Route::any('save', 'purchasematerialcontroller@savepurchasematerial');
Route::any('get', 'purchasematerialcontroller@getpurchasematerial');
Route::any('list', 'purchasematerialcontroller@listpurchasematerial');
Route::any('listcount', 'purchasematerialcontroller@listpurchasematerialcount');
Route::any('getcount', 'purchasematerialcontroller@getpurchasematerialcount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'purchaseorder'], function () { 
Route::any('save', 'purchaseordercontroller@savepurchaseorder');
Route::any('get', 'purchaseordercontroller@getpurchaseorder');
Route::any('list', 'purchaseordercontroller@listpurchaseorder');
Route::any('listcount', 'purchaseordercontroller@listpurchaseordercount');
Route::any('getcount', 'purchaseordercontroller@getpurchaseordercount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'purchaseordermaterial'], function () { 
Route::any('save', 'purchaseordermaterialcontroller@savepurchaseordermaterial');
Route::any('get', 'purchaseordermaterialcontroller@getpurchaseordermaterial');
Route::any('list', 'purchaseordermaterialcontroller@listpurchaseordermaterial');
Route::any('listcount', 'purchaseordermaterialcontroller@listpurchaseordermaterialcount');
Route::any('getcount', 'purchaseordermaterialcontroller@getpurchaseordermaterialcount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'purchasepayments'], function () { 
Route::any('save', 'purchasepaymentscontroller@savepurchasepayments');
Route::any('get', 'purchasepaymentscontroller@getpurchasepayments');
Route::any('list', 'purchasepaymentscontroller@listpurchasepayments');
Route::any('listcount', 'purchasepaymentscontroller@listpurchasepaymentscount');
Route::any('getcount', 'purchasepaymentscontroller@getpurchasepaymentscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'purchasereturn'], function () { 
Route::any('save', 'purchasereturncontroller@savepurchasereturn');
Route::any('get', 'purchasereturncontroller@getpurchasereturn');
Route::any('list', 'purchasereturncontroller@listpurchasereturn');
Route::any('listcount', 'purchasereturncontroller@listpurchasereturncount');
Route::any('getcount', 'purchasereturncontroller@getpurchasereturncount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'purchasereturnmaterial'], function () { 
Route::any('save', 'purchasereturnmaterialcontroller@savepurchasereturnmaterial');
Route::any('get', 'purchasereturnmaterialcontroller@getpurchasereturnmaterial');
Route::any('list', 'purchasereturnmaterialcontroller@listpurchasereturnmaterial');
Route::any('listcount', 'purchasereturnmaterialcontroller@listpurchasereturnmaterialcount');
Route::any('getcount', 'purchasereturnmaterialcontroller@getpurchasereturnmaterialcount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'purchases'], function () { 
Route::any('save', 'purchasescontroller@savepurchases');
Route::any('get', 'purchasescontroller@getpurchases');
Route::any('list', 'purchasescontroller@listpurchases');
Route::any('listcount', 'purchasescontroller@listpurchasescount');
Route::any('getcount', 'purchasescontroller@getpurchasescount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'requesttype'], function () { 
Route::any('save', 'requesttypecontroller@saverequesttype');
Route::any('get', 'requesttypecontroller@getrequesttype');
Route::any('list', 'requesttypecontroller@listrequesttype');
Route::any('listcount', 'requesttypecontroller@listrequesttypecount');
Route::any('getcount', 'requesttypecontroller@getrequesttypecount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'roles'], function () { 
Route::any('save', 'rolescontroller@saveroles');
Route::any('get', 'rolescontroller@getroles');
Route::any('list', 'rolescontroller@listroles');
Route::any('listcount', 'rolescontroller@listrolescount');
Route::any('getcount', 'rolescontroller@getrolescount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'salarytype'], function () { 
Route::any('save', 'salarytypecontroller@savesalarytype');
Route::any('get', 'salarytypecontroller@getsalarytype');
Route::any('list', 'salarytypecontroller@listsalarytype');
Route::any('listcount', 'salarytypecontroller@listsalarytypecount');
Route::any('getcount', 'salarytypecontroller@getsalarytypecount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'sites'], function () { 
Route::any('save', 'sitescontroller@savesites');
Route::any('get', 'sitescontroller@getsites');
Route::any('list', 'sitescontroller@listsites');
Route::any('listcount', 'sitescontroller@listsitescount');
Route::any('getcount', 'sitescontroller@getsitescount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'stock'], function () { 
Route::any('save', 'stockcontroller@savestock');
Route::any('get', 'stockcontroller@getstock');
Route::any('list', 'stockcontroller@liststock');
Route::any('listcount', 'stockcontroller@liststockcount');
Route::any('getcount', 'stockcontroller@getstockcount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'stocklocationqty'], function () { 
Route::any('save', 'stocklocationqtycontroller@savestocklocationqty');
Route::any('get', 'stocklocationqtycontroller@getstocklocationqty');
Route::any('list', 'stocklocationqtycontroller@liststocklocationqty');
Route::any('listcount', 'stocklocationqtycontroller@liststocklocationqtycount');
Route::any('getcount', 'stocklocationqtycontroller@getstocklocationqtycount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'stockqty'], function () { 
Route::any('save', 'stockqtycontroller@savestockqty');
Route::any('get', 'stockqtycontroller@getstockqty');
Route::any('list', 'stockqtycontroller@liststockqty');
Route::any('listcount', 'stockqtycontroller@liststockqtycount');
Route::any('getcount', 'stockqtycontroller@getstockqtycount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'stocktype'], function () { 
Route::any('save', 'stocktypecontroller@savestocktype');
Route::any('get', 'stocktypecontroller@getstocktype');
Route::any('list', 'stocktypecontroller@liststocktype');
Route::any('listcount', 'stocktypecontroller@liststocktypecount');
Route::any('getcount', 'stocktypecontroller@getstocktypecount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'stockvalue'], function () { 
Route::any('save', 'stockvaluecontroller@savestockvalue');
Route::any('get', 'stockvaluecontroller@getstockvalue');
Route::any('list', 'stockvaluecontroller@liststockvalue');
Route::any('listcount', 'stockvaluecontroller@liststockvaluecount');
Route::any('getcount', 'stockvaluecontroller@getstockvaluecount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'suppliers'], function () { 
Route::any('save', 'supplierscontroller@savesuppliers');
Route::any('get', 'supplierscontroller@getsuppliers');
Route::any('list', 'supplierscontroller@listsuppliers');
Route::any('listcount', 'supplierscontroller@listsupplierscount');
Route::any('getcount', 'supplierscontroller@getsupplierscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'units'], function () { 
Route::any('save', 'unitscontroller@saveunits');
Route::any('get', 'unitscontroller@getunits');
Route::any('list', 'unitscontroller@listunits');
Route::any('listcount', 'unitscontroller@listunitscount');
Route::any('getcount', 'unitscontroller@getunitscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'userlogins'], function () { 
Route::any('save', 'userloginscontroller@saveuserlogins');
Route::any('get', 'userloginscontroller@getuserlogins');
Route::any('list', 'userloginscontroller@listuserlogins');
Route::any('listcount', 'userloginscontroller@listuserloginscount');
Route::any('getcount', 'userloginscontroller@getuserloginscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'usermenus'], function () { 
Route::any('save', 'usermenuscontroller@saveusermenus');
Route::any('get', 'usermenuscontroller@getusermenus');
Route::any('list', 'usermenuscontroller@listusermenus');
Route::any('listcount', 'usermenuscontroller@listusermenuscount');
Route::any('getcount', 'usermenuscontroller@getusermenuscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'userroles'], function () { 
Route::any('save', 'userrolescontroller@saveuserroles');
Route::any('get', 'userrolescontroller@getuserroles');
Route::any('list', 'userrolescontroller@listuserroles');
Route::any('listcount', 'userrolescontroller@listuserrolescount');
Route::any('getcount', 'userrolescontroller@getuserrolescount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'users'], function () { 
Route::any('save', 'userscontroller@saveusers');
Route::any('get', 'userscontroller@getusers');
Route::any('list', 'userscontroller@listusers');
Route::any('listcount', 'userscontroller@listuserscount');
Route::any('getcount', 'userscontroller@getuserscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'userviewaccess'], function () { 
Route::any('save', 'userviewaccesscontroller@saveuserviewaccess');
Route::any('get', 'userviewaccesscontroller@getuserviewaccess');
Route::any('list', 'userviewaccesscontroller@listuserviewaccess');
Route::any('listcount', 'userviewaccesscontroller@listuserviewaccesscount');
Route::any('getcount', 'userviewaccesscontroller@getuserviewaccesscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'viewaccess'], function () { 
Route::any('save', 'viewaccesscontroller@saveviewaccess');
Route::any('get', 'viewaccesscontroller@getviewaccess');
Route::any('list', 'viewaccesscontroller@listviewaccess');
Route::any('listcount', 'viewaccesscontroller@listviewaccesscount');
Route::any('getcount', 'viewaccesscontroller@getviewaccesscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workers'], function () { 
Route::any('save', 'workerscontroller@saveworkers');
Route::any('get', 'workerscontroller@getworkers');
Route::any('list', 'workerscontroller@listworkers');
Route::any('listcount', 'workerscontroller@listworkerscount');
Route::any('getcount', 'workerscontroller@getworkerscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workerstimesheet'], function () { 
Route::any('save', 'workerstimesheetcontroller@saveworkerstimesheet');
Route::any('get', 'workerstimesheetcontroller@getworkerstimesheet');
Route::any('list', 'workerstimesheetcontroller@listworkerstimesheet');
Route::any('listcount', 'workerstimesheetcontroller@listworkerstimesheetcount');
Route::any('getcount', 'workerstimesheetcontroller@getworkerstimesheetcount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workflows'], function () { 
Route::any('save', 'workflowscontroller@saveworkflows');
Route::any('get', 'workflowscontroller@getworkflows');
Route::any('list', 'workflowscontroller@listworkflows');
Route::any('listcount', 'workflowscontroller@listworkflowscount');
Route::any('getcount', 'workflowscontroller@getworkflowscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workordercost'], function () { 
Route::any('save', 'workordercostcontroller@saveworkordercost');
Route::any('get', 'workordercostcontroller@getworkordercost');
Route::any('list', 'workordercostcontroller@listworkordercost');
Route::any('listcount', 'workordercostcontroller@listworkordercostcount');
Route::any('getcount', 'workordercostcontroller@getworkordercostcount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workorderitems'], function () { 
Route::any('save', 'workorderitemscontroller@saveworkorderitems');
Route::any('get', 'workorderitemscontroller@getworkorderitems');
Route::any('list', 'workorderitemscontroller@listworkorderitems');
Route::any('listcount', 'workorderitemscontroller@listworkorderitemscount');
Route::any('getcount', 'workorderitemscontroller@getworkorderitemscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workordermaterials'], function () { 
Route::any('save', 'workordermaterialscontroller@saveworkordermaterials');
Route::any('get', 'workordermaterialscontroller@getworkordermaterials');
Route::any('list', 'workordermaterialscontroller@listworkordermaterials');
Route::any('listcount', 'workordermaterialscontroller@listworkordermaterialscount');
Route::any('getcount', 'workordermaterialscontroller@getworkordermaterialscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workorders'], function () { 
Route::any('save', 'workorderscontroller@saveworkorders');
Route::any('get', 'workorderscontroller@getworkorders');
Route::any('list', 'workorderscontroller@listworkorders');
Route::any('listcount', 'workorderscontroller@listworkorderscount');
Route::any('getcount', 'workorderscontroller@getworkorderscount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workorderutilization'], function () { 
Route::any('save', 'workorderutilizationcontroller@saveworkorderutilization');
Route::any('get', 'workorderutilizationcontroller@getworkorderutilization');
Route::any('list', 'workorderutilizationcontroller@listworkorderutilization');
Route::any('listcount', 'workorderutilizationcontroller@listworkorderutilizationcount');
Route::any('getcount', 'workorderutilizationcontroller@getworkorderutilizationcount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'workorderutlizationmaterial'], function () { 
Route::any('save', 'workorderutlizationmaterialcontroller@saveworkorderutlizationmaterial');
Route::any('get', 'workorderutlizationmaterialcontroller@getworkorderutlizationmaterial');
Route::any('list', 'workorderutlizationmaterialcontroller@listworkorderutlizationmaterial');
Route::any('listcount', 'workorderutlizationmaterialcontroller@listworkorderutlizationmaterialcount');
Route::any('getcount', 'workorderutlizationmaterialcontroller@getworkorderutlizationmaterialcount');
});

Route::group(['namespace' => 'meta', 'prefix' => 'worktype'], function () { 
Route::any('save', 'worktypecontroller@saveworktype');
Route::any('get', 'worktypecontroller@getworktype');
Route::any('list', 'worktypecontroller@listworktype');
Route::any('listcount', 'worktypecontroller@listworktypecount');
Route::any('getcount', 'worktypecontroller@getworktypecount');
});


