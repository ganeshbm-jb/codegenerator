<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\stockvalueservice;
use App\Services\customservice;


class stockvaluecontroller  extends ParentController
{
    //
    public static function savestockvalue(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,stockvalueservice::getstockvalue($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return stockvalueservice::savestockvalue($id, $request);
    }

	// Method to get all records
	public static function liststockvalue(Request $request)
	{
		$request = Self::digestInput($request);
        return stockvalueservice::liststockvalue($request);
	}

    public static function getstockvalue(Request $request)
    {
        $request = Self::digestInput($request);
        return stockvalueservice::getstockvalue($request);
    }

	// Method to get all records
	public static function liststockvaluecount(Request $request)
	{
		$request = Self::digestInput($request);
        return stockvalueservice::liststockvaluecount($request);
	}

    public static function getstockvaluecount(Request $request)
    {
        $request = Self::digestInput($request);
        return stockvalueservice::getstockvaluecount($request);
    }
}

