<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\stocktypeservice;
use App\Services\customservice;


class stocktypecontroller  extends ParentController
{
    //
    public static function savestocktype(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,stocktypeservice::getstocktype($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return stocktypeservice::savestocktype($id, $request);
    }

	// Method to get all records
	public static function liststocktype(Request $request)
	{
		$request = Self::digestInput($request);
        return stocktypeservice::liststocktype($request);
	}

    public static function getstocktype(Request $request)
    {
        $request = Self::digestInput($request);
        return stocktypeservice::getstocktype($request);
    }

	// Method to get all records
	public static function liststocktypecount(Request $request)
	{
		$request = Self::digestInput($request);
        return stocktypeservice::liststocktypecount($request);
	}

    public static function getstocktypecount(Request $request)
    {
        $request = Self::digestInput($request);
        return stocktypeservice::getstocktypecount($request);
    }
}

