<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\materialrequestservice;
use App\Services\customservice;


class materialrequestcontroller  extends ParentController
{
    //
    public static function savematerialrequest(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,materialrequestservice::getmaterialrequest($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return materialrequestservice::savematerialrequest($id, $request);
    }

	// Method to get all records
	public static function listmaterialrequest(Request $request)
	{
		$request = Self::digestInput($request);
        return materialrequestservice::listmaterialrequest($request);
	}

    public static function getmaterialrequest(Request $request)
    {
        $request = Self::digestInput($request);
        return materialrequestservice::getmaterialrequest($request);
    }

	// Method to get all records
	public static function listmaterialrequestcount(Request $request)
	{
		$request = Self::digestInput($request);
        return materialrequestservice::listmaterialrequestcount($request);
	}

    public static function getmaterialrequestcount(Request $request)
    {
        $request = Self::digestInput($request);
        return materialrequestservice::getmaterialrequestcount($request);
    }
}

