<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\materialnotedetailsservice;
use App\Services\customservice;


class materialnotedetailscontroller  extends ParentController
{
    //
    public static function savematerialnotedetails(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,materialnotedetailsservice::getmaterialnotedetails($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return materialnotedetailsservice::savematerialnotedetails($id, $request);
    }

	// Method to get all records
	public static function listmaterialnotedetails(Request $request)
	{
		$request = Self::digestInput($request);
        return materialnotedetailsservice::listmaterialnotedetails($request);
	}

    public static function getmaterialnotedetails(Request $request)
    {
        $request = Self::digestInput($request);
        return materialnotedetailsservice::getmaterialnotedetails($request);
    }

	// Method to get all records
	public static function listmaterialnotedetailscount(Request $request)
	{
		$request = Self::digestInput($request);
        return materialnotedetailsservice::listmaterialnotedetailscount($request);
	}

    public static function getmaterialnotedetailscount(Request $request)
    {
        $request = Self::digestInput($request);
        return materialnotedetailsservice::getmaterialnotedetailscount($request);
    }
}

