<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\purchasematerialservice;
use App\Services\customservice;


class purchasematerialcontroller  extends ParentController
{
    //
    public static function savepurchasematerial(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,purchasematerialservice::getpurchasematerial($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return purchasematerialservice::savepurchasematerial($id, $request);
    }

	// Method to get all records
	public static function listpurchasematerial(Request $request)
	{
		$request = Self::digestInput($request);
        return purchasematerialservice::listpurchasematerial($request);
	}

    public static function getpurchasematerial(Request $request)
    {
        $request = Self::digestInput($request);
        return purchasematerialservice::getpurchasematerial($request);
    }

	// Method to get all records
	public static function listpurchasematerialcount(Request $request)
	{
		$request = Self::digestInput($request);
        return purchasematerialservice::listpurchasematerialcount($request);
	}

    public static function getpurchasematerialcount(Request $request)
    {
        $request = Self::digestInput($request);
        return purchasematerialservice::getpurchasematerialcount($request);
    }
}

