<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\unitsservice;
use App\Services\customservice;


class unitscontroller  extends ParentController
{
    //
    public static function saveunits(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,unitsservice::getunits($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return unitsservice::saveunits($id, $request);
    }

	// Method to get all records
	public static function listunits(Request $request)
	{
		$request = Self::digestInput($request);
        return unitsservice::listunits($request);
	}

    public static function getunits(Request $request)
    {
        $request = Self::digestInput($request);
        return unitsservice::getunits($request);
    }

	// Method to get all records
	public static function listunitscount(Request $request)
	{
		$request = Self::digestInput($request);
        return unitsservice::listunitscount($request);
	}

    public static function getunitscount(Request $request)
    {
        $request = Self::digestInput($request);
        return unitsservice::getunitscount($request);
    }
}

