<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\workflowsservice;
use App\Services\customservice;


class workflowscontroller  extends ParentController
{
    //
    public static function saveworkflows(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,workflowsservice::getworkflows($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return workflowsservice::saveworkflows($id, $request);
    }

	// Method to get all records
	public static function listworkflows(Request $request)
	{
		$request = Self::digestInput($request);
        return workflowsservice::listworkflows($request);
	}

    public static function getworkflows(Request $request)
    {
        $request = Self::digestInput($request);
        return workflowsservice::getworkflows($request);
    }

	// Method to get all records
	public static function listworkflowscount(Request $request)
	{
		$request = Self::digestInput($request);
        return workflowsservice::listworkflowscount($request);
	}

    public static function getworkflowscount(Request $request)
    {
        $request = Self::digestInput($request);
        return workflowsservice::getworkflowscount($request);
    }
}

