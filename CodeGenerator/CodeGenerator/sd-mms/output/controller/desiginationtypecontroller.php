<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\desiginationtypeservice;
use App\Services\customservice;


class desiginationtypecontroller  extends ParentController
{
    //
    public static function savedesiginationtype(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,desiginationtypeservice::getdesiginationtype($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return desiginationtypeservice::savedesiginationtype($id, $request);
    }

	// Method to get all records
	public static function listdesiginationtype(Request $request)
	{
		$request = Self::digestInput($request);
        return desiginationtypeservice::listdesiginationtype($request);
	}

    public static function getdesiginationtype(Request $request)
    {
        $request = Self::digestInput($request);
        return desiginationtypeservice::getdesiginationtype($request);
    }

	// Method to get all records
	public static function listdesiginationtypecount(Request $request)
	{
		$request = Self::digestInput($request);
        return desiginationtypeservice::listdesiginationtypecount($request);
	}

    public static function getdesiginationtypecount(Request $request)
    {
        $request = Self::digestInput($request);
        return desiginationtypeservice::getdesiginationtypecount($request);
    }
}

