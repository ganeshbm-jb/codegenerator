<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\invoiceservice;
use App\Services\customservice;


class invoicecontroller  extends ParentController
{
    //
    public static function saveinvoice(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,invoiceservice::getinvoice($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return invoiceservice::saveinvoice($id, $request);
    }

	// Method to get all records
	public static function listinvoice(Request $request)
	{
		$request = Self::digestInput($request);
        return invoiceservice::listinvoice($request);
	}

    public static function getinvoice(Request $request)
    {
        $request = Self::digestInput($request);
        return invoiceservice::getinvoice($request);
    }

	// Method to get all records
	public static function listinvoicecount(Request $request)
	{
		$request = Self::digestInput($request);
        return invoiceservice::listinvoicecount($request);
	}

    public static function getinvoicecount(Request $request)
    {
        $request = Self::digestInput($request);
        return invoiceservice::getinvoicecount($request);
    }
}

