<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\materialcatergoryservice;
use App\Services\customservice;


class materialcatergorycontroller  extends ParentController
{
    //
    public static function savematerialcatergory(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,materialcatergoryservice::getmaterialcatergory($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return materialcatergoryservice::savematerialcatergory($id, $request);
    }

	// Method to get all records
	public static function listmaterialcatergory(Request $request)
	{
		$request = Self::digestInput($request);
        return materialcatergoryservice::listmaterialcatergory($request);
	}

    public static function getmaterialcatergory(Request $request)
    {
        $request = Self::digestInput($request);
        return materialcatergoryservice::getmaterialcatergory($request);
    }

	// Method to get all records
	public static function listmaterialcatergorycount(Request $request)
	{
		$request = Self::digestInput($request);
        return materialcatergoryservice::listmaterialcatergorycount($request);
	}

    public static function getmaterialcatergorycount(Request $request)
    {
        $request = Self::digestInput($request);
        return materialcatergoryservice::getmaterialcatergorycount($request);
    }
}

