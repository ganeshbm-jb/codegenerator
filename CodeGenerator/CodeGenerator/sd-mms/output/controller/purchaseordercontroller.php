<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\purchaseorderservice;
use App\Services\customservice;


class purchaseordercontroller  extends ParentController
{
    //
    public static function savepurchaseorder(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,purchaseorderservice::getpurchaseorder($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return purchaseorderservice::savepurchaseorder($id, $request);
    }

	// Method to get all records
	public static function listpurchaseorder(Request $request)
	{
		$request = Self::digestInput($request);
        return purchaseorderservice::listpurchaseorder($request);
	}

    public static function getpurchaseorder(Request $request)
    {
        $request = Self::digestInput($request);
        return purchaseorderservice::getpurchaseorder($request);
    }

	// Method to get all records
	public static function listpurchaseordercount(Request $request)
	{
		$request = Self::digestInput($request);
        return purchaseorderservice::listpurchaseordercount($request);
	}

    public static function getpurchaseordercount(Request $request)
    {
        $request = Self::digestInput($request);
        return purchaseorderservice::getpurchaseordercount($request);
    }
}

