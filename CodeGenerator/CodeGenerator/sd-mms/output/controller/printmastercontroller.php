<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\printmasterservice;
use App\Services\customservice;


class printmastercontroller  extends ParentController
{
    //
    public static function saveprintmaster(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,printmasterservice::getprintmaster($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return printmasterservice::saveprintmaster($id, $request);
    }

	// Method to get all records
	public static function listprintmaster(Request $request)
	{
		$request = Self::digestInput($request);
        return printmasterservice::listprintmaster($request);
	}

    public static function getprintmaster(Request $request)
    {
        $request = Self::digestInput($request);
        return printmasterservice::getprintmaster($request);
    }

	// Method to get all records
	public static function listprintmastercount(Request $request)
	{
		$request = Self::digestInput($request);
        return printmasterservice::listprintmastercount($request);
	}

    public static function getprintmastercount(Request $request)
    {
        $request = Self::digestInput($request);
        return printmasterservice::getprintmastercount($request);
    }
}

