<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\stocklocationqtyservice;
use App\Services\customservice;


class stocklocationqtycontroller  extends ParentController
{
    //
    public static function savestocklocationqty(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,stocklocationqtyservice::getstocklocationqty($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return stocklocationqtyservice::savestocklocationqty($id, $request);
    }

	// Method to get all records
	public static function liststocklocationqty(Request $request)
	{
		$request = Self::digestInput($request);
        return stocklocationqtyservice::liststocklocationqty($request);
	}

    public static function getstocklocationqty(Request $request)
    {
        $request = Self::digestInput($request);
        return stocklocationqtyservice::getstocklocationqty($request);
    }

	// Method to get all records
	public static function liststocklocationqtycount(Request $request)
	{
		$request = Self::digestInput($request);
        return stocklocationqtyservice::liststocklocationqtycount($request);
	}

    public static function getstocklocationqtycount(Request $request)
    {
        $request = Self::digestInput($request);
        return stocklocationqtyservice::getstocklocationqtycount($request);
    }
}

