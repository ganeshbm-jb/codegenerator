<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\requesttypeservice;
use App\Services\customservice;


class requesttypecontroller  extends ParentController
{
    //
    public static function saverequesttype(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,requesttypeservice::getrequesttype($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return requesttypeservice::saverequesttype($id, $request);
    }

	// Method to get all records
	public static function listrequesttype(Request $request)
	{
		$request = Self::digestInput($request);
        return requesttypeservice::listrequesttype($request);
	}

    public static function getrequesttype(Request $request)
    {
        $request = Self::digestInput($request);
        return requesttypeservice::getrequesttype($request);
    }

	// Method to get all records
	public static function listrequesttypecount(Request $request)
	{
		$request = Self::digestInput($request);
        return requesttypeservice::listrequesttypecount($request);
	}

    public static function getrequesttypecount(Request $request)
    {
        $request = Self::digestInput($request);
        return requesttypeservice::getrequesttypecount($request);
    }
}

