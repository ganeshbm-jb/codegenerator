<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\purchaseordermaterialservice;
use App\Services\customservice;


class purchaseordermaterialcontroller  extends ParentController
{
    //
    public static function savepurchaseordermaterial(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,purchaseordermaterialservice::getpurchaseordermaterial($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return purchaseordermaterialservice::savepurchaseordermaterial($id, $request);
    }

	// Method to get all records
	public static function listpurchaseordermaterial(Request $request)
	{
		$request = Self::digestInput($request);
        return purchaseordermaterialservice::listpurchaseordermaterial($request);
	}

    public static function getpurchaseordermaterial(Request $request)
    {
        $request = Self::digestInput($request);
        return purchaseordermaterialservice::getpurchaseordermaterial($request);
    }

	// Method to get all records
	public static function listpurchaseordermaterialcount(Request $request)
	{
		$request = Self::digestInput($request);
        return purchaseordermaterialservice::listpurchaseordermaterialcount($request);
	}

    public static function getpurchaseordermaterialcount(Request $request)
    {
        $request = Self::digestInput($request);
        return purchaseordermaterialservice::getpurchaseordermaterialcount($request);
    }
}

