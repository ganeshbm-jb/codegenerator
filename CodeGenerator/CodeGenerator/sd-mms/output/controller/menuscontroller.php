<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\menusservice;
use App\Services\customservice;


class menuscontroller  extends ParentController
{
    //
    public static function savemenus(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,menusservice::getmenus($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return menusservice::savemenus($id, $request);
    }

	// Method to get all records
	public static function listmenus(Request $request)
	{
		$request = Self::digestInput($request);
        return menusservice::listmenus($request);
	}

    public static function getmenus(Request $request)
    {
        $request = Self::digestInput($request);
        return menusservice::getmenus($request);
    }

	// Method to get all records
	public static function listmenuscount(Request $request)
	{
		$request = Self::digestInput($request);
        return menusservice::listmenuscount($request);
	}

    public static function getmenuscount(Request $request)
    {
        $request = Self::digestInput($request);
        return menusservice::getmenuscount($request);
    }
}

