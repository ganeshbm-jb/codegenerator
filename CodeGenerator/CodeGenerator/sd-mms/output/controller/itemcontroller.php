<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\itemservice;
use App\Services\customservice;


class itemcontroller  extends ParentController
{
    //
    public static function saveitem(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,itemservice::getitem($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return itemservice::saveitem($id, $request);
    }

	// Method to get all records
	public static function listitem(Request $request)
	{
		$request = Self::digestInput($request);
        return itemservice::listitem($request);
	}

    public static function getitem(Request $request)
    {
        $request = Self::digestInput($request);
        return itemservice::getitem($request);
    }

	// Method to get all records
	public static function listitemcount(Request $request)
	{
		$request = Self::digestInput($request);
        return itemservice::listitemcount($request);
	}

    public static function getitemcount(Request $request)
    {
        $request = Self::digestInput($request);
        return itemservice::getitemcount($request);
    }
}

