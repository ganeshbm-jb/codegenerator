<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\suppliersservice;
use App\Services\customservice;


class supplierscontroller  extends ParentController
{
    //
    public static function savesuppliers(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,suppliersservice::getsuppliers($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return suppliersservice::savesuppliers($id, $request);
    }

	// Method to get all records
	public static function listsuppliers(Request $request)
	{
		$request = Self::digestInput($request);
        return suppliersservice::listsuppliers($request);
	}

    public static function getsuppliers(Request $request)
    {
        $request = Self::digestInput($request);
        return suppliersservice::getsuppliers($request);
    }

	// Method to get all records
	public static function listsupplierscount(Request $request)
	{
		$request = Self::digestInput($request);
        return suppliersservice::listsupplierscount($request);
	}

    public static function getsupplierscount(Request $request)
    {
        $request = Self::digestInput($request);
        return suppliersservice::getsupplierscount($request);
    }
}

