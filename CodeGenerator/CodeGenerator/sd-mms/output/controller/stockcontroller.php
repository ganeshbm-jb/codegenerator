<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\stockservice;
use App\Services\customservice;


class stockcontroller  extends ParentController
{
    //
    public static function savestock(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,stockservice::getstock($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return stockservice::savestock($id, $request);
    }

	// Method to get all records
	public static function liststock(Request $request)
	{
		$request = Self::digestInput($request);
        return stockservice::liststock($request);
	}

    public static function getstock(Request $request)
    {
        $request = Self::digestInput($request);
        return stockservice::getstock($request);
    }

	// Method to get all records
	public static function liststockcount(Request $request)
	{
		$request = Self::digestInput($request);
        return stockservice::liststockcount($request);
	}

    public static function getstockcount(Request $request)
    {
        $request = Self::digestInput($request);
        return stockservice::getstockcount($request);
    }
}

