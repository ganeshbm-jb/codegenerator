<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\workorderitemsservice;
use App\Services\customservice;


class workorderitemscontroller  extends ParentController
{
    //
    public static function saveworkorderitems(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,workorderitemsservice::getworkorderitems($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return workorderitemsservice::saveworkorderitems($id, $request);
    }

	// Method to get all records
	public static function listworkorderitems(Request $request)
	{
		$request = Self::digestInput($request);
        return workorderitemsservice::listworkorderitems($request);
	}

    public static function getworkorderitems(Request $request)
    {
        $request = Self::digestInput($request);
        return workorderitemsservice::getworkorderitems($request);
    }

	// Method to get all records
	public static function listworkorderitemscount(Request $request)
	{
		$request = Self::digestInput($request);
        return workorderitemsservice::listworkorderitemscount($request);
	}

    public static function getworkorderitemscount(Request $request)
    {
        $request = Self::digestInput($request);
        return workorderitemsservice::getworkorderitemscount($request);
    }
}

