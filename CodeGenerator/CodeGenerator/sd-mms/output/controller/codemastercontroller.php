<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\codemasterservice;
use App\Services\customservice;


class codemastercontroller  extends ParentController
{
    //
    public static function savecodemaster(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,codemasterservice::getcodemaster($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return codemasterservice::savecodemaster($id, $request);
    }

	// Method to get all records
	public static function listcodemaster(Request $request)
	{
		$request = Self::digestInput($request);
        return codemasterservice::listcodemaster($request);
	}

    public static function getcodemaster(Request $request)
    {
        $request = Self::digestInput($request);
        return codemasterservice::getcodemaster($request);
    }

	// Method to get all records
	public static function listcodemastercount(Request $request)
	{
		$request = Self::digestInput($request);
        return codemasterservice::listcodemastercount($request);
	}

    public static function getcodemastercount(Request $request)
    {
        $request = Self::digestInput($request);
        return codemasterservice::getcodemastercount($request);
    }
}

