<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\usermenusservice;
use App\Services\customservice;


class usermenuscontroller  extends ParentController
{
    //
    public static function saveusermenus(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,usermenusservice::getusermenus($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return usermenusservice::saveusermenus($id, $request);
    }

	// Method to get all records
	public static function listusermenus(Request $request)
	{
		$request = Self::digestInput($request);
        return usermenusservice::listusermenus($request);
	}

    public static function getusermenus(Request $request)
    {
        $request = Self::digestInput($request);
        return usermenusservice::getusermenus($request);
    }

	// Method to get all records
	public static function listusermenuscount(Request $request)
	{
		$request = Self::digestInput($request);
        return usermenusservice::listusermenuscount($request);
	}

    public static function getusermenuscount(Request $request)
    {
        $request = Self::digestInput($request);
        return usermenusservice::getusermenuscount($request);
    }
}

