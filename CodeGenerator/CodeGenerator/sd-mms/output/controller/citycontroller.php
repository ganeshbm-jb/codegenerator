<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\cityservice;
use App\Services\customservice;


class citycontroller  extends ParentController
{
    //
    public static function savecity(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,cityservice::getcity($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return cityservice::savecity($id, $request);
    }

	// Method to get all records
	public static function listcity(Request $request)
	{
		$request = Self::digestInput($request);
        return cityservice::listcity($request);
	}

    public static function getcity(Request $request)
    {
        $request = Self::digestInput($request);
        return cityservice::getcity($request);
    }

	// Method to get all records
	public static function listcitycount(Request $request)
	{
		$request = Self::digestInput($request);
        return cityservice::listcitycount($request);
	}

    public static function getcitycount(Request $request)
    {
        $request = Self::digestInput($request);
        return cityservice::getcitycount($request);
    }
}

