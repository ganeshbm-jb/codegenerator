<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\usersservice;
use App\Services\customservice;


class userscontroller  extends ParentController
{
    //
    public static function saveusers(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,usersservice::getusers($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return usersservice::saveusers($id, $request);
    }

	// Method to get all records
	public static function listusers(Request $request)
	{
		$request = Self::digestInput($request);
        return usersservice::listusers($request);
	}

    public static function getusers(Request $request)
    {
        $request = Self::digestInput($request);
        return usersservice::getusers($request);
    }

	// Method to get all records
	public static function listuserscount(Request $request)
	{
		$request = Self::digestInput($request);
        return usersservice::listuserscount($request);
	}

    public static function getuserscount(Request $request)
    {
        $request = Self::digestInput($request);
        return usersservice::getuserscount($request);
    }
}

