<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\viewaccessservice;
use App\Services\customservice;


class viewaccesscontroller  extends ParentController
{
    //
    public static function saveviewaccess(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,viewaccessservice::getviewaccess($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return viewaccessservice::saveviewaccess($id, $request);
    }

	// Method to get all records
	public static function listviewaccess(Request $request)
	{
		$request = Self::digestInput($request);
        return viewaccessservice::listviewaccess($request);
	}

    public static function getviewaccess(Request $request)
    {
        $request = Self::digestInput($request);
        return viewaccessservice::getviewaccess($request);
    }

	// Method to get all records
	public static function listviewaccesscount(Request $request)
	{
		$request = Self::digestInput($request);
        return viewaccessservice::listviewaccesscount($request);
	}

    public static function getviewaccesscount(Request $request)
    {
        $request = Self::digestInput($request);
        return viewaccessservice::getviewaccesscount($request);
    }
}

