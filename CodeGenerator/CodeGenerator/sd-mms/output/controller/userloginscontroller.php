<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\userloginsservice;
use App\Services\customservice;


class userloginscontroller  extends ParentController
{
    //
    public static function saveuserlogins(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,userloginsservice::getuserlogins($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return userloginsservice::saveuserlogins($id, $request);
    }

	// Method to get all records
	public static function listuserlogins(Request $request)
	{
		$request = Self::digestInput($request);
        return userloginsservice::listuserlogins($request);
	}

    public static function getuserlogins(Request $request)
    {
        $request = Self::digestInput($request);
        return userloginsservice::getuserlogins($request);
    }

	// Method to get all records
	public static function listuserloginscount(Request $request)
	{
		$request = Self::digestInput($request);
        return userloginsservice::listuserloginscount($request);
	}

    public static function getuserloginscount(Request $request)
    {
        $request = Self::digestInput($request);
        return userloginsservice::getuserloginscount($request);
    }
}

