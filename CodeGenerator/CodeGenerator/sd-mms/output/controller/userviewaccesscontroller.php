<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\userviewaccessservice;
use App\Services\customservice;


class userviewaccesscontroller  extends ParentController
{
    //
    public static function saveuserviewaccess(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,userviewaccessservice::getuserviewaccess($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return userviewaccessservice::saveuserviewaccess($id, $request);
    }

	// Method to get all records
	public static function listuserviewaccess(Request $request)
	{
		$request = Self::digestInput($request);
        return userviewaccessservice::listuserviewaccess($request);
	}

    public static function getuserviewaccess(Request $request)
    {
        $request = Self::digestInput($request);
        return userviewaccessservice::getuserviewaccess($request);
    }

	// Method to get all records
	public static function listuserviewaccesscount(Request $request)
	{
		$request = Self::digestInput($request);
        return userviewaccessservice::listuserviewaccesscount($request);
	}

    public static function getuserviewaccesscount(Request $request)
    {
        $request = Self::digestInput($request);
        return userviewaccessservice::getuserviewaccesscount($request);
    }
}

