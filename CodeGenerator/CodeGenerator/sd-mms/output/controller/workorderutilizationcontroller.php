<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\workorderutilizationservice;
use App\Services\customservice;


class workorderutilizationcontroller  extends ParentController
{
    //
    public static function saveworkorderutilization(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,workorderutilizationservice::getworkorderutilization($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return workorderutilizationservice::saveworkorderutilization($id, $request);
    }

	// Method to get all records
	public static function listworkorderutilization(Request $request)
	{
		$request = Self::digestInput($request);
        return workorderutilizationservice::listworkorderutilization($request);
	}

    public static function getworkorderutilization(Request $request)
    {
        $request = Self::digestInput($request);
        return workorderutilizationservice::getworkorderutilization($request);
    }

	// Method to get all records
	public static function listworkorderutilizationcount(Request $request)
	{
		$request = Self::digestInput($request);
        return workorderutilizationservice::listworkorderutilizationcount($request);
	}

    public static function getworkorderutilizationcount(Request $request)
    {
        $request = Self::digestInput($request);
        return workorderutilizationservice::getworkorderutilizationcount($request);
    }
}

