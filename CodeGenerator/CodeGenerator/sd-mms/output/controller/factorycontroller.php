<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\factoryservice;
use App\Services\customservice;


class factorycontroller  extends ParentController
{
    //
    public static function savefactory(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,factoryservice::getfactory($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return factoryservice::savefactory($id, $request);
    }

	// Method to get all records
	public static function listfactory(Request $request)
	{
		$request = Self::digestInput($request);
        return factoryservice::listfactory($request);
	}

    public static function getfactory(Request $request)
    {
        $request = Self::digestInput($request);
        return factoryservice::getfactory($request);
    }

	// Method to get all records
	public static function listfactorycount(Request $request)
	{
		$request = Self::digestInput($request);
        return factoryservice::listfactorycount($request);
	}

    public static function getfactorycount(Request $request)
    {
        $request = Self::digestInput($request);
        return factoryservice::getfactorycount($request);
    }
}

