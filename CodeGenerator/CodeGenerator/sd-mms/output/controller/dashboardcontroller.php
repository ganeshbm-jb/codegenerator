<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\dashboardservice;
use App\Services\customservice;


class dashboardcontroller  extends ParentController
{
    //
    public static function savedashboard(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,dashboardservice::getdashboard($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return dashboardservice::savedashboard($id, $request);
    }

	// Method to get all records
	public static function listdashboard(Request $request)
	{
		$request = Self::digestInput($request);
        return dashboardservice::listdashboard($request);
	}

    public static function getdashboard(Request $request)
    {
        $request = Self::digestInput($request);
        return dashboardservice::getdashboard($request);
    }

	// Method to get all records
	public static function listdashboardcount(Request $request)
	{
		$request = Self::digestInput($request);
        return dashboardservice::listdashboardcount($request);
	}

    public static function getdashboardcount(Request $request)
    {
        $request = Self::digestInput($request);
        return dashboardservice::getdashboardcount($request);
    }
}

