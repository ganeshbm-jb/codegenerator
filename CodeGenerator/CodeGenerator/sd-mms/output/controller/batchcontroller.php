<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\batchservice;
use App\Services\customservice;


class batchcontroller  extends ParentController
{
    //
    public static function savebatch(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,batchservice::getbatch($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return batchservice::savebatch($id, $request);
    }

	// Method to get all records
	public static function listbatch(Request $request)
	{
		$request = Self::digestInput($request);
        return batchservice::listbatch($request);
	}

    public static function getbatch(Request $request)
    {
        $request = Self::digestInput($request);
        return batchservice::getbatch($request);
    }

	// Method to get all records
	public static function listbatchcount(Request $request)
	{
		$request = Self::digestInput($request);
        return batchservice::listbatchcount($request);
	}

    public static function getbatchcount(Request $request)
    {
        $request = Self::digestInput($request);
        return batchservice::getbatchcount($request);
    }
}

