<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\materialtypeservice;
use App\Services\customservice;


class materialtypecontroller  extends ParentController
{
    //
    public static function savematerialtype(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,materialtypeservice::getmaterialtype($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return materialtypeservice::savematerialtype($id, $request);
    }

	// Method to get all records
	public static function listmaterialtype(Request $request)
	{
		$request = Self::digestInput($request);
        return materialtypeservice::listmaterialtype($request);
	}

    public static function getmaterialtype(Request $request)
    {
        $request = Self::digestInput($request);
        return materialtypeservice::getmaterialtype($request);
    }

	// Method to get all records
	public static function listmaterialtypecount(Request $request)
	{
		$request = Self::digestInput($request);
        return materialtypeservice::listmaterialtypecount($request);
	}

    public static function getmaterialtypecount(Request $request)
    {
        $request = Self::digestInput($request);
        return materialtypeservice::getmaterialtypecount($request);
    }
}

