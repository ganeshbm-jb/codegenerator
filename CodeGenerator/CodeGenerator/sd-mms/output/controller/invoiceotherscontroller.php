<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\invoiceothersservice;
use App\Services\customservice;


class invoiceotherscontroller  extends ParentController
{
    //
    public static function saveinvoiceothers(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,invoiceothersservice::getinvoiceothers($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return invoiceothersservice::saveinvoiceothers($id, $request);
    }

	// Method to get all records
	public static function listinvoiceothers(Request $request)
	{
		$request = Self::digestInput($request);
        return invoiceothersservice::listinvoiceothers($request);
	}

    public static function getinvoiceothers(Request $request)
    {
        $request = Self::digestInput($request);
        return invoiceothersservice::getinvoiceothers($request);
    }

	// Method to get all records
	public static function listinvoiceotherscount(Request $request)
	{
		$request = Self::digestInput($request);
        return invoiceothersservice::listinvoiceotherscount($request);
	}

    public static function getinvoiceotherscount(Request $request)
    {
        $request = Self::digestInput($request);
        return invoiceothersservice::getinvoiceotherscount($request);
    }
}

