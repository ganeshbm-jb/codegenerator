<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\workorderutlizationmaterialservice;
use App\Services\customservice;


class workorderutlizationmaterialcontroller  extends ParentController
{
    //
    public static function saveworkorderutlizationmaterial(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,workorderutlizationmaterialservice::getworkorderutlizationmaterial($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return workorderutlizationmaterialservice::saveworkorderutlizationmaterial($id, $request);
    }

	// Method to get all records
	public static function listworkorderutlizationmaterial(Request $request)
	{
		$request = Self::digestInput($request);
        return workorderutlizationmaterialservice::listworkorderutlizationmaterial($request);
	}

    public static function getworkorderutlizationmaterial(Request $request)
    {
        $request = Self::digestInput($request);
        return workorderutlizationmaterialservice::getworkorderutlizationmaterial($request);
    }

	// Method to get all records
	public static function listworkorderutlizationmaterialcount(Request $request)
	{
		$request = Self::digestInput($request);
        return workorderutlizationmaterialservice::listworkorderutlizationmaterialcount($request);
	}

    public static function getworkorderutlizationmaterialcount(Request $request)
    {
        $request = Self::digestInput($request);
        return workorderutlizationmaterialservice::getworkorderutlizationmaterialcount($request);
    }
}

