<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\materialcategoryservice;
use App\Services\customservice;


class materialcategorycontroller  extends ParentController
{
    //
    public static function savematerialcategory(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,materialcategoryservice::getmaterialcategory($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return materialcategoryservice::savematerialcategory($id, $request);
    }

	// Method to get all records
	public static function listmaterialcategory(Request $request)
	{
		$request = Self::digestInput($request);
        return materialcategoryservice::listmaterialcategory($request);
	}

    public static function getmaterialcategory(Request $request)
    {
        $request = Self::digestInput($request);
        return materialcategoryservice::getmaterialcategory($request);
    }

	// Method to get all records
	public static function listmaterialcategorycount(Request $request)
	{
		$request = Self::digestInput($request);
        return materialcategoryservice::listmaterialcategorycount($request);
	}

    public static function getmaterialcategorycount(Request $request)
    {
        $request = Self::digestInput($request);
        return materialcategoryservice::getmaterialcategorycount($request);
    }
}

