<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\workordercostservice;
use App\Services\customservice;


class workordercostcontroller  extends ParentController
{
    //
    public static function saveworkordercost(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,workordercostservice::getworkordercost($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return workordercostservice::saveworkordercost($id, $request);
    }

	// Method to get all records
	public static function listworkordercost(Request $request)
	{
		$request = Self::digestInput($request);
        return workordercostservice::listworkordercost($request);
	}

    public static function getworkordercost(Request $request)
    {
        $request = Self::digestInput($request);
        return workordercostservice::getworkordercost($request);
    }

	// Method to get all records
	public static function listworkordercostcount(Request $request)
	{
		$request = Self::digestInput($request);
        return workordercostservice::listworkordercostcount($request);
	}

    public static function getworkordercostcount(Request $request)
    {
        $request = Self::digestInput($request);
        return workordercostservice::getworkordercostcount($request);
    }
}

