<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\advancetypeservice;
use App\Services\customservice;


class advancetypecontroller  extends ParentController
{
    //
    public static function saveadvancetype(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,advancetypeservice::getadvancetype($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return advancetypeservice::saveadvancetype($id, $request);
    }

	// Method to get all records
	public static function listadvancetype(Request $request)
	{
		$request = Self::digestInput($request);
        return advancetypeservice::listadvancetype($request);
	}

    public static function getadvancetype(Request $request)
    {
        $request = Self::digestInput($request);
        return advancetypeservice::getadvancetype($request);
    }

	// Method to get all records
	public static function listadvancetypecount(Request $request)
	{
		$request = Self::digestInput($request);
        return advancetypeservice::listadvancetypecount($request);
	}

    public static function getadvancetypecount(Request $request)
    {
        $request = Self::digestInput($request);
        return advancetypeservice::getadvancetypecount($request);
    }
}

