<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\purchasesservice;
use App\Services\customservice;


class purchasescontroller  extends ParentController
{
    //
    public static function savepurchases(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,purchasesservice::getpurchases($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return purchasesservice::savepurchases($id, $request);
    }

	// Method to get all records
	public static function listpurchases(Request $request)
	{
		$request = Self::digestInput($request);
        return purchasesservice::listpurchases($request);
	}

    public static function getpurchases(Request $request)
    {
        $request = Self::digestInput($request);
        return purchasesservice::getpurchases($request);
    }

	// Method to get all records
	public static function listpurchasescount(Request $request)
	{
		$request = Self::digestInput($request);
        return purchasesservice::listpurchasescount($request);
	}

    public static function getpurchasescount(Request $request)
    {
        $request = Self::digestInput($request);
        return purchasesservice::getpurchasescount($request);
    }
}

