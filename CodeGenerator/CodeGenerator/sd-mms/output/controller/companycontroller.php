<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\companyservice;
use App\Services\customservice;


class companycontroller  extends ParentController
{
    //
    public static function savecompany(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,companyservice::getcompany($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return companyservice::savecompany($id, $request);
    }

	// Method to get all records
	public static function listcompany(Request $request)
	{
		$request = Self::digestInput($request);
        return companyservice::listcompany($request);
	}

    public static function getcompany(Request $request)
    {
        $request = Self::digestInput($request);
        return companyservice::getcompany($request);
    }

	// Method to get all records
	public static function listcompanycount(Request $request)
	{
		$request = Self::digestInput($request);
        return companyservice::listcompanycount($request);
	}

    public static function getcompanycount(Request $request)
    {
        $request = Self::digestInput($request);
        return companyservice::getcompanycount($request);
    }
}

