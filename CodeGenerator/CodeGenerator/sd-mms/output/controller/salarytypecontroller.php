<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\salarytypeservice;
use App\Services\customservice;


class salarytypecontroller  extends ParentController
{
    //
    public static function savesalarytype(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,salarytypeservice::getsalarytype($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return salarytypeservice::savesalarytype($id, $request);
    }

	// Method to get all records
	public static function listsalarytype(Request $request)
	{
		$request = Self::digestInput($request);
        return salarytypeservice::listsalarytype($request);
	}

    public static function getsalarytype(Request $request)
    {
        $request = Self::digestInput($request);
        return salarytypeservice::getsalarytype($request);
    }

	// Method to get all records
	public static function listsalarytypecount(Request $request)
	{
		$request = Self::digestInput($request);
        return salarytypeservice::listsalarytypecount($request);
	}

    public static function getsalarytypecount(Request $request)
    {
        $request = Self::digestInput($request);
        return salarytypeservice::getsalarytypecount($request);
    }
}

