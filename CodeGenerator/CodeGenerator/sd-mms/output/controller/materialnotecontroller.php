<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\materialnoteservice;
use App\Services\customservice;


class materialnotecontroller  extends ParentController
{
    //
    public static function savematerialnote(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,materialnoteservice::getmaterialnote($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return materialnoteservice::savematerialnote($id, $request);
    }

	// Method to get all records
	public static function listmaterialnote(Request $request)
	{
		$request = Self::digestInput($request);
        return materialnoteservice::listmaterialnote($request);
	}

    public static function getmaterialnote(Request $request)
    {
        $request = Self::digestInput($request);
        return materialnoteservice::getmaterialnote($request);
    }

	// Method to get all records
	public static function listmaterialnotecount(Request $request)
	{
		$request = Self::digestInput($request);
        return materialnoteservice::listmaterialnotecount($request);
	}

    public static function getmaterialnotecount(Request $request)
    {
        $request = Self::digestInput($request);
        return materialnoteservice::getmaterialnotecount($request);
    }
}

