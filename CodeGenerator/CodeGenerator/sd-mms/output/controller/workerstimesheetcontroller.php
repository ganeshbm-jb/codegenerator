<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\workerstimesheetservice;
use App\Services\customservice;


class workerstimesheetcontroller  extends ParentController
{
    //
    public static function saveworkerstimesheet(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,workerstimesheetservice::getworkerstimesheet($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return workerstimesheetservice::saveworkerstimesheet($id, $request);
    }

	// Method to get all records
	public static function listworkerstimesheet(Request $request)
	{
		$request = Self::digestInput($request);
        return workerstimesheetservice::listworkerstimesheet($request);
	}

    public static function getworkerstimesheet(Request $request)
    {
        $request = Self::digestInput($request);
        return workerstimesheetservice::getworkerstimesheet($request);
    }

	// Method to get all records
	public static function listworkerstimesheetcount(Request $request)
	{
		$request = Self::digestInput($request);
        return workerstimesheetservice::listworkerstimesheetcount($request);
	}

    public static function getworkerstimesheetcount(Request $request)
    {
        $request = Self::digestInput($request);
        return workerstimesheetservice::getworkerstimesheetcount($request);
    }
}

