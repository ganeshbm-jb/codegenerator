<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\expenseservice;
use App\Services\customservice;


class expensecontroller  extends ParentController
{
    //
    public static function saveexpense(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,expenseservice::getexpense($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return expenseservice::saveexpense($id, $request);
    }

	// Method to get all records
	public static function listexpense(Request $request)
	{
		$request = Self::digestInput($request);
        return expenseservice::listexpense($request);
	}

    public static function getexpense(Request $request)
    {
        $request = Self::digestInput($request);
        return expenseservice::getexpense($request);
    }

	// Method to get all records
	public static function listexpensecount(Request $request)
	{
		$request = Self::digestInput($request);
        return expenseservice::listexpensecount($request);
	}

    public static function getexpensecount(Request $request)
    {
        $request = Self::digestInput($request);
        return expenseservice::getexpensecount($request);
    }
}

