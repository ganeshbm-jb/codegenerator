<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\userrolesservice;
use App\Services\customservice;


class userrolescontroller  extends ParentController
{
    //
    public static function saveuserroles(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,userrolesservice::getuserroles($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return userrolesservice::saveuserroles($id, $request);
    }

	// Method to get all records
	public static function listuserroles(Request $request)
	{
		$request = Self::digestInput($request);
        return userrolesservice::listuserroles($request);
	}

    public static function getuserroles(Request $request)
    {
        $request = Self::digestInput($request);
        return userrolesservice::getuserroles($request);
    }

	// Method to get all records
	public static function listuserrolescount(Request $request)
	{
		$request = Self::digestInput($request);
        return userrolesservice::listuserrolescount($request);
	}

    public static function getuserrolescount(Request $request)
    {
        $request = Self::digestInput($request);
        return userrolesservice::getuserrolescount($request);
    }
}

