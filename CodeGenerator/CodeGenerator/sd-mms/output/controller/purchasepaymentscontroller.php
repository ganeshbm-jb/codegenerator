<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\purchasepaymentsservice;
use App\Services\customservice;


class purchasepaymentscontroller  extends ParentController
{
    //
    public static function savepurchasepayments(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,purchasepaymentsservice::getpurchasepayments($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return purchasepaymentsservice::savepurchasepayments($id, $request);
    }

	// Method to get all records
	public static function listpurchasepayments(Request $request)
	{
		$request = Self::digestInput($request);
        return purchasepaymentsservice::listpurchasepayments($request);
	}

    public static function getpurchasepayments(Request $request)
    {
        $request = Self::digestInput($request);
        return purchasepaymentsservice::getpurchasepayments($request);
    }

	// Method to get all records
	public static function listpurchasepaymentscount(Request $request)
	{
		$request = Self::digestInput($request);
        return purchasepaymentsservice::listpurchasepaymentscount($request);
	}

    public static function getpurchasepaymentscount(Request $request)
    {
        $request = Self::digestInput($request);
        return purchasepaymentsservice::getpurchasepaymentscount($request);
    }
}

