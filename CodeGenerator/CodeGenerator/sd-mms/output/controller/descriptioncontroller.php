<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\descriptionservice;
use App\Services\customservice;


class descriptioncontroller  extends ParentController
{
    //
    public static function savedescription(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,descriptionservice::getdescription($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return descriptionservice::savedescription($id, $request);
    }

	// Method to get all records
	public static function listdescription(Request $request)
	{
		$request = Self::digestInput($request);
        return descriptionservice::listdescription($request);
	}

    public static function getdescription(Request $request)
    {
        $request = Self::digestInput($request);
        return descriptionservice::getdescription($request);
    }

	// Method to get all records
	public static function listdescriptioncount(Request $request)
	{
		$request = Self::digestInput($request);
        return descriptionservice::listdescriptioncount($request);
	}

    public static function getdescriptioncount(Request $request)
    {
        $request = Self::digestInput($request);
        return descriptionservice::getdescriptioncount($request);
    }
}

