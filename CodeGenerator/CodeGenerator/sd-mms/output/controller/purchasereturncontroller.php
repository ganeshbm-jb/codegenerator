<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\purchasereturnservice;
use App\Services\customservice;


class purchasereturncontroller  extends ParentController
{
    //
    public static function savepurchasereturn(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,purchasereturnservice::getpurchasereturn($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return purchasereturnservice::savepurchasereturn($id, $request);
    }

	// Method to get all records
	public static function listpurchasereturn(Request $request)
	{
		$request = Self::digestInput($request);
        return purchasereturnservice::listpurchasereturn($request);
	}

    public static function getpurchasereturn(Request $request)
    {
        $request = Self::digestInput($request);
        return purchasereturnservice::getpurchasereturn($request);
    }

	// Method to get all records
	public static function listpurchasereturncount(Request $request)
	{
		$request = Self::digestInput($request);
        return purchasereturnservice::listpurchasereturncount($request);
	}

    public static function getpurchasereturncount(Request $request)
    {
        $request = Self::digestInput($request);
        return purchasereturnservice::getpurchasereturncount($request);
    }
}

