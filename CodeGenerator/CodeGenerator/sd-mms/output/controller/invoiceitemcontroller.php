<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\invoiceitemservice;
use App\Services\customservice;


class invoiceitemcontroller  extends ParentController
{
    //
    public static function saveinvoiceitem(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,invoiceitemservice::getinvoiceitem($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return invoiceitemservice::saveinvoiceitem($id, $request);
    }

	// Method to get all records
	public static function listinvoiceitem(Request $request)
	{
		$request = Self::digestInput($request);
        return invoiceitemservice::listinvoiceitem($request);
	}

    public static function getinvoiceitem(Request $request)
    {
        $request = Self::digestInput($request);
        return invoiceitemservice::getinvoiceitem($request);
    }

	// Method to get all records
	public static function listinvoiceitemcount(Request $request)
	{
		$request = Self::digestInput($request);
        return invoiceitemservice::listinvoiceitemcount($request);
	}

    public static function getinvoiceitemcount(Request $request)
    {
        $request = Self::digestInput($request);
        return invoiceitemservice::getinvoiceitemcount($request);
    }
}

