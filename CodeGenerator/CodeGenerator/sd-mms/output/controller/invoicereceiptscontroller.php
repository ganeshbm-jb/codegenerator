<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\invoicereceiptsservice;
use App\Services\customservice;


class invoicereceiptscontroller  extends ParentController
{
    //
    public static function saveinvoicereceipts(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,invoicereceiptsservice::getinvoicereceipts($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return invoicereceiptsservice::saveinvoicereceipts($id, $request);
    }

	// Method to get all records
	public static function listinvoicereceipts(Request $request)
	{
		$request = Self::digestInput($request);
        return invoicereceiptsservice::listinvoicereceipts($request);
	}

    public static function getinvoicereceipts(Request $request)
    {
        $request = Self::digestInput($request);
        return invoicereceiptsservice::getinvoicereceipts($request);
    }

	// Method to get all records
	public static function listinvoicereceiptscount(Request $request)
	{
		$request = Self::digestInput($request);
        return invoicereceiptsservice::listinvoicereceiptscount($request);
	}

    public static function getinvoicereceiptscount(Request $request)
    {
        $request = Self::digestInput($request);
        return invoicereceiptsservice::getinvoicereceiptscount($request);
    }
}

