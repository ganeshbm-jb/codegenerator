<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\expensetypeservice;
use App\Services\customservice;


class expensetypecontroller  extends ParentController
{
    //
    public static function saveexpensetype(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,expensetypeservice::getexpensetype($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return expensetypeservice::saveexpensetype($id, $request);
    }

	// Method to get all records
	public static function listexpensetype(Request $request)
	{
		$request = Self::digestInput($request);
        return expensetypeservice::listexpensetype($request);
	}

    public static function getexpensetype(Request $request)
    {
        $request = Self::digestInput($request);
        return expensetypeservice::getexpensetype($request);
    }

	// Method to get all records
	public static function listexpensetypecount(Request $request)
	{
		$request = Self::digestInput($request);
        return expensetypeservice::listexpensetypecount($request);
	}

    public static function getexpensetypecount(Request $request)
    {
        $request = Self::digestInput($request);
        return expensetypeservice::getexpensetypecount($request);
    }
}

