<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\projectsservice;
use App\Services\customservice;


class projectscontroller  extends ParentController
{
    //
    public static function saveprojects(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,projectsservice::getprojects($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return projectsservice::saveprojects($id, $request);
    }

	// Method to get all records
	public static function listprojects(Request $request)
	{
		$request = Self::digestInput($request);
        return projectsservice::listprojects($request);
	}

    public static function getprojects(Request $request)
    {
        $request = Self::digestInput($request);
        return projectsservice::getprojects($request);
    }

	// Method to get all records
	public static function listprojectscount(Request $request)
	{
		$request = Self::digestInput($request);
        return projectsservice::listprojectscount($request);
	}

    public static function getprojectscount(Request $request)
    {
        $request = Self::digestInput($request);
        return projectsservice::getprojectscount($request);
    }
}

