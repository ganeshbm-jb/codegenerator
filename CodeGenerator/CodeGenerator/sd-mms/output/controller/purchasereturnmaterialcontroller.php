<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\purchasereturnmaterialservice;
use App\Services\customservice;


class purchasereturnmaterialcontroller  extends ParentController
{
    //
    public static function savepurchasereturnmaterial(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,purchasereturnmaterialservice::getpurchasereturnmaterial($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return purchasereturnmaterialservice::savepurchasereturnmaterial($id, $request);
    }

	// Method to get all records
	public static function listpurchasereturnmaterial(Request $request)
	{
		$request = Self::digestInput($request);
        return purchasereturnmaterialservice::listpurchasereturnmaterial($request);
	}

    public static function getpurchasereturnmaterial(Request $request)
    {
        $request = Self::digestInput($request);
        return purchasereturnmaterialservice::getpurchasereturnmaterial($request);
    }

	// Method to get all records
	public static function listpurchasereturnmaterialcount(Request $request)
	{
		$request = Self::digestInput($request);
        return purchasereturnmaterialservice::listpurchasereturnmaterialcount($request);
	}

    public static function getpurchasereturnmaterialcount(Request $request)
    {
        $request = Self::digestInput($request);
        return purchasereturnmaterialservice::getpurchasereturnmaterialcount($request);
    }
}

