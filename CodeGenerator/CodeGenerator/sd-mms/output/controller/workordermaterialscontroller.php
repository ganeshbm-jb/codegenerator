<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\workordermaterialsservice;
use App\Services\customservice;


class workordermaterialscontroller  extends ParentController
{
    //
    public static function saveworkordermaterials(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,workordermaterialsservice::getworkordermaterials($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return workordermaterialsservice::saveworkordermaterials($id, $request);
    }

	// Method to get all records
	public static function listworkordermaterials(Request $request)
	{
		$request = Self::digestInput($request);
        return workordermaterialsservice::listworkordermaterials($request);
	}

    public static function getworkordermaterials(Request $request)
    {
        $request = Self::digestInput($request);
        return workordermaterialsservice::getworkordermaterials($request);
    }

	// Method to get all records
	public static function listworkordermaterialscount(Request $request)
	{
		$request = Self::digestInput($request);
        return workordermaterialsservice::listworkordermaterialscount($request);
	}

    public static function getworkordermaterialscount(Request $request)
    {
        $request = Self::digestInput($request);
        return workordermaterialsservice::getworkordermaterialscount($request);
    }
}

