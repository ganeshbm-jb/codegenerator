<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\workordersservice;
use App\Services\customservice;


class workorderscontroller  extends ParentController
{
    //
    public static function saveworkorders(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,workordersservice::getworkorders($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return workordersservice::saveworkorders($id, $request);
    }

	// Method to get all records
	public static function listworkorders(Request $request)
	{
		$request = Self::digestInput($request);
        return workordersservice::listworkorders($request);
	}

    public static function getworkorders(Request $request)
    {
        $request = Self::digestInput($request);
        return workordersservice::getworkorders($request);
    }

	// Method to get all records
	public static function listworkorderscount(Request $request)
	{
		$request = Self::digestInput($request);
        return workordersservice::listworkorderscount($request);
	}

    public static function getworkorderscount(Request $request)
    {
        $request = Self::digestInput($request);
        return workordersservice::getworkorderscount($request);
    }
}

