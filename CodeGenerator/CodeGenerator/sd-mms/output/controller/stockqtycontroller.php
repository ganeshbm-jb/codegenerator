<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\stockqtyservice;
use App\Services\customservice;


class stockqtycontroller  extends ParentController
{
    //
    public static function savestockqty(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,stockqtyservice::getstockqty($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return stockqtyservice::savestockqty($id, $request);
    }

	// Method to get all records
	public static function liststockqty(Request $request)
	{
		$request = Self::digestInput($request);
        return stockqtyservice::liststockqty($request);
	}

    public static function getstockqty(Request $request)
    {
        $request = Self::digestInput($request);
        return stockqtyservice::getstockqty($request);
    }

	// Method to get all records
	public static function liststockqtycount(Request $request)
	{
		$request = Self::digestInput($request);
        return stockqtyservice::liststockqtycount($request);
	}

    public static function getstockqtycount(Request $request)
    {
        $request = Self::digestInput($request);
        return stockqtyservice::getstockqtycount($request);
    }
}

