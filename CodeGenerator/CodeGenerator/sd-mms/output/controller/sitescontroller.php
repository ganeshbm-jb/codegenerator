<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\sitesservice;
use App\Services\customservice;


class sitescontroller  extends ParentController
{
    //
    public static function savesites(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,sitesservice::getsites($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return sitesservice::savesites($id, $request);
    }

	// Method to get all records
	public static function listsites(Request $request)
	{
		$request = Self::digestInput($request);
        return sitesservice::listsites($request);
	}

    public static function getsites(Request $request)
    {
        $request = Self::digestInput($request);
        return sitesservice::getsites($request);
    }

	// Method to get all records
	public static function listsitescount(Request $request)
	{
		$request = Self::digestInput($request);
        return sitesservice::listsitescount($request);
	}

    public static function getsitescount(Request $request)
    {
        $request = Self::digestInput($request);
        return sitesservice::getsitescount($request);
    }
}

