<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\materialservice;
use App\Services\customservice;


class materialcontroller  extends ParentController
{
    //
    public static function savematerial(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,materialservice::getmaterial($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return materialservice::savematerial($id, $request);
    }

	// Method to get all records
	public static function listmaterial(Request $request)
	{
		$request = Self::digestInput($request);
        return materialservice::listmaterial($request);
	}

    public static function getmaterial(Request $request)
    {
        $request = Self::digestInput($request);
        return materialservice::getmaterial($request);
    }

	// Method to get all records
	public static function listmaterialcount(Request $request)
	{
		$request = Self::digestInput($request);
        return materialservice::listmaterialcount($request);
	}

    public static function getmaterialcount(Request $request)
    {
        $request = Self::digestInput($request);
        return materialservice::getmaterialcount($request);
    }
}

