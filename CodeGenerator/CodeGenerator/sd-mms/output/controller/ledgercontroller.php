<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\ledgerservice;
use App\Services\customservice;


class ledgercontroller  extends ParentController
{
    //
    public static function saveledger(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,ledgerservice::getledger($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return ledgerservice::saveledger($id, $request);
    }

	// Method to get all records
	public static function listledger(Request $request)
	{
		$request = Self::digestInput($request);
        return ledgerservice::listledger($request);
	}

    public static function getledger(Request $request)
    {
        $request = Self::digestInput($request);
        return ledgerservice::getledger($request);
    }

	// Method to get all records
	public static function listledgercount(Request $request)
	{
		$request = Self::digestInput($request);
        return ledgerservice::listledgercount($request);
	}

    public static function getledgercount(Request $request)
    {
        $request = Self::digestInput($request);
        return ledgerservice::getledgercount($request);
    }
}

