<?php

namespace App\Http\Controllers\meta;
use App\Http\Controllers\ParentController;
use Illuminate\Http\Request;
use App\Services\worktypeservice;
use App\Services\customservice;


class worktypecontroller  extends ParentController
{
    //
    public static function saveworktype(Request $request)
    {        
        $id = $request['id'];
        if (isset($request['submit']) && ($request['submit'] == '1')) {    
			$filter = [];
			$filter['col'] = 'id';
			$filter['value'] = $id;
            $request = Self::digestSubmit($request,worktypeservice::getworktype($filter));      
			if(isset($request['printdata']))     
				customservice::saveprintmaster($id, $request);
        }        
        $request = Self::digestInput($request);         
        return worktypeservice::saveworktype($id, $request);
    }

	// Method to get all records
	public static function listworktype(Request $request)
	{
		$request = Self::digestInput($request);
        return worktypeservice::listworktype($request);
	}

    public static function getworktype(Request $request)
    {
        $request = Self::digestInput($request);
        return worktypeservice::getworktype($request);
    }

	// Method to get all records
	public static function listworktypecount(Request $request)
	{
		$request = Self::digestInput($request);
        return worktypeservice::listworktypecount($request);
	}

    public static function getworktypecount(Request $request)
    {
        $request = Self::digestInput($request);
        return worktypeservice::getworktypecount($request);
    }
}

