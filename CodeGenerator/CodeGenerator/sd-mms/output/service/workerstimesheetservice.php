<?php

namespace App\Services;

use App\Models\workerstimesheet;
use App\ServicesEx\workerstimesheetserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class workerstimesheetservice extends BaseService
{
	// Method to save data
	public static function saveworkerstimesheet($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = workerstimesheetserviceex::saveworkerstimesheet_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = workerstimesheet::saveworkerstimesheet($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			workerstimesheetserviceex::saveworkerstimesheet_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listworkerstimesheet($request)
	{
		// Pre Operation
		$request = workerstimesheetserviceex::listworkerstimesheet_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workerstimesheet::listworkerstimesheet($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		workerstimesheetserviceex::listworkerstimesheet_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkerstimesheet($request)
	{
		// Pre Operation
		$request = workerstimesheetserviceex::getworkerstimesheet_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workerstimesheet::getworkerstimesheet($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		workerstimesheetserviceex::getworkerstimesheet_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listworkerstimesheetcount($request)
	{
		// Pre Operation
		//$request = workerstimesheetserviceex::listworkerstimesheet_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listworkerstimesheet($request)['data']); //workerstimesheet::listworkerstimesheet($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//workerstimesheetserviceex::listworkerstimesheet_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkerstimesheetcount($request)
	{
		// Pre Operation
		//$request = workerstimesheetserviceex::getworkerstimesheet_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getworkerstimesheet($request)['data']); //workerstimesheet::getworkerstimesheet($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//workerstimesheetserviceex::getworkerstimesheet_after($request);

		return $returnValue;		
	}
}
