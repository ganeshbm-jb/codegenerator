<?php

namespace App\Services;

use App\Models\materialnote;
use App\ServicesEx\materialnoteserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class materialnoteservice extends BaseService
{
	// Method to save data
	public static function savematerialnote($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = materialnoteserviceex::savematerialnote_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = materialnote::savematerialnote($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			materialnoteserviceex::savematerialnote_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listmaterialnote($request)
	{
		// Pre Operation
		$request = materialnoteserviceex::listmaterialnote_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materialnote::listmaterialnote($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		materialnoteserviceex::listmaterialnote_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialnote($request)
	{
		// Pre Operation
		$request = materialnoteserviceex::getmaterialnote_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materialnote::getmaterialnote($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		materialnoteserviceex::getmaterialnote_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listmaterialnotecount($request)
	{
		// Pre Operation
		//$request = materialnoteserviceex::listmaterialnote_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listmaterialnote($request)['data']); //materialnote::listmaterialnote($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//materialnoteserviceex::listmaterialnote_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialnotecount($request)
	{
		// Pre Operation
		//$request = materialnoteserviceex::getmaterialnote_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getmaterialnote($request)['data']); //materialnote::getmaterialnote($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//materialnoteserviceex::getmaterialnote_after($request);

		return $returnValue;		
	}
}
