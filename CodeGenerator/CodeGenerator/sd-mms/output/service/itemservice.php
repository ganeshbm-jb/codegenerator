<?php

namespace App\Services;

use App\Models\item;
use App\ServicesEx\itemserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class itemservice extends BaseService
{
	// Method to save data
	public static function saveitem($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = itemserviceex::saveitem_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = item::saveitem($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			itemserviceex::saveitem_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listitem($request)
	{
		// Pre Operation
		$request = itemserviceex::listitem_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = item::listitem($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		itemserviceex::listitem_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getitem($request)
	{
		// Pre Operation
		$request = itemserviceex::getitem_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = item::getitem($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		itemserviceex::getitem_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listitemcount($request)
	{
		// Pre Operation
		//$request = itemserviceex::listitem_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listitem($request)['data']); //item::listitem($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//itemserviceex::listitem_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getitemcount($request)
	{
		// Pre Operation
		//$request = itemserviceex::getitem_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getitem($request)['data']); //item::getitem($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//itemserviceex::getitem_after($request);

		return $returnValue;		
	}
}
