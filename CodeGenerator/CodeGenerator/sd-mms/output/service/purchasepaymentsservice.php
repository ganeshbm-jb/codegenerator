<?php

namespace App\Services;

use App\Models\purchasepayments;
use App\ServicesEx\purchasepaymentsserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class purchasepaymentsservice extends BaseService
{
	// Method to save data
	public static function savepurchasepayments($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = purchasepaymentsserviceex::savepurchasepayments_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = purchasepayments::savepurchasepayments($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			purchasepaymentsserviceex::savepurchasepayments_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listpurchasepayments($request)
	{
		// Pre Operation
		$request = purchasepaymentsserviceex::listpurchasepayments_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = purchasepayments::listpurchasepayments($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		purchasepaymentsserviceex::listpurchasepayments_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getpurchasepayments($request)
	{
		// Pre Operation
		$request = purchasepaymentsserviceex::getpurchasepayments_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = purchasepayments::getpurchasepayments($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		purchasepaymentsserviceex::getpurchasepayments_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listpurchasepaymentscount($request)
	{
		// Pre Operation
		//$request = purchasepaymentsserviceex::listpurchasepayments_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listpurchasepayments($request)['data']); //purchasepayments::listpurchasepayments($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//purchasepaymentsserviceex::listpurchasepayments_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getpurchasepaymentscount($request)
	{
		// Pre Operation
		//$request = purchasepaymentsserviceex::getpurchasepayments_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getpurchasepayments($request)['data']); //purchasepayments::getpurchasepayments($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//purchasepaymentsserviceex::getpurchasepayments_after($request);

		return $returnValue;		
	}
}
