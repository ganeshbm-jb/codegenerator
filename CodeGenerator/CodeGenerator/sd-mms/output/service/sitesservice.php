<?php

namespace App\Services;

use App\Models\sites;
use App\ServicesEx\sitesserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class sitesservice extends BaseService
{
	// Method to save data
	public static function savesites($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = sitesserviceex::savesites_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = sites::savesites($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			sitesserviceex::savesites_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listsites($request)
	{
		// Pre Operation
		$request = sitesserviceex::listsites_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = sites::listsites($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		sitesserviceex::listsites_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getsites($request)
	{
		// Pre Operation
		$request = sitesserviceex::getsites_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = sites::getsites($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		sitesserviceex::getsites_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listsitescount($request)
	{
		// Pre Operation
		//$request = sitesserviceex::listsites_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listsites($request)['data']); //sites::listsites($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//sitesserviceex::listsites_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getsitescount($request)
	{
		// Pre Operation
		//$request = sitesserviceex::getsites_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getsites($request)['data']); //sites::getsites($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//sitesserviceex::getsites_after($request);

		return $returnValue;		
	}
}
