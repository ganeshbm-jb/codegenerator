<?php

namespace App\Services;

use App\Models\invoicereceipts;
use App\ServicesEx\invoicereceiptsserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class invoicereceiptsservice extends BaseService
{
	// Method to save data
	public static function saveinvoicereceipts($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = invoicereceiptsserviceex::saveinvoicereceipts_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = invoicereceipts::saveinvoicereceipts($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			invoicereceiptsserviceex::saveinvoicereceipts_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listinvoicereceipts($request)
	{
		// Pre Operation
		$request = invoicereceiptsserviceex::listinvoicereceipts_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = invoicereceipts::listinvoicereceipts($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		invoicereceiptsserviceex::listinvoicereceipts_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getinvoicereceipts($request)
	{
		// Pre Operation
		$request = invoicereceiptsserviceex::getinvoicereceipts_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = invoicereceipts::getinvoicereceipts($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		invoicereceiptsserviceex::getinvoicereceipts_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listinvoicereceiptscount($request)
	{
		// Pre Operation
		//$request = invoicereceiptsserviceex::listinvoicereceipts_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listinvoicereceipts($request)['data']); //invoicereceipts::listinvoicereceipts($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//invoicereceiptsserviceex::listinvoicereceipts_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getinvoicereceiptscount($request)
	{
		// Pre Operation
		//$request = invoicereceiptsserviceex::getinvoicereceipts_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getinvoicereceipts($request)['data']); //invoicereceipts::getinvoicereceipts($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//invoicereceiptsserviceex::getinvoicereceipts_after($request);

		return $returnValue;		
	}
}
