<?php

namespace App\Services;

use App\Models\itemmaterials;
use App\ServicesEx\itemmaterialsserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class itemmaterialsservice extends BaseService
{
	// Method to save data
	public static function saveitemmaterials($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = itemmaterialsserviceex::saveitemmaterials_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = itemmaterials::saveitemmaterials($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			itemmaterialsserviceex::saveitemmaterials_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listitemmaterials($request)
	{
		// Pre Operation
		$request = itemmaterialsserviceex::listitemmaterials_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = itemmaterials::listitemmaterials($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		itemmaterialsserviceex::listitemmaterials_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getitemmaterials($request)
	{
		// Pre Operation
		$request = itemmaterialsserviceex::getitemmaterials_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = itemmaterials::getitemmaterials($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		itemmaterialsserviceex::getitemmaterials_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listitemmaterialscount($request)
	{
		// Pre Operation
		//$request = itemmaterialsserviceex::listitemmaterials_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listitemmaterials($request)['data']); //itemmaterials::listitemmaterials($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//itemmaterialsserviceex::listitemmaterials_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getitemmaterialscount($request)
	{
		// Pre Operation
		//$request = itemmaterialsserviceex::getitemmaterials_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getitemmaterials($request)['data']); //itemmaterials::getitemmaterials($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//itemmaterialsserviceex::getitemmaterials_after($request);

		return $returnValue;		
	}
}
