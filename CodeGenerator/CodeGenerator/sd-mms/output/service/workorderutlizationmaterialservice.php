<?php

namespace App\Services;

use App\Models\workorderutlizationmaterial;
use App\ServicesEx\workorderutlizationmaterialserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class workorderutlizationmaterialservice extends BaseService
{
	// Method to save data
	public static function saveworkorderutlizationmaterial($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = workorderutlizationmaterialserviceex::saveworkorderutlizationmaterial_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = workorderutlizationmaterial::saveworkorderutlizationmaterial($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			workorderutlizationmaterialserviceex::saveworkorderutlizationmaterial_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listworkorderutlizationmaterial($request)
	{
		// Pre Operation
		$request = workorderutlizationmaterialserviceex::listworkorderutlizationmaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workorderutlizationmaterial::listworkorderutlizationmaterial($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		workorderutlizationmaterialserviceex::listworkorderutlizationmaterial_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkorderutlizationmaterial($request)
	{
		// Pre Operation
		$request = workorderutlizationmaterialserviceex::getworkorderutlizationmaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workorderutlizationmaterial::getworkorderutlizationmaterial($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		workorderutlizationmaterialserviceex::getworkorderutlizationmaterial_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listworkorderutlizationmaterialcount($request)
	{
		// Pre Operation
		//$request = workorderutlizationmaterialserviceex::listworkorderutlizationmaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listworkorderutlizationmaterial($request)['data']); //workorderutlizationmaterial::listworkorderutlizationmaterial($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//workorderutlizationmaterialserviceex::listworkorderutlizationmaterial_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkorderutlizationmaterialcount($request)
	{
		// Pre Operation
		//$request = workorderutlizationmaterialserviceex::getworkorderutlizationmaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getworkorderutlizationmaterial($request)['data']); //workorderutlizationmaterial::getworkorderutlizationmaterial($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//workorderutlizationmaterialserviceex::getworkorderutlizationmaterial_after($request);

		return $returnValue;		
	}
}
