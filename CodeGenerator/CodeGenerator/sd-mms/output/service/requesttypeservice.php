<?php

namespace App\Services;

use App\Models\requesttype;
use App\ServicesEx\requesttypeserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class requesttypeservice extends BaseService
{
	// Method to save data
	public static function saverequesttype($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = requesttypeserviceex::saverequesttype_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = requesttype::saverequesttype($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			requesttypeserviceex::saverequesttype_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listrequesttype($request)
	{
		// Pre Operation
		$request = requesttypeserviceex::listrequesttype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = requesttype::listrequesttype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		requesttypeserviceex::listrequesttype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getrequesttype($request)
	{
		// Pre Operation
		$request = requesttypeserviceex::getrequesttype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = requesttype::getrequesttype($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		requesttypeserviceex::getrequesttype_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listrequesttypecount($request)
	{
		// Pre Operation
		//$request = requesttypeserviceex::listrequesttype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listrequesttype($request)['data']); //requesttype::listrequesttype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//requesttypeserviceex::listrequesttype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getrequesttypecount($request)
	{
		// Pre Operation
		//$request = requesttypeserviceex::getrequesttype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getrequesttype($request)['data']); //requesttype::getrequesttype($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//requesttypeserviceex::getrequesttype_after($request);

		return $returnValue;		
	}
}
