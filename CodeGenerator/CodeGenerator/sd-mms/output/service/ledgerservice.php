<?php

namespace App\Services;

use App\Models\ledger;
use App\ServicesEx\ledgerserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class ledgerservice extends BaseService
{
	// Method to save data
	public static function saveledger($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = ledgerserviceex::saveledger_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = ledger::saveledger($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			ledgerserviceex::saveledger_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listledger($request)
	{
		// Pre Operation
		$request = ledgerserviceex::listledger_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = ledger::listledger($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		ledgerserviceex::listledger_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getledger($request)
	{
		// Pre Operation
		$request = ledgerserviceex::getledger_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = ledger::getledger($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		ledgerserviceex::getledger_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listledgercount($request)
	{
		// Pre Operation
		//$request = ledgerserviceex::listledger_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listledger($request)['data']); //ledger::listledger($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//ledgerserviceex::listledger_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getledgercount($request)
	{
		// Pre Operation
		//$request = ledgerserviceex::getledger_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getledger($request)['data']); //ledger::getledger($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//ledgerserviceex::getledger_after($request);

		return $returnValue;		
	}
}
