<?php

namespace App\Services;

use App\Models\stockvalue;
use App\ServicesEx\stockvalueserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class stockvalueservice extends BaseService
{
	// Method to save data
	public static function savestockvalue($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = stockvalueserviceex::savestockvalue_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = stockvalue::savestockvalue($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			stockvalueserviceex::savestockvalue_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function liststockvalue($request)
	{
		// Pre Operation
		$request = stockvalueserviceex::liststockvalue_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = stockvalue::liststockvalue($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		stockvalueserviceex::liststockvalue_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getstockvalue($request)
	{
		// Pre Operation
		$request = stockvalueserviceex::getstockvalue_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = stockvalue::getstockvalue($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		stockvalueserviceex::getstockvalue_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function liststockvaluecount($request)
	{
		// Pre Operation
		//$request = stockvalueserviceex::liststockvalue_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::liststockvalue($request)['data']); //stockvalue::liststockvalue($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//stockvalueserviceex::liststockvalue_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getstockvaluecount($request)
	{
		// Pre Operation
		//$request = stockvalueserviceex::getstockvalue_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getstockvalue($request)['data']); //stockvalue::getstockvalue($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//stockvalueserviceex::getstockvalue_after($request);

		return $returnValue;		
	}
}
