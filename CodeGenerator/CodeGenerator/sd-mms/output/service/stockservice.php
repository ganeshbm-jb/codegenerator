<?php

namespace App\Services;

use App\Models\stock;
use App\ServicesEx\stockserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class stockservice extends BaseService
{
	// Method to save data
	public static function savestock($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = stockserviceex::savestock_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = stock::savestock($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			stockserviceex::savestock_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function liststock($request)
	{
		// Pre Operation
		$request = stockserviceex::liststock_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = stock::liststock($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		stockserviceex::liststock_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getstock($request)
	{
		// Pre Operation
		$request = stockserviceex::getstock_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = stock::getstock($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		stockserviceex::getstock_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function liststockcount($request)
	{
		// Pre Operation
		//$request = stockserviceex::liststock_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::liststock($request)['data']); //stock::liststock($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//stockserviceex::liststock_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getstockcount($request)
	{
		// Pre Operation
		//$request = stockserviceex::getstock_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getstock($request)['data']); //stock::getstock($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//stockserviceex::getstock_after($request);

		return $returnValue;		
	}
}
