<?php

namespace App\Services;

use App\Models\batch;
use App\ServicesEx\batchserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class batchservice extends BaseService
{
	// Method to save data
	public static function savebatch($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = batchserviceex::savebatch_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = batch::savebatch($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			batchserviceex::savebatch_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listbatch($request)
	{
		// Pre Operation
		$request = batchserviceex::listbatch_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = batch::listbatch($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		batchserviceex::listbatch_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getbatch($request)
	{
		// Pre Operation
		$request = batchserviceex::getbatch_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = batch::getbatch($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		batchserviceex::getbatch_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listbatchcount($request)
	{
		// Pre Operation
		//$request = batchserviceex::listbatch_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listbatch($request)['data']); //batch::listbatch($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//batchserviceex::listbatch_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getbatchcount($request)
	{
		// Pre Operation
		//$request = batchserviceex::getbatch_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getbatch($request)['data']); //batch::getbatch($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//batchserviceex::getbatch_after($request);

		return $returnValue;		
	}
}
