<?php

namespace App\Services;

use App\Models\advance;
use App\ServicesEx\advanceserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class advanceservice extends BaseService
{
	// Method to save data
	public static function saveadvance($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = advanceserviceex::saveadvance_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = advance::saveadvance($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			advanceserviceex::saveadvance_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listadvance($request)
	{
		// Pre Operation
		$request = advanceserviceex::listadvance_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = advance::listadvance($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		advanceserviceex::listadvance_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getadvance($request)
	{
		// Pre Operation
		$request = advanceserviceex::getadvance_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = advance::getadvance($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		advanceserviceex::getadvance_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listadvancecount($request)
	{
		// Pre Operation
		//$request = advanceserviceex::listadvance_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listadvance($request)['data']); //advance::listadvance($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//advanceserviceex::listadvance_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getadvancecount($request)
	{
		// Pre Operation
		//$request = advanceserviceex::getadvance_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getadvance($request)['data']); //advance::getadvance($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//advanceserviceex::getadvance_after($request);

		return $returnValue;		
	}
}
