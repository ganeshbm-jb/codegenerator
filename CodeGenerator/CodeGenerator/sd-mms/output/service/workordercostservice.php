<?php

namespace App\Services;

use App\Models\workordercost;
use App\ServicesEx\workordercostserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class workordercostservice extends BaseService
{
	// Method to save data
	public static function saveworkordercost($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = workordercostserviceex::saveworkordercost_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = workordercost::saveworkordercost($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			workordercostserviceex::saveworkordercost_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listworkordercost($request)
	{
		// Pre Operation
		$request = workordercostserviceex::listworkordercost_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workordercost::listworkordercost($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		workordercostserviceex::listworkordercost_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkordercost($request)
	{
		// Pre Operation
		$request = workordercostserviceex::getworkordercost_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workordercost::getworkordercost($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		workordercostserviceex::getworkordercost_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listworkordercostcount($request)
	{
		// Pre Operation
		//$request = workordercostserviceex::listworkordercost_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listworkordercost($request)['data']); //workordercost::listworkordercost($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//workordercostserviceex::listworkordercost_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkordercostcount($request)
	{
		// Pre Operation
		//$request = workordercostserviceex::getworkordercost_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getworkordercost($request)['data']); //workordercost::getworkordercost($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//workordercostserviceex::getworkordercost_after($request);

		return $returnValue;		
	}
}
