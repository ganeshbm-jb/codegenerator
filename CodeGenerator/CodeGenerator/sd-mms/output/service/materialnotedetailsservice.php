<?php

namespace App\Services;

use App\Models\materialnotedetails;
use App\ServicesEx\materialnotedetailsserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class materialnotedetailsservice extends BaseService
{
	// Method to save data
	public static function savematerialnotedetails($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = materialnotedetailsserviceex::savematerialnotedetails_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = materialnotedetails::savematerialnotedetails($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			materialnotedetailsserviceex::savematerialnotedetails_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listmaterialnotedetails($request)
	{
		// Pre Operation
		$request = materialnotedetailsserviceex::listmaterialnotedetails_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materialnotedetails::listmaterialnotedetails($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		materialnotedetailsserviceex::listmaterialnotedetails_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialnotedetails($request)
	{
		// Pre Operation
		$request = materialnotedetailsserviceex::getmaterialnotedetails_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materialnotedetails::getmaterialnotedetails($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		materialnotedetailsserviceex::getmaterialnotedetails_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listmaterialnotedetailscount($request)
	{
		// Pre Operation
		//$request = materialnotedetailsserviceex::listmaterialnotedetails_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listmaterialnotedetails($request)['data']); //materialnotedetails::listmaterialnotedetails($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//materialnotedetailsserviceex::listmaterialnotedetails_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialnotedetailscount($request)
	{
		// Pre Operation
		//$request = materialnotedetailsserviceex::getmaterialnotedetails_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getmaterialnotedetails($request)['data']); //materialnotedetails::getmaterialnotedetails($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//materialnotedetailsserviceex::getmaterialnotedetails_after($request);

		return $returnValue;		
	}
}
