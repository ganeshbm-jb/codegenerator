<?php

namespace App\Services;

use App\Models\materialcategory;
use App\ServicesEx\materialcategoryserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class materialcategoryservice extends BaseService
{
	// Method to save data
	public static function savematerialcategory($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = materialcategoryserviceex::savematerialcategory_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = materialcategory::savematerialcategory($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			materialcategoryserviceex::savematerialcategory_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listmaterialcategory($request)
	{
		// Pre Operation
		$request = materialcategoryserviceex::listmaterialcategory_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materialcategory::listmaterialcategory($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		materialcategoryserviceex::listmaterialcategory_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialcategory($request)
	{
		// Pre Operation
		$request = materialcategoryserviceex::getmaterialcategory_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materialcategory::getmaterialcategory($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		materialcategoryserviceex::getmaterialcategory_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listmaterialcategorycount($request)
	{
		// Pre Operation
		//$request = materialcategoryserviceex::listmaterialcategory_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listmaterialcategory($request)['data']); //materialcategory::listmaterialcategory($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//materialcategoryserviceex::listmaterialcategory_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialcategorycount($request)
	{
		// Pre Operation
		//$request = materialcategoryserviceex::getmaterialcategory_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getmaterialcategory($request)['data']); //materialcategory::getmaterialcategory($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//materialcategoryserviceex::getmaterialcategory_after($request);

		return $returnValue;		
	}
}
