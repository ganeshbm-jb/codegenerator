<?php

namespace App\Services;

use App\Models\users;
use App\ServicesEx\usersserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class usersservice extends BaseService
{
	// Method to save data
	public static function saveusers($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = usersserviceex::saveusers_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = users::saveusers($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			usersserviceex::saveusers_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listusers($request)
	{
		// Pre Operation
		$request = usersserviceex::listusers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = users::listusers($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		usersserviceex::listusers_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getusers($request)
	{
		// Pre Operation
		$request = usersserviceex::getusers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = users::getusers($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		usersserviceex::getusers_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listuserscount($request)
	{
		// Pre Operation
		//$request = usersserviceex::listusers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listusers($request)['data']); //users::listusers($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//usersserviceex::listusers_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getuserscount($request)
	{
		// Pre Operation
		//$request = usersserviceex::getusers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getusers($request)['data']); //users::getusers($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//usersserviceex::getusers_after($request);

		return $returnValue;		
	}
}
