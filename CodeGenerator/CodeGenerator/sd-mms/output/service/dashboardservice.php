<?php

namespace App\Services;

use App\Models\dashboard;
use App\ServicesEx\dashboardserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class dashboardservice extends BaseService
{
	// Method to save data
	public static function savedashboard($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = dashboardserviceex::savedashboard_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = dashboard::savedashboard($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			dashboardserviceex::savedashboard_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listdashboard($request)
	{
		// Pre Operation
		$request = dashboardserviceex::listdashboard_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = dashboard::listdashboard($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		dashboardserviceex::listdashboard_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getdashboard($request)
	{
		// Pre Operation
		$request = dashboardserviceex::getdashboard_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = dashboard::getdashboard($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		dashboardserviceex::getdashboard_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listdashboardcount($request)
	{
		// Pre Operation
		//$request = dashboardserviceex::listdashboard_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listdashboard($request)['data']); //dashboard::listdashboard($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//dashboardserviceex::listdashboard_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getdashboardcount($request)
	{
		// Pre Operation
		//$request = dashboardserviceex::getdashboard_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getdashboard($request)['data']); //dashboard::getdashboard($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//dashboardserviceex::getdashboard_after($request);

		return $returnValue;		
	}
}
