<?php

namespace App\Services;

use App\Models\invoice;
use App\ServicesEx\invoiceserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class invoiceservice extends BaseService
{
	// Method to save data
	public static function saveinvoice($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = invoiceserviceex::saveinvoice_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = invoice::saveinvoice($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			invoiceserviceex::saveinvoice_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listinvoice($request)
	{
		// Pre Operation
		$request = invoiceserviceex::listinvoice_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = invoice::listinvoice($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		invoiceserviceex::listinvoice_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getinvoice($request)
	{
		// Pre Operation
		$request = invoiceserviceex::getinvoice_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = invoice::getinvoice($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		invoiceserviceex::getinvoice_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listinvoicecount($request)
	{
		// Pre Operation
		//$request = invoiceserviceex::listinvoice_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listinvoice($request)['data']); //invoice::listinvoice($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//invoiceserviceex::listinvoice_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getinvoicecount($request)
	{
		// Pre Operation
		//$request = invoiceserviceex::getinvoice_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getinvoice($request)['data']); //invoice::getinvoice($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//invoiceserviceex::getinvoice_after($request);

		return $returnValue;		
	}
}
