<?php

namespace App\Services;

use App\Models\userviewaccess;
use App\ServicesEx\userviewaccessserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class userviewaccessservice extends BaseService
{
	// Method to save data
	public static function saveuserviewaccess($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = userviewaccessserviceex::saveuserviewaccess_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = userviewaccess::saveuserviewaccess($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			userviewaccessserviceex::saveuserviewaccess_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listuserviewaccess($request)
	{
		// Pre Operation
		$request = userviewaccessserviceex::listuserviewaccess_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = userviewaccess::listuserviewaccess($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		userviewaccessserviceex::listuserviewaccess_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getuserviewaccess($request)
	{
		// Pre Operation
		$request = userviewaccessserviceex::getuserviewaccess_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = userviewaccess::getuserviewaccess($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		userviewaccessserviceex::getuserviewaccess_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listuserviewaccesscount($request)
	{
		// Pre Operation
		//$request = userviewaccessserviceex::listuserviewaccess_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listuserviewaccess($request)['data']); //userviewaccess::listuserviewaccess($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//userviewaccessserviceex::listuserviewaccess_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getuserviewaccesscount($request)
	{
		// Pre Operation
		//$request = userviewaccessserviceex::getuserviewaccess_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getuserviewaccess($request)['data']); //userviewaccess::getuserviewaccess($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//userviewaccessserviceex::getuserviewaccess_after($request);

		return $returnValue;		
	}
}
