<?php

namespace App\Services;

use App\Models\purchases;
use App\ServicesEx\purchasesserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class purchasesservice extends BaseService
{
	// Method to save data
	public static function savepurchases($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = purchasesserviceex::savepurchases_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = purchases::savepurchases($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			purchasesserviceex::savepurchases_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listpurchases($request)
	{
		// Pre Operation
		$request = purchasesserviceex::listpurchases_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = purchases::listpurchases($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		purchasesserviceex::listpurchases_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getpurchases($request)
	{
		// Pre Operation
		$request = purchasesserviceex::getpurchases_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = purchases::getpurchases($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		purchasesserviceex::getpurchases_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listpurchasescount($request)
	{
		// Pre Operation
		//$request = purchasesserviceex::listpurchases_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listpurchases($request)['data']); //purchases::listpurchases($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//purchasesserviceex::listpurchases_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getpurchasescount($request)
	{
		// Pre Operation
		//$request = purchasesserviceex::getpurchases_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getpurchases($request)['data']); //purchases::getpurchases($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//purchasesserviceex::getpurchases_after($request);

		return $returnValue;		
	}
}
