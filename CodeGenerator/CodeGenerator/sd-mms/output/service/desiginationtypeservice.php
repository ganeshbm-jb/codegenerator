<?php

namespace App\Services;

use App\Models\desiginationtype;
use App\ServicesEx\desiginationtypeserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class desiginationtypeservice extends BaseService
{
	// Method to save data
	public static function savedesiginationtype($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = desiginationtypeserviceex::savedesiginationtype_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = desiginationtype::savedesiginationtype($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			desiginationtypeserviceex::savedesiginationtype_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listdesiginationtype($request)
	{
		// Pre Operation
		$request = desiginationtypeserviceex::listdesiginationtype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = desiginationtype::listdesiginationtype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		desiginationtypeserviceex::listdesiginationtype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getdesiginationtype($request)
	{
		// Pre Operation
		$request = desiginationtypeserviceex::getdesiginationtype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = desiginationtype::getdesiginationtype($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		desiginationtypeserviceex::getdesiginationtype_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listdesiginationtypecount($request)
	{
		// Pre Operation
		//$request = desiginationtypeserviceex::listdesiginationtype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listdesiginationtype($request)['data']); //desiginationtype::listdesiginationtype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//desiginationtypeserviceex::listdesiginationtype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getdesiginationtypecount($request)
	{
		// Pre Operation
		//$request = desiginationtypeserviceex::getdesiginationtype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getdesiginationtype($request)['data']); //desiginationtype::getdesiginationtype($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//desiginationtypeserviceex::getdesiginationtype_after($request);

		return $returnValue;		
	}
}
