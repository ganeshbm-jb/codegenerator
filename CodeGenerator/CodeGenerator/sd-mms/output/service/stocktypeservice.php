<?php

namespace App\Services;

use App\Models\stocktype;
use App\ServicesEx\stocktypeserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class stocktypeservice extends BaseService
{
	// Method to save data
	public static function savestocktype($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = stocktypeserviceex::savestocktype_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = stocktype::savestocktype($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			stocktypeserviceex::savestocktype_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function liststocktype($request)
	{
		// Pre Operation
		$request = stocktypeserviceex::liststocktype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = stocktype::liststocktype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		stocktypeserviceex::liststocktype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getstocktype($request)
	{
		// Pre Operation
		$request = stocktypeserviceex::getstocktype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = stocktype::getstocktype($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		stocktypeserviceex::getstocktype_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function liststocktypecount($request)
	{
		// Pre Operation
		//$request = stocktypeserviceex::liststocktype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::liststocktype($request)['data']); //stocktype::liststocktype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//stocktypeserviceex::liststocktype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getstocktypecount($request)
	{
		// Pre Operation
		//$request = stocktypeserviceex::getstocktype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getstocktype($request)['data']); //stocktype::getstocktype($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//stocktypeserviceex::getstocktype_after($request);

		return $returnValue;		
	}
}
