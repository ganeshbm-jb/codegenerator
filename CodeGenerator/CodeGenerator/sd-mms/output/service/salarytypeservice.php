<?php

namespace App\Services;

use App\Models\salarytype;
use App\ServicesEx\salarytypeserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class salarytypeservice extends BaseService
{
	// Method to save data
	public static function savesalarytype($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = salarytypeserviceex::savesalarytype_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = salarytype::savesalarytype($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			salarytypeserviceex::savesalarytype_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listsalarytype($request)
	{
		// Pre Operation
		$request = salarytypeserviceex::listsalarytype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = salarytype::listsalarytype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		salarytypeserviceex::listsalarytype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getsalarytype($request)
	{
		// Pre Operation
		$request = salarytypeserviceex::getsalarytype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = salarytype::getsalarytype($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		salarytypeserviceex::getsalarytype_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listsalarytypecount($request)
	{
		// Pre Operation
		//$request = salarytypeserviceex::listsalarytype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listsalarytype($request)['data']); //salarytype::listsalarytype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//salarytypeserviceex::listsalarytype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getsalarytypecount($request)
	{
		// Pre Operation
		//$request = salarytypeserviceex::getsalarytype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getsalarytype($request)['data']); //salarytype::getsalarytype($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//salarytypeserviceex::getsalarytype_after($request);

		return $returnValue;		
	}
}
