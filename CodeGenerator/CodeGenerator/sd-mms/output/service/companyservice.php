<?php

namespace App\Services;

use App\Models\company;
use App\ServicesEx\companyserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class companyservice extends BaseService
{
	// Method to save data
	public static function savecompany($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = companyserviceex::savecompany_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = company::savecompany($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			companyserviceex::savecompany_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listcompany($request)
	{
		// Pre Operation
		$request = companyserviceex::listcompany_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = company::listcompany($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		companyserviceex::listcompany_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getcompany($request)
	{
		// Pre Operation
		$request = companyserviceex::getcompany_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = company::getcompany($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		companyserviceex::getcompany_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listcompanycount($request)
	{
		// Pre Operation
		//$request = companyserviceex::listcompany_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listcompany($request)['data']); //company::listcompany($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//companyserviceex::listcompany_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getcompanycount($request)
	{
		// Pre Operation
		//$request = companyserviceex::getcompany_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getcompany($request)['data']); //company::getcompany($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//companyserviceex::getcompany_after($request);

		return $returnValue;		
	}
}
