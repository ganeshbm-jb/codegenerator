<?php

namespace App\Services;

use App\Models\purchasereturn;
use App\ServicesEx\purchasereturnserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class purchasereturnservice extends BaseService
{
	// Method to save data
	public static function savepurchasereturn($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = purchasereturnserviceex::savepurchasereturn_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = purchasereturn::savepurchasereturn($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			purchasereturnserviceex::savepurchasereturn_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listpurchasereturn($request)
	{
		// Pre Operation
		$request = purchasereturnserviceex::listpurchasereturn_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = purchasereturn::listpurchasereturn($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		purchasereturnserviceex::listpurchasereturn_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getpurchasereturn($request)
	{
		// Pre Operation
		$request = purchasereturnserviceex::getpurchasereturn_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = purchasereturn::getpurchasereturn($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		purchasereturnserviceex::getpurchasereturn_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listpurchasereturncount($request)
	{
		// Pre Operation
		//$request = purchasereturnserviceex::listpurchasereturn_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listpurchasereturn($request)['data']); //purchasereturn::listpurchasereturn($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//purchasereturnserviceex::listpurchasereturn_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getpurchasereturncount($request)
	{
		// Pre Operation
		//$request = purchasereturnserviceex::getpurchasereturn_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getpurchasereturn($request)['data']); //purchasereturn::getpurchasereturn($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//purchasereturnserviceex::getpurchasereturn_after($request);

		return $returnValue;		
	}
}
