<?php

namespace App\Services;

use App\Models\advancetype;
use App\ServicesEx\advancetypeserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class advancetypeservice extends BaseService
{
	// Method to save data
	public static function saveadvancetype($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = advancetypeserviceex::saveadvancetype_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = advancetype::saveadvancetype($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			advancetypeserviceex::saveadvancetype_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listadvancetype($request)
	{
		// Pre Operation
		$request = advancetypeserviceex::listadvancetype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = advancetype::listadvancetype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		advancetypeserviceex::listadvancetype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getadvancetype($request)
	{
		// Pre Operation
		$request = advancetypeserviceex::getadvancetype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = advancetype::getadvancetype($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		advancetypeserviceex::getadvancetype_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listadvancetypecount($request)
	{
		// Pre Operation
		//$request = advancetypeserviceex::listadvancetype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listadvancetype($request)['data']); //advancetype::listadvancetype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//advancetypeserviceex::listadvancetype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getadvancetypecount($request)
	{
		// Pre Operation
		//$request = advancetypeserviceex::getadvancetype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getadvancetype($request)['data']); //advancetype::getadvancetype($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//advancetypeserviceex::getadvancetype_after($request);

		return $returnValue;		
	}
}
