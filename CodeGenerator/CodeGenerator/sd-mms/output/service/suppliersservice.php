<?php

namespace App\Services;

use App\Models\suppliers;
use App\ServicesEx\suppliersserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class suppliersservice extends BaseService
{
	// Method to save data
	public static function savesuppliers($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = suppliersserviceex::savesuppliers_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = suppliers::savesuppliers($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			suppliersserviceex::savesuppliers_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listsuppliers($request)
	{
		// Pre Operation
		$request = suppliersserviceex::listsuppliers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = suppliers::listsuppliers($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		suppliersserviceex::listsuppliers_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getsuppliers($request)
	{
		// Pre Operation
		$request = suppliersserviceex::getsuppliers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = suppliers::getsuppliers($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		suppliersserviceex::getsuppliers_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listsupplierscount($request)
	{
		// Pre Operation
		//$request = suppliersserviceex::listsuppliers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listsuppliers($request)['data']); //suppliers::listsuppliers($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//suppliersserviceex::listsuppliers_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getsupplierscount($request)
	{
		// Pre Operation
		//$request = suppliersserviceex::getsuppliers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getsuppliers($request)['data']); //suppliers::getsuppliers($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//suppliersserviceex::getsuppliers_after($request);

		return $returnValue;		
	}
}
