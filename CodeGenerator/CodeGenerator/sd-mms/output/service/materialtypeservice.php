<?php

namespace App\Services;

use App\Models\materialtype;
use App\ServicesEx\materialtypeserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class materialtypeservice extends BaseService
{
	// Method to save data
	public static function savematerialtype($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = materialtypeserviceex::savematerialtype_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = materialtype::savematerialtype($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			materialtypeserviceex::savematerialtype_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listmaterialtype($request)
	{
		// Pre Operation
		$request = materialtypeserviceex::listmaterialtype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materialtype::listmaterialtype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		materialtypeserviceex::listmaterialtype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialtype($request)
	{
		// Pre Operation
		$request = materialtypeserviceex::getmaterialtype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materialtype::getmaterialtype($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		materialtypeserviceex::getmaterialtype_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listmaterialtypecount($request)
	{
		// Pre Operation
		//$request = materialtypeserviceex::listmaterialtype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listmaterialtype($request)['data']); //materialtype::listmaterialtype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//materialtypeserviceex::listmaterialtype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialtypecount($request)
	{
		// Pre Operation
		//$request = materialtypeserviceex::getmaterialtype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getmaterialtype($request)['data']); //materialtype::getmaterialtype($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//materialtypeserviceex::getmaterialtype_after($request);

		return $returnValue;		
	}
}
