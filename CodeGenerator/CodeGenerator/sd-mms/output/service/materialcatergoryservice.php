<?php

namespace App\Services;

use App\Models\materialcatergory;
use App\ServicesEx\materialcatergoryserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class materialcatergoryservice extends BaseService
{
	// Method to save data
	public static function savematerialcatergory($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = materialcatergoryserviceex::savematerialcatergory_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = materialcatergory::savematerialcatergory($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			materialcatergoryserviceex::savematerialcatergory_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listmaterialcatergory($request)
	{
		// Pre Operation
		$request = materialcatergoryserviceex::listmaterialcatergory_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materialcatergory::listmaterialcatergory($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		materialcatergoryserviceex::listmaterialcatergory_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialcatergory($request)
	{
		// Pre Operation
		$request = materialcatergoryserviceex::getmaterialcatergory_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materialcatergory::getmaterialcatergory($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		materialcatergoryserviceex::getmaterialcatergory_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listmaterialcatergorycount($request)
	{
		// Pre Operation
		//$request = materialcatergoryserviceex::listmaterialcatergory_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listmaterialcatergory($request)['data']); //materialcatergory::listmaterialcatergory($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//materialcatergoryserviceex::listmaterialcatergory_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialcatergorycount($request)
	{
		// Pre Operation
		//$request = materialcatergoryserviceex::getmaterialcatergory_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getmaterialcatergory($request)['data']); //materialcatergory::getmaterialcatergory($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//materialcatergoryserviceex::getmaterialcatergory_after($request);

		return $returnValue;		
	}
}
