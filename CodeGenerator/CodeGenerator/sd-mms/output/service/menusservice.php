<?php

namespace App\Services;

use App\Models\menus;
use App\ServicesEx\menusserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class menusservice extends BaseService
{
	// Method to save data
	public static function savemenus($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = menusserviceex::savemenus_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = menus::savemenus($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			menusserviceex::savemenus_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listmenus($request)
	{
		// Pre Operation
		$request = menusserviceex::listmenus_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = menus::listmenus($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		menusserviceex::listmenus_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmenus($request)
	{
		// Pre Operation
		$request = menusserviceex::getmenus_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = menus::getmenus($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		menusserviceex::getmenus_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listmenuscount($request)
	{
		// Pre Operation
		//$request = menusserviceex::listmenus_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listmenus($request)['data']); //menus::listmenus($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//menusserviceex::listmenus_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmenuscount($request)
	{
		// Pre Operation
		//$request = menusserviceex::getmenus_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getmenus($request)['data']); //menus::getmenus($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//menusserviceex::getmenus_after($request);

		return $returnValue;		
	}
}
