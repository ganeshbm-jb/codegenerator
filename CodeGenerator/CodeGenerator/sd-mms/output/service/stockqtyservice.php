<?php

namespace App\Services;

use App\Models\stockqty;
use App\ServicesEx\stockqtyserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class stockqtyservice extends BaseService
{
	// Method to save data
	public static function savestockqty($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = stockqtyserviceex::savestockqty_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = stockqty::savestockqty($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			stockqtyserviceex::savestockqty_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function liststockqty($request)
	{
		// Pre Operation
		$request = stockqtyserviceex::liststockqty_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = stockqty::liststockqty($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		stockqtyserviceex::liststockqty_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getstockqty($request)
	{
		// Pre Operation
		$request = stockqtyserviceex::getstockqty_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = stockqty::getstockqty($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		stockqtyserviceex::getstockqty_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function liststockqtycount($request)
	{
		// Pre Operation
		//$request = stockqtyserviceex::liststockqty_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::liststockqty($request)['data']); //stockqty::liststockqty($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//stockqtyserviceex::liststockqty_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getstockqtycount($request)
	{
		// Pre Operation
		//$request = stockqtyserviceex::getstockqty_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getstockqty($request)['data']); //stockqty::getstockqty($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//stockqtyserviceex::getstockqty_after($request);

		return $returnValue;		
	}
}
