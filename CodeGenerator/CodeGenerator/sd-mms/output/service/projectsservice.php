<?php

namespace App\Services;

use App\Models\projects;
use App\ServicesEx\projectsserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class projectsservice extends BaseService
{
	// Method to save data
	public static function saveprojects($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = projectsserviceex::saveprojects_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = projects::saveprojects($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			projectsserviceex::saveprojects_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listprojects($request)
	{
		// Pre Operation
		$request = projectsserviceex::listprojects_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = projects::listprojects($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		projectsserviceex::listprojects_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getprojects($request)
	{
		// Pre Operation
		$request = projectsserviceex::getprojects_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = projects::getprojects($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		projectsserviceex::getprojects_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listprojectscount($request)
	{
		// Pre Operation
		//$request = projectsserviceex::listprojects_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listprojects($request)['data']); //projects::listprojects($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//projectsserviceex::listprojects_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getprojectscount($request)
	{
		// Pre Operation
		//$request = projectsserviceex::getprojects_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getprojects($request)['data']); //projects::getprojects($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//projectsserviceex::getprojects_after($request);

		return $returnValue;		
	}
}
