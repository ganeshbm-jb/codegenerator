<?php

namespace App\Services;

use App\Models\purchaseordermaterial;
use App\ServicesEx\purchaseordermaterialserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class purchaseordermaterialservice extends BaseService
{
	// Method to save data
	public static function savepurchaseordermaterial($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = purchaseordermaterialserviceex::savepurchaseordermaterial_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = purchaseordermaterial::savepurchaseordermaterial($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			purchaseordermaterialserviceex::savepurchaseordermaterial_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listpurchaseordermaterial($request)
	{
		// Pre Operation
		$request = purchaseordermaterialserviceex::listpurchaseordermaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = purchaseordermaterial::listpurchaseordermaterial($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		purchaseordermaterialserviceex::listpurchaseordermaterial_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getpurchaseordermaterial($request)
	{
		// Pre Operation
		$request = purchaseordermaterialserviceex::getpurchaseordermaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = purchaseordermaterial::getpurchaseordermaterial($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		purchaseordermaterialserviceex::getpurchaseordermaterial_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listpurchaseordermaterialcount($request)
	{
		// Pre Operation
		//$request = purchaseordermaterialserviceex::listpurchaseordermaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listpurchaseordermaterial($request)['data']); //purchaseordermaterial::listpurchaseordermaterial($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//purchaseordermaterialserviceex::listpurchaseordermaterial_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getpurchaseordermaterialcount($request)
	{
		// Pre Operation
		//$request = purchaseordermaterialserviceex::getpurchaseordermaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getpurchaseordermaterial($request)['data']); //purchaseordermaterial::getpurchaseordermaterial($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//purchaseordermaterialserviceex::getpurchaseordermaterial_after($request);

		return $returnValue;		
	}
}
