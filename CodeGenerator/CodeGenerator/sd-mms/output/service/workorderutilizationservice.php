<?php

namespace App\Services;

use App\Models\workorderutilization;
use App\ServicesEx\workorderutilizationserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class workorderutilizationservice extends BaseService
{
	// Method to save data
	public static function saveworkorderutilization($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = workorderutilizationserviceex::saveworkorderutilization_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = workorderutilization::saveworkorderutilization($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			workorderutilizationserviceex::saveworkorderutilization_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listworkorderutilization($request)
	{
		// Pre Operation
		$request = workorderutilizationserviceex::listworkorderutilization_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workorderutilization::listworkorderutilization($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		workorderutilizationserviceex::listworkorderutilization_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkorderutilization($request)
	{
		// Pre Operation
		$request = workorderutilizationserviceex::getworkorderutilization_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workorderutilization::getworkorderutilization($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		workorderutilizationserviceex::getworkorderutilization_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listworkorderutilizationcount($request)
	{
		// Pre Operation
		//$request = workorderutilizationserviceex::listworkorderutilization_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listworkorderutilization($request)['data']); //workorderutilization::listworkorderutilization($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//workorderutilizationserviceex::listworkorderutilization_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkorderutilizationcount($request)
	{
		// Pre Operation
		//$request = workorderutilizationserviceex::getworkorderutilization_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getworkorderutilization($request)['data']); //workorderutilization::getworkorderutilization($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//workorderutilizationserviceex::getworkorderutilization_after($request);

		return $returnValue;		
	}
}
