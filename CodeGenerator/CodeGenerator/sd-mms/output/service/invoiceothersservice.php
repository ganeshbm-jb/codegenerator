<?php

namespace App\Services;

use App\Models\invoiceothers;
use App\ServicesEx\invoiceothersserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class invoiceothersservice extends BaseService
{
	// Method to save data
	public static function saveinvoiceothers($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = invoiceothersserviceex::saveinvoiceothers_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = invoiceothers::saveinvoiceothers($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			invoiceothersserviceex::saveinvoiceothers_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listinvoiceothers($request)
	{
		// Pre Operation
		$request = invoiceothersserviceex::listinvoiceothers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = invoiceothers::listinvoiceothers($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		invoiceothersserviceex::listinvoiceothers_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getinvoiceothers($request)
	{
		// Pre Operation
		$request = invoiceothersserviceex::getinvoiceothers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = invoiceothers::getinvoiceothers($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		invoiceothersserviceex::getinvoiceothers_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listinvoiceotherscount($request)
	{
		// Pre Operation
		//$request = invoiceothersserviceex::listinvoiceothers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listinvoiceothers($request)['data']); //invoiceothers::listinvoiceothers($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//invoiceothersserviceex::listinvoiceothers_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getinvoiceotherscount($request)
	{
		// Pre Operation
		//$request = invoiceothersserviceex::getinvoiceothers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getinvoiceothers($request)['data']); //invoiceothers::getinvoiceothers($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//invoiceothersserviceex::getinvoiceothers_after($request);

		return $returnValue;		
	}
}
