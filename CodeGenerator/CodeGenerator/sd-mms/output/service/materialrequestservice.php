<?php

namespace App\Services;

use App\Models\materialrequest;
use App\ServicesEx\materialrequestserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class materialrequestservice extends BaseService
{
	// Method to save data
	public static function savematerialrequest($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = materialrequestserviceex::savematerialrequest_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = materialrequest::savematerialrequest($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			materialrequestserviceex::savematerialrequest_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listmaterialrequest($request)
	{
		// Pre Operation
		$request = materialrequestserviceex::listmaterialrequest_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materialrequest::listmaterialrequest($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		materialrequestserviceex::listmaterialrequest_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialrequest($request)
	{
		// Pre Operation
		$request = materialrequestserviceex::getmaterialrequest_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materialrequest::getmaterialrequest($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		materialrequestserviceex::getmaterialrequest_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listmaterialrequestcount($request)
	{
		// Pre Operation
		//$request = materialrequestserviceex::listmaterialrequest_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listmaterialrequest($request)['data']); //materialrequest::listmaterialrequest($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//materialrequestserviceex::listmaterialrequest_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialrequestcount($request)
	{
		// Pre Operation
		//$request = materialrequestserviceex::getmaterialrequest_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getmaterialrequest($request)['data']); //materialrequest::getmaterialrequest($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//materialrequestserviceex::getmaterialrequest_after($request);

		return $returnValue;		
	}
}
