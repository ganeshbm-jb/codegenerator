<?php

namespace App\Services;

use App\Models\invoiceitem;
use App\ServicesEx\invoiceitemserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class invoiceitemservice extends BaseService
{
	// Method to save data
	public static function saveinvoiceitem($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = invoiceitemserviceex::saveinvoiceitem_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = invoiceitem::saveinvoiceitem($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			invoiceitemserviceex::saveinvoiceitem_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listinvoiceitem($request)
	{
		// Pre Operation
		$request = invoiceitemserviceex::listinvoiceitem_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = invoiceitem::listinvoiceitem($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		invoiceitemserviceex::listinvoiceitem_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getinvoiceitem($request)
	{
		// Pre Operation
		$request = invoiceitemserviceex::getinvoiceitem_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = invoiceitem::getinvoiceitem($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		invoiceitemserviceex::getinvoiceitem_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listinvoiceitemcount($request)
	{
		// Pre Operation
		//$request = invoiceitemserviceex::listinvoiceitem_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listinvoiceitem($request)['data']); //invoiceitem::listinvoiceitem($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//invoiceitemserviceex::listinvoiceitem_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getinvoiceitemcount($request)
	{
		// Pre Operation
		//$request = invoiceitemserviceex::getinvoiceitem_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getinvoiceitem($request)['data']); //invoiceitem::getinvoiceitem($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//invoiceitemserviceex::getinvoiceitem_after($request);

		return $returnValue;		
	}
}
