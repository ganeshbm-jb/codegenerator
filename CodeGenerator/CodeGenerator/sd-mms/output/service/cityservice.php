<?php

namespace App\Services;

use App\Models\city;
use App\ServicesEx\cityserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class cityservice extends BaseService
{
	// Method to save data
	public static function savecity($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = cityserviceex::savecity_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = city::savecity($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			cityserviceex::savecity_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listcity($request)
	{
		// Pre Operation
		$request = cityserviceex::listcity_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = city::listcity($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		cityserviceex::listcity_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getcity($request)
	{
		// Pre Operation
		$request = cityserviceex::getcity_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = city::getcity($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		cityserviceex::getcity_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listcitycount($request)
	{
		// Pre Operation
		//$request = cityserviceex::listcity_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listcity($request)['data']); //city::listcity($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//cityserviceex::listcity_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getcitycount($request)
	{
		// Pre Operation
		//$request = cityserviceex::getcity_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getcity($request)['data']); //city::getcity($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//cityserviceex::getcity_after($request);

		return $returnValue;		
	}
}
