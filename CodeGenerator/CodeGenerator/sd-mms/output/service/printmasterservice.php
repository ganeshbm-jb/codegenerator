<?php

namespace App\Services;

use App\Models\printmaster;
use App\ServicesEx\printmasterserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class printmasterservice extends BaseService
{
	// Method to save data
	public static function saveprintmaster($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = printmasterserviceex::saveprintmaster_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = printmaster::saveprintmaster($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			printmasterserviceex::saveprintmaster_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listprintmaster($request)
	{
		// Pre Operation
		$request = printmasterserviceex::listprintmaster_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = printmaster::listprintmaster($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		printmasterserviceex::listprintmaster_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getprintmaster($request)
	{
		// Pre Operation
		$request = printmasterserviceex::getprintmaster_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = printmaster::getprintmaster($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		printmasterserviceex::getprintmaster_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listprintmastercount($request)
	{
		// Pre Operation
		//$request = printmasterserviceex::listprintmaster_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listprintmaster($request)['data']); //printmaster::listprintmaster($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//printmasterserviceex::listprintmaster_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getprintmastercount($request)
	{
		// Pre Operation
		//$request = printmasterserviceex::getprintmaster_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getprintmaster($request)['data']); //printmaster::getprintmaster($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//printmasterserviceex::getprintmaster_after($request);

		return $returnValue;		
	}
}
