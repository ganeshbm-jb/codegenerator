<?php

namespace App\Services;

use App\Models\units;
use App\ServicesEx\unitsserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class unitsservice extends BaseService
{
	// Method to save data
	public static function saveunits($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = unitsserviceex::saveunits_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = units::saveunits($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			unitsserviceex::saveunits_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listunits($request)
	{
		// Pre Operation
		$request = unitsserviceex::listunits_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = units::listunits($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		unitsserviceex::listunits_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getunits($request)
	{
		// Pre Operation
		$request = unitsserviceex::getunits_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = units::getunits($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		unitsserviceex::getunits_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listunitscount($request)
	{
		// Pre Operation
		//$request = unitsserviceex::listunits_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listunits($request)['data']); //units::listunits($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//unitsserviceex::listunits_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getunitscount($request)
	{
		// Pre Operation
		//$request = unitsserviceex::getunits_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getunits($request)['data']); //units::getunits($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//unitsserviceex::getunits_after($request);

		return $returnValue;		
	}
}
