<?php

namespace App\Services;

use App\Models\worktype;
use App\ServicesEx\worktypeserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class worktypeservice extends BaseService
{
	// Method to save data
	public static function saveworktype($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = worktypeserviceex::saveworktype_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = worktype::saveworktype($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			worktypeserviceex::saveworktype_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listworktype($request)
	{
		// Pre Operation
		$request = worktypeserviceex::listworktype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = worktype::listworktype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		worktypeserviceex::listworktype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworktype($request)
	{
		// Pre Operation
		$request = worktypeserviceex::getworktype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = worktype::getworktype($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		worktypeserviceex::getworktype_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listworktypecount($request)
	{
		// Pre Operation
		//$request = worktypeserviceex::listworktype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listworktype($request)['data']); //worktype::listworktype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//worktypeserviceex::listworktype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworktypecount($request)
	{
		// Pre Operation
		//$request = worktypeserviceex::getworktype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getworktype($request)['data']); //worktype::getworktype($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//worktypeserviceex::getworktype_after($request);

		return $returnValue;		
	}
}
