<?php

namespace App\Services;

use App\Models\workorders;
use App\ServicesEx\workordersserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class workordersservice extends BaseService
{
	// Method to save data
	public static function saveworkorders($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = workordersserviceex::saveworkorders_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = workorders::saveworkorders($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			workordersserviceex::saveworkorders_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listworkorders($request)
	{
		// Pre Operation
		$request = workordersserviceex::listworkorders_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workorders::listworkorders($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		workordersserviceex::listworkorders_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkorders($request)
	{
		// Pre Operation
		$request = workordersserviceex::getworkorders_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workorders::getworkorders($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		workordersserviceex::getworkorders_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listworkorderscount($request)
	{
		// Pre Operation
		//$request = workordersserviceex::listworkorders_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listworkorders($request)['data']); //workorders::listworkorders($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//workordersserviceex::listworkorders_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkorderscount($request)
	{
		// Pre Operation
		//$request = workordersserviceex::getworkorders_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getworkorders($request)['data']); //workorders::getworkorders($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//workordersserviceex::getworkorders_after($request);

		return $returnValue;		
	}
}
