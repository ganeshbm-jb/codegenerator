<?php

namespace App\Services;

use App\Models\material;
use App\ServicesEx\materialserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class materialservice extends BaseService
{
	// Method to save data
	public static function savematerial($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = materialserviceex::savematerial_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = material::savematerial($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			materialserviceex::savematerial_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listmaterial($request)
	{
		// Pre Operation
		$request = materialserviceex::listmaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = material::listmaterial($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		materialserviceex::listmaterial_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterial($request)
	{
		// Pre Operation
		$request = materialserviceex::getmaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = material::getmaterial($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		materialserviceex::getmaterial_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listmaterialcount($request)
	{
		// Pre Operation
		//$request = materialserviceex::listmaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listmaterial($request)['data']); //material::listmaterial($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//materialserviceex::listmaterial_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialcount($request)
	{
		// Pre Operation
		//$request = materialserviceex::getmaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getmaterial($request)['data']); //material::getmaterial($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//materialserviceex::getmaterial_after($request);

		return $returnValue;		
	}
}
