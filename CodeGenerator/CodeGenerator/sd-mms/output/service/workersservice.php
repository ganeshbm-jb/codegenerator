<?php

namespace App\Services;

use App\Models\workers;
use App\ServicesEx\workersserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class workersservice extends BaseService
{
	// Method to save data
	public static function saveworkers($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = workersserviceex::saveworkers_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = workers::saveworkers($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			workersserviceex::saveworkers_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listworkers($request)
	{
		// Pre Operation
		$request = workersserviceex::listworkers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workers::listworkers($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		workersserviceex::listworkers_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkers($request)
	{
		// Pre Operation
		$request = workersserviceex::getworkers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = workers::getworkers($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		workersserviceex::getworkers_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listworkerscount($request)
	{
		// Pre Operation
		//$request = workersserviceex::listworkers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listworkers($request)['data']); //workers::listworkers($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//workersserviceex::listworkers_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getworkerscount($request)
	{
		// Pre Operation
		//$request = workersserviceex::getworkers_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getworkers($request)['data']); //workers::getworkers($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//workersserviceex::getworkers_after($request);

		return $returnValue;		
	}
}
