<?php

namespace App\Services;

use App\Models\materialrequestdetails;
use App\ServicesEx\materialrequestdetailsserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class materialrequestdetailsservice extends BaseService
{
	// Method to save data
	public static function savematerialrequestdetails($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = materialrequestdetailsserviceex::savematerialrequestdetails_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = materialrequestdetails::savematerialrequestdetails($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			materialrequestdetailsserviceex::savematerialrequestdetails_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listmaterialrequestdetails($request)
	{
		// Pre Operation
		$request = materialrequestdetailsserviceex::listmaterialrequestdetails_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materialrequestdetails::listmaterialrequestdetails($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		materialrequestdetailsserviceex::listmaterialrequestdetails_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialrequestdetails($request)
	{
		// Pre Operation
		$request = materialrequestdetailsserviceex::getmaterialrequestdetails_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = materialrequestdetails::getmaterialrequestdetails($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		materialrequestdetailsserviceex::getmaterialrequestdetails_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listmaterialrequestdetailscount($request)
	{
		// Pre Operation
		//$request = materialrequestdetailsserviceex::listmaterialrequestdetails_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listmaterialrequestdetails($request)['data']); //materialrequestdetails::listmaterialrequestdetails($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//materialrequestdetailsserviceex::listmaterialrequestdetails_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getmaterialrequestdetailscount($request)
	{
		// Pre Operation
		//$request = materialrequestdetailsserviceex::getmaterialrequestdetails_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getmaterialrequestdetails($request)['data']); //materialrequestdetails::getmaterialrequestdetails($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//materialrequestdetailsserviceex::getmaterialrequestdetails_after($request);

		return $returnValue;		
	}
}
