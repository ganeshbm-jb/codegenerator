<?php

namespace App\Services;

use App\Models\purchasereturnmaterial;
use App\ServicesEx\purchasereturnmaterialserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class purchasereturnmaterialservice extends BaseService
{
	// Method to save data
	public static function savepurchasereturnmaterial($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = purchasereturnmaterialserviceex::savepurchasereturnmaterial_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = purchasereturnmaterial::savepurchasereturnmaterial($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			purchasereturnmaterialserviceex::savepurchasereturnmaterial_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listpurchasereturnmaterial($request)
	{
		// Pre Operation
		$request = purchasereturnmaterialserviceex::listpurchasereturnmaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = purchasereturnmaterial::listpurchasereturnmaterial($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		purchasereturnmaterialserviceex::listpurchasereturnmaterial_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getpurchasereturnmaterial($request)
	{
		// Pre Operation
		$request = purchasereturnmaterialserviceex::getpurchasereturnmaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = purchasereturnmaterial::getpurchasereturnmaterial($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		purchasereturnmaterialserviceex::getpurchasereturnmaterial_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listpurchasereturnmaterialcount($request)
	{
		// Pre Operation
		//$request = purchasereturnmaterialserviceex::listpurchasereturnmaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listpurchasereturnmaterial($request)['data']); //purchasereturnmaterial::listpurchasereturnmaterial($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//purchasereturnmaterialserviceex::listpurchasereturnmaterial_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getpurchasereturnmaterialcount($request)
	{
		// Pre Operation
		//$request = purchasereturnmaterialserviceex::getpurchasereturnmaterial_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getpurchasereturnmaterial($request)['data']); //purchasereturnmaterial::getpurchasereturnmaterial($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//purchasereturnmaterialserviceex::getpurchasereturnmaterial_after($request);

		return $returnValue;		
	}
}
