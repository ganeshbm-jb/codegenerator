<?php

namespace App\Services;

use App\Models\viewaccess;
use App\ServicesEx\viewaccessserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class viewaccessservice extends BaseService
{
	// Method to save data
	public static function saveviewaccess($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = viewaccessserviceex::saveviewaccess_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = viewaccess::saveviewaccess($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			viewaccessserviceex::saveviewaccess_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listviewaccess($request)
	{
		// Pre Operation
		$request = viewaccessserviceex::listviewaccess_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = viewaccess::listviewaccess($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		viewaccessserviceex::listviewaccess_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getviewaccess($request)
	{
		// Pre Operation
		$request = viewaccessserviceex::getviewaccess_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = viewaccess::getviewaccess($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		viewaccessserviceex::getviewaccess_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listviewaccesscount($request)
	{
		// Pre Operation
		//$request = viewaccessserviceex::listviewaccess_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listviewaccess($request)['data']); //viewaccess::listviewaccess($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//viewaccessserviceex::listviewaccess_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getviewaccesscount($request)
	{
		// Pre Operation
		//$request = viewaccessserviceex::getviewaccess_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getviewaccess($request)['data']); //viewaccess::getviewaccess($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//viewaccessserviceex::getviewaccess_after($request);

		return $returnValue;		
	}
}
