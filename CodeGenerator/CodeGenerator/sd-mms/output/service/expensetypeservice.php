<?php

namespace App\Services;

use App\Models\expensetype;
use App\ServicesEx\expensetypeserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class expensetypeservice extends BaseService
{
	// Method to save data
	public static function saveexpensetype($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = expensetypeserviceex::saveexpensetype_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = expensetype::saveexpensetype($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			expensetypeserviceex::saveexpensetype_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listexpensetype($request)
	{
		// Pre Operation
		$request = expensetypeserviceex::listexpensetype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = expensetype::listexpensetype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		expensetypeserviceex::listexpensetype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getexpensetype($request)
	{
		// Pre Operation
		$request = expensetypeserviceex::getexpensetype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = expensetype::getexpensetype($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		expensetypeserviceex::getexpensetype_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listexpensetypecount($request)
	{
		// Pre Operation
		//$request = expensetypeserviceex::listexpensetype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listexpensetype($request)['data']); //expensetype::listexpensetype($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//expensetypeserviceex::listexpensetype_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getexpensetypecount($request)
	{
		// Pre Operation
		//$request = expensetypeserviceex::getexpensetype_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getexpensetype($request)['data']); //expensetype::getexpensetype($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//expensetypeserviceex::getexpensetype_after($request);

		return $returnValue;		
	}
}
