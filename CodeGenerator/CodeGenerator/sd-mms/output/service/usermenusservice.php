<?php

namespace App\Services;

use App\Models\usermenus;
use App\ServicesEx\usermenusserviceex;
use DB;
use Webpatser\Uuid\Uuid;

class usermenusservice extends BaseService
{
	// Method to save data
	public static function saveusermenus($id, $input)
	{
		$id = Self::generateID($id);

		// Pre Operation
		$input = usermenusserviceex::saveusermenus_before($id, $input);

		// Save Logic
		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";
		

		try {     
			$returnValue['data'] = usermenus::saveusermenus($id, $input);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		if($returnValue['status'] == 'success')
			usermenusserviceex::saveusermenus_after($id, $input);

		return $returnValue;
	}

	// Method to get all records
	public static function listusermenus($request)
	{
		// Pre Operation
		$request = usermenusserviceex::listusermenus_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = usermenus::listusermenus($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		usermenusserviceex::listusermenus_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getusermenus($request)
	{
		// Pre Operation
		$request = usermenusserviceex::getusermenus_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = usermenus::getusermenus($request['col'], $request['value'], $request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		usermenusserviceex::getusermenus_after($request);

		return $returnValue;		
	}

	// Method to get all records
	public static function listusermenuscount($request)
	{
		// Pre Operation
		//$request = usermenusserviceex::listusermenus_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::listusermenus($request)['data']); //usermenus::listusermenus($request);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }

		// Pre Operation
		//usermenusserviceex::listusermenus_after($request);

		return $returnValue;
	}


	// Method to get a record
	public static function getusermenuscount($request)
	{
		// Pre Operation
		//$request = usermenusserviceex::getusermenus_before($request);

		$returnValue = [];
        $returnValue['status'] = "success";
        $returnValue['data'] = "";

		try {     
			$returnValue['data'] = count(Self::getusermenus($request)['data']); //usermenus::getusermenus($request['col'], $request['value']);
		} catch (\Exception $ex) {
            $returnValue['status'] = "failed";
            $returnValue['data'] = $ex->getMessage();
        }		

		// Post Operation
		//usermenusserviceex::getusermenus_after($request);

		return $returnValue;		
	}
}
